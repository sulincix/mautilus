This is mautilus, the fork of file manager of the GNOME desktop.

This forks main purpose is add legacy desktop-icons int new gnome releases.

We forked nautilus and disable file manager and remove to mautilus.

Forked from this commit tag: https://gitlab.gnome.org/GNOME/nautilus/-/tree/816b8247f3147637d13b55446ee38d4470c0f225 (3.27.2)
