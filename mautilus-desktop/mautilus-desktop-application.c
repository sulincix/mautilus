/* mautilus-desktop-application.c
 *
 * Copyright (C) 2016 Carlos Soriano <csoriano@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "mautilus-desktop-application.h"
#include "mautilus-desktop-window.h"
#include "mautilus-desktop-directory.h"
#include "mautilus-file-utilities.h"

#include "mautilus-freedesktop-generated.h"

#include <src/mautilus-global-preferences.h>
#include <eel/eel.h>
#include <gdk/gdkx.h>
#include <stdlib.h>
#include <glib/gi18n.h>

static mautilusFreedesktopFileManager1 *freedesktop_proxy = NULL;

struct _mautilusDesktopApplication
{
    mautilusApplication parent_instance;

    gboolean force;
    GCancellable *freedesktop_cancellable;
};

G_DEFINE_TYPE (mautilusDesktopApplication, mautilus_desktop_application, MAUTILUS_TYPE_APPLICATION)

static void
on_show_folders (GObject      *source_object,
                 GAsyncResult *res,
                 gpointer      user_data)
{
    GError *error = NULL;

    mautilus_freedesktop_file_manager1_call_show_items_finish (freedesktop_proxy,
                                                               res,
                                                               &error);
    if (error && !g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
    {
        g_warning ("Unable to show items with File Manager freedesktop proxy: %s", error->message);
    }

    g_clear_error (&error);
}

static void
open_location_on_dbus (mautilusDesktopApplication *self,
                       const gchar                *uri)
{
    const gchar *uris[] = { uri, NULL };

    mautilus_freedesktop_file_manager1_call_show_folders (freedesktop_proxy,
                                                          uris,
                                                          "",
                                                          self->freedesktop_cancellable,
                                                          on_show_folders,
                                                          self);
}

static void
open_location_full (mautilusApplication     *app,
                    GFile                   *location,
                    mautilusWindowOpenFlags  flags,
                    GList                   *selection,
                    mautilusWindow          *target_window,
                    mautilusWindowSlot      *target_slot)
{
    mautilusDesktopApplication *self = MAUTILUS_DESKTOP_APPLICATION (app);
    gchar *uri;

    uri = g_file_get_uri (location);
    if (eel_uri_is_desktop (uri) && target_window &&
        MAUTILUS_IS_DESKTOP_WINDOW (target_window))
    {
        mautilus_window_open_location_full (target_window, location, flags, selection, NULL);
    }
    else
    {
        if (freedesktop_proxy)
        {
            open_location_on_dbus (self, uri);
        }
        else
        {
            g_warning ("cannot open folder on desktop, freedesktop bus not ready\n");
        }
    }

    g_free (uri);
}

static void
mautilus_application_set_desktop_visible (mautilusDesktopApplication *self,
                                          gboolean                    visible)
{
    GtkWidget *desktop_window;

    if (visible)
    {
        mautilus_desktop_window_ensure ();
    }
    else
    {
        desktop_window = mautilus_desktop_window_get ();
        if (desktop_window != NULL)
        {
            gtk_widget_destroy (desktop_window);
        }
    }
}

static void
update_desktop_from_gsettings (mautilusDesktopApplication *self)
{
    GdkDisplay *display;
    gboolean visible;

#ifdef GDK_WINDOWING_X11
    display = gdk_display_get_default ();
    visible = g_settings_get_boolean (gnome_background_preferences,
                                      MAUTILUS_PREFERENCES_SHOW_DESKTOP);
    visible = visible || self->force;

    if (!GDK_IS_X11_DISPLAY (display))
    {
        if (visible)
        {
            g_warning ("Desktop icons only supported on X11. Desktop not created");
        }

        return;
    }

    mautilus_application_set_desktop_visible (self, visible);

    return;
#endif

    g_warning ("Desktop icons only supported on X11. Desktop not created");
}

static void
init_desktop (mautilusDesktopApplication *self)
{
    if (!self->force)
    {
        g_signal_connect_swapped (gnome_background_preferences, "changed::" MAUTILUS_PREFERENCES_SHOW_DESKTOP,
                                  G_CALLBACK (update_desktop_from_gsettings),
                                  self);
    }
    update_desktop_from_gsettings (self);
}

static void
mautilus_desktop_application_activate (GApplication *app)
{
    /* Do nothing */
}

static gint
mautilus_desktop_application_command_line (GApplication            *application,
                                           GApplicationCommandLine *command_line)
{
    mautilusDesktopApplication *self = MAUTILUS_DESKTOP_APPLICATION (application);
    GVariantDict *options;

    options = g_application_command_line_get_options_dict (command_line);

    if (g_variant_dict_contains (options, "force"))
    {
        self->force = TRUE;
    }

    init_desktop (self);

    return EXIT_SUCCESS;
}

static void
mautilus_desktop_application_startup (GApplication *app)
{
    mautilusDesktopApplication *self = MAUTILUS_DESKTOP_APPLICATION (app);
    GError *error = NULL;

    mautilus_application_startup_common (MAUTILUS_APPLICATION (app));
    self->freedesktop_cancellable = g_cancellable_new ();
    freedesktop_proxy = mautilus_freedesktop_file_manager1_proxy_new_for_bus_sync (G_BUS_TYPE_SESSION,
                                                                                   G_DBUS_PROXY_FLAGS_NONE,
                                                                                   "org.freedesktop.FileManager1",
                                                                                   "/org/freedesktop/FileManager1",
                                                                                   self->freedesktop_cancellable,
                                                                                   &error);

    if (error && !g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
    {
        g_warning ("Unable to create File Manager freedesktop proxy: %s", error->message);
    }

    g_clear_error (&error);
}

static void
mautilus_desktop_application_dispose (GObject *object)
{
    mautilusDesktopApplication *self = MAUTILUS_DESKTOP_APPLICATION (object);

    g_clear_object (&self->freedesktop_cancellable);


    G_OBJECT_CLASS (mautilus_desktop_application_parent_class)->dispose (object);
}

static void
mautilus_desktop_application_class_init (mautilusDesktopApplicationClass *klass)
{
    GApplicationClass *application_class = G_APPLICATION_CLASS (klass);
    GObjectClass *gobject_class = G_OBJECT_CLASS (klass);
    mautilusApplicationClass *parent_class = MAUTILUS_APPLICATION_CLASS (klass);

    parent_class->open_location_full = open_location_full;

    application_class->startup = mautilus_desktop_application_startup;
    application_class->activate = mautilus_desktop_application_activate;
    application_class->command_line = mautilus_desktop_application_command_line;

    gobject_class->dispose = mautilus_desktop_application_dispose;
}

static void
mautilus_desktop_ensure_builtins (void)
{
    /* Ensure the type so it can be registered early as a directory extension provider*/
    g_type_ensure (MAUTILUS_TYPE_DESKTOP_DIRECTORY);
}

const GOptionEntry desktop_options[] =
{
    { "force", '\0', 0, G_OPTION_ARG_NONE, NULL,
      N_("Always manage the desktop (ignore the GSettings preference)."), NULL },
    { NULL }
};

static void
mautilus_desktop_application_init (mautilusDesktopApplication *self)
{
    self->force = FALSE;

    g_application_add_main_option_entries (G_APPLICATION (self), desktop_options);
    mautilus_ensure_extension_points ();
    mautilus_ensure_extension_builtins ();
    mautilus_desktop_ensure_builtins ();
}

mautilusDesktopApplication *
mautilus_desktop_application_new (void)
{
    return g_object_new (MAUTILUS_TYPE_DESKTOP_APPLICATION,
                         "application-id", "org.gnome.mautilusDesktop",
                         "register-session", TRUE,
                         "flags", G_APPLICATION_HANDLES_COMMAND_LINE,
                         NULL);
}
