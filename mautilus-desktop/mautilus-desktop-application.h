/* mautilus-desktop-application.h
 *
 * Copyright (C) 2016 Carlos Soriano <csoriano@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAUTILUS_DESKTOP_APPLICATION_H
#define MAUTILUS_DESKTOP_APPLICATION_H

#include <glib.h>
#include "mautilus-application.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_DESKTOP_APPLICATION (mautilus_desktop_application_get_type())

G_DECLARE_FINAL_TYPE (mautilusDesktopApplication, mautilus_desktop_application, MAUTILUS, DESKTOP_APPLICATION, mautilusApplication)

mautilusDesktopApplication *mautilus_desktop_application_new (void);

G_END_DECLS

#endif /* MAUTILUS_DESKTOP_APPLICATION_H */

