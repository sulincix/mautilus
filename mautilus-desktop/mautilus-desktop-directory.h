/*
   mautilus-desktop-directory.h: Subclass of mautilusDirectory to implement
   a virtual directory consisting of the desktop directory and the desktop
   icons
 
   Copyright (C) 2003 Red Hat, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Alexander Larsson <alexl@redhat.com>
*/

#ifndef MAUTILUS_DESKTOP_DIRECTORY_H
#define MAUTILUS_DESKTOP_DIRECTORY_H

#include <src/mautilus-directory.h>

#define MAUTILUS_TYPE_DESKTOP_DIRECTORY mautilus_desktop_directory_get_type()
#define MAUTILUS_DESKTOP_DIRECTORY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_DESKTOP_DIRECTORY, mautilusDesktopDirectory))
#define MAUTILUS_DESKTOP_DIRECTORY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_DESKTOP_DIRECTORY, mautilusDesktopDirectoryClass))
#define MAUTILUS_IS_DESKTOP_DIRECTORY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_DESKTOP_DIRECTORY))
#define MAUTILUS_IS_DESKTOP_DIRECTORY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_DESKTOP_DIRECTORY))
#define MAUTILUS_DESKTOP_DIRECTORY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_DESKTOP_DIRECTORY, mautilusDesktopDirectoryClass))

#define MAUTILUS_DESKTOP_DIRECTORY_PROVIDER_NAME "desktop-directory-provider"

typedef struct mautilusDesktopDirectoryDetails mautilusDesktopDirectoryDetails;

typedef struct {
	mautilusDirectory parent_slot;
	mautilusDesktopDirectoryDetails *details;
} mautilusDesktopDirectory;

typedef struct {
	mautilusDirectoryClass parent_slot;

} mautilusDesktopDirectoryClass;

GType   mautilus_desktop_directory_get_type             (void);
mautilusDirectory * mautilus_desktop_directory_get_real_directory   (mautilusDesktopDirectory *desktop_directory);

#endif /* MAUTILUS_DESKTOP_DIRECTORY_H */
