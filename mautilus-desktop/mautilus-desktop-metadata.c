/*
 * mautilus
 *
 * Copyright (C) 2011 Red Hat, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Authors: Cosimo Cecchi <cosimoc@redhat.com>
 */

#include <config.h>

#include "mautilus-desktop-metadata.h"

#include <src/mautilus-file-utilities.h>
#include <src/mautilus-keyfile-metadata.h>

static gchar *
get_keyfile_path (void)
{
    gchar *xdg_dir, *retval;

    xdg_dir = mautilus_get_user_directory ();
    retval = g_build_filename (xdg_dir, "desktop-metadata", NULL);

    g_free (xdg_dir);

    return retval;
}

void
mautilus_desktop_set_metadata_string (mautilusFile *file,
                                      const gchar  *name,
                                      const gchar  *key,
                                      const gchar  *string)
{
    gchar *keyfile_filename;

    keyfile_filename = get_keyfile_path ();

    mautilus_keyfile_metadata_set_string (file, keyfile_filename,
                                          name, key, string);

    g_free (keyfile_filename);
}

void
mautilus_desktop_set_metadata_stringv (mautilusFile       *file,
                                       const char         *name,
                                       const char         *key,
                                       const char * const *stringv)
{
    gchar *keyfile_filename;

    keyfile_filename = get_keyfile_path ();

    mautilus_keyfile_metadata_set_stringv (file, keyfile_filename,
                                           name, key, stringv);

    g_free (keyfile_filename);
}

gboolean
mautilus_desktop_update_metadata_from_keyfile (mautilusFile *file,
                                               const gchar  *name)
{
    gchar *keyfile_filename;
    gboolean result;

    keyfile_filename = get_keyfile_path ();

    result = mautilus_keyfile_metadata_update_from_keyfile (file,
                                                            keyfile_filename,
                                                            name);

    g_free (keyfile_filename);
    return result;
}
