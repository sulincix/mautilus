/*
 *  mautilus-desktop-link.c: Class that handles the links on the desktop
 *
 *  Copyright (C) 2003 Red Hat, Inc.
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Alexander Larsson <alexl@redhat.com>
 */

#include <config.h>

#include "mautilus-desktop-link.h"
#include "mautilus-desktop-link-monitor.h"
#include "mautilus-desktop-icon-file.h"
#include "mautilus-directory-private.h"
#include "mautilus-desktop-directory.h"

#include <glib/gi18n.h>
#include <gio/gio.h>

#include <src/mautilus-file-utilities.h>
#include <src/mautilus-trash-monitor.h>
#include <src/mautilus-global-preferences.h>
#include <src/mautilus-icon-names.h>

#include <string.h>

struct mautilusDesktopLinkDetails
{
    mautilusDesktopLinkType type;
    char *filename;
    char *display_name;
    GFile *activation_location;
    GIcon *icon;

    mautilusDesktopIconFile *icon_file;

    /* Just for mount icons: */
    GMount *mount;
};

G_DEFINE_TYPE (mautilusDesktopLink, mautilus_desktop_link, G_TYPE_OBJECT)

static void
create_icon_file (mautilusDesktopLink *link)
{
    link->details->icon_file = mautilus_desktop_icon_file_new (link);
}

static void
mautilus_desktop_link_changed (mautilusDesktopLink *link)
{
    if (link->details->icon_file != NULL)
    {
        mautilus_desktop_icon_file_update (link->details->icon_file);
    }
}

static void
mount_changed_callback (GMount              *mount,
                        mautilusDesktopLink *link)
{
    g_free (link->details->display_name);
    if (link->details->activation_location)
    {
        g_object_unref (link->details->activation_location);
    }
    if (link->details->icon)
    {
        g_object_unref (link->details->icon);
    }

    link->details->display_name = g_mount_get_name (mount);
    link->details->activation_location = g_mount_get_default_location (mount);
    link->details->icon = g_mount_get_icon (mount);

    mautilus_desktop_link_changed (link);
}

static GIcon *
get_desktop_trash_icon (void)
{
    const gchar *icon_name;

    if (mautilus_trash_monitor_is_empty ())
    {
        icon_name = MAUTILUS_DESKTOP_ICON_TRASH;
    }
    else
    {
        icon_name = MAUTILUS_DESKTOP_ICON_TRASH_FULL;
    }

    return g_themed_icon_new (icon_name);
}

static void
trash_state_changed_callback (mautilusTrashMonitor *trash_monitor,
                              gboolean              state,
                              gpointer              callback_data)
{
    mautilusDesktopLink *link;

    link = MAUTILUS_DESKTOP_LINK (callback_data);
    g_assert (link->details->type == MAUTILUS_DESKTOP_LINK_TRASH);

    if (link->details->icon)
    {
        g_object_unref (link->details->icon);
    }
    link->details->icon = get_desktop_trash_icon ();

    mautilus_desktop_link_changed (link);
}

static void
home_name_changed (gpointer callback_data)
{
    mautilusDesktopLink *link;

    link = MAUTILUS_DESKTOP_LINK (callback_data);
    g_assert (link->details->type == MAUTILUS_DESKTOP_LINK_HOME);

    g_free (link->details->display_name);
    link->details->display_name = g_settings_get_string (mautilus_desktop_preferences,
                                                         MAUTILUS_PREFERENCES_DESKTOP_HOME_NAME);
    if (link->details->display_name[0] == 0)
    {
        g_free (link->details->display_name);
        link->details->display_name = g_strdup (_("Home"));
    }

    mautilus_desktop_link_changed (link);
}

static void
trash_name_changed (gpointer callback_data)
{
    mautilusDesktopLink *link;

    link = MAUTILUS_DESKTOP_LINK (callback_data);
    g_assert (link->details->type == MAUTILUS_DESKTOP_LINK_TRASH);

    g_free (link->details->display_name);
    link->details->display_name = g_settings_get_string (mautilus_desktop_preferences,
                                                         MAUTILUS_PREFERENCES_DESKTOP_TRASH_NAME);
    mautilus_desktop_link_changed (link);
}

static void
network_name_changed (gpointer callback_data)
{
    mautilusDesktopLink *link;

    link = MAUTILUS_DESKTOP_LINK (callback_data);
    g_assert (link->details->type == MAUTILUS_DESKTOP_LINK_NETWORK);

    g_free (link->details->display_name);
    link->details->display_name = g_settings_get_string (mautilus_desktop_preferences,
                                                         MAUTILUS_PREFERENCES_DESKTOP_NETWORK_NAME);
    mautilus_desktop_link_changed (link);
}

mautilusDesktopLink *
mautilus_desktop_link_new (mautilusDesktopLinkType type)
{
    mautilusDesktopLink *link;

    link = MAUTILUS_DESKTOP_LINK (g_object_new (MAUTILUS_TYPE_DESKTOP_LINK, NULL));

    link->details->type = type;
    switch (type)
    {
        case MAUTILUS_DESKTOP_LINK_HOME:
        {
            link->details->filename = g_strdup ("home");
            link->details->display_name = g_settings_get_string (mautilus_desktop_preferences,
                                                                 MAUTILUS_PREFERENCES_DESKTOP_HOME_NAME);
            link->details->activation_location = g_file_new_for_path (g_get_home_dir ());
            link->details->icon = g_themed_icon_new (MAUTILUS_DESKTOP_ICON_HOME);

            g_signal_connect_swapped (mautilus_desktop_preferences,
                                      "changed::" MAUTILUS_PREFERENCES_DESKTOP_HOME_NAME,
                                      G_CALLBACK (home_name_changed),
                                      link);
        }
        break;

        case MAUTILUS_DESKTOP_LINK_TRASH:
        {
            link->details->filename = g_strdup ("trash");
            link->details->display_name = g_settings_get_string (mautilus_desktop_preferences,
                                                                 MAUTILUS_PREFERENCES_DESKTOP_TRASH_NAME);
            link->details->activation_location = g_file_new_for_uri (EEL_TRASH_URI);
            link->details->icon = get_desktop_trash_icon ();

            g_signal_connect_swapped (mautilus_desktop_preferences,
                                      "changed::" MAUTILUS_PREFERENCES_DESKTOP_TRASH_NAME,
                                      G_CALLBACK (trash_name_changed),
                                      link);
            g_signal_connect_object (mautilus_trash_monitor_get (), "trash-state-changed",
                                     G_CALLBACK (trash_state_changed_callback), link, 0);
        }
        break;

        case MAUTILUS_DESKTOP_LINK_NETWORK:
        {
            link->details->filename = g_strdup ("network");
            link->details->display_name = g_settings_get_string (mautilus_desktop_preferences,
                                                                 MAUTILUS_PREFERENCES_DESKTOP_NETWORK_NAME);
            link->details->activation_location = g_file_new_for_uri ("network:///");
            link->details->icon = g_themed_icon_new (MAUTILUS_DESKTOP_ICON_NETWORK);

            g_signal_connect_swapped (mautilus_desktop_preferences,
                                      "changed::" MAUTILUS_PREFERENCES_DESKTOP_NETWORK_NAME,
                                      G_CALLBACK (network_name_changed),
                                      link);
        }
        break;

        default:
        case MAUTILUS_DESKTOP_LINK_MOUNT:
        {
            g_assert_not_reached ();
        }
        break;
    }

    create_icon_file (link);

    return link;
}

mautilusDesktopLink *
mautilus_desktop_link_new_from_mount (GMount *mount)
{
    mautilusDesktopLink *link;
    GVolume *volume;
    char *name, *filename;

    link = MAUTILUS_DESKTOP_LINK (g_object_new (MAUTILUS_TYPE_DESKTOP_LINK, NULL));

    link->details->type = MAUTILUS_DESKTOP_LINK_MOUNT;

    link->details->mount = g_object_ref (mount);

    /* We try to use the drive name to get somewhat stable filenames
     *  for metadata */
    volume = g_mount_get_volume (mount);
    if (volume != NULL)
    {
        name = g_volume_get_name (volume);
        g_object_unref (volume);
    }
    else
    {
        name = g_mount_get_name (mount);
    }

    /* Replace slashes in name */
    filename = g_strconcat (g_strdelimit (name, "/", '-'), ".volume", NULL);
    link->details->filename =
        mautilus_desktop_link_monitor_make_filename_unique (mautilus_desktop_link_monitor_get (),
                                                            filename);
    g_free (filename);
    g_free (name);

    link->details->display_name = g_mount_get_name (mount);

    link->details->activation_location = g_mount_get_default_location (mount);
    link->details->icon = g_mount_get_icon (mount);

    g_signal_connect_object (mount, "changed",
                             G_CALLBACK (mount_changed_callback), link, 0);

    create_icon_file (link);

    return link;
}

GMount *
mautilus_desktop_link_get_mount (mautilusDesktopLink *link)
{
    if (link->details->mount)
    {
        return g_object_ref (link->details->mount);
    }
    return NULL;
}

mautilusDesktopLinkType
mautilus_desktop_link_get_link_type (mautilusDesktopLink *link)
{
    return link->details->type;
}

char *
mautilus_desktop_link_get_file_name (mautilusDesktopLink *link)
{
    return g_strdup (link->details->filename);
}

char *
mautilus_desktop_link_get_display_name (mautilusDesktopLink *link)
{
    return g_strdup (link->details->display_name);
}

GIcon *
mautilus_desktop_link_get_icon (mautilusDesktopLink *link)
{
    if (link->details->icon != NULL)
    {
        return g_object_ref (link->details->icon);
    }
    return NULL;
}

GFile *
mautilus_desktop_link_get_activation_location (mautilusDesktopLink *link)
{
    if (link->details->activation_location)
    {
        return g_object_ref (link->details->activation_location);
    }
    return NULL;
}

char *
mautilus_desktop_link_get_activation_uri (mautilusDesktopLink *link)
{
    if (link->details->activation_location)
    {
        return g_file_get_uri (link->details->activation_location);
    }
    return NULL;
}


gboolean
mautilus_desktop_link_get_date (mautilusDesktopLink *link,
                                mautilusDateType     date_type,
                                time_t              *date)
{
    return FALSE;
}

gboolean
mautilus_desktop_link_can_rename (mautilusDesktopLink *link)
{
    return (link->details->type == MAUTILUS_DESKTOP_LINK_HOME ||
            link->details->type == MAUTILUS_DESKTOP_LINK_TRASH ||
            link->details->type == MAUTILUS_DESKTOP_LINK_NETWORK);
}

gboolean
mautilus_desktop_link_rename (mautilusDesktopLink *link,
                              const char          *name)
{
    switch (link->details->type)
    {
        case MAUTILUS_DESKTOP_LINK_HOME:
        {
            g_settings_set_string (mautilus_desktop_preferences,
                                   MAUTILUS_PREFERENCES_DESKTOP_HOME_NAME,
                                   name);
        }
        break;

        case MAUTILUS_DESKTOP_LINK_TRASH:
        {
            g_settings_set_string (mautilus_desktop_preferences,
                                   MAUTILUS_PREFERENCES_DESKTOP_TRASH_NAME,
                                   name);
        }
        break;

        case MAUTILUS_DESKTOP_LINK_NETWORK:
        {
            g_settings_set_string (mautilus_desktop_preferences,
                                   MAUTILUS_PREFERENCES_DESKTOP_NETWORK_NAME,
                                   name);
        }
        break;

        default:
        {
            g_assert_not_reached ();
            /* FIXME: Do we want volume renaming?
             * We didn't support that before. */
        }
        break;
    }

    return TRUE;
}

static void
mautilus_desktop_link_init (mautilusDesktopLink *link)
{
    link->details = G_TYPE_INSTANCE_GET_PRIVATE (link,
                                                 MAUTILUS_TYPE_DESKTOP_LINK,
                                                 mautilusDesktopLinkDetails);
}

static void
desktop_link_finalize (GObject *object)
{
    mautilusDesktopLink *link;

    link = MAUTILUS_DESKTOP_LINK (object);

    if (link->details->icon_file != NULL)
    {
        mautilus_desktop_icon_file_remove (link->details->icon_file);
        mautilus_file_unref (MAUTILUS_FILE (link->details->icon_file));
        link->details->icon_file = NULL;
    }

    if (link->details->type == MAUTILUS_DESKTOP_LINK_HOME)
    {
        g_signal_handlers_disconnect_by_func (mautilus_desktop_preferences,
                                              home_name_changed,
                                              link);
    }

    if (link->details->type == MAUTILUS_DESKTOP_LINK_TRASH)
    {
        g_signal_handlers_disconnect_by_func (mautilus_desktop_preferences,
                                              trash_name_changed,
                                              link);
    }

    if (link->details->type == MAUTILUS_DESKTOP_LINK_NETWORK)
    {
        g_signal_handlers_disconnect_by_func (mautilus_desktop_preferences,
                                              network_name_changed,
                                              link);
    }

    if (link->details->type == MAUTILUS_DESKTOP_LINK_MOUNT)
    {
        g_object_unref (link->details->mount);
    }

    g_free (link->details->filename);
    g_free (link->details->display_name);
    if (link->details->activation_location)
    {
        g_object_unref (link->details->activation_location);
    }
    if (link->details->icon)
    {
        g_object_unref (link->details->icon);
    }

    G_OBJECT_CLASS (mautilus_desktop_link_parent_class)->finalize (object);
}

static void
mautilus_desktop_link_class_init (mautilusDesktopLinkClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = desktop_link_finalize;

    g_type_class_add_private (object_class, sizeof (mautilusDesktopLinkDetails));
}
