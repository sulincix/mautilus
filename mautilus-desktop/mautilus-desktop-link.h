/*
   mautilus-desktop-link.h: Class that handles the links on the desktop
    
   Copyright (C) 2003 Red Hat, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Alexander Larsson <alexl@redhat.com>
*/

#ifndef MAUTILUS_DESKTOP_LINK_H
#define MAUTILUS_DESKTOP_LINK_H

#include <src/mautilus-file.h>
#include <gio/gio.h>

#define MAUTILUS_TYPE_DESKTOP_LINK mautilus_desktop_link_get_type()
#define MAUTILUS_DESKTOP_LINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_DESKTOP_LINK, mautilusDesktopLink))
#define MAUTILUS_DESKTOP_LINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_DESKTOP_LINK, mautilusDesktopLinkClass))
#define MAUTILUS_IS_DESKTOP_LINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_DESKTOP_LINK))
#define MAUTILUS_IS_DESKTOP_LINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_DESKTOP_LINK))
#define MAUTILUS_DESKTOP_LINK_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_DESKTOP_LINK, mautilusDesktopLinkClass))

typedef struct mautilusDesktopLinkDetails mautilusDesktopLinkDetails;

typedef struct {
	GObject parent_slot;
	mautilusDesktopLinkDetails *details;
} mautilusDesktopLink;

typedef struct {
	GObjectClass parent_slot;
} mautilusDesktopLinkClass;

typedef enum {
	MAUTILUS_DESKTOP_LINK_HOME,
	MAUTILUS_DESKTOP_LINK_TRASH,
	MAUTILUS_DESKTOP_LINK_MOUNT,
	MAUTILUS_DESKTOP_LINK_NETWORK
} mautilusDesktopLinkType;

GType   mautilus_desktop_link_get_type (void);

mautilusDesktopLink *   mautilus_desktop_link_new                     (mautilusDesktopLinkType  type);
mautilusDesktopLink *   mautilus_desktop_link_new_from_mount          (GMount                 *mount);
mautilusDesktopLinkType mautilus_desktop_link_get_link_type           (mautilusDesktopLink     *link);
char *                  mautilus_desktop_link_get_file_name           (mautilusDesktopLink     *link);
char *                  mautilus_desktop_link_get_display_name        (mautilusDesktopLink     *link);
GIcon *                 mautilus_desktop_link_get_icon                (mautilusDesktopLink     *link);
GFile *                 mautilus_desktop_link_get_activation_location (mautilusDesktopLink     *link);
char *                  mautilus_desktop_link_get_activation_uri      (mautilusDesktopLink     *link);
gboolean                mautilus_desktop_link_get_date                (mautilusDesktopLink     *link,
								       mautilusDateType         date_type,
								       time_t                  *date);
GMount *                mautilus_desktop_link_get_mount               (mautilusDesktopLink     *link);
gboolean                mautilus_desktop_link_can_rename              (mautilusDesktopLink     *link);
gboolean                mautilus_desktop_link_rename                  (mautilusDesktopLink     *link,
								       const char              *name);


#endif /* MAUTILUS_DESKTOP_LINK_H */
