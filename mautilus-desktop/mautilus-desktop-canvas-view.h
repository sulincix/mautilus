
/* fm-icon-view.h - interface for icon view of directory.

   Copyright (C) 2000 Eazel, Inc.

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   see <http://www.gnu.org/licenses/>.

   Authors: Mike Engber <engber@eazel.com>
*/

#ifndef MAUTILUS_DESKTOP_CANVAS_VIEW_H
#define MAUTILUS_DESKTOP_CANVAS_VIEW_H

#include "mautilus-canvas-view.h"

#define MAUTILUS_TYPE_DESKTOP_CANVAS_VIEW mautilus_desktop_canvas_view_get_type()
#define MAUTILUS_DESKTOP_CANVAS_VIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_DESKTOP_CANVAS_VIEW, mautilusDesktopCanvasView))
#define MAUTILUS_DESKTOP_CANVAS_VIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_DESKTOP_CANVAS_VIEW, mautilusDesktopCanvasViewClass))
#define MAUTILUS_IS_DESKTOP_CANVAS_VIEW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_DESKTOP_CANVAS_VIEW))
#define MAUTILUS_IS_DESKTOP_CANVAS_VIEW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_DESKTOP_CANVAS_VIEW))
#define MAUTILUS_DESKTOP_CANVAS_VIEW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_DESKTOP_CANVAS_VIEW, mautilusDesktopCanvasViewClass))

typedef struct mautilusDesktopCanvasViewDetails mautilusDesktopCanvasViewDetails;
typedef struct {
	mautilusCanvasView parent;
	mautilusDesktopCanvasViewDetails *details;
} mautilusDesktopCanvasView;

typedef struct {
	mautilusCanvasViewClass parent_class;
} mautilusDesktopCanvasViewClass;

/* GObject support */
GType   mautilus_desktop_canvas_view_get_type (void);
mautilusFilesView * mautilus_desktop_canvas_view_new (mautilusWindowSlot *slot);

#endif /* MAUTILUS_DESKTOP_CANVAS_VIEW_H */
