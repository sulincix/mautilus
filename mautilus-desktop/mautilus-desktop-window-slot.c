/* mautilus-desktop-window-slot.c
 *
 * Copyright (C) 2016 Carlos Soriano <csoriano@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mautilus-desktop-window-slot.h"
#include "mautilus-desktop-canvas-view.h"

struct _mautilusDesktopWindowSlot
{
    mautilusWindowSlot parent_instance;
};

G_DEFINE_TYPE (mautilusDesktopWindowSlot, mautilus_desktop_window_slot, MAUTILUS_TYPE_WINDOW_SLOT)

static mautilusView *
real_get_view_for_location (mautilusWindowSlot *self,
                            GFile              *location)
{
    return MAUTILUS_VIEW (mautilus_desktop_canvas_view_new (self));
}

mautilusDesktopWindowSlot *
mautilus_desktop_window_slot_new (mautilusWindow *window)
{
    return g_object_new (MAUTILUS_TYPE_DESKTOP_WINDOW_SLOT,
                         "window", window,
                         NULL);
}

static void
mautilus_desktop_window_slot_class_init (mautilusDesktopWindowSlotClass *klass)
{
    mautilusWindowSlotClass *parent_class = MAUTILUS_WINDOW_SLOT_CLASS (klass);

    parent_class->get_view_for_location = real_get_view_for_location;
}

static void
mautilus_desktop_window_slot_init (mautilusDesktopWindowSlot *self)
{
    GAction *action;
    GActionGroup *action_group;

    /* Disable search on desktop */
    action_group = gtk_widget_get_action_group (GTK_WIDGET (self), "slot");

    action = g_action_map_lookup_action (G_ACTION_MAP (action_group), "search-visible");
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), FALSE);

    /* Disable the ability to change between types of views */
    action = g_action_map_lookup_action (G_ACTION_MAP (action_group), "files-view-mode");
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), FALSE);
    action = g_action_map_lookup_action (G_ACTION_MAP (action_group), "files-view-mode-toggle");
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), FALSE);
}
