/*
   mautilus-desktop-link-monitor.h: singleton that manages the desktop links
    
   Copyright (C) 2003 Red Hat, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Alexander Larsson <alexl@redhat.com>
*/

#ifndef MAUTILUS_DESKTOP_LINK_MONITOR_H
#define MAUTILUS_DESKTOP_LINK_MONITOR_H

#include <gtk/gtk.h>
#include "mautilus-desktop-link.h"

#define MAUTILUS_TYPE_DESKTOP_LINK_MONITOR mautilus_desktop_link_monitor_get_type()
#define MAUTILUS_DESKTOP_LINK_MONITOR(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_DESKTOP_LINK_MONITOR, mautilusDesktopLinkMonitor))
#define MAUTILUS_DESKTOP_LINK_MONITOR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_DESKTOP_LINK_MONITOR, mautilusDesktopLinkMonitorClass))
#define MAUTILUS_IS_DESKTOP_LINK_MONITOR(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_DESKTOP_LINK_MONITOR))
#define MAUTILUS_IS_DESKTOP_LINK_MONITOR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_DESKTOP_LINK_MONITOR))
#define MAUTILUS_DESKTOP_LINK_MONITOR_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_DESKTOP_LINK_MONITOR, mautilusDesktopLinkMonitorClass))

typedef struct mautilusDesktopLinkMonitorDetails mautilusDesktopLinkMonitorDetails;

typedef struct {
	GObject parent_slot;
	mautilusDesktopLinkMonitorDetails *details;
} mautilusDesktopLinkMonitor;

typedef struct {
	GObjectClass parent_slot;
} mautilusDesktopLinkMonitorClass;

GType   mautilus_desktop_link_monitor_get_type (void);

mautilusDesktopLinkMonitor *   mautilus_desktop_link_monitor_get (void);
void mautilus_desktop_link_monitor_shutdown (void);

/* Used by mautilus-desktop-link.c */
char * mautilus_desktop_link_monitor_make_filename_unique (mautilusDesktopLinkMonitor *monitor,
							   const char *filename);

#endif /* MAUTILUS_DESKTOP_LINK_MONITOR_H */
