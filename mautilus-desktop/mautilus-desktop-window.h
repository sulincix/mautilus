
/*
 * mautilus
 *
 * Copyright (C) 2000 Eazel, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Darin Adler <darin@bentspoon.com>
 */

/* mautilus-desktop-window.h
 */

#ifndef MAUTILUS_DESKTOP_WINDOW_H
#define MAUTILUS_DESKTOP_WINDOW_H

#include <src/mautilus-window.h>

#define MAUTILUS_TYPE_DESKTOP_WINDOW mautilus_desktop_window_get_type()
#define MAUTILUS_DESKTOP_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_DESKTOP_WINDOW, mautilusDesktopWindow))
#define MAUTILUS_DESKTOP_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_DESKTOP_WINDOW, mautilusDesktopWindowClass))
#define MAUTILUS_IS_DESKTOP_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_DESKTOP_WINDOW))
#define MAUTILUS_IS_DESKTOP_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_DESKTOP_WINDOW))
#define MAUTILUS_DESKTOP_WINDOW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_DESKTOP_WINDOW, mautilusDesktopWindowClass))

typedef struct mautilusDesktopWindowDetails mautilusDesktopWindowDetails;

typedef struct {
	mautilusWindow parent_spot;
	mautilusDesktopWindowDetails *details;
} mautilusDesktopWindow;

typedef struct {
	mautilusWindowClass parent_spot;
} mautilusDesktopWindowClass;

GType                  mautilus_desktop_window_get_type            (void);
GtkWidget *            mautilus_desktop_window_get                 (void);
void                   mautilus_desktop_window_ensure              (void);
gboolean               mautilus_desktop_window_loaded              (mautilusDesktopWindow *window);

#endif /* MAUTILUS_DESKTOP_WINDOW_H */
