/* mautilus-desktop-canvas-view-container.c
 *
 * Copyright (C) 2016 Carlos Soriano <csoriano@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mautilus-desktop-canvas-view-container.h"
#include "mautilus-desktop-icon-file.h"

struct _mautilusDesktopCanvasViewContainer
{
    mautilusCanvasViewContainer parent_instance;
};

G_DEFINE_TYPE (mautilusDesktopCanvasViewContainer, mautilus_desktop_canvas_view_container, MAUTILUS_TYPE_CANVAS_VIEW_CONTAINER)

/* Sort as follows:
 *   0) home link
 *   1) network link
 *   2) mount links
 *   3) other
 *   4) trash link
 */
typedef enum
{
    SORT_HOME_LINK,
    SORT_NETWORK_LINK,
    SORT_MOUNT_LINK,
    SORT_OTHER,
    SORT_TRASH_LINK
} SortCategory;

static SortCategory
get_sort_category (mautilusFile *file)
{
    mautilusDesktopLink *link;
    SortCategory category;

    category = SORT_OTHER;

    if (MAUTILUS_IS_DESKTOP_ICON_FILE (file))
    {
        link = mautilus_desktop_icon_file_get_link (MAUTILUS_DESKTOP_ICON_FILE (file));
        if (link != NULL)
        {
            switch (mautilus_desktop_link_get_link_type (link))
            {
                case MAUTILUS_DESKTOP_LINK_HOME:
                {
                    category = SORT_HOME_LINK;
                }
                break;

                case MAUTILUS_DESKTOP_LINK_MOUNT:
                {
                    category = SORT_MOUNT_LINK;
                }
                break;

                case MAUTILUS_DESKTOP_LINK_TRASH:
                {
                    category = SORT_TRASH_LINK;
                }
                break;

                case MAUTILUS_DESKTOP_LINK_NETWORK:
                {
                    category = SORT_NETWORK_LINK;
                }
                break;

                default:
                {
                    category = SORT_OTHER;
                }
                break;
            }
            g_object_unref (link);
        }
    }

    return category;
}

static int
real_compare_icons (mautilusCanvasContainer *container,
                    mautilusCanvasIconData  *data_a,
                    mautilusCanvasIconData  *data_b)
{
    mautilusFile *file_a;
    mautilusFile *file_b;
    mautilusFilesView *directory_view;
    SortCategory category_a, category_b;

    file_a = (mautilusFile *) data_a;
    file_b = (mautilusFile *) data_b;

    directory_view = MAUTILUS_FILES_VIEW (MAUTILUS_CANVAS_VIEW_CONTAINER (container)->view);
    g_return_val_if_fail (directory_view != NULL, 0);

    category_a = get_sort_category (file_a);
    category_b = get_sort_category (file_b);

    if (category_a == category_b)
    {
        return mautilus_file_compare_for_sort (file_a,
                                               file_b,
                                               MAUTILUS_FILE_SORT_BY_DISPLAY_NAME,
                                               mautilus_files_view_should_sort_directories_first (directory_view),
                                               FALSE);
    }

    if (category_a < category_b)
    {
        return -1;
    }
    else
    {
        return +1;
    }
}

static void
real_get_icon_text (mautilusCanvasContainer  *container,
                    mautilusCanvasIconData   *data,
                    char                    **editable_text,
                    char                    **additional_text,
                    gboolean                  include_invisible)
{
    mautilusFile *file;
    gboolean use_additional;

    file = MAUTILUS_FILE (data);

    g_assert (MAUTILUS_IS_FILE (file));
    g_assert (editable_text != NULL);

    use_additional = (additional_text != NULL);

    /* Strip the suffix for mautilus object xml files. */
    *editable_text = mautilus_file_get_display_name (file);

    if (!use_additional)
    {
        return;
    }

    if (MAUTILUS_IS_DESKTOP_ICON_FILE (file) ||
        mautilus_file_is_mautilus_link (file))
    {
        /* Don't show the normal extra information for desktop icons,
         * or desktop files, it doesn't make sense.
         */
        *additional_text = NULL;

        return;
    }

    return MAUTILUS_CANVAS_CONTAINER_CLASS (mautilus_desktop_canvas_view_container_parent_class)->get_icon_text (container,
                                                                                                                 data,
                                                                                                                 editable_text,
                                                                                                                 additional_text,
                                                                                                                 include_invisible);
}

static char *
real_get_icon_description (mautilusCanvasContainer *container,
                           mautilusCanvasIconData  *data)
{
    mautilusFile *file;

    file = MAUTILUS_FILE (data);
    g_assert (MAUTILUS_IS_FILE (file));

    if (MAUTILUS_IS_DESKTOP_ICON_FILE (file))
    {
        return NULL;
    }

    return MAUTILUS_CANVAS_CONTAINER_CLASS (mautilus_desktop_canvas_view_container_parent_class)->get_icon_description (container,
                                                                                                                        data);
}

mautilusDesktopCanvasViewContainer *
mautilus_desktop_canvas_view_container_new (void)
{
    return g_object_new (MAUTILUS_TYPE_DESKTOP_CANVAS_VIEW_CONTAINER, NULL);
}

static void
mautilus_desktop_canvas_view_container_class_init (mautilusDesktopCanvasViewContainerClass *klass)
{
    mautilusCanvasContainerClass *container_class = MAUTILUS_CANVAS_CONTAINER_CLASS (klass);

    container_class->get_icon_description = real_get_icon_description;
    container_class->get_icon_text = real_get_icon_text;
    container_class->compare_icons = real_compare_icons;
}

static void
mautilus_desktop_canvas_view_container_init (mautilusDesktopCanvasViewContainer *self)
{
}
