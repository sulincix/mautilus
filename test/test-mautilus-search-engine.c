#include <src/mautilus-file-utilities.h>
#include <src/mautilus-search-provider.h>
#include <src/mautilus-search-engine.h>
#include <gtk/gtk.h>

static void
hits_added_cb (mautilusSearchEngine *engine,
               GSList               *hits)
{
    g_print ("hits added\n");
    while (hits)
    {
        g_print (" - %s\n", (char *) hits->data);
        hits = hits->next;
    }
}

static void
finished_cb (mautilusSearchEngine         *engine,
             mautilusSearchProviderStatus  status)
{
    g_print ("finished!\n");
    gtk_main_quit ();
}

int
main (int   argc,
      char *argv[])
{
    mautilusSearchEngine *engine;
    mautilusSearchEngineModel *model;
    mautilusDirectory *directory;
    mautilusQuery *query;
    GFile *location;

    gtk_init (&argc, &argv);

    mautilus_ensure_extension_points ();

    engine = mautilus_search_engine_new ();
    g_signal_connect (engine, "hits-added",
                      G_CALLBACK (hits_added_cb), NULL);
    g_signal_connect (engine, "finished",
                      G_CALLBACK (finished_cb), NULL);

    query = mautilus_query_new ();
    mautilus_query_set_text (query, "richard hult");
    mautilus_search_provider_set_query (MAUTILUS_SEARCH_PROVIDER (engine), query);
    g_object_unref (query);

    location = g_file_new_for_path (g_get_home_dir ());
    directory = mautilus_directory_get (location);
    g_object_unref (location);

    model = mautilus_search_engine_get_model_provider (engine);
    mautilus_search_engine_model_set_model (model, directory);
    g_object_unref (directory);

    mautilus_search_provider_start (MAUTILUS_SEARCH_PROVIDER (engine));
    mautilus_search_provider_stop (MAUTILUS_SEARCH_PROVIDER (engine));
    g_object_unref (engine);

    gtk_main ();
    return 0;
}
