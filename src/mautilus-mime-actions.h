
/* mautilus-mime-actions.h - uri-specific versions of mime action functions

   Copyright (C) 2000 Eazel, Inc.

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   see <http://www.gnu.org/licenses/>.

   Authors: Maciej Stachowiak <mjs@eazel.com>
*/

#ifndef MAUTILUS_MIME_ACTIONS_H
#define MAUTILUS_MIME_ACTIONS_H

#include <gio/gio.h>
#include <glib/gi18n.h>

#include "mautilus-file.h"

#include "mautilus-window.h"

mautilusFileAttributes mautilus_mime_actions_get_required_file_attributes (void);

GAppInfo *             mautilus_mime_get_default_application_for_file     (mautilusFile            *file);
GList *                mautilus_mime_get_applications_for_file            (mautilusFile            *file);

GAppInfo *             mautilus_mime_get_default_application_for_files    (GList                   *files);

gboolean               mautilus_mime_file_extracts                        (mautilusFile            *file);
gboolean               mautilus_mime_file_opens_in_external_app           (mautilusFile            *file);
gboolean               mautilus_mime_file_launches                        (mautilusFile            *file);
void                   mautilus_mime_activate_files                       (GtkWindow               *parent_window,
									   mautilusWindowSlot      *slot,
									   GList                   *files,
									   const char              *launch_directory,
									   mautilusWindowOpenFlags  flags,
									   gboolean                 user_confirmation);
void                   mautilus_mime_activate_file                        (GtkWindow               *parent_window,
									   mautilusWindowSlot      *slot_info,
									   mautilusFile            *file,
									   const char              *launch_directory,
									   mautilusWindowOpenFlags  flags);
gint                   mautilus_mime_types_get_number_of_groups           (void);
const gchar*           mautilus_mime_types_group_get_name                 (gint                     group_index);
GList*                 mautilus_mime_types_group_get_mimetypes            (gint                     group_index);


#endif /* MAUTILUS_MIME_ACTIONS_H */
