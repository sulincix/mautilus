/*
 * Copyright (C) 2012 Red Hat, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MAUTILUS_SEARCH_HIT_H
#define MAUTILUS_SEARCH_HIT_H

#include <glib-object.h>
#include "mautilus-query.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_SEARCH_HIT (mautilus_search_hit_get_type ())

G_DECLARE_FINAL_TYPE (mautilusSearchHit, mautilus_search_hit, MAUTILUS, SEARCH_HIT, GObject);

mautilusSearchHit * mautilus_search_hit_new                   (const char        *uri);

void                mautilus_search_hit_set_fts_rank          (mautilusSearchHit *hit,
							       gdouble            fts_rank);
void                mautilus_search_hit_set_modification_time (mautilusSearchHit *hit,
							       GDateTime         *date);
void                mautilus_search_hit_set_access_time       (mautilusSearchHit *hit,
							       GDateTime         *date);
void                mautilus_search_hit_set_fts_snippet       (mautilusSearchHit *hit,
                                                               const gchar       *snippet);
void                mautilus_search_hit_compute_scores        (mautilusSearchHit *hit,
							       mautilusQuery     *query);

const char *        mautilus_search_hit_get_uri               (mautilusSearchHit *hit);
gdouble             mautilus_search_hit_get_relevance         (mautilusSearchHit *hit);
const gchar *       mautilus_search_hit_get_fts_snippet       (mautilusSearchHit *hit);

G_END_DECLS

#endif /* MAUTILUS_SEARCH_HIT_H */
