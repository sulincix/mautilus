/*
 * Copyright (C) 2005 Novell, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Author: Anders Carlsson <andersca@imendio.com>
 *
 */

#include <config.h>

#include <glib/gi18n.h>
#include "mautilus-search-provider.h"
#include "mautilus-search-engine.h"
#include "mautilus-search-engine-simple.h"
#include "mautilus-search-engine-model.h"
#define DEBUG_FLAG MAUTILUS_DEBUG_SEARCH
#include "mautilus-debug.h"
#include "mautilus-search-engine-tracker.h"

typedef struct
{
    mautilusSearchEngineTracker *tracker;
    mautilusSearchEngineSimple *simple;
    mautilusSearchEngineModel *model;

    GHashTable *uris;
    guint providers_running;
    guint providers_finished;
    guint providers_error;

    gboolean running;
    gboolean restart;
} mautilusSearchEnginePrivate;

enum
{
    PROP_0,
    PROP_RUNNING,
    LAST_PROP
};

static void mautilus_search_provider_init (mautilusSearchProviderInterface *iface);

static gboolean mautilus_search_engine_is_running (mautilusSearchProvider *provider);

G_DEFINE_TYPE_WITH_CODE (mautilusSearchEngine,
                         mautilus_search_engine,
                         G_TYPE_OBJECT,
                         G_ADD_PRIVATE (mautilusSearchEngine)
                         G_IMPLEMENT_INTERFACE (MAUTILUS_TYPE_SEARCH_PROVIDER,
                                                mautilus_search_provider_init))

static void
mautilus_search_engine_set_query (mautilusSearchProvider *provider,
                                  mautilusQuery          *query)
{
    mautilusSearchEngine *engine;
    mautilusSearchEnginePrivate *priv;

    engine = MAUTILUS_SEARCH_ENGINE (provider);
    priv = mautilus_search_engine_get_instance_private (engine);

    //mautilus_search_provider_set_query (MAUTILUS_SEARCH_PROVIDER (priv->tracker), query);
    mautilus_search_provider_set_query (MAUTILUS_SEARCH_PROVIDER (priv->model), query);
    mautilus_search_provider_set_query (MAUTILUS_SEARCH_PROVIDER (priv->simple), query);
}

static void
search_engine_start_real (mautilusSearchEngine *engine)
{
    mautilusSearchEnginePrivate *priv;

    priv = mautilus_search_engine_get_instance_private (engine);

    priv->providers_running = 0;
    priv->providers_finished = 0;
    priv->providers_error = 0;

    priv->restart = FALSE;

    DEBUG ("Search engine start real");

    g_object_ref (engine);

    priv->providers_running++;
    mautilus_search_provider_start (MAUTILUS_SEARCH_PROVIDER (priv->tracker));

    if (mautilus_search_engine_model_get_model (priv->model))
    {
        priv->providers_running++;
        mautilus_search_provider_start (MAUTILUS_SEARCH_PROVIDER (priv->model));
    }

    priv->providers_running++;
    mautilus_search_provider_start (MAUTILUS_SEARCH_PROVIDER (priv->simple));
}

static void
mautilus_search_engine_start (mautilusSearchProvider *provider)
{
    mautilusSearchEngine *engine;
    mautilusSearchEnginePrivate *priv;
    gint num_finished;

    engine = MAUTILUS_SEARCH_ENGINE (provider);
    priv = mautilus_search_engine_get_instance_private (engine);

    DEBUG ("Search engine start");

    num_finished = priv->providers_error + priv->providers_finished;

    if (priv->running)
    {
        if (num_finished == priv->providers_running &&
            priv->restart)
        {
            search_engine_start_real (engine);
        }

        return;
    }

    priv->running = TRUE;

    g_object_notify (G_OBJECT (provider), "running");

    if (num_finished < priv->providers_running)
    {
        priv->restart = TRUE;
    }
    else
    {
        search_engine_start_real (engine);
    }
}

static void
mautilus_search_engine_stop (mautilusSearchProvider *provider)
{
    mautilusSearchEngine *engine;
    mautilusSearchEnginePrivate *priv;

    engine = MAUTILUS_SEARCH_ENGINE (provider);
    priv = mautilus_search_engine_get_instance_private (engine);

    DEBUG ("Search engine stop");

    mautilus_search_provider_stop (MAUTILUS_SEARCH_PROVIDER (priv->tracker));
    mautilus_search_provider_stop (MAUTILUS_SEARCH_PROVIDER (priv->model));
    mautilus_search_provider_stop (MAUTILUS_SEARCH_PROVIDER (priv->simple));

    priv->running = FALSE;
    priv->restart = FALSE;

    g_object_notify (G_OBJECT (provider), "running");
}

static void
search_provider_hits_added (mautilusSearchProvider *provider,
                            GList                  *hits,
                            mautilusSearchEngine   *engine)
{
    mautilusSearchEnginePrivate *priv;
    GList *added = NULL;
    GList *l;

    priv = mautilus_search_engine_get_instance_private (engine);

    if (!priv->running || priv->restart)
    {
        DEBUG ("Ignoring hits-added, since engine is %s",
               !priv->running ? "not running" : "waiting to restart");
        return;
    }

    for (l = hits; l != NULL; l = l->next)
    {
        mautilusSearchHit *hit = l->data;
        int count;
        const char *uri;

        uri = mautilus_search_hit_get_uri (hit);
        count = GPOINTER_TO_INT (g_hash_table_lookup (priv->uris, uri));
        if (count == 0)
        {
            added = g_list_prepend (added, hit);
        }
        g_hash_table_replace (priv->uris, g_strdup (uri), GINT_TO_POINTER (++count));
    }
    if (added != NULL)
    {
        added = g_list_reverse (added);
        mautilus_search_provider_hits_added (MAUTILUS_SEARCH_PROVIDER (engine), added);
        g_list_free (added);
    }
}

static void
check_providers_status (mautilusSearchEngine *engine)
{
    mautilusSearchEnginePrivate *priv;
    gint num_finished;

    priv = mautilus_search_engine_get_instance_private (engine);
    num_finished = priv->providers_error + priv->providers_finished;

    if (num_finished < priv->providers_running)
    {
        return;
    }

    if (num_finished == priv->providers_error)
    {
        DEBUG ("Search engine error");
        mautilus_search_provider_error (MAUTILUS_SEARCH_PROVIDER (engine),
                                        _("Unable to complete the requested search"));
    }
    else
    {
        if (priv->restart)
        {
            DEBUG ("Search engine finished and restarting");
        }
        else
        {
            DEBUG ("Search engine finished");
        }
        mautilus_search_provider_finished (MAUTILUS_SEARCH_PROVIDER (engine),
                                           priv->restart ? MAUTILUS_SEARCH_PROVIDER_STATUS_RESTARTING :
                                           MAUTILUS_SEARCH_PROVIDER_STATUS_NORMAL);
    }

    priv->running = FALSE;
    g_object_notify (G_OBJECT (engine), "running");

    g_hash_table_remove_all (priv->uris);

    if (priv->restart)
    {
        mautilus_search_engine_start (MAUTILUS_SEARCH_PROVIDER (engine));
    }

    g_object_unref (engine);
}

static void
search_provider_error (mautilusSearchProvider *provider,
                       const char             *error_message,
                       mautilusSearchEngine   *engine)
{
    mautilusSearchEnginePrivate *priv;

    DEBUG ("Search provider error: %s", error_message);

    priv = mautilus_search_engine_get_instance_private (engine);
    priv->providers_error++;

    check_providers_status (engine);
}

static void
search_provider_finished (mautilusSearchProvider       *provider,
                          mautilusSearchProviderStatus  status,
                          mautilusSearchEngine         *engine)
{
    mautilusSearchEnginePrivate *priv;

    DEBUG ("Search provider finished");

    priv = mautilus_search_engine_get_instance_private (engine);
    priv->providers_finished++;

    check_providers_status (engine);
}

static void
connect_provider_signals (mautilusSearchEngine   *engine,
                          mautilusSearchProvider *provider)
{
    g_signal_connect (provider, "hits-added",
                      G_CALLBACK (search_provider_hits_added),
                      engine);
    g_signal_connect (provider, "finished",
                      G_CALLBACK (search_provider_finished),
                      engine);
    g_signal_connect (provider, "error",
                      G_CALLBACK (search_provider_error),
                      engine);
}

static gboolean
mautilus_search_engine_is_running (mautilusSearchProvider *provider)
{
    mautilusSearchEngine *engine;
    mautilusSearchEnginePrivate *priv;

    engine = MAUTILUS_SEARCH_ENGINE (provider);
    priv = mautilus_search_engine_get_instance_private (engine);

    return priv->running;
}

static void
mautilus_search_provider_init (mautilusSearchProviderInterface *iface)
{
    iface->set_query = mautilus_search_engine_set_query;
    iface->start = mautilus_search_engine_start;
    iface->stop = mautilus_search_engine_stop;
    iface->is_running = mautilus_search_engine_is_running;
}

static void
mautilus_search_engine_finalize (GObject *object)
{
    mautilusSearchEngine *engine;
    mautilusSearchEnginePrivate *priv;

    engine = MAUTILUS_SEARCH_ENGINE (object);
    priv = mautilus_search_engine_get_instance_private (engine);

    g_hash_table_destroy (priv->uris);

    g_clear_object (&priv->tracker);
    g_clear_object (&priv->model);
    g_clear_object (&priv->simple);

    G_OBJECT_CLASS (mautilus_search_engine_parent_class)->finalize (object);
}

static void
mautilus_search_engine_get_property (GObject    *object,
                                     guint       prop_id,
                                     GValue     *value,
                                     GParamSpec *pspec)
{
    mautilusSearchProvider *self = MAUTILUS_SEARCH_PROVIDER (object);

    switch (prop_id)
    {
        case PROP_RUNNING:
        {
            g_value_set_boolean (value, mautilus_search_engine_is_running (self));
        }
        break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mautilus_search_engine_class_init (mautilusSearchEngineClass *class)
{
    GObjectClass *object_class;

    object_class = (GObjectClass *) class;

    object_class->finalize = mautilus_search_engine_finalize;
    object_class->get_property = mautilus_search_engine_get_property;

    /**
     * mautilusSearchEngine::running:
     *
     * Whether the search engine is running a search.
     */
    g_object_class_override_property (object_class, PROP_RUNNING, "running");
}

static void
mautilus_search_engine_init (mautilusSearchEngine *engine)
{
    mautilusSearchEnginePrivate *priv;

    priv = mautilus_search_engine_get_instance_private (engine);
    priv->uris = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);

    priv->tracker = mautilus_search_engine_tracker_new ();
    connect_provider_signals (engine, MAUTILUS_SEARCH_PROVIDER (priv->tracker));

    priv->model = mautilus_search_engine_model_new ();
    connect_provider_signals (engine, MAUTILUS_SEARCH_PROVIDER (priv->model));

    priv->simple = mautilus_search_engine_simple_new ();
    connect_provider_signals (engine, MAUTILUS_SEARCH_PROVIDER (priv->simple));
}

mautilusSearchEngine *
mautilus_search_engine_new (void)
{
    mautilusSearchEngine *engine;

    engine = g_object_new (MAUTILUS_TYPE_SEARCH_ENGINE, NULL);

    return engine;
}

mautilusSearchEngineModel *
mautilus_search_engine_get_model_provider (mautilusSearchEngine *engine)
{
    mautilusSearchEnginePrivate *priv;

    priv = mautilus_search_engine_get_instance_private (engine);

    return priv->model;
}

mautilusSearchEngineSimple *
mautilus_search_engine_get_simple_provider (mautilusSearchEngine *engine)
{
    mautilusSearchEnginePrivate *priv;

    priv = mautilus_search_engine_get_instance_private (engine);

    return priv->simple;
}
