
/*
 * mautilus
 *
 * Copyright (C) 2011, Red Hat, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Cosimo Cecchi <cosimoc@redhat.com>
 *
 */

#ifndef __MAUTILUS_TOOLBAR_H__
#define __MAUTILUS_TOOLBAR_H__

#include <gtk/gtk.h>

#include "mautilus-window-slot.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_TOOLBAR mautilus_toolbar_get_type()

G_DECLARE_FINAL_TYPE (mautilusToolbar, mautilus_toolbar, MAUTILUS, TOOLBAR, GtkHeaderBar)

GtkWidget *mautilus_toolbar_new (void);

GtkWidget *mautilus_toolbar_get_path_bar (mautilusToolbar *self);
GtkWidget *mautilus_toolbar_get_location_entry (mautilusToolbar *self);

void       mautilus_toolbar_set_show_location_entry (mautilusToolbar *self,
                                                     gboolean show_location_entry);

void       mautilus_toolbar_set_active_slot    (mautilusToolbar    *toolbar,
                                                mautilusWindowSlot *slot);

gboolean   mautilus_toolbar_is_menu_visible    (mautilusToolbar *toolbar);

gboolean   mautilus_toolbar_is_operations_button_active (mautilusToolbar *toolbar);

void       mautilus_toolbar_on_window_constructed       (mautilusToolbar *toolbar);

G_END_DECLS

#endif /* __MAUTILUS_TOOLBAR_H__ */
