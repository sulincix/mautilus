/*
 * Copyright (C) 2005 Novell, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Author: Anders Carlsson <andersca@imendio.com>
 *
 */

#ifndef MAUTILUS_SEARCH_ENGINE_H
#define MAUTILUS_SEARCH_ENGINE_H

#include <glib-object.h>

#include "mautilus-directory.h"
#include "mautilus-search-engine-model.h"
#include "mautilus-search-engine-simple.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_SEARCH_ENGINE		(mautilus_search_engine_get_type ())

G_DECLARE_DERIVABLE_TYPE (mautilusSearchEngine, mautilus_search_engine, MAUTILUS, SEARCH_ENGINE, GObject)

struct _mautilusSearchEngineClass
{
  GObjectClass parent_class;
};

mautilusSearchEngine *mautilus_search_engine_new                (void);
mautilusSearchEngineModel *
                      mautilus_search_engine_get_model_provider (mautilusSearchEngine *engine);
mautilusSearchEngineSimple *
                      mautilus_search_engine_get_simple_provider (mautilusSearchEngine *engine);

G_END_DECLS

#endif /* MAUTILUS_SEARCH_ENGINE_H */
