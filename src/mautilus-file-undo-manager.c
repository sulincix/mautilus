/* mautilus-file-undo-manager.c - Manages the undo/redo stack
 *
 * Copyright (C) 2007-2011 Amos Brocco
 * Copyright (C) 2010, 2012 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Amos Brocco <amos.brocco@gmail.com>
 *          Cosimo Cecchi <cosimoc@redhat.com>
 *
 */

#include <config.h>

#include "mautilus-file-undo-manager.h"

#include "mautilus-file-operations.h"
#include "mautilus-file.h"
#include "mautilus-trash-monitor.h"

#include <glib/gi18n.h>

#define DEBUG_FLAG MAUTILUS_DEBUG_UNDO
#include "mautilus-debug.h"

enum
{
    SIGNAL_UNDO_CHANGED,
    NUM_SIGNALS,
};

static guint signals[NUM_SIGNALS] = { 0, };

struct _mautilusFileUndoManager
{
    GObject parent_instance;
    mautilusFileUndoInfo *info;
    mautilusFileUndoManagerState state;
    mautilusFileUndoManagerState last_state;

    guint is_operating : 1;

    gulong trash_signal_id;
};

G_DEFINE_TYPE (mautilusFileUndoManager, mautilus_file_undo_manager, G_TYPE_OBJECT)

static mautilusFileUndoManager * undo_singleton = NULL;

mautilusFileUndoManager *
mautilus_file_undo_manager_new (void)
{
    if (undo_singleton != NULL)
    {
        return g_object_ref (undo_singleton);
    }

    undo_singleton = g_object_new (MAUTILUS_TYPE_FILE_UNDO_MANAGER, NULL);
    g_object_add_weak_pointer (G_OBJECT (undo_singleton), (gpointer) & undo_singleton);

    return undo_singleton;
}

static void
file_undo_manager_clear (mautilusFileUndoManager *self)
{
    g_clear_object (&self->info);
    self->state = MAUTILUS_FILE_UNDO_MANAGER_STATE_NONE;
}

static void
trash_state_changed_cb (mautilusTrashMonitor *monitor,
                        gboolean              is_empty,
                        gpointer              user_data)
{
    mautilusFileUndoManager *self = user_data;

    /* A trash operation cannot be undone if the trash is empty */
    if (is_empty &&
        self->state == MAUTILUS_FILE_UNDO_MANAGER_STATE_UNDO &&
        MAUTILUS_IS_FILE_UNDO_INFO_TRASH (self->info))
    {
        file_undo_manager_clear (self);
        g_signal_emit (self, signals[SIGNAL_UNDO_CHANGED], 0);
    }
}

static void
mautilus_file_undo_manager_init (mautilusFileUndoManager *self)
{
    self->trash_signal_id = g_signal_connect (mautilus_trash_monitor_get (),
                                              "trash-state-changed",
                                              G_CALLBACK (trash_state_changed_cb), self);
}

static void
mautilus_file_undo_manager_finalize (GObject *object)
{
    mautilusFileUndoManager *self = MAUTILUS_FILE_UNDO_MANAGER (object);

    if (self->trash_signal_id != 0)
    {
        g_signal_handler_disconnect (mautilus_trash_monitor_get (),
                                     self->trash_signal_id);
        self->trash_signal_id = 0;
    }

    file_undo_manager_clear (self);

    G_OBJECT_CLASS (mautilus_file_undo_manager_parent_class)->finalize (object);
}

static void
mautilus_file_undo_manager_class_init (mautilusFileUndoManagerClass *klass)
{
    GObjectClass *oclass;

    oclass = G_OBJECT_CLASS (klass);

    oclass->finalize = mautilus_file_undo_manager_finalize;

    signals[SIGNAL_UNDO_CHANGED] =
        g_signal_new ("undo-changed",
                      G_TYPE_FROM_CLASS (klass),
                      G_SIGNAL_RUN_LAST,
                      0, NULL, NULL,
                      g_cclosure_marshal_VOID__VOID,
                      G_TYPE_NONE, 0);
}

static void
undo_info_apply_ready (GObject      *source,
                       GAsyncResult *res,
                       gpointer      user_data)
{
    mautilusFileUndoManager *self = user_data;
    mautilusFileUndoInfo *info = MAUTILUS_FILE_UNDO_INFO (source);
    gboolean success, user_cancel;

    success = mautilus_file_undo_info_apply_finish (info, res, &user_cancel, NULL);

    self->is_operating = FALSE;

    /* just return in case we got another another operation set */
    if ((self->info != NULL) &&
        (self->info != info))
    {
        return;
    }

    if (success)
    {
        if (self->last_state == MAUTILUS_FILE_UNDO_MANAGER_STATE_UNDO)
        {
            self->state = MAUTILUS_FILE_UNDO_MANAGER_STATE_REDO;
        }
        else if (self->last_state == MAUTILUS_FILE_UNDO_MANAGER_STATE_REDO)
        {
            self->state = MAUTILUS_FILE_UNDO_MANAGER_STATE_UNDO;
        }

        self->info = g_object_ref (info);
    }
    else if (user_cancel)
    {
        self->state = self->last_state;
        self->info = g_object_ref (info);
    }
    else
    {
        file_undo_manager_clear (self);
    }

    g_signal_emit (self, signals[SIGNAL_UNDO_CHANGED], 0);
}

static void
do_undo_redo (mautilusFileUndoManager *self,
              GtkWindow               *parent_window)
{
    gboolean undo = self->state == MAUTILUS_FILE_UNDO_MANAGER_STATE_UNDO;

    self->last_state = self->state;

    self->is_operating = TRUE;
    mautilus_file_undo_info_apply_async (self->info, undo, parent_window,
                                         undo_info_apply_ready, self);

    /* clear actions while undoing */
    file_undo_manager_clear (self);
    g_signal_emit (self, signals[SIGNAL_UNDO_CHANGED], 0);
}

void
mautilus_file_undo_manager_redo (GtkWindow *parent_window)
{
    if (undo_singleton->state != MAUTILUS_FILE_UNDO_MANAGER_STATE_REDO)
    {
        g_warning ("Called redo, but state is %s!", undo_singleton->state == 0 ?
                   "none" : "undo");
        return;
    }

    do_undo_redo (undo_singleton, parent_window);
}

void
mautilus_file_undo_manager_undo (GtkWindow *parent_window)
{
    if (undo_singleton->state != MAUTILUS_FILE_UNDO_MANAGER_STATE_UNDO)
    {
        g_warning ("Called undo, but state is %s!", undo_singleton->state == 0 ?
                   "none" : "redo");
        return;
    }

    do_undo_redo (undo_singleton, parent_window);
}

void
mautilus_file_undo_manager_set_action (mautilusFileUndoInfo *info)
{
    DEBUG ("Setting undo information %p", info);

    file_undo_manager_clear (undo_singleton);

    if (info != NULL)
    {
        undo_singleton->info = g_object_ref (info);
        undo_singleton->state = MAUTILUS_FILE_UNDO_MANAGER_STATE_UNDO;
        undo_singleton->last_state = MAUTILUS_FILE_UNDO_MANAGER_STATE_NONE;
    }

    g_signal_emit (undo_singleton, signals[SIGNAL_UNDO_CHANGED], 0);
}

mautilusFileUndoInfo *
mautilus_file_undo_manager_get_action (void)
{
    return undo_singleton->info;
}

mautilusFileUndoManagerState
mautilus_file_undo_manager_get_state (void)
{
    return undo_singleton->state;
}


gboolean
mautilus_file_undo_manager_is_operating ()
{
    return undo_singleton->is_operating;
}

mautilusFileUndoManager *
mautilus_file_undo_manager_get ()
{
    return undo_singleton;
}
