
/* fm-properties-window.h - interface for window that lets user modify 
                            icon properties

   Copyright (C) 2000 Eazel, Inc.

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   see <http://www.gnu.org/licenses/>.

   Authors: Darin Adler <darin@bentspoon.com>
*/

#ifndef MAUTILUS_PROPERTIES_WINDOW_H
#define MAUTILUS_PROPERTIES_WINDOW_H

#include <gtk/gtk.h>
#include "mautilus-file.h"

typedef struct mautilusPropertiesWindow mautilusPropertiesWindow;

#define MAUTILUS_TYPE_PROPERTIES_WINDOW mautilus_properties_window_get_type()
#define MAUTILUS_PROPERTIES_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_PROPERTIES_WINDOW, mautilusPropertiesWindow))
#define MAUTILUS_PROPERTIES_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_PROPERTIES_WINDOW, mautilusPropertiesWindowClass))
#define MAUTILUS_IS_PROPERTIES_WINDOW(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_PROPERTIES_WINDOW))
#define MAUTILUS_IS_PROPERTIES_WINDOW_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_PROPERTIES_WINDOW))
#define MAUTILUS_PROPERTIES_WINDOW_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_PROPERTIES_WINDOW, mautilusPropertiesWindowClass))

typedef struct mautilusPropertiesWindowDetails mautilusPropertiesWindowDetails;

struct mautilusPropertiesWindow {
	GtkDialog window;
	mautilusPropertiesWindowDetails *details;	
};

struct mautilusPropertiesWindowClass {
	GtkDialogClass parent_class;
	
	/* Keybinding signals */
	void (* close)    (mautilusPropertiesWindow *window);
};

typedef struct mautilusPropertiesWindowClass mautilusPropertiesWindowClass;

GType   mautilus_properties_window_get_type   (void);

void 	mautilus_properties_window_present    (GList       *files,
					       GtkWidget   *parent_widget,
					       const gchar *startup_id);

#endif /* MAUTILUS_PROPERTIES_WINDOW_H */
