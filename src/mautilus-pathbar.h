/* mautilus-pathbar.h
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * 
 */

#ifndef MAUTILUS_PATHBAR_H
#define MAUTILUS_PATHBAR_H

#include <gtk/gtk.h>
#include <gio/gio.h>

#define MAUTILUS_TYPE_PATH_BAR (mautilus_path_bar_get_type ())
G_DECLARE_DERIVABLE_TYPE (mautilusPathBar, mautilus_path_bar, MAUTILUS, PATH_BAR, GtkContainer)

struct _mautilusPathBarClass
{
	GtkContainerClass parent_class;

	void     (* path_clicked)   (mautilusPathBar  *self,
				     GFile            *location);
        void     (* open_location)  (mautilusPathBar   *self,
                                     GFile             *location,
                                     GtkPlacesOpenFlags flags);
};
void     mautilus_path_bar_set_path    (mautilusPathBar *self, GFile *file);

#endif /* MAUTILUS_PATHBAR_H */
