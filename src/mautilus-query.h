/*
 * Copyright (C) 2005 Novell, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Author: Anders Carlsson <andersca@imendio.com>
 *
 */

#ifndef MAUTILUS_QUERY_H
#define MAUTILUS_QUERY_H

#include <glib-object.h>
#include <gio/gio.h>

typedef enum {
        MAUTILUS_QUERY_SEARCH_TYPE_LAST_ACCESS,
        MAUTILUS_QUERY_SEARCH_TYPE_LAST_MODIFIED
} mautilusQuerySearchType;

typedef enum {
        MAUTILUS_QUERY_SEARCH_CONTENT_SIMPLE,
        MAUTILUS_QUERY_SEARCH_CONTENT_FULL_TEXT,
} mautilusQuerySearchContent;

#define MAUTILUS_TYPE_QUERY		(mautilus_query_get_type ())

G_DECLARE_FINAL_TYPE (mautilusQuery, mautilus_query, MAUTILUS, QUERY, GObject)

mautilusQuery* mautilus_query_new      (void);

char *         mautilus_query_get_text           (mautilusQuery *query);
void           mautilus_query_set_text           (mautilusQuery *query, const char *text);

gboolean       mautilus_query_get_show_hidden_files (mautilusQuery *query);
void           mautilus_query_set_show_hidden_files (mautilusQuery *query, gboolean show_hidden);

GFile*         mautilus_query_get_location       (mautilusQuery *query);
void           mautilus_query_set_location       (mautilusQuery *query,
                                                  GFile         *location);

GList *        mautilus_query_get_mime_types     (mautilusQuery *query);
void           mautilus_query_set_mime_types     (mautilusQuery *query, GList *mime_types);
void           mautilus_query_add_mime_type      (mautilusQuery *query, const char *mime_type);

mautilusQuerySearchContent mautilus_query_get_search_content (mautilusQuery *query);
void                       mautilus_query_set_search_content (mautilusQuery              *query,
                                                              mautilusQuerySearchContent  content);

gboolean mautilus_query_get_search_favorite (mautilusQuery *query);
void     mautilus_query_set_search_favorite (mautilusQuery *query,
                                             gboolean       search_favorite);

mautilusQuerySearchType mautilus_query_get_search_type (mautilusQuery *query);
void                    mautilus_query_set_search_type (mautilusQuery           *query,
                                                        mautilusQuerySearchType  type);

GPtrArray*     mautilus_query_get_date_range     (mautilusQuery *query);
void           mautilus_query_set_date_range     (mautilusQuery *query,
                                                  GPtrArray     *date_range);

gboolean       mautilus_query_get_recursive      (mautilusQuery *query);

void           mautilus_query_set_recursive      (mautilusQuery *query,
                                                  gboolean       recursive);

gboolean       mautilus_query_get_searching      (mautilusQuery *query);

void           mautilus_query_set_searching      (mautilusQuery *query,
                                                  gboolean       searching);

gdouble        mautilus_query_matches_string     (mautilusQuery *query, const gchar *string);

char *         mautilus_query_to_readable_string (mautilusQuery *query);

gboolean       mautilus_query_is_empty           (mautilusQuery *query);

#endif /* MAUTILUS_QUERY_H */
