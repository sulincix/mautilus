

#ifndef __MAUTILUS_RECENT_H__
#define __MAUTILUS_RECENT_H__

#include <gtk/gtk.h>
#include "mautilus-file.h"
#include <gio/gio.h>

void mautilus_recent_add_file (mautilusFile *file,
			       GAppInfo *application);

#endif
