/*
 * Copyright (C) 2005 Red Hat, Inc
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Author: Alexander Larsson <alexl@redhat.com>
 *
 */

#ifndef MAUTILUS_SEARCH_ENGINE_SIMPLE_H
#define MAUTILUS_SEARCH_ENGINE_SIMPLE_H

G_BEGIN_DECLS

#define MAUTILUS_TYPE_SEARCH_ENGINE_SIMPLE (mautilus_search_engine_simple_get_type ())

G_DECLARE_FINAL_TYPE (mautilusSearchEngineSimple, mautilus_search_engine_simple, MAUTILUS, SEARCH_ENGINE_SIMPLE, GObject);

mautilusSearchEngineSimple* mautilus_search_engine_simple_new (void);

G_END_DECLS

#endif /* MAUTILUS_SEARCH_ENGINE_SIMPLE_H */
