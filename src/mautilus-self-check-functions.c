/*
 * mautilus
 *
 * Copyright (C) 1999, 2000 Eazel, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Darin Adler <darin@bentspoon.com>
 */

/* mautilus-self-check-functions.c: Wrapper for all self check functions
 * in mautilus proper.
 */

#include <config.h>

#if !defined (MAUTILUS_OMIT_SELF_CHECK)

#include "mautilus-self-check-functions.h"

void mautilus_run_self_checks(void)
{
    MAUTILUS_FOR_EACH_SELF_CHECK_FUNCTION (MAUTILUS_CALL_SELF_CHECK_FUNCTION)
}

#endif /* ! MAUTILUS_OMIT_SELF_CHECK */
