/*
   Copyright (C) 2001 Maciej Stachowiak
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.

   Author: Maciej Stachowiak <mjs@noisehavoc.org>
*/

#ifndef MAUTILUS_FILE_QUEUE_H
#define MAUTILUS_FILE_QUEUE_H

#include "mautilus-file.h"

typedef struct mautilusFileQueue mautilusFileQueue;

mautilusFileQueue *mautilus_file_queue_new      (void);
void               mautilus_file_queue_destroy  (mautilusFileQueue *queue);

/* Add a file to the tail of the queue, unless it's already in the queue */
void               mautilus_file_queue_enqueue  (mautilusFileQueue *queue,
						 mautilusFile      *file);

/* Return the file at the head of the queue after removing it from the
 * queue. This is dangerous unless you have another ref to the file,
 * since it will unref it.  
 */
mautilusFile *     mautilus_file_queue_dequeue  (mautilusFileQueue *queue);

/* Remove a file from an arbitrary point in the queue in constant time. */
void               mautilus_file_queue_remove   (mautilusFileQueue *queue,
						 mautilusFile      *file);

/* Get the file at the head of the queue without removing or unrefing it. */
mautilusFile *     mautilus_file_queue_head     (mautilusFileQueue *queue);

gboolean           mautilus_file_queue_is_empty (mautilusFileQueue *queue);

#endif /* MAUTILUS_FILE_CHANGES_QUEUE_H */
