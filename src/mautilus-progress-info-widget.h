/*
 * mautilus-progress-info-widget.h: file operation progress user interface.
 *
 * Copyright (C) 2007, 2011 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Alexander Larsson <alexl@redhat.com>
 *          Cosimo Cecchi <cosimoc@redhat.com>
 *
 */

#ifndef __MAUTILUS_PROGRESS_INFO_WIDGET_H__
#define __MAUTILUS_PROGRESS_INFO_WIDGET_H__

#include <gtk/gtk.h>

#include "mautilus-progress-info.h"

#define MAUTILUS_TYPE_PROGRESS_INFO_WIDGET mautilus_progress_info_widget_get_type()
#define MAUTILUS_PROGRESS_INFO_WIDGET(obj) \
	(G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_PROGRESS_INFO_WIDGET, mautilusProgressInfoWidget))
#define MAUTILUS_PROGRESS_INFO_WIDGET_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_PROGRESS_INFO_WIDGET, mautilusProgressInfoWidgetClass))
#define MAUTILUS_IS_PROGRESS_INFO_WIDGET(obj) \
	(G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_PROGRESS_INFO_WIDGET))
#define MAUTILUS_IS_PROGRESS_INFO_WIDGET_CLASS(klass) \
	(G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_PROGRESS_INFO_WIDGET))
#define MAUTILUS_PROGRESS_INFO_WIDGET_GET_CLASS(obj) \
	(G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_PROGRESS_INFO_WIDGET, mautilusProgressInfoWidgetClass))

typedef struct _mautilusProgressInfoWidgetPrivate mautilusProgressInfoWidgetPrivate;

typedef struct {
	GtkGrid parent;

	/* private */
	mautilusProgressInfoWidgetPrivate *priv;
} mautilusProgressInfoWidget;

typedef struct {
	GtkGridClass parent_class;
} mautilusProgressInfoWidgetClass;

GType mautilus_progress_info_widget_get_type (void);

GtkWidget * mautilus_progress_info_widget_new (mautilusProgressInfo *info);

#endif /* __MAUTILUS_PROGRESS_INFO_WIDGET_H__ */
