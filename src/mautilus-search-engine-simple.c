/*
 * Copyright (C) 2005 Red Hat, Inc
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Author: Alexander Larsson <alexl@redhat.com>
 *
 */

#include <config.h>
#include "mautilus-search-hit.h"
#include "mautilus-search-provider.h"
#include "mautilus-search-engine-simple.h"
#include "mautilus-ui-utilities.h"
#include "mautilus-tag-manager.h"
#define DEBUG_FLAG MAUTILUS_DEBUG_SEARCH
#include "mautilus-debug.h"

#include <string.h>
#include <glib.h>
#include <gio/gio.h>

#define BATCH_SIZE 500

enum
{
    PROP_RECURSIVE = 1,
    PROP_RUNNING,
    NUM_PROPERTIES
};

typedef struct
{
    mautilusSearchEngineSimple *engine;
    GCancellable *cancellable;

    GList *mime_types;
    GList *found_list;

    GQueue *directories;     /* GFiles */

    GHashTable *visited;

    gboolean recursive;
    gint n_processed_files;
    GList *hits;

    mautilusQuery *query;
} SearchThreadData;


struct _mautilusSearchEngineSimple
{
    GObject parent_instance;
    mautilusQuery *query;

    SearchThreadData *active_search;

    gboolean recursive;
};

static void mautilus_search_provider_init (mautilusSearchProviderInterface *iface);

G_DEFINE_TYPE_WITH_CODE (mautilusSearchEngineSimple,
                         mautilus_search_engine_simple,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (MAUTILUS_TYPE_SEARCH_PROVIDER,
                                                mautilus_search_provider_init))

static void
finalize (GObject *object)
{
    mautilusSearchEngineSimple *simple = MAUTILUS_SEARCH_ENGINE_SIMPLE (object);
    g_clear_object (&simple->query);

    G_OBJECT_CLASS (mautilus_search_engine_simple_parent_class)->finalize (object);
}

static SearchThreadData *
search_thread_data_new (mautilusSearchEngineSimple *engine,
                        mautilusQuery              *query)
{
    SearchThreadData *data;
    GFile *location;

    data = g_new0 (SearchThreadData, 1);

    data->engine = g_object_ref (engine);
    data->directories = g_queue_new ();
    data->visited = g_hash_table_new_full (g_str_hash, g_str_equal, g_free, NULL);
    data->query = g_object_ref (query);

    location = mautilus_query_get_location (query);

    g_queue_push_tail (data->directories, location);
    data->mime_types = mautilus_query_get_mime_types (query);

    data->cancellable = g_cancellable_new ();

    return data;
}

static void
search_thread_data_free (SearchThreadData *data)
{
    g_queue_foreach (data->directories,
                     (GFunc) g_object_unref, NULL);
    g_queue_free (data->directories);
    g_hash_table_destroy (data->visited);
    g_object_unref (data->cancellable);
    g_object_unref (data->query);
    g_list_free_full (data->mime_types, g_free);
    g_list_free_full (data->hits, g_object_unref);
    g_object_unref (data->engine);

    g_free (data);
}

static gboolean
search_thread_done_idle (gpointer user_data)
{
    SearchThreadData *data = user_data;
    mautilusSearchEngineSimple *engine = data->engine;

    if (g_cancellable_is_cancelled (data->cancellable))
    {
        DEBUG ("Simple engine finished and cancelled");
    }
    else
    {
        DEBUG ("Simple engine finished");
    }
    engine->active_search = NULL;
    // FIXME: uncommend this
    //mautilus_search_provider_finished (MAUTILUS_SEARCH_PROVIDER (engine),
    //                                   MAUTILUS_SEARCH_PROVIDER_STATUS_NORMAL);

    g_object_notify (G_OBJECT (engine), "running");

    search_thread_data_free (data);

    return FALSE;
}

typedef struct
{
    GList *hits;
    SearchThreadData *thread_data;
} SearchHitsData;


static gboolean
search_thread_add_hits_idle (gpointer user_data)
{
    SearchHitsData *data = user_data;

    if (!g_cancellable_is_cancelled (data->thread_data->cancellable))
    {
        DEBUG ("Simple engine add hits");
        mautilus_search_provider_hits_added (MAUTILUS_SEARCH_PROVIDER (data->thread_data->engine),
                                             data->hits);
    }

    g_list_free_full (data->hits, g_object_unref);
    g_free (data);

    return FALSE;
}

static void
send_batch (SearchThreadData *thread_data)
{
    SearchHitsData *data;

    thread_data->n_processed_files = 0;

    if (thread_data->hits)
    {
        data = g_new (SearchHitsData, 1);
        data->hits = thread_data->hits;
        data->thread_data = thread_data;
        g_idle_add (search_thread_add_hits_idle, data);
    }
    thread_data->hits = NULL;
}

#define STD_ATTRIBUTES \
    G_FILE_ATTRIBUTE_STANDARD_NAME "," \
    G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME "," \
    G_FILE_ATTRIBUTE_STANDARD_IS_BACKUP "," \
    G_FILE_ATTRIBUTE_STANDARD_IS_HIDDEN "," \
    G_FILE_ATTRIBUTE_STANDARD_TYPE "," \
    G_FILE_ATTRIBUTE_TIME_MODIFIED "," \
    G_FILE_ATTRIBUTE_TIME_ACCESS "," \
    G_FILE_ATTRIBUTE_ID_FILE

static void
visit_directory (GFile            *dir,
                 SearchThreadData *data)
{
    GFileEnumerator *enumerator;
    GFileInfo *info;
    GFile *child;
    const char *mime_type, *display_name;
    gdouble match;
    gboolean is_hidden, found;
    GList *l;
    const char *id;
    gboolean visited;
    guint64 atime;
    guint64 mtime;
    GPtrArray *date_range;
    GDateTime *initial_date;
    GDateTime *end_date;
    mautilusTagManager *tag_manager;
    gchar *uri;

    enumerator = g_file_enumerate_children (dir,
                                            data->mime_types != NULL ?
                                            STD_ATTRIBUTES ","
                                            G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE
                                            :
                                            STD_ATTRIBUTES
                                            ,
                                            G_FILE_QUERY_INFO_NOFOLLOW_SYMLINKS,
                                            data->cancellable, NULL);

    if (enumerator == NULL)
    {
        return;
    }

    while ((info = g_file_enumerator_next_file (enumerator, data->cancellable, NULL)) != NULL)
    {
        display_name = g_file_info_get_display_name (info);
        if (display_name == NULL)
        {
            goto next;
        }

        is_hidden = g_file_info_get_is_hidden (info) || g_file_info_get_is_backup (info);
        if (is_hidden && !mautilus_query_get_show_hidden_files (data->query))
        {
            goto next;
        }

        child = g_file_get_child (dir, g_file_info_get_name (info));
        match = mautilus_query_matches_string (data->query, display_name);
        found = (match > -1);

        if (found && data->mime_types)
        {
            mime_type = g_file_info_get_content_type (info);
            found = FALSE;

            for (l = data->mime_types; mime_type != NULL && l != NULL; l = l->next)
            {
                if (g_content_type_is_a (mime_type, l->data))
                {
                    found = TRUE;
                    break;
                }
            }
        }

        mtime = g_file_info_get_attribute_uint64 (info, "time::modified");
        atime = g_file_info_get_attribute_uint64 (info, "time::access");

        date_range = mautilus_query_get_date_range (data->query);
        if (found && date_range != NULL)
        {
            mautilusQuerySearchType type;
            guint64 current_file_time;

            initial_date = g_ptr_array_index (date_range, 0);
            end_date = g_ptr_array_index (date_range, 1);
            type = mautilus_query_get_search_type (data->query);

            if (type == MAUTILUS_QUERY_SEARCH_TYPE_LAST_ACCESS)
            {
                current_file_time = atime;
            }
            else
            {
                current_file_time = mtime;
            }
            found = mautilus_file_date_in_between (current_file_time,
                                                   initial_date,
                                                   end_date);
            g_ptr_array_unref (date_range);
        }

        if (mautilus_query_get_search_favorite (data->query))
        {
            tag_manager = mautilus_tag_manager_get ();

            uri = g_file_get_uri (child);

            if (!mautilus_tag_manager_file_is_favorite (tag_manager, uri))
            {
                found = FALSE;
            }

            g_free (uri);
        }

        if (found)
        {
            mautilusSearchHit *hit;
            GDateTime *date;

            uri = g_file_get_uri (child);
            hit = mautilus_search_hit_new (uri);
            g_free (uri);
            mautilus_search_hit_set_fts_rank (hit, match);
            date = g_date_time_new_from_unix_local (mtime);
            mautilus_search_hit_set_modification_time (hit, date);
            g_date_time_unref (date);

            data->hits = g_list_prepend (data->hits, hit);
        }

        data->n_processed_files++;
        if (data->n_processed_files > BATCH_SIZE)
        {
            send_batch (data);
        }

        if (data->engine->recursive && g_file_info_get_file_type (info) == G_FILE_TYPE_DIRECTORY)
        {
            id = g_file_info_get_attribute_string (info, G_FILE_ATTRIBUTE_ID_FILE);
            visited = FALSE;
            if (id)
            {
                if (g_hash_table_lookup_extended (data->visited,
                                                  id, NULL, NULL))
                {
                    visited = TRUE;
                }
                else
                {
                    g_hash_table_insert (data->visited, g_strdup (id), NULL);
                }
            }

            if (!visited)
            {
                g_queue_push_tail (data->directories, g_object_ref (child));
            }
        }

        g_object_unref (child);
next:
        g_object_unref (info);
    }

    g_object_unref (enumerator);
}


static gpointer
search_thread_func (gpointer user_data)
{
    SearchThreadData *data;
    GFile *dir;
    GFileInfo *info;
    const char *id;

    data = user_data;

    /* Insert id for toplevel directory into visited */
    dir = g_queue_peek_head (data->directories);
    info = g_file_query_info (dir, G_FILE_ATTRIBUTE_ID_FILE, 0, data->cancellable, NULL);
    if (info)
    {
        id = g_file_info_get_attribute_string (info, G_FILE_ATTRIBUTE_ID_FILE);
        if (id)
        {
            g_hash_table_insert (data->visited, g_strdup (id), NULL);
        }
        g_object_unref (info);
    }

    while (!g_cancellable_is_cancelled (data->cancellable) &&
           (dir = g_queue_pop_head (data->directories)) != NULL)
    {
        visit_directory (dir, data);
        g_object_unref (dir);
    }

    if (!g_cancellable_is_cancelled (data->cancellable))
    {
        send_batch (data);
    }

    g_idle_add (search_thread_done_idle, data);

    return NULL;
}

static void
mautilus_search_engine_simple_start (mautilusSearchProvider *provider)
{
    mautilusSearchEngineSimple *simple;
    SearchThreadData *data;
    GThread *thread;

    simple = MAUTILUS_SEARCH_ENGINE_SIMPLE (provider);

    if (simple->active_search != NULL)
    {
        return;
    }

    DEBUG ("Simple engine start");

    data = search_thread_data_new (simple, simple->query);

    thread = g_thread_new ("mautilus-search-simple", search_thread_func, data);
    simple->active_search = data;

    g_object_notify (G_OBJECT (provider), "running");

    g_thread_unref (thread);
}

static void
mautilus_search_engine_simple_stop (mautilusSearchProvider *provider)
{
    mautilusSearchEngineSimple *simple = MAUTILUS_SEARCH_ENGINE_SIMPLE (provider);

    if (simple->active_search != NULL)
    {
        DEBUG ("Simple engine stop");
        g_cancellable_cancel (simple->active_search->cancellable);
    }
}

static void
mautilus_search_engine_simple_set_query (mautilusSearchProvider *provider,
                                         mautilusQuery          *query)
{
    mautilusSearchEngineSimple *simple = MAUTILUS_SEARCH_ENGINE_SIMPLE (provider);

    g_object_ref (query);
    g_clear_object (&simple->query);
    simple->query = query;
}

static gboolean
mautilus_search_engine_simple_is_running (mautilusSearchProvider *provider)
{
    mautilusSearchEngineSimple *simple;

    simple = MAUTILUS_SEARCH_ENGINE_SIMPLE (provider);

    return simple->active_search != NULL;
}

static void
mautilus_search_engine_simple_set_property (GObject      *object,
                                            guint         arg_id,
                                            const GValue *value,
                                            GParamSpec   *pspec)
{
    mautilusSearchEngineSimple *engine = MAUTILUS_SEARCH_ENGINE_SIMPLE (object);

    switch (arg_id)
    {
        case PROP_RECURSIVE:
        {
            engine->recursive = g_value_get_boolean (value);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, arg_id, pspec);
        }
        break;
    }
}

static void
mautilus_search_engine_simple_get_property (GObject    *object,
                                            guint       arg_id,
                                            GValue     *value,
                                            GParamSpec *pspec)
{
    mautilusSearchEngineSimple *engine = MAUTILUS_SEARCH_ENGINE_SIMPLE (object);

    switch (arg_id)
    {
        case PROP_RUNNING:
        {
            g_value_set_boolean (value, mautilus_search_engine_simple_is_running (MAUTILUS_SEARCH_PROVIDER (engine)));
        }
        break;

        case PROP_RECURSIVE:
        {
            g_value_set_boolean (value, engine->recursive);
        }
        break;
    }
}

static void
mautilus_search_provider_init (mautilusSearchProviderInterface *iface)
{
    iface->set_query = mautilus_search_engine_simple_set_query;
    iface->start = mautilus_search_engine_simple_start;
    iface->stop = mautilus_search_engine_simple_stop;
    iface->is_running = mautilus_search_engine_simple_is_running;
}

static void
mautilus_search_engine_simple_class_init (mautilusSearchEngineSimpleClass *class)
{
    GObjectClass *gobject_class;

    gobject_class = G_OBJECT_CLASS (class);
    gobject_class->finalize = finalize;
    gobject_class->get_property = mautilus_search_engine_simple_get_property;
    gobject_class->set_property = mautilus_search_engine_simple_set_property;

    /**
     * mautilusSearchEngineSimple::recursive:
     *
     * Whether the search is recursive or not.
     */
    g_object_class_install_property (gobject_class,
                                     PROP_RECURSIVE,
                                     g_param_spec_boolean ("recursive",
                                                           "recursive",
                                                           "recursive",
                                                           FALSE,
                                                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_STATIC_STRINGS));

    /**
     * mautilusSearchEngine::running:
     *
     * Whether the search engine is running a search.
     */
    g_object_class_override_property (gobject_class, PROP_RUNNING, "running");
}

static void
mautilus_search_engine_simple_init (mautilusSearchEngineSimple *engine)
{
    engine->query = NULL;
    engine->active_search = NULL;
}

mautilusSearchEngineSimple *
mautilus_search_engine_simple_new (void)
{
    mautilusSearchEngineSimple *engine;

    engine = g_object_new (MAUTILUS_TYPE_SEARCH_ENGINE_SIMPLE, NULL);

    return engine;
}
