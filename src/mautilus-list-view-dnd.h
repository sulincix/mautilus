/* mautilus-list-view-dnd.h
 *
 * Copyright (C) 2015 Carlos Soriano <csoriano@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MAUTILUS_LIST_VIEW_DND_H
#define MAUTILUS_LIST_VIEW_DND_H

#include <gdk/gdk.h>

#include "mautilus-list-view.h"

#include "mautilus-dnd.h"

void mautilus_list_view_dnd_init (mautilusListView *list_view);
gboolean mautilus_list_view_dnd_drag_begin (mautilusListView *list_view,
                                            GdkEventMotion   *event);
mautilusDragInfo *
mautilus_list_view_dnd_get_drag_source_data (mautilusListView *list_view,
                                             GdkDragContext   *context);

#endif /* MAUTILUS_LIST_VIEW_DND_H */
