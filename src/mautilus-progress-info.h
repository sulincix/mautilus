/*
   mautilus-progress-info.h: file operation progress info.
 
   Copyright (C) 2007 Red Hat, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Alexander Larsson <alexl@redhat.com>
*/

#ifndef MAUTILUS_PROGRESS_INFO_H
#define MAUTILUS_PROGRESS_INFO_H

#include <glib-object.h>
#include <gio/gio.h>

#define MAUTILUS_TYPE_PROGRESS_INFO (mautilus_progress_info_get_type ())

G_DECLARE_FINAL_TYPE (mautilusProgressInfo, mautilus_progress_info, MAUTILUS, PROGRESS_INFO, GObject)

/* Signals:
   "changed" - status or details changed
   "progress-changed" - the percentage progress changed (or we pulsed if in activity_mode
   "started" - emited on job start
   "finished" - emitted when job is done
   
   All signals are emitted from idles in main loop.
   All methods are threadsafe.
 */

mautilusProgressInfo *mautilus_progress_info_new (void);

GList *       mautilus_get_all_progress_info (void);

char *        mautilus_progress_info_get_status      (mautilusProgressInfo *info);
char *        mautilus_progress_info_get_details     (mautilusProgressInfo *info);
double        mautilus_progress_info_get_progress    (mautilusProgressInfo *info);
GCancellable *mautilus_progress_info_get_cancellable (mautilusProgressInfo *info);
void          mautilus_progress_info_cancel          (mautilusProgressInfo *info);
gboolean      mautilus_progress_info_get_is_started  (mautilusProgressInfo *info);
gboolean      mautilus_progress_info_get_is_finished (mautilusProgressInfo *info);
gboolean      mautilus_progress_info_get_is_paused   (mautilusProgressInfo *info);
gboolean      mautilus_progress_info_get_is_cancelled (mautilusProgressInfo *info);

void          mautilus_progress_info_start           (mautilusProgressInfo *info);
void          mautilus_progress_info_finish          (mautilusProgressInfo *info);
void          mautilus_progress_info_pause           (mautilusProgressInfo *info);
void          mautilus_progress_info_resume          (mautilusProgressInfo *info);
void          mautilus_progress_info_set_status      (mautilusProgressInfo *info,
						      const char           *status);
void          mautilus_progress_info_take_status     (mautilusProgressInfo *info,
						      char                 *status);
void          mautilus_progress_info_set_details     (mautilusProgressInfo *info,
						      const char           *details);
void          mautilus_progress_info_take_details    (mautilusProgressInfo *info,
						      char                 *details);
void          mautilus_progress_info_set_progress    (mautilusProgressInfo *info,
						      double                current,
						      double                total);
void          mautilus_progress_info_pulse_progress  (mautilusProgressInfo *info);

void          mautilus_progress_info_set_remaining_time (mautilusProgressInfo *info,
                                                         gdouble               time);
gdouble       mautilus_progress_info_get_remaining_time (mautilusProgressInfo *info);
void          mautilus_progress_info_set_elapsed_time (mautilusProgressInfo *info,
                                                       gdouble               time);
gdouble       mautilus_progress_info_get_elapsed_time (mautilusProgressInfo *info);
gdouble       mautilus_progress_info_get_total_elapsed_time (mautilusProgressInfo *info);

void mautilus_progress_info_set_destination (mautilusProgressInfo *info,
                                             GFile                *file);
GFile *mautilus_progress_info_get_destination (mautilusProgressInfo *info);



#endif /* MAUTILUS_PROGRESS_INFO_H */
