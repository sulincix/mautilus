
/* mautilus - Floating status bar.
 *
 * Copyright (C) 2011 Red Hat Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Cosimo Cecchi <cosimoc@redhat.com>
 *
 */

#ifndef __MAUTILUS_FLOATING_BAR_H__
#define __MAUTILUS_FLOATING_BAR_H__

#include <gtk/gtk.h>

#define MAUTILUS_FLOATING_BAR_ACTION_ID_STOP 1

#define MAUTILUS_TYPE_FLOATING_BAR mautilus_floating_bar_get_type()
G_DECLARE_FINAL_TYPE (mautilusFloatingBar, mautilus_floating_bar, MAUTILUS, FLOATING_BAR, GtkBox)

GtkWidget * mautilus_floating_bar_new              (const gchar *primary_label,
						    const gchar *details_label,
						    gboolean show_spinner);

void       mautilus_floating_bar_set_primary_label (mautilusFloatingBar *self,
						    const gchar *label);
void       mautilus_floating_bar_set_details_label (mautilusFloatingBar *self,
						    const gchar *label);
void        mautilus_floating_bar_set_labels        (mautilusFloatingBar *self,
						     const gchar *primary,
						     const gchar *detail);
void        mautilus_floating_bar_set_show_spinner (mautilusFloatingBar *self,
						    gboolean show_spinner);

void        mautilus_floating_bar_add_action       (mautilusFloatingBar *self,
						    const gchar *icon_name,
						    gint action_id);
void        mautilus_floating_bar_cleanup_actions  (mautilusFloatingBar *self);

void        mautilus_floating_bar_remove_hover_timeout (mautilusFloatingBar *self);

#endif /* __MAUTILUS_FLOATING_BAR_H__ */

