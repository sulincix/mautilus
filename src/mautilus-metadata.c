/* mautilus-metadata.c - metadata utils
 *
 * Copyright (C) 2009 Red Hatl, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include "mautilus-metadata.h"
#include <glib.h>

static char *used_metadata_names[] =
{
    MAUTILUS_METADATA_KEY_LOCATION_BACKGROUND_COLOR,
    MAUTILUS_METADATA_KEY_LOCATION_BACKGROUND_IMAGE,
    MAUTILUS_METADATA_KEY_ICON_VIEW_AUTO_LAYOUT,
    MAUTILUS_METADATA_KEY_ICON_VIEW_SORT_BY,
    MAUTILUS_METADATA_KEY_ICON_VIEW_SORT_REVERSED,
    MAUTILUS_METADATA_KEY_ICON_VIEW_KEEP_ALIGNED,
    MAUTILUS_METADATA_KEY_ICON_VIEW_LAYOUT_TIMESTAMP,
    MAUTILUS_METADATA_KEY_DESKTOP_ICON_SIZE,
    MAUTILUS_METADATA_KEY_LIST_VIEW_SORT_COLUMN,
    MAUTILUS_METADATA_KEY_LIST_VIEW_SORT_REVERSED,
    MAUTILUS_METADATA_KEY_LIST_VIEW_VISIBLE_COLUMNS,
    MAUTILUS_METADATA_KEY_LIST_VIEW_COLUMN_ORDER,
    MAUTILUS_METADATA_KEY_WINDOW_GEOMETRY,
    MAUTILUS_METADATA_KEY_WINDOW_SCROLL_POSITION,
    MAUTILUS_METADATA_KEY_WINDOW_SHOW_HIDDEN_FILES,
    MAUTILUS_METADATA_KEY_WINDOW_MAXIMIZED,
    MAUTILUS_METADATA_KEY_WINDOW_STICKY,
    MAUTILUS_METADATA_KEY_WINDOW_KEEP_ABOVE,
    MAUTILUS_METADATA_KEY_SIDEBAR_BACKGROUND_COLOR,
    MAUTILUS_METADATA_KEY_SIDEBAR_BACKGROUND_IMAGE,
    MAUTILUS_METADATA_KEY_SIDEBAR_BUTTONS,
    MAUTILUS_METADATA_KEY_ANNOTATION,
    MAUTILUS_METADATA_KEY_ICON_POSITION,
    MAUTILUS_METADATA_KEY_ICON_POSITION_TIMESTAMP,
    MAUTILUS_METADATA_KEY_ICON_SCALE,
    MAUTILUS_METADATA_KEY_CUSTOM_ICON,
    MAUTILUS_METADATA_KEY_CUSTOM_ICON_NAME,
    MAUTILUS_METADATA_KEY_SCREEN,
    MAUTILUS_METADATA_KEY_EMBLEMS,
    MAUTILUS_METADATA_KEY_DESKTOP_FILE_TRUSTED,
    NULL
};

guint
mautilus_metadata_get_id (const char *metadata)
{
    static GHashTable *hash;
    int i;

    if (hash == NULL)
    {
        hash = g_hash_table_new (g_str_hash, g_str_equal);
        for (i = 0; used_metadata_names[i] != NULL; i++)
        {
            g_hash_table_insert (hash,
                                 used_metadata_names[i],
                                 GINT_TO_POINTER (i + 1));
        }
    }

    return GPOINTER_TO_INT (g_hash_table_lookup (hash, metadata));
}
