/*
 *  Copyright (C) 2001 Maciej Stachowiak
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Maciej Stachowiak <mjs@noisehavoc.org>
 */

#include <config.h>
#include "mautilus-file-queue.h"

#include <glib.h>

struct mautilusFileQueue
{
    GList *head;
    GList *tail;
    GHashTable *item_to_link_map;
};

mautilusFileQueue *
mautilus_file_queue_new (void)
{
    mautilusFileQueue *queue;

    queue = g_new0 (mautilusFileQueue, 1);
    queue->item_to_link_map = g_hash_table_new (g_direct_hash, g_direct_equal);

    return queue;
}

void
mautilus_file_queue_destroy (mautilusFileQueue *queue)
{
    g_hash_table_destroy (queue->item_to_link_map);
    mautilus_file_list_free (queue->head);
    g_free (queue);
}

void
mautilus_file_queue_enqueue (mautilusFileQueue *queue,
                             mautilusFile      *file)
{
    if (g_hash_table_lookup (queue->item_to_link_map, file) != NULL)
    {
        /* It's already on the queue. */
        return;
    }

    if (queue->tail == NULL)
    {
        queue->head = g_list_append (NULL, file);
        queue->tail = queue->head;
    }
    else
    {
        queue->tail = g_list_append (queue->tail, file);
        queue->tail = queue->tail->next;
    }

    mautilus_file_ref (file);
    g_hash_table_insert (queue->item_to_link_map, file, queue->tail);
}

mautilusFile *
mautilus_file_queue_dequeue (mautilusFileQueue *queue)
{
    mautilusFile *file;

    file = mautilus_file_queue_head (queue);
    mautilus_file_queue_remove (queue, file);

    return file;
}


void
mautilus_file_queue_remove (mautilusFileQueue *queue,
                            mautilusFile      *file)
{
    GList *link;

    link = g_hash_table_lookup (queue->item_to_link_map, file);

    if (link == NULL)
    {
        /* It's not on the queue */
        return;
    }

    if (link == queue->tail)
    {
        /* Need to special-case removing the tail. */
        queue->tail = queue->tail->prev;
    }

    queue->head = g_list_remove_link (queue->head, link);
    g_list_free (link);
    g_hash_table_remove (queue->item_to_link_map, file);

    mautilus_file_unref (file);
}

mautilusFile *
mautilus_file_queue_head (mautilusFileQueue *queue)
{
    if (queue->head == NULL)
    {
        return NULL;
    }

    return MAUTILUS_FILE (queue->head->data);
}

gboolean
mautilus_file_queue_is_empty (mautilusFileQueue *queue)
{
    return (queue->head == NULL);
}
