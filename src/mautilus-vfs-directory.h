/*
   mautilus-vfs-directory.h: Subclass of mautilusDirectory to implement the
   the case of a VFS directory.
 
   Copyright (C) 1999, 2000 Eazel, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Darin Adler <darin@bentspoon.com>
*/

#ifndef MAUTILUS_VFS_DIRECTORY_H
#define MAUTILUS_VFS_DIRECTORY_H

#include "mautilus-directory.h"

#define MAUTILUS_TYPE_VFS_DIRECTORY mautilus_vfs_directory_get_type()
#define MAUTILUS_VFS_DIRECTORY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_VFS_DIRECTORY, mautilusVFSDirectory))
#define MAUTILUS_VFS_DIRECTORY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_VFS_DIRECTORY, mautilusVFSDirectoryClass))
#define MAUTILUS_IS_VFS_DIRECTORY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_VFS_DIRECTORY))
#define MAUTILUS_IS_VFS_DIRECTORY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_VFS_DIRECTORY))
#define MAUTILUS_VFS_DIRECTORY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_VFS_DIRECTORY, mautilusVFSDirectoryClass))

typedef struct mautilusVFSDirectoryDetails mautilusVFSDirectoryDetails;

typedef struct {
	mautilusDirectory parent_slot;
} mautilusVFSDirectory;

typedef struct {
	mautilusDirectoryClass parent_slot;
} mautilusVFSDirectoryClass;

GType   mautilus_vfs_directory_get_type (void);

#endif /* MAUTILUS_VFS_DIRECTORY_H */
