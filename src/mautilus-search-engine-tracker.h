/*
 * Copyright (C) 2005 Mr Jamie McCracken
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Author: Jamie McCracken (jamiemcc@gnome.org)
 *
 */

#ifndef MAUTILUS_SEARCH_ENGINE_TRACKER_H
#define MAUTILUS_SEARCH_ENGINE_TRACKER_H

#include "mautilus-search-engine.h"

#define MAUTILUS_TYPE_SEARCH_ENGINE_TRACKER (mautilus_search_engine_tracker_get_type ())
G_DECLARE_FINAL_TYPE (mautilusSearchEngineTracker, mautilus_search_engine_tracker, MAUTILUS, SEARCH_ENGINE_TRACKER, GObject)

mautilusSearchEngineTracker* mautilus_search_engine_tracker_new (void);

#endif /* MAUTILUS_SEARCH_ENGINE_TRACKER_H */
