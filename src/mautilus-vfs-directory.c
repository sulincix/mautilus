/*
 *  mautilus-vfs-directory.c: Subclass of mautilusDirectory to help implement the
 *  virtual trash directory.
 *
 *  Copyright (C) 1999, 2000 Eazel, Inc.
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Darin Adler <darin@bentspoon.com>
 */

#include <config.h>
#include "mautilus-vfs-directory.h"

#include "mautilus-directory-private.h"
#include "mautilus-file-private.h"

G_DEFINE_TYPE (mautilusVFSDirectory, mautilus_vfs_directory, MAUTILUS_TYPE_DIRECTORY);

static void
mautilus_vfs_directory_init (mautilusVFSDirectory *directory)
{
}

static void
vfs_call_when_ready (mautilusDirectory         *directory,
                     mautilusFileAttributes     file_attributes,
                     gboolean                   wait_for_file_list,
                     mautilusDirectoryCallback  callback,
                     gpointer                   callback_data)
{
    g_assert (MAUTILUS_IS_VFS_DIRECTORY (directory));

    mautilus_directory_call_when_ready_internal
        (directory,
        NULL,
        file_attributes,
        wait_for_file_list,
        callback,
        NULL,
        callback_data);
}

static void
vfs_cancel_callback (mautilusDirectory         *directory,
                     mautilusDirectoryCallback  callback,
                     gpointer                   callback_data)
{
    g_assert (MAUTILUS_IS_VFS_DIRECTORY (directory));

    mautilus_directory_cancel_callback_internal
        (directory,
        NULL,
        callback,
        NULL,
        callback_data);
}

static void
vfs_file_monitor_add (mautilusDirectory         *directory,
                      gconstpointer              client,
                      gboolean                   monitor_hidden_files,
                      mautilusFileAttributes     file_attributes,
                      mautilusDirectoryCallback  callback,
                      gpointer                   callback_data)
{
    g_assert (MAUTILUS_IS_VFS_DIRECTORY (directory));
    g_assert (client != NULL);

    mautilus_directory_monitor_add_internal
        (directory, NULL,
        client,
        monitor_hidden_files,
        file_attributes,
        callback, callback_data);
}

static void
vfs_file_monitor_remove (mautilusDirectory *directory,
                         gconstpointer      client)
{
    g_assert (MAUTILUS_IS_VFS_DIRECTORY (directory));
    g_assert (client != NULL);

    mautilus_directory_monitor_remove_internal (directory, NULL, client);
}

static void
vfs_force_reload (mautilusDirectory *directory)
{
    mautilusFileAttributes all_attributes;

    g_assert (MAUTILUS_IS_DIRECTORY (directory));

    all_attributes = mautilus_file_get_all_attributes ();
    mautilus_directory_force_reload_internal (directory,
                                              all_attributes);
}

static void
mautilus_vfs_directory_class_init (mautilusVFSDirectoryClass *klass)
{
    mautilusDirectoryClass *directory_class = MAUTILUS_DIRECTORY_CLASS (klass);

    directory_class->call_when_ready = vfs_call_when_ready;
    directory_class->cancel_callback = vfs_cancel_callback;
    directory_class->file_monitor_add = vfs_file_monitor_add;
    directory_class->file_monitor_remove = vfs_file_monitor_remove;
    directory_class->force_reload = vfs_force_reload;
}
