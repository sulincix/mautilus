
/* 
 * Copyright (C) 2004 Red Hat, Inc
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Alexander Larsson <alexl@redhat.com>
 */

#ifndef MAUTILUS_IMAGE_PROPERTIES_PAGE_H
#define MAUTILUS_IMAGE_PROPERTIES_PAGE_H

#include <gtk/gtk.h>

#define MAUTILUS_TYPE_IMAGE_PROPERTIES_PAGE mautilus_image_properties_page_get_type()
G_DECLARE_FINAL_TYPE (mautilusImagePropertiesPage, mautilus_image_properties_page, MAUTILUS, IMAGE_PROPERTIES_PAGE, GtkBox)


void  mautilus_image_properties_page_register (void);

#endif /* MAUTILUS_IMAGE_PROPERTIES_PAGE_H */
