/*
   mautilus-window-slot.h: mautilus window slot
 
   Copyright (C) 2008 Free Software Foundation, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Christian Neumair <cneumair@gnome.org>
*/

#ifndef MAUTILUS_WINDOW_SLOT_H
#define MAUTILUS_WINDOW_SLOT_H

#include <gdk/gdk.h>
#include <gio/gio.h>
#include <gtk/gtk.h>

typedef enum {
	MAUTILUS_LOCATION_CHANGE_STANDARD,
	MAUTILUS_LOCATION_CHANGE_BACK,
	MAUTILUS_LOCATION_CHANGE_FORWARD,
	MAUTILUS_LOCATION_CHANGE_RELOAD
} mautilusLocationChangeType;

#define MAUTILUS_TYPE_WINDOW_SLOT (mautilus_window_slot_get_type ())
G_DECLARE_DERIVABLE_TYPE (mautilusWindowSlot, mautilus_window_slot, MAUTILUS, WINDOW_SLOT, GtkBox)

#include "mautilus-query-editor.h"
#include "mautilus-files-view.h"
#include "mautilus-view.h"
#include "mautilus-window.h"
#include "mautilus-toolbar-menu-sections.h"

typedef struct
{
    mautilusFile *file;
    gint view_before_search;
    GList *back_list;
    GList *forward_list;
} RestoreTabData;

struct _mautilusWindowSlotClass {
	GtkBoxClass parent_class;

	/* wrapped mautilusWindowInfo signals, for overloading */
	void (* active)   (mautilusWindowSlot *slot);
	void (* inactive) (mautilusWindowSlot *slot);

        /* Use this in case the subclassed slot has some special views differents
         * that the ones supported here. You can return your mautilus view
         * subclass in this function.
         */
        mautilusView*  (* get_view_for_location) (mautilusWindowSlot *slot,
                                                  GFile              *location);
        /* Whether this type of slot handles the location or not. This can be used
         * for the special slots which handle special locations like the desktop
         * or the other locations. */
        gboolean (* handles_location) (mautilusWindowSlot *slot,
                                       GFile              *location);
};

mautilusWindowSlot * mautilus_window_slot_new              (mautilusWindow     *window);

mautilusWindow * mautilus_window_slot_get_window           (mautilusWindowSlot *slot);
void             mautilus_window_slot_set_window           (mautilusWindowSlot *slot,
							    mautilusWindow     *window);

void mautilus_window_slot_open_location_full              (mautilusWindowSlot      *slot,
                                                           GFile                   *location,
                                                           mautilusWindowOpenFlags  flags,
                                                           GList                   *new_selection);

GFile * mautilus_window_slot_get_location		   (mautilusWindowSlot *slot);

mautilusBookmark *mautilus_window_slot_get_bookmark        (mautilusWindowSlot *slot);

GList * mautilus_window_slot_get_back_history              (mautilusWindowSlot *slot);
GList * mautilus_window_slot_get_forward_history           (mautilusWindowSlot *slot);

gboolean mautilus_window_slot_get_allow_stop               (mautilusWindowSlot *slot);
void     mautilus_window_slot_set_allow_stop		   (mautilusWindowSlot *slot,
							    gboolean	        allow_stop);
void     mautilus_window_slot_stop_loading                 (mautilusWindowSlot *slot);

const gchar *mautilus_window_slot_get_title                (mautilusWindowSlot *slot);
void         mautilus_window_slot_update_title		   (mautilusWindowSlot *slot);

gboolean mautilus_window_slot_handle_event       	   (mautilusWindowSlot *slot,
							    GdkEventKey        *event);

void    mautilus_window_slot_queue_reload		   (mautilusWindowSlot *slot);

GIcon*   mautilus_window_slot_get_icon                     (mautilusWindowSlot *slot);

mautilusToolbarMenuSections * mautilus_window_slot_get_toolbar_menu_sections (mautilusWindowSlot *slot);

gboolean mautilus_window_slot_get_active                   (mautilusWindowSlot *slot);

void     mautilus_window_slot_set_active                   (mautilusWindowSlot *slot,
                                                            gboolean            active);
gboolean mautilus_window_slot_get_loading                  (mautilusWindowSlot *slot);

void     mautilus_window_slot_search                       (mautilusWindowSlot *slot,
                                                            const gchar        *text);

gboolean mautilus_window_slot_handles_location (mautilusWindowSlot *self,
                                                GFile              *location);

void mautilus_window_slot_restore_from_data (mautilusWindowSlot *self,
                                             RestoreTabData     *data);

RestoreTabData* mautilus_window_slot_get_restore_tab_data (mautilusWindowSlot *self);

/* Only used by slot-dnd */
mautilusView*  mautilus_window_slot_get_current_view       (mautilusWindowSlot *slot);

#endif /* MAUTILUS_WINDOW_SLOT_H */
