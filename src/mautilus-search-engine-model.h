/*
 * Copyright (C) 2005 Red Hat, Inc
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Author: Alexander Larsson <alexl@redhat.com>
 *
 */

#ifndef MAUTILUS_SEARCH_ENGINE_MODEL_H
#define MAUTILUS_SEARCH_ENGINE_MODEL_H

#include "mautilus-directory.h"

#define MAUTILUS_TYPE_SEARCH_ENGINE_MODEL (mautilus_search_engine_model_get_type ())
G_DECLARE_FINAL_TYPE (mautilusSearchEngineModel, mautilus_search_engine_model, MAUTILUS, SEARCH_ENGINE_MODEL, GObject)

mautilusSearchEngineModel* mautilus_search_engine_model_new       (void);
void                       mautilus_search_engine_model_set_model (mautilusSearchEngineModel *model,
								   mautilusDirectory         *directory);
mautilusDirectory *        mautilus_search_engine_model_get_model (mautilusSearchEngineModel *model);

#endif /* MAUTILUS_SEARCH_ENGINE_MODEL_H */
