/*
   mautilus-search-directory-file.h: Subclass of mautilusFile to implement the
   the case of the search directory
 
   Copyright (C) 2003 Red Hat, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Alexander Larsson <alexl@redhat.com>
*/

#ifndef MAUTILUS_SEARCH_DIRECTORY_FILE_H
#define MAUTILUS_SEARCH_DIRECTORY_FILE_H

#include "mautilus-file.h"

#define MAUTILUS_TYPE_SEARCH_DIRECTORY_FILE mautilus_search_directory_file_get_type()
#define MAUTILUS_SEARCH_DIRECTORY_FILE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_SEARCH_DIRECTORY_FILE, mautilusSearchDirectoryFile))
#define MAUTILUS_SEARCH_DIRECTORY_FILE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_SEARCH_DIRECTORY_FILE, mautilusSearchDirectoryFileClass))
#define MAUTILUS_IS_SEARCH_DIRECTORY_FILE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_SEARCH_DIRECTORY_FILE))
#define MAUTILUS_IS_SEARCH_DIRECTORY_FILE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_SEARCH_DIRECTORY_FILE))
#define MAUTILUS_SEARCH_DIRECTORY_FILE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_SEARCH_DIRECTORY_FILE, mautilusSearchDirectoryFileClass))

typedef struct mautilusSearchDirectoryFileDetails mautilusSearchDirectoryFileDetails;

typedef struct {
	mautilusFile parent_slot;
	mautilusSearchDirectoryFileDetails *details;
} mautilusSearchDirectoryFile;

typedef struct {
	mautilusFileClass parent_slot;
} mautilusSearchDirectoryFileClass;

GType   mautilus_search_directory_file_get_type (void);
void    mautilus_search_directory_file_update_display_name (mautilusSearchDirectoryFile *search_file);

#endif /* MAUTILUS_SEARCH_DIRECTORY_FILE_H */
