/* mautilusgtkplacesview.h
 *
 * Copyright (C) 2015 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAUTILUS_GTK_PLACES_VIEW_H
#define MAUTILUS_GTK_PLACES_VIEW_H

#if !defined (__GTK_H_INSIDE__) && !defined (GTK_COMPILATION)
#endif


G_BEGIN_DECLS

#define MAUTILUS_TYPE_GTK_PLACES_VIEW        (mautilus_gtk_places_view_get_type ())
#define MAUTILUS_GTK_PLACES_VIEW(obj)        (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_GTK_PLACES_VIEW, mautilusGtkPlacesView))
#define MAUTILUS_GTK_PLACES_VIEW_CLASS(klass)(G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_GTK_PLACES_VIEW, mautilusGtkPlacesViewClass))
#define MAUTILUS_IS_GTK_PLACES_VIEW(obj)     (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_GTK_PLACES_VIEW))
#define MAUTILUS_IS_GTK_PLACES_VIEW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_GTK_PLACES_VIEW))
#define MAUTILUS_GTK_PLACES_VIEW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_GTK_PLACES_VIEW, mautilusGtkPlacesViewClass))

typedef struct _mautilusGtkPlacesView mautilusGtkPlacesView;
typedef struct _mautilusGtkPlacesViewClass mautilusGtkPlacesViewClass;
typedef struct _mautilusGtkPlacesViewPrivate mautilusGtkPlacesViewPrivate;

struct _mautilusGtkPlacesViewClass
{
  GtkBoxClass parent_class;

  void     (* open_location)        (mautilusGtkPlacesView          *view,
                                     GFile                  *location,
                                     GtkPlacesOpenFlags  open_flags);

  void    (* show_error_message)     (GtkPlacesSidebar      *sidebar,
                                      const gchar           *primary,
                                      const gchar           *secondary);

  /*< private >*/

  /* Padding for future expansion */
  gpointer reserved[10];
};

struct _mautilusGtkPlacesView
{
  GtkBox parent_instance;
};

GType              mautilus_gtk_places_view_get_type                      (void) G_GNUC_CONST;

GtkPlacesOpenFlags mautilus_gtk_places_view_get_open_flags                (mautilusGtkPlacesView      *view);
void               mautilus_gtk_places_view_set_open_flags                (mautilusGtkPlacesView      *view,
                                                                  GtkPlacesOpenFlags  flags);

const gchar*       mautilus_gtk_places_view_get_search_query              (mautilusGtkPlacesView      *view);
void               mautilus_gtk_places_view_set_search_query              (mautilusGtkPlacesView      *view,
                                                                  const gchar        *query_text);

gboolean           mautilus_gtk_places_view_get_local_only                (mautilusGtkPlacesView         *view);

void               mautilus_gtk_places_view_set_local_only                (mautilusGtkPlacesView         *view,
                                                                  gboolean               local_only);

gboolean           mautilus_gtk_places_view_get_loading                   (mautilusGtkPlacesView         *view);

GtkWidget *        mautilus_gtk_places_view_new                           (void);

G_END_DECLS

#endif /* MAUTILUS_GTK_PLACES_VIEW_H */
