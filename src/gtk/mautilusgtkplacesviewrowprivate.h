/* mautilusgtkplacesviewrow.h
 *
 * Copyright (C) 2015 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAUTILUS_GTK_PLACES_VIEW_ROW_H
#define MAUTILUS_GTK_PLACES_VIEW_ROW_H

#if !defined (__GTK_H_INSIDE__) && !defined (GTK_COMPILATION)
#endif


G_BEGIN_DECLS

#define MAUTILUS_TYPE_GTK_PLACES_VIEW_ROW (mautilus_gtk_places_view_row_get_type())

 G_DECLARE_FINAL_TYPE (mautilusGtkPlacesViewRow, mautilus_gtk_places_view_row, MAUTILUS, GTK_PLACES_VIEW_ROW, GtkListBoxRow)

GtkWidget*         mautilus_gtk_places_view_row_new                       (GVolume            *volume,
                                                                  GMount             *mount);

GtkWidget*         mautilus_gtk_places_view_row_get_eject_button          (mautilusGtkPlacesViewRow   *row);

GtkWidget*         mautilus_gtk_places_view_row_get_event_box             (mautilusGtkPlacesViewRow   *row);

GMount*            mautilus_gtk_places_view_row_get_mount                 (mautilusGtkPlacesViewRow   *row);

GVolume*           mautilus_gtk_places_view_row_get_volume                (mautilusGtkPlacesViewRow   *row);

GFile*             mautilus_gtk_places_view_row_get_file                  (mautilusGtkPlacesViewRow   *row);

void               mautilus_gtk_places_view_row_set_busy                  (mautilusGtkPlacesViewRow   *row,
                                                                  gboolean            is_busy);

gboolean           mautilus_gtk_places_view_row_get_is_network            (mautilusGtkPlacesViewRow   *row);

void               mautilus_gtk_places_view_row_set_is_network            (mautilusGtkPlacesViewRow   *row,
                                                                  gboolean            is_network);

void               mautilus_gtk_places_view_row_set_path_size_group       (mautilusGtkPlacesViewRow   *row,
                                                                  GtkSizeGroup       *group);

void               mautilus_gtk_places_view_row_set_space_size_group      (mautilusGtkPlacesViewRow   *row,
                                                                  GtkSizeGroup       *group);

G_END_DECLS

#endif /* MAUTILUS_GTK_PLACES_VIEW_ROW_H */
