#ifndef MAUTILUS_VIEW_ICON_ITEM_UI_H
#define MAUTILUS_VIEW_ICON_ITEM_UI_H

#include <glib.h>
#include <gtk/gtk.h>

#include "mautilus-view-item-model.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_VIEW_ICON_ITEM_UI (mautilus_view_icon_item_ui_get_type())

G_DECLARE_FINAL_TYPE (mautilusViewIconItemUi, mautilus_view_icon_item_ui, MAUTILUS, VIEW_ICON_ITEM_UI, GtkFlowBoxChild)

mautilusViewIconItemUi * mautilus_view_icon_item_ui_new (mautilusViewItemModel *item_model);

mautilusViewItemModel * mautilus_view_icon_item_ui_get_model (mautilusViewIconItemUi *self);

G_END_DECLS

#endif /* MAUTILUS_VIEW_ICON_ITEM_UI_H */

