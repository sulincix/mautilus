/*
 *  mautilus-search-directory-file.c: Subclass of mautilusFile to help implement the
 *  searches
 *
 *  Copyright (C) 2005 Novell, Inc.
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Anders Carlsson <andersca@imendio.com>
 */

#include <config.h>
#include "mautilus-search-directory-file.h"

#include "mautilus-directory-notify.h"
#include "mautilus-directory-private.h"
#include "mautilus-file-attributes.h"
#include "mautilus-file-private.h"
#include "mautilus-file-utilities.h"
#include "mautilus-keyfile-metadata.h"
#include <eel/eel-glib-extensions.h>
#include "mautilus-search-directory.h"
#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <string.h>

struct mautilusSearchDirectoryFileDetails
{
    gchar *metadata_filename;
};

G_DEFINE_TYPE (mautilusSearchDirectoryFile, mautilus_search_directory_file, MAUTILUS_TYPE_FILE);


static void
search_directory_file_monitor_add (mautilusFile           *file,
                                   gconstpointer           client,
                                   mautilusFileAttributes  attributes)
{
    /* No need for monitoring, we always emit changed when files
     *  are added/removed, and no other metadata changes */

    /* Update display name, in case this didn't happen yet */
    mautilus_search_directory_file_update_display_name (MAUTILUS_SEARCH_DIRECTORY_FILE (file));
}

static void
search_directory_file_monitor_remove (mautilusFile  *file,
                                      gconstpointer  client)
{
    /* Do nothing here, we don't have any monitors */
}

static void
search_directory_file_call_when_ready (mautilusFile           *file,
                                       mautilusFileAttributes  file_attributes,
                                       mautilusFileCallback    callback,
                                       gpointer                callback_data)
{
    /* Update display name, in case this didn't happen yet */
    mautilus_search_directory_file_update_display_name (MAUTILUS_SEARCH_DIRECTORY_FILE (file));

    /* All data for directory-as-file is always uptodate */
    (*callback)(file, callback_data);
}

static void
search_directory_file_cancel_call_when_ready (mautilusFile         *file,
                                              mautilusFileCallback  callback,
                                              gpointer              callback_data)
{
    /* Do nothing here, we don't have any pending calls */
}

static gboolean
search_directory_file_check_if_ready (mautilusFile           *file,
                                      mautilusFileAttributes  attributes)
{
    return TRUE;
}

static gboolean
search_directory_file_get_item_count (mautilusFile *file,
                                      guint        *count,
                                      gboolean     *count_unreadable)
{
    GList *file_list;

    if (count)
    {
        mautilusDirectory *directory;

        directory = mautilus_file_get_directory (file);
        file_list = mautilus_directory_get_file_list (directory);

        *count = g_list_length (file_list);

        mautilus_file_list_free (file_list);
    }

    return TRUE;
}

static mautilusRequestStatus
search_directory_file_get_deep_counts (mautilusFile *file,
                                       guint        *directory_count,
                                       guint        *file_count,
                                       guint        *unreadable_directory_count,
                                       goffset      *total_size)
{
    mautilusDirectory *directory;
    mautilusFile *dir_file;
    GList *file_list, *l;
    guint dirs, files;
    GFileType type;

    directory = mautilus_file_get_directory (file);
    file_list = mautilus_directory_get_file_list (directory);

    dirs = files = 0;
    for (l = file_list; l != NULL; l = l->next)
    {
        dir_file = MAUTILUS_FILE (l->data);
        type = mautilus_file_get_file_type (dir_file);
        if (type == G_FILE_TYPE_DIRECTORY)
        {
            dirs++;
        }
        else
        {
            files++;
        }
    }

    if (directory_count != NULL)
    {
        *directory_count = dirs;
    }
    if (file_count != NULL)
    {
        *file_count = files;
    }
    if (unreadable_directory_count != NULL)
    {
        *unreadable_directory_count = 0;
    }
    if (total_size != NULL)
    {
        /* FIXME: Maybe we want to calculate this? */
        *total_size = 0;
    }

    mautilus_file_list_free (file_list);

    return MAUTILUS_REQUEST_DONE;
}

static char *
search_directory_file_get_where_string (mautilusFile *file)
{
    return g_strdup (_("Search"));
}

static void
search_directory_file_set_metadata (mautilusFile *file,
                                    const char   *key,
                                    const char   *value)
{
    mautilusSearchDirectoryFile *search_file;

    search_file = MAUTILUS_SEARCH_DIRECTORY_FILE (file);
    mautilus_keyfile_metadata_set_string (file,
                                          search_file->details->metadata_filename,
                                          "directory", key, value);
}

static void
search_directory_file_set_metadata_as_list (mautilusFile  *file,
                                            const char    *key,
                                            char         **value)
{
    mautilusSearchDirectoryFile *search_file;

    search_file = MAUTILUS_SEARCH_DIRECTORY_FILE (file);
    mautilus_keyfile_metadata_set_stringv (file,
                                           search_file->details->metadata_filename,
                                           "directory", key, (const gchar **) value);
}

void
mautilus_search_directory_file_update_display_name (mautilusSearchDirectoryFile *search_file)
{
    mautilusFile *file;
    mautilusDirectory *directory;
    mautilusSearchDirectory *search_dir;
    mautilusQuery *query;
    char *display_name;
    gboolean changed;


    display_name = NULL;
    file = MAUTILUS_FILE (search_file);
    directory = mautilus_file_get_directory (file);
    if (directory != NULL)
    {
        search_dir = MAUTILUS_SEARCH_DIRECTORY (directory);
        query = mautilus_search_directory_get_query (search_dir);

        if (query != NULL)
        {
            display_name = mautilus_query_to_readable_string (query);
            g_object_unref (query);
        }
    }

    if (display_name == NULL)
    {
        display_name = g_strdup (_("Search"));
    }

    changed = mautilus_file_set_display_name (file, display_name, NULL, TRUE);
    if (changed)
    {
        mautilus_file_emit_changed (file);
    }

    g_free (display_name);
}

static void
mautilus_search_directory_file_init (mautilusSearchDirectoryFile *search_file)
{
    mautilusFile *file;
    gchar *xdg_dir;

    file = MAUTILUS_FILE (search_file);

    search_file->details = G_TYPE_INSTANCE_GET_PRIVATE (search_file,
                                                        MAUTILUS_TYPE_SEARCH_DIRECTORY_FILE,
                                                        mautilusSearchDirectoryFileDetails);

    xdg_dir = mautilus_get_user_directory ();
    search_file->details->metadata_filename = g_build_filename (xdg_dir,
                                                                "search-metadata",
                                                                NULL);
    g_free (xdg_dir);

    file->details->got_file_info = TRUE;
    file->details->mime_type = eel_ref_str_get_unique ("x-directory/normal");
    file->details->type = G_FILE_TYPE_DIRECTORY;
    file->details->size = 0;

    file->details->file_info_is_up_to_date = TRUE;

    file->details->custom_icon = NULL;
    file->details->activation_uri = NULL;
    file->details->got_link_info = TRUE;
    file->details->link_info_is_up_to_date = TRUE;

    file->details->directory_count = 0;
    file->details->got_directory_count = TRUE;
    file->details->directory_count_is_up_to_date = TRUE;

    mautilus_file_set_display_name (file, _("Search"), NULL, TRUE);
}

static void
mautilus_search_directory_file_finalize (GObject *object)
{
    mautilusSearchDirectoryFile *search_file;

    search_file = MAUTILUS_SEARCH_DIRECTORY_FILE (object);

    g_free (search_file->details->metadata_filename);

    G_OBJECT_CLASS (mautilus_search_directory_file_parent_class)->finalize (object);
}

static void
mautilus_search_directory_file_class_init (mautilusSearchDirectoryFileClass *klass)
{
    GObjectClass *object_class;
    mautilusFileClass *file_class;

    object_class = G_OBJECT_CLASS (klass);
    file_class = MAUTILUS_FILE_CLASS (klass);

    object_class->finalize = mautilus_search_directory_file_finalize;

    file_class->default_file_type = G_FILE_TYPE_DIRECTORY;

    file_class->monitor_add = search_directory_file_monitor_add;
    file_class->monitor_remove = search_directory_file_monitor_remove;
    file_class->call_when_ready = search_directory_file_call_when_ready;
    file_class->cancel_call_when_ready = search_directory_file_cancel_call_when_ready;
    file_class->check_if_ready = search_directory_file_check_if_ready;
    file_class->get_item_count = search_directory_file_get_item_count;
    file_class->get_deep_counts = search_directory_file_get_deep_counts;
    file_class->get_where_string = search_directory_file_get_where_string;
    file_class->set_metadata = search_directory_file_set_metadata;
    file_class->set_metadata_as_list = search_directory_file_set_metadata_as_list;

    g_type_class_add_private (object_class, sizeof (mautilusSearchDirectoryFileDetails));
}
