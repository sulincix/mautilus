
/*
 * mautilus
 *
 * Copyright (C) 2002 Sun Microsystems, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, see <http://www.gnu.org/licenses/>.
 * 
 * Author: Dave Camp <dave@ximian.com>
 */

/* mautilus-tree-view-drag-dest.h: Handles drag and drop for treeviews which 
 *                                 contain a hierarchy of files
 */

#ifndef MAUTILUS_TREE_VIEW_DRAG_DEST_H
#define MAUTILUS_TREE_VIEW_DRAG_DEST_H

#include <gtk/gtk.h>

#include "mautilus-file.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_TREE_VIEW_DRAG_DEST	(mautilus_tree_view_drag_dest_get_type ())
#define MAUTILUS_TREE_VIEW_DRAG_DEST(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_TREE_VIEW_DRAG_DEST, mautilusTreeViewDragDest))
#define MAUTILUS_TREE_VIEW_DRAG_DEST_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_TREE_VIEW_DRAG_DEST, mautilusTreeViewDragDestClass))
#define MAUTILUS_IS_TREE_VIEW_DRAG_DEST(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_TREE_VIEW_DRAG_DEST))
#define MAUTILUS_IS_TREE_VIEW_DRAG_DEST_CLASS(klass)	(G_TYPE_CLASS_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_TREE_VIEW_DRAG_DEST))

typedef struct _mautilusTreeViewDragDest        mautilusTreeViewDragDest;
typedef struct _mautilusTreeViewDragDestClass   mautilusTreeViewDragDestClass;
typedef struct _mautilusTreeViewDragDestDetails mautilusTreeViewDragDestDetails;

struct _mautilusTreeViewDragDest {
	GObject parent;
	
	mautilusTreeViewDragDestDetails *details;
};

struct _mautilusTreeViewDragDestClass {
	GObjectClass parent;
	
	char *(*get_root_uri) (mautilusTreeViewDragDest *dest);
	mautilusFile *(*get_file_for_path) (mautilusTreeViewDragDest *dest,
					    GtkTreePath *path);
	void (*move_copy_items) (mautilusTreeViewDragDest *dest,
				 const GList *item_uris,
				 const char *target_uri,
				 GdkDragAction action,
				 int x,
				 int y);
	void (* handle_netscape_url) (mautilusTreeViewDragDest *dest,
				 const char *url,
				 const char *target_uri,
				 GdkDragAction action,
				 int x,
				 int y);
	void (* handle_uri_list) (mautilusTreeViewDragDest *dest,
				  const char *uri_list,
				  const char *target_uri,
				  GdkDragAction action,
				  int x,
				  int y);
	void (* handle_text)    (mautilusTreeViewDragDest *dest,
				  const char *text,
				  const char *target_uri,
				  GdkDragAction action,
				  int x,
				  int y);
	void (* handle_raw)    (mautilusTreeViewDragDest *dest,
				  char *raw_data,
				  int length,
				  const char *target_uri,
				  const char *direct_save_uri,
				  GdkDragAction action,
				  int x,
				  int y);
	void (* handle_hover)   (mautilusTreeViewDragDest *dest,
				 const char *target_uri);
};

GType                     mautilus_tree_view_drag_dest_get_type (void);
mautilusTreeViewDragDest *mautilus_tree_view_drag_dest_new      (GtkTreeView *tree_view);

G_END_DECLS

#endif
