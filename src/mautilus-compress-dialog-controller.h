/* mautilus-compress-dialog-controller.h
 *
 * Copyright (C) 2016 the mautilus developers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MAUTILUS_COMPRESS_DIALOG_CONTROLLER_H
#define MAUTILUS_COMPRESS_DIALOG_CONTROLLER_H

#include <glib.h>
#include <gtk/gtk.h>

#include "mautilus-file-name-widget-controller.h"
#include "mautilus-directory.h"

#define MAUTILUS_TYPE_COMPRESS_DIALOG_CONTROLLER mautilus_compress_dialog_controller_get_type ()
G_DECLARE_FINAL_TYPE (mautilusCompressDialogController, mautilus_compress_dialog_controller, MAUTILUS, COMPRESS_DIALOG_CONTROLLER, mautilusFileNameWidgetController)

mautilusCompressDialogController * mautilus_compress_dialog_controller_new (GtkWindow         *parent_window,
                                                                            mautilusDirectory *destination_directory,
                                                                            gchar             *initial_name);

#endif /* MAUTILUS_COMPRESS_DIALOG_CONTROLLER_H */
