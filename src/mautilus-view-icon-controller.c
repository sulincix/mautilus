#include "mautilus-view-icon-controller.h"
#include "mautilus-view-icon-ui.h"
#include "mautilus-view-item-model.h"
#include "mautilus-view-icon-item-ui.h"
#include "mautilus-view-model.h"
#include "mautilus-files-view.h"
#include "mautilus-file.h"
#include "mautilus-metadata.h"
#include "mautilus-window-slot.h"
#include "mautilus-directory.h"
#include "mautilus-global-preferences.h"

struct _mautilusViewIconController
{
    mautilusFilesView parent_instance;

    mautilusViewIconUi *view_ui;
    mautilusViewModel *model;
    GtkEventBox *event_box;

    GIcon *view_icon;
    GActionGroup *action_group;
    gint zoom_level;
};

G_DEFINE_TYPE (mautilusViewIconController, mautilus_view_icon_controller, MAUTILUS_TYPE_FILES_VIEW)

typedef struct
{
    const mautilusFileSortType sort_type;
    const gchar *metadata_name;
    const gchar *action_target_name;
    gboolean reversed;
} SortConstants;

static const SortConstants sorts_constants[] =
{
    {
        MAUTILUS_FILE_SORT_BY_DISPLAY_NAME,
        "name",
        "name",
        FALSE,
    },
    {
        MAUTILUS_FILE_SORT_BY_DISPLAY_NAME,
        "name",
        "name-desc",
        TRUE,
    },
    {
        MAUTILUS_FILE_SORT_BY_SIZE,
        "size",
        "size",
        TRUE,
    },
    {
        MAUTILUS_FILE_SORT_BY_TYPE,
        "type",
        "type",
        FALSE,
    },
    {
        MAUTILUS_FILE_SORT_BY_MTIME,
        "modification date",
        "modification-date",
        FALSE,
    },
    {
        MAUTILUS_FILE_SORT_BY_MTIME,
        "modification date",
        "modification-date-desc",
        TRUE,
    },
    {
        MAUTILUS_FILE_SORT_BY_ATIME,
        "access date",
        "access-date",
        FALSE,
    },
    {
        MAUTILUS_FILE_SORT_BY_ATIME,
        "access date",
        "access-date-desc",
        TRUE,
    },
    {
        MAUTILUS_FILE_SORT_BY_TRASHED_TIME,
        "trashed",
        "trash-time",
        TRUE,
    },
    {
        MAUTILUS_FILE_SORT_BY_SEARCH_RELEVANCE,
        NULL,
        "search-relevance",
        TRUE,
    }
};

static guint get_icon_size_for_zoom_level (mautilusCanvasZoomLevel zoom_level);

static const SortConstants *
get_sorts_constants_from_action_target_name (const gchar *action_target_name)
{
    int i;

    for (i = 0; i < G_N_ELEMENTS (sorts_constants); i++)
    {
        if (g_strcmp0 (sorts_constants[i].action_target_name, action_target_name) == 0)
        {
            return &sorts_constants[i];
        }
    }

    return &sorts_constants[0];
}

static const SortConstants *
get_sorts_constants_from_sort_type (mautilusFileSortType sort_type,
                                    gboolean             reversed)
{
    guint i;

    for (i = 0; i < G_N_ELEMENTS (sorts_constants); i++)
    {
        if (sort_type == sorts_constants[i].sort_type
            && reversed == sorts_constants[i].reversed)
        {
            return &sorts_constants[i];
        }
    }

    return &sorts_constants[0];
}

static const SortConstants *
get_sorts_constants_from_metadata_text (const char *metadata_name,
                                        gboolean    reversed)
{
    guint i;

    for (i = 0; i < G_N_ELEMENTS (sorts_constants); i++)
    {
        if (g_strcmp0 (sorts_constants[i].metadata_name, metadata_name) == 0
            && reversed == sorts_constants[i].reversed)
        {
            return &sorts_constants[i];
        }
    }

    return &sorts_constants[0];
}

static const SortConstants *
get_default_sort_order (mautilusFile *file)
{
    mautilusFileSortType sort_type;
    mautilusFileSortType default_sort_order;
    gboolean reversed;

    default_sort_order = g_settings_get_enum (mautilus_preferences,
                                              MAUTILUS_PREFERENCES_DEFAULT_SORT_ORDER);
    reversed = g_settings_get_boolean (mautilus_preferences,
                                       MAUTILUS_PREFERENCES_DEFAULT_SORT_IN_REVERSE_ORDER);

    /* If this is a special folder (e.g. search or recent), override the sort
     * order and reversed flag with values appropriate for the folder */
    sort_type = mautilus_file_get_default_sort_type (file, &reversed);

    if (sort_type == MAUTILUS_FILE_SORT_NONE)
    {
        sort_type = CLAMP (default_sort_order,
                           MAUTILUS_FILE_SORT_BY_DISPLAY_NAME,
                           MAUTILUS_FILE_SORT_BY_ATIME);
    }

    return get_sorts_constants_from_sort_type (sort_type, reversed);
}

static const SortConstants *
get_directory_sort_by (mautilusFile *file)
{
    const SortConstants *default_sort;
    g_autofree char *sort_by = NULL;
    gboolean reversed;

    default_sort = get_default_sort_order (file);
    g_return_val_if_fail (default_sort != NULL, NULL);

    sort_by = mautilus_file_get_metadata (file,
                                          MAUTILUS_METADATA_KEY_ICON_VIEW_SORT_BY,
                                          default_sort->metadata_name);

    reversed = mautilus_file_get_boolean_metadata (file,
                                                   MAUTILUS_METADATA_KEY_ICON_VIEW_SORT_REVERSED,
                                                   default_sort->reversed);

    return get_sorts_constants_from_metadata_text (sort_by, reversed);
}

static void
set_directory_sort_metadata (mautilusFile        *file,
                             const SortConstants *sort)
{
    const SortConstants *default_sort;

    default_sort = get_default_sort_order (file);

    mautilus_file_set_metadata (file,
                                MAUTILUS_METADATA_KEY_ICON_VIEW_SORT_BY,
                                default_sort->metadata_name,
                                sort->metadata_name);
    mautilus_file_set_boolean_metadata (file,
                                        MAUTILUS_METADATA_KEY_ICON_VIEW_SORT_REVERSED,
                                        default_sort->reversed,
                                        sort->reversed);
}

static void
update_sort_order_from_metadata_and_preferences (mautilusViewIconController *self)
{
    const SortConstants *default_directory_sort;
    GActionGroup *view_action_group;

    default_directory_sort = get_directory_sort_by (mautilus_files_view_get_directory_as_file (MAUTILUS_FILES_VIEW (self)));
    view_action_group = mautilus_files_view_get_action_group (MAUTILUS_FILES_VIEW (self));
    g_action_group_change_action_state (view_action_group,
                                        "sort",
                                        g_variant_new_string (get_sorts_constants_from_sort_type (default_directory_sort->sort_type, default_directory_sort->reversed)->action_target_name));
}

static void
real_begin_loading (mautilusFilesView *files_view)
{
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);

    /* TODO: This calls sort once, and update_context_menus calls update_actions which calls */
    /* the action again */
    update_sort_order_from_metadata_and_preferences (self);

    /*TODO move this to the files view class begin_loading and hook up? */

    /* We could have changed to the trash directory or to searching, and then
     * we need to update the menus */
    mautilus_files_view_update_context_menus (files_view);
    mautilus_files_view_update_toolbar_menus (files_view);
}

static void
real_clear (mautilusFilesView *files_view)
{
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);

    mautilus_view_model_remove_all_items (self->model);
}


/* FIXME: ideally this should go into the model so there is not need to
 * recreate the model with the new data */
static void
real_file_changed (mautilusFilesView *files_view,
                   mautilusFile      *file,
                   mautilusDirectory *directory)
{
    mautilusViewIconController *self;
    mautilusViewItemModel *item_model;
    mautilusViewItemModel *new_item_model;

    self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);
    item_model = mautilus_view_model_get_item_from_file (self->model, file);
    mautilus_view_model_remove_item (self->model, item_model);
    new_item_model = mautilus_view_item_model_new (file,
                                                   get_icon_size_for_zoom_level (self->zoom_level));
    mautilus_view_model_add_item (self->model, new_item_model);
}

static GList *
real_get_selection (mautilusFilesView *files_view)
{
    mautilusViewIconController *self;
    GList *selected_files = NULL;
    GList *l;
    g_autoptr (GList) selected_items = NULL;

    self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);
    selected_items = gtk_flow_box_get_selected_children (GTK_FLOW_BOX (self->view_ui));
    for (l = selected_items; l != NULL; l = l->next)
    {
        mautilusViewItemModel *item_model;

        item_model = mautilus_view_icon_item_ui_get_model (MAUTILUS_VIEW_ICON_ITEM_UI (l->data));
        selected_files = g_list_prepend (selected_files,
                                         g_object_ref (mautilus_view_item_model_get_file (item_model)));
    }

    return selected_files;
}

static gboolean
real_is_empty (mautilusFilesView *files_view)
{
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);

    return g_list_model_get_n_items (G_LIST_MODEL (mautilus_view_model_get_g_model (self->model))) == 0;
}

static void
real_end_file_changes (mautilusFilesView *files_view)
{
}

static void
real_remove_file (mautilusFilesView *files_view,
                  mautilusFile      *file,
                  mautilusDirectory *directory)
{
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);
    mautilusFile *current_file;
    mautilusViewItemModel *current_item_model;
    guint i = 0;

    while ((current_item_model = MAUTILUS_VIEW_ITEM_MODEL (g_list_model_get_item (G_LIST_MODEL (mautilus_view_model_get_g_model (self->model)), i))))
    {
        current_file = mautilus_view_item_model_get_file (current_item_model);
        if (current_file == file)
        {
            g_list_store_remove (mautilus_view_model_get_g_model (self->model), i);
            break;
        }
        i++;
    }
}

static GQueue *
convert_glist_to_queue (GList *list)
{
    GList *l;
    GQueue *queue;

    queue = g_queue_new ();
    for (l = list; l != NULL; l = l->next)
    {
        g_queue_push_tail (queue, l->data);
    }

    return queue;
}

static GQueue *
convert_files_to_item_models (mautilusViewIconController *self,
                              GQueue                     *files)
{
    GList *l;
    GQueue *models;

    models = g_queue_new ();
    for (l = g_queue_peek_head_link (files); l != NULL; l = l->next)
    {
        mautilusViewItemModel *item_model;

        item_model = mautilus_view_item_model_new (MAUTILUS_FILE (l->data),
                                                   get_icon_size_for_zoom_level (self->zoom_level));
        g_queue_push_tail (models, item_model);
    }

    return models;
}

static void
real_set_selection (mautilusFilesView *files_view,
                    GList             *selection)
{
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);
    g_autoptr (GQueue) selection_files = NULL;
    g_autoptr (GQueue) selection_item_models = NULL;

    selection_files = convert_glist_to_queue (selection);
    selection_item_models = mautilus_view_model_get_items_from_files (self->model, selection_files);
    mautilus_view_icon_ui_set_selection (self->view_ui, selection_item_models);
    mautilus_files_view_notify_selection_changed (files_view);
}

static void
real_select_all (mautilusFilesView *files_view)
{
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);
    gtk_flow_box_select_all (GTK_FLOW_BOX (self->view_ui));
}

static void
real_reveal_selection (mautilusFilesView *files_view)
{
    GList *selection;
    mautilusViewItemModel *item_model;
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);
    GtkWidget *item_ui;
    GtkAllocation allocation;
    GtkWidget *content_widget;
    GtkAdjustment *vadjustment;

    selection = mautilus_view_get_selection (MAUTILUS_VIEW (files_view));
    if (selection == NULL)
    {
        return;
    }

    item_model = mautilus_view_model_get_item_from_file (self->model,
                                                         MAUTILUS_FILE (selection->data));
    item_ui = mautilus_view_item_model_get_item_ui (item_model);
    gtk_widget_get_allocation (item_ui, &allocation);
    content_widget = mautilus_files_view_get_content_widget (files_view);
    vadjustment = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (content_widget));
    gtk_adjustment_set_value (vadjustment, allocation.y);

    g_list_foreach (selection, (GFunc) g_object_unref, NULL);
}

static gboolean
showing_recent_directory (mautilusFilesView *view)
{
    mautilusFile *file;

    file = mautilus_files_view_get_directory_as_file (view);
    if (file != NULL)
    {
        return mautilus_file_is_in_recent (file);
    }
    return FALSE;
}

static gboolean
showing_search_directory (mautilusFilesView *view)
{
    mautilusFile *file;

    file = mautilus_files_view_get_directory_as_file (view);
    if (file != NULL)
    {
        return mautilus_file_is_in_search (file);
    }
    return FALSE;
}

static void
real_update_actions_state (mautilusFilesView *files_view)
{
    GAction *action;
    GActionGroup *view_action_group;

    MAUTILUS_FILES_VIEW_CLASS (mautilus_view_icon_controller_parent_class)->update_actions_state (files_view);

    view_action_group = mautilus_files_view_get_action_group (files_view);
    action = g_action_map_lookup_action (G_ACTION_MAP (view_action_group), "sort");
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action),
                                 !showing_recent_directory (files_view) &&
                                 !showing_search_directory (files_view));
}

static void
real_bump_zoom_level (mautilusFilesView *files_view,
                      int                zoom_increment)
{
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);
    mautilusCanvasZoomLevel new_level;

    new_level = self->zoom_level + zoom_increment;

    if (new_level >= MAUTILUS_CANVAS_ZOOM_LEVEL_SMALL &&
        new_level <= MAUTILUS_CANVAS_ZOOM_LEVEL_LARGEST)
    {
        g_action_group_change_action_state (self->action_group,
                                            "zoom-to-level",
                                            g_variant_new_int32 (new_level));
    }
}

static guint
get_icon_size_for_zoom_level (mautilusCanvasZoomLevel zoom_level)
{
    switch (zoom_level)
    {
        case MAUTILUS_CANVAS_ZOOM_LEVEL_SMALL:
        {
            return MAUTILUS_CANVAS_ICON_SIZE_SMALL;
        }
        break;

        case MAUTILUS_CANVAS_ZOOM_LEVEL_STANDARD:
        {
            return MAUTILUS_CANVAS_ICON_SIZE_STANDARD;
        }
        break;

        case MAUTILUS_CANVAS_ZOOM_LEVEL_LARGE:
        {
            return MAUTILUS_CANVAS_ICON_SIZE_LARGE;
        }
        break;

        case MAUTILUS_CANVAS_ZOOM_LEVEL_LARGER:
        {
            return MAUTILUS_CANVAS_ICON_SIZE_LARGER;
        }
        break;

        case MAUTILUS_CANVAS_ZOOM_LEVEL_LARGEST:
        {
            return MAUTILUS_CANVAS_ICON_SIZE_LARGEST;
        }
        break;
    }
    g_return_val_if_reached (MAUTILUS_CANVAS_ICON_SIZE_STANDARD);
}

static gint
get_default_zoom_level ()
{
    mautilusCanvasZoomLevel default_zoom_level;

    default_zoom_level = g_settings_get_enum (mautilus_icon_view_preferences,
                                              MAUTILUS_PREFERENCES_ICON_VIEW_DEFAULT_ZOOM_LEVEL);

    return default_zoom_level;
}

static void
set_icon_size (mautilusViewIconController *self,
               gint                        icon_size)
{
    mautilusViewItemModel *current_item_model;
    guint i = 0;

    while ((current_item_model = MAUTILUS_VIEW_ITEM_MODEL (g_list_model_get_item (G_LIST_MODEL (mautilus_view_model_get_g_model (self->model)), i))))
    {
        mautilus_view_item_model_set_icon_size (current_item_model,
                                                get_icon_size_for_zoom_level (self->zoom_level));
        i++;
    }
}

static void
set_zoom_level (mautilusViewIconController *self,
                guint                       new_level)
{
    self->zoom_level = new_level;

    set_icon_size (self, get_icon_size_for_zoom_level (new_level));

    mautilus_files_view_update_toolbar_menus (MAUTILUS_FILES_VIEW (self));
}

static void
real_restore_standard_zoom_level (mautilusFilesView *files_view)
{
    mautilusViewIconController *self;

    self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);
    g_action_group_change_action_state (self->action_group,
                                        "zoom-to-level",
                                        g_variant_new_int32 (MAUTILUS_CANVAS_ZOOM_LEVEL_LARGE));
}

static gfloat
real_get_zoom_level_percentage (mautilusFilesView *files_view)
{
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);

    return (gfloat) get_icon_size_for_zoom_level (self->zoom_level) /
           MAUTILUS_CANVAS_ICON_SIZE_LARGE;
}

static gboolean
real_is_zoom_level_default (mautilusFilesView *files_view)
{
    mautilusViewIconController *self;
    guint icon_size;

    self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);
    icon_size = get_icon_size_for_zoom_level (self->zoom_level);

    return icon_size == MAUTILUS_CANVAS_ICON_SIZE_LARGE;
}

static gboolean
real_can_zoom_in (mautilusFilesView *files_view)
{
    return TRUE;
}

static gboolean
real_can_zoom_out (mautilusFilesView *files_view)
{
    return TRUE;
}

static GdkRectangle *
real_compute_rename_popover_pointing_to (mautilusFilesView *files_view)
{
    mautilusViewIconController *self;
    GdkRectangle *allocation;
    GtkAdjustment *vadjustment;
    GtkAdjustment *hadjustment;
    GtkWidget *parent_container;
    g_autoptr (GQueue) selection_files = NULL;
    g_autoptr (GQueue) selection_item_models = NULL;
    GList *selection;
    GtkWidget *icon_item_ui;

    self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);
    allocation = g_new0 (GdkRectangle, 1);

    parent_container = mautilus_files_view_get_content_widget (files_view);
    vadjustment = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (parent_container));
    hadjustment = gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (parent_container));
    selection = mautilus_view_get_selection (MAUTILUS_VIEW (files_view));
    selection_files = convert_glist_to_queue (selection);
    selection_item_models = mautilus_view_model_get_items_from_files (self->model, selection_files);
    /* We only allow one item to be renamed with a popover */
    icon_item_ui = mautilus_view_item_model_get_item_ui (g_queue_peek_head (selection_item_models));
    gtk_widget_get_allocation (icon_item_ui, allocation);

    allocation->x -= gtk_adjustment_get_value (hadjustment);
    allocation->y -= gtk_adjustment_get_value (vadjustment);

    return allocation;
}

static void
real_click_policy_changed (mautilusFilesView *files_view)
{
}

static gboolean
on_button_press_event (GtkWidget *widget,
                       GdkEvent  *event,
                       gpointer   user_data)
{
    mautilusViewIconController *self;
    g_autoptr (GList) selection = NULL;
    GtkWidget *child_at_pos;
    GdkEventButton *event_button;

    self = MAUTILUS_VIEW_ICON_CONTROLLER (user_data);
    event_button = (GdkEventButton *) event;

    /* Need to update the selection so the popup has the right actions enabled */
    selection = mautilus_view_get_selection (MAUTILUS_VIEW (self));
    child_at_pos = GTK_WIDGET (gtk_flow_box_get_child_at_pos (GTK_FLOW_BOX (self->view_ui),
                                                              event_button->x, event_button->y));
    if (child_at_pos != NULL)
    {
        mautilusFile *selected_file;
        mautilusViewItemModel *item_model;

        item_model = mautilus_view_icon_item_ui_get_model (MAUTILUS_VIEW_ICON_ITEM_UI (child_at_pos));
        selected_file = mautilus_view_item_model_get_file (item_model);
        if (g_list_find (selection, selected_file) == NULL)
        {
            g_list_foreach (selection, (GFunc) g_object_unref, NULL);
            selection = g_list_append (NULL, g_object_ref (selected_file));
        }
        else
        {
            selection = g_list_prepend (selection, g_object_ref (selected_file));
        }

        mautilus_view_set_selection (MAUTILUS_VIEW (self), selection);

        if (event_button->button == GDK_BUTTON_SECONDARY)
        {
            mautilus_files_view_pop_up_selection_context_menu (MAUTILUS_FILES_VIEW (self),
                                                               event_button);
        }
    }
    else
    {
        mautilus_view_set_selection (MAUTILUS_VIEW (self), NULL);
        if (event_button->button == GDK_BUTTON_SECONDARY)
        {
            mautilus_files_view_pop_up_background_context_menu (MAUTILUS_FILES_VIEW (self),
                                                                event_button);
        }
    }

    g_list_foreach (selection, (GFunc) g_object_unref, NULL);

    return GDK_EVENT_STOP;
}

static int
real_compare_files (mautilusFilesView *files_view,
                    mautilusFile      *file1,
                    mautilusFile      *file2)
{
    if (file1 < file2)
    {
        return -1;
    }

    if (file1 > file2)
    {
        return +1;
    }

    return 0;
}

static gboolean
real_using_manual_layout (mautilusFilesView *files_view)
{
    return FALSE;
}

static void
real_end_loading (mautilusFilesView *files_view,
                  gboolean           all_files_seen)
{
}

static char *
real_get_first_visible_file (mautilusFilesView *files_view)
{
    return NULL;
}

static void
real_scroll_to_file (mautilusFilesView *files_view,
                     const char        *uri)
{
}

static void
real_sort_directories_first_changed (mautilusFilesView *files_view)
{
    mautilusViewModelSortData sort_data;
    mautilusViewModelSortData *current_sort_data;
    mautilusViewIconController *self;

    self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);
    current_sort_data = mautilus_view_model_get_sort_type (self->model);
    sort_data.sort_type = current_sort_data->sort_type;
    sort_data.reversed = current_sort_data->reversed;
    sort_data.directories_first = mautilus_files_view_should_sort_directories_first (MAUTILUS_FILES_VIEW (self));

    mautilus_view_model_set_sort_type (self->model, &sort_data);
}

static void
action_sort_order_changed (GSimpleAction *action,
                           GVariant      *value,
                           gpointer       user_data)
{
    const gchar *target_name;
    const SortConstants *sorts_constants;
    mautilusViewModelSortData sort_data;
    mautilusViewIconController *self;

    /* Don't resort if the action is in the same state as before */
    if (g_strcmp0 (g_variant_get_string (value, NULL), g_variant_get_string (g_action_get_state (G_ACTION (action)), NULL)) == 0)
    {
        return;
    }

    self = MAUTILUS_VIEW_ICON_CONTROLLER (user_data);
    target_name = g_variant_get_string (value, NULL);
    sorts_constants = get_sorts_constants_from_action_target_name (target_name);
    sort_data.sort_type = sorts_constants->sort_type;
    sort_data.reversed = sorts_constants->reversed;
    sort_data.directories_first = mautilus_files_view_should_sort_directories_first (MAUTILUS_FILES_VIEW (self));

    mautilus_view_model_set_sort_type (self->model, &sort_data);
    set_directory_sort_metadata (mautilus_files_view_get_directory_as_file (MAUTILUS_FILES_VIEW (self)),
                                 sorts_constants);

    g_simple_action_set_state (action, value);
}

static void
real_add_files (mautilusFilesView *files_view,
                GList             *files)
{
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);
    g_autoptr (GQueue) files_queue;
    g_autoptr (GQueue) item_models;

    files_queue = convert_glist_to_queue (files);
    item_models = convert_files_to_item_models (self, files_queue);
    mautilus_view_model_add_items (self->model, item_models);
}


static guint
real_get_view_id (mautilusFilesView *files_view)
{
    return MAUTILUS_VIEW_GRID_ID;
}

static GIcon *
real_get_icon (mautilusFilesView *files_view)
{
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (files_view);

    return self->view_icon;
}

static void
real_select_first (mautilusFilesView *files_view)
{
}

static void
action_zoom_to_level (GSimpleAction *action,
                      GVariant      *state,
                      gpointer       user_data)
{
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (user_data);
    int zoom_level;

    zoom_level = g_variant_get_int32 (state);
    set_zoom_level (self, zoom_level);
    g_simple_action_set_state (G_SIMPLE_ACTION (action), state);

    if (g_settings_get_enum (mautilus_icon_view_preferences,
                             MAUTILUS_PREFERENCES_ICON_VIEW_DEFAULT_ZOOM_LEVEL) != zoom_level)
    {
        g_settings_set_enum (mautilus_icon_view_preferences,
                             MAUTILUS_PREFERENCES_ICON_VIEW_DEFAULT_ZOOM_LEVEL,
                             zoom_level);
    }
}

static void
finalize (GObject *object)
{
    G_OBJECT_CLASS (mautilus_view_icon_controller_parent_class)->finalize (object);
}


const GActionEntry view_icon_actions[] =
{
    { "sort", NULL, "s", "'invalid'", action_sort_order_changed },
    { "zoom-to-level", NULL, NULL, "100", action_zoom_to_level }
};

static void
constructed (GObject *object)
{
    mautilusViewIconController *self = MAUTILUS_VIEW_ICON_CONTROLLER (object);
    GtkWidget *content_widget;
    GActionGroup *view_action_group;

    self->model = mautilus_view_model_new ();
    self->view_ui = mautilus_view_icon_ui_new (self);
    gtk_widget_show (GTK_WIDGET (self->view_ui));
    self->view_icon = g_themed_icon_new ("view-grid-symbolic");

    self->event_box = GTK_EVENT_BOX (gtk_event_box_new ());
    gtk_container_add (GTK_CONTAINER (self->event_box), GTK_WIDGET (self->view_ui));
    g_signal_connect (GTK_WIDGET (self->event_box), "button-press-event",
                      (GCallback) on_button_press_event, self);

    content_widget = mautilus_files_view_get_content_widget (MAUTILUS_FILES_VIEW (self));
    gtk_container_add (GTK_CONTAINER (content_widget), GTK_WIDGET (self->event_box));

    self->action_group = mautilus_files_view_get_action_group (MAUTILUS_FILES_VIEW (self));
    g_action_map_add_action_entries (G_ACTION_MAP (self->action_group),
                                     view_icon_actions,
                                     G_N_ELEMENTS (view_icon_actions),
                                     self);

    gtk_widget_show_all (GTK_WIDGET (self));

    view_action_group = mautilus_files_view_get_action_group (MAUTILUS_FILES_VIEW (self));
    g_action_map_add_action_entries (G_ACTION_MAP (view_action_group),
                                     view_icon_actions,
                                     G_N_ELEMENTS (view_icon_actions),
                                     self);
    self->zoom_level = get_default_zoom_level ();
    /* Keep the action synced with the actual value, so the toolbar can poll it */
    g_action_group_change_action_state (mautilus_files_view_get_action_group (MAUTILUS_FILES_VIEW (self)),
                                        "zoom-to-level", g_variant_new_int32 (self->zoom_level));
}

static void
mautilus_view_icon_controller_class_init (mautilusViewIconControllerClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    mautilusFilesViewClass *files_view_class = MAUTILUS_FILES_VIEW_CLASS (klass);

    object_class->finalize = finalize;
    object_class->constructed = constructed;

    files_view_class->add_files = real_add_files;
    files_view_class->begin_loading = real_begin_loading;
    files_view_class->bump_zoom_level = real_bump_zoom_level;
    files_view_class->can_zoom_in = real_can_zoom_in;
    files_view_class->can_zoom_out = real_can_zoom_out;
    files_view_class->click_policy_changed = real_click_policy_changed;
    files_view_class->clear = real_clear;
    files_view_class->file_changed = real_file_changed;
    files_view_class->get_selection = real_get_selection;
    /* TODO: remove this get_selection_for_file_transfer, this doesn't even
     * take into account we could us the view for recursive search :/
     * CanvasView has the same issue. */
    files_view_class->get_selection_for_file_transfer = real_get_selection;
    files_view_class->is_empty = real_is_empty;
    files_view_class->remove_file = real_remove_file;
    files_view_class->update_actions_state = real_update_actions_state;
    files_view_class->reveal_selection = real_reveal_selection;
    files_view_class->select_all = real_select_all;
    files_view_class->set_selection = real_set_selection;
    files_view_class->compare_files = real_compare_files;
    files_view_class->sort_directories_first_changed = real_sort_directories_first_changed;
    files_view_class->end_file_changes = real_end_file_changes;
    files_view_class->using_manual_layout = real_using_manual_layout;
    files_view_class->end_loading = real_end_loading;
    files_view_class->get_view_id = real_get_view_id;
    files_view_class->get_first_visible_file = real_get_first_visible_file;
    files_view_class->scroll_to_file = real_scroll_to_file;
    files_view_class->get_icon = real_get_icon;
    files_view_class->select_first = real_select_first;
    files_view_class->restore_standard_zoom_level = real_restore_standard_zoom_level;
    files_view_class->get_zoom_level_percentage = real_get_zoom_level_percentage;
    files_view_class->is_zoom_level_default = real_is_zoom_level_default;
    files_view_class->compute_rename_popover_pointing_to = real_compute_rename_popover_pointing_to;
}

static void
mautilus_view_icon_controller_init (mautilusViewIconController *self)
{
}

mautilusViewIconController *
mautilus_view_icon_controller_new (mautilusWindowSlot *slot)
{
    return g_object_new (MAUTILUS_TYPE_VIEW_ICON_CONTROLLER,
                         "window-slot", slot,
                         NULL);
}

mautilusViewModel *
mautilus_view_icon_controller_get_model (mautilusViewIconController *self)
{
    g_return_val_if_fail (MAUTILUS_IS_VIEW_ICON_CONTROLLER (self), NULL);

    return self->model;
}
