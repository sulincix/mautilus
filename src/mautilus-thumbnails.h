/*
   mautilus-thumbnails.h: Thumbnail code for icon factory.
 
   Copyright (C) 2000 Eazel, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Andy Hertzfeld <andy@eazel.com>
*/

#ifndef MAUTILUS_THUMBNAILS_H
#define MAUTILUS_THUMBNAILS_H

#include <gdk-pixbuf/gdk-pixbuf.h>
#include "mautilus-file.h"

/* Returns NULL if there's no thumbnail yet. */
void       mautilus_create_thumbnail                (mautilusFile *file);
gboolean   mautilus_can_thumbnail                   (mautilusFile *file);
gboolean   mautilus_can_thumbnail_internally        (mautilusFile *file);
gboolean   mautilus_thumbnail_is_mimetype_limited_by_size
						    (const char *mime_type);

/* Queue handling: */
void       mautilus_thumbnail_remove_from_queue     (const char   *file_uri);
void       mautilus_thumbnail_prioritize            (const char   *file_uri);


#endif /* MAUTILUS_THUMBNAILS_H */
