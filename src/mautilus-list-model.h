
/* fm-list-model.h - a GtkTreeModel for file lists. 

   Copyright (C) 2001, 2002 Anders Carlsson

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   see <http://www.gnu.org/licenses/>.

   Authors: Anders Carlsson <andersca@gnu.org>
*/

#include <gtk/gtk.h>
#include <gdk/gdk.h>
#include "mautilus-file.h"
#include "mautilus-directory.h"
#include <libmautilus-extension/mautilus-column.h>

#ifndef MAUTILUS_LIST_MODEL_H
#define MAUTILUS_LIST_MODEL_H

#define MAUTILUS_TYPE_LIST_MODEL mautilus_list_model_get_type()
G_DECLARE_DERIVABLE_TYPE (mautilusListModel, mautilus_list_model, MAUTILUS, LIST_MODEL, GObject);

enum {
	MAUTILUS_LIST_MODEL_FILE_COLUMN,
	MAUTILUS_LIST_MODEL_SUBDIRECTORY_COLUMN,
	MAUTILUS_LIST_MODEL_SMALL_ICON_COLUMN,
	MAUTILUS_LIST_MODEL_STANDARD_ICON_COLUMN,
	MAUTILUS_LIST_MODEL_LARGE_ICON_COLUMN,
	MAUTILUS_LIST_MODEL_LARGER_ICON_COLUMN,
	MAUTILUS_LIST_MODEL_FILE_NAME_IS_EDITABLE_COLUMN,
	MAUTILUS_LIST_MODEL_NUM_COLUMNS
};

struct _mautilusListModelClass
{
	GObjectClass parent_class;

	void (* subdirectory_unloaded)(mautilusListModel *model,
				       mautilusDirectory *subdirectory);
};

gboolean mautilus_list_model_add_file                          (mautilusListModel          *model,
								mautilusFile         *file,
								mautilusDirectory    *directory);
void     mautilus_list_model_file_changed                      (mautilusListModel          *model,
								mautilusFile         *file,
								mautilusDirectory    *directory);
gboolean mautilus_list_model_is_empty                          (mautilusListModel          *model);
void     mautilus_list_model_remove_file                       (mautilusListModel          *model,
								mautilusFile         *file,
								mautilusDirectory    *directory);
void     mautilus_list_model_clear                             (mautilusListModel          *model);
gboolean mautilus_list_model_get_tree_iter_from_file           (mautilusListModel          *model,
								mautilusFile         *file,
								mautilusDirectory    *directory,
								GtkTreeIter          *iter);
GList *  mautilus_list_model_get_all_iters_for_file            (mautilusListModel          *model,
								mautilusFile         *file);
gboolean mautilus_list_model_get_first_iter_for_file           (mautilusListModel          *model,
								mautilusFile         *file,
								GtkTreeIter          *iter);
void     mautilus_list_model_set_should_sort_directories_first (mautilusListModel          *model,
								gboolean              sort_directories_first);

int      mautilus_list_model_get_sort_column_id_from_attribute (mautilusListModel *model,
								GQuark       attribute);
GQuark   mautilus_list_model_get_attribute_from_sort_column_id (mautilusListModel *model,
								int sort_column_id);
void     mautilus_list_model_sort_files                        (mautilusListModel *model,
								GList **files);

mautilusListZoomLevel mautilus_list_model_get_zoom_level_from_column_id (int               column);
int               mautilus_list_model_get_column_id_from_zoom_level (mautilusListZoomLevel zoom_level);
guint    mautilus_list_model_get_icon_size_for_zoom_level      (mautilusListZoomLevel zoom_level);

mautilusFile *    mautilus_list_model_file_for_path (mautilusListModel *model, GtkTreePath *path);
gboolean          mautilus_list_model_load_subdirectory (mautilusListModel *model, GtkTreePath *path, mautilusDirectory **directory);
void              mautilus_list_model_unload_subdirectory (mautilusListModel *model, GtkTreeIter *iter);

void              mautilus_list_model_set_drag_view (mautilusListModel *model,
						     GtkTreeView *view,
						     int begin_x, 
						     int begin_y);
GtkTreeView *     mautilus_list_model_get_drag_view (mautilusListModel *model,
						     int *drag_begin_x,
						     int *drag_begin_y);

GtkTargetList *   mautilus_list_model_get_drag_target_list (void);

int               mautilus_list_model_compare_func (mautilusListModel *model,
						    mautilusFile *file1,
						    mautilusFile *file2);


int               mautilus_list_model_add_column (mautilusListModel *model,
						  mautilusColumn *column);

void              mautilus_list_model_subdirectory_done_loading (mautilusListModel       *model,
								 mautilusDirectory *directory);

void              mautilus_list_model_set_highlight_for_files (mautilusListModel *model,
							       GList *files);
						   
#endif /* MAUTILUS_LIST_MODEL_H */
