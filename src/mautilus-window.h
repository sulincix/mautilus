
/*
 *  mautilus
 *
 *  Copyright (C) 1999, 2000 Red Hat, Inc.
 *  Copyright (C) 1999, 2000, 2001 Eazel, Inc.
 *
 *  mautilus is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  mautilus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Authors: Elliot Lee <sopwith@redhat.com>
 *           Darin Adler <darin@bentspoon.com>
 *
 */
/* mautilus-window.h: Interface of the main window object */

#ifndef MAUTILUS_WINDOW_H
#define MAUTILUS_WINDOW_H

#include <gtk/gtk.h>
#include <eel/eel-glib-extensions.h>
#include "mautilus-bookmark.h"
#include "mautilus-search-directory.h"

G_BEGIN_DECLS

typedef enum {
        MAUTILUS_WINDOW_OPEN_FLAG_CLOSE_BEHIND = 1 << 0,
        MAUTILUS_WINDOW_OPEN_FLAG_NEW_WINDOW = 1 << 1,
        MAUTILUS_WINDOW_OPEN_FLAG_NEW_TAB = 1 << 2,
        MAUTILUS_WINDOW_OPEN_SLOT_APPEND = 1 << 3,
        MAUTILUS_WINDOW_OPEN_FLAG_DONT_MAKE_ACTIVE = 1 << 4
} mautilusWindowOpenFlags;

#define MAUTILUS_TYPE_WINDOW (mautilus_window_get_type ())
G_DECLARE_DERIVABLE_TYPE (mautilusWindow, mautilus_window, MAUTILUS, WINDOW, GtkApplicationWindow);

typedef gboolean (* mautilusWindowGoToCallback) (mautilusWindow *window,
                                                 GFile *location,
                                                 GError *error,
                                                 gpointer user_data);

#include "mautilus-files-view.h"
#include "mautilus-window-slot.h"

#define MAUTILUS_WINDOW_SIDEBAR_PLACES "places"
#define MAUTILUS_WINDOW_SIDEBAR_TREE "tree"

/* window geometry */
/* Min values are very small, and a mautilus window at this tiny size is *almost*
 * completely unusable. However, if all the extra bits (sidebar, location bar, etc)
 * are turned off, you can see an icon or two at this size. See bug 5946.
 */

#define MAUTILUS_WINDOW_MIN_WIDTH		200
#define MAUTILUS_WINDOW_MIN_HEIGHT		200
#define MAUTILUS_WINDOW_DEFAULT_WIDTH		890
#define MAUTILUS_WINDOW_DEFAULT_HEIGHT		550


struct _mautilusWindowClass
{
        GtkApplicationWindowClass parent_spot;

	/* Function pointers for overriding, without corresponding signals */
        void   (* sync_title) (mautilusWindow *window,
			       mautilusWindowSlot *slot);
        void   (* close) (mautilusWindow *window);
        /* Use this in case your window has a special slot. Also is expected that
         * the slot is initialized with mautilus_window_initialize_slot.
         */
        mautilusWindowSlot * (* create_slot) (mautilusWindow *window,
                                              GFile          *location);
};

mautilusWindow * mautilus_window_new                  (GdkScreen         *screen);
void             mautilus_window_close                (mautilusWindow    *window);

void mautilus_window_open_location_full               (mautilusWindow          *window,
                                                       GFile                   *location,
                                                       mautilusWindowOpenFlags  flags,
                                                       GList                   *selection,
                                                       mautilusWindowSlot      *target_slot);

void             mautilus_window_new_tab              (mautilusWindow    *window);
mautilusWindowSlot * mautilus_window_get_active_slot       (mautilusWindow *window);
void                 mautilus_window_set_active_slot       (mautilusWindow    *window,
                                                            mautilusWindowSlot *slot);
GList *              mautilus_window_get_slots             (mautilusWindow *window);
void                 mautilus_window_slot_close            (mautilusWindow *window,
                                                            mautilusWindowSlot *slot);

void                 mautilus_window_sync_location_widgets (mautilusWindow *window);

void     mautilus_window_hide_sidebar         (mautilusWindow *window);
void     mautilus_window_show_sidebar         (mautilusWindow *window);
void     mautilus_window_back_or_forward      (mautilusWindow *window,
                                               gboolean        back,
                                               guint           distance,
                                               mautilusWindowOpenFlags flags);
void mautilus_window_hide_view_menu (mautilusWindow *window);
void mautilus_window_reset_menus (mautilusWindow *window);

GtkWidget *         mautilus_window_get_notebook (mautilusWindow *window);

mautilusWindowOpenFlags mautilus_event_get_window_open_flags   (void);
void     mautilus_window_show_about_dialog    (mautilusWindow *window);

GtkWidget *mautilus_window_get_toolbar (mautilusWindow *window);

/* sync window GUI with current slot. Used when changing slots,
 * and when updating the slot state.
 */
void mautilus_window_sync_allow_stop       (mautilusWindow *window,
					    mautilusWindowSlot *slot);
void mautilus_window_sync_title            (mautilusWindow *window,
					    mautilusWindowSlot *slot);

void mautilus_window_show_operation_notification (mautilusWindow *window,
                                                  gchar          *main_label,
                                                  GFile          *folder_to_open);
void mautilus_window_start_dnd (mautilusWindow *window,
                                GdkDragContext *context);
void mautilus_window_end_dnd (mautilusWindow *window,
                              GdkDragContext *context);

void mautilus_window_search (mautilusWindow *window,
                             const gchar    *text);

void mautilus_window_initialize_slot (mautilusWindow          *window,
                                      mautilusWindowSlot      *slot,
                                      mautilusWindowOpenFlags  flags);

G_END_DECLS

#endif
