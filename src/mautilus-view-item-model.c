#include "mautilus-view-item-model.h"
#include "mautilus-file.h"

struct _mautilusViewItemModel
{
    GObject parent_instance;
    guint icon_size;
    mautilusFile *file;
    GtkLabel *label;
    GtkWidget *item_ui;
};

G_DEFINE_TYPE (mautilusViewItemModel, mautilus_view_item_model, G_TYPE_OBJECT)

enum
{
    PROP_0,
    PROP_FILE,
    PROP_ICON_SIZE,
    PROP_ITEM_UI,
    N_PROPS
};

static void
mautilus_view_item_model_finalize (GObject *object)
{
    G_OBJECT_CLASS (mautilus_view_item_model_parent_class)->finalize (object);
}

static void
mautilus_view_item_model_get_property (GObject    *object,
                                       guint       prop_id,
                                       GValue     *value,
                                       GParamSpec *pspec)
{
    mautilusViewItemModel *self = MAUTILUS_VIEW_ITEM_MODEL (object);

    switch (prop_id)
    {
        case PROP_FILE:
        {
            g_value_set_object (value, self->file);
        }
        break;

        case PROP_ICON_SIZE:
        {
            g_value_set_int (value, self->icon_size);
        }
        break;

        case PROP_ITEM_UI:
        {
            g_value_set_object (value, self->item_ui);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        }
    }
}

static void
mautilus_view_item_model_set_property (GObject      *object,
                                       guint         prop_id,
                                       const GValue *value,
                                       GParamSpec   *pspec)
{
    mautilusViewItemModel *self = MAUTILUS_VIEW_ITEM_MODEL (object);

    switch (prop_id)
    {
        case PROP_FILE:
        {
            mautilus_view_item_model_set_file (self, g_value_get_object (value));
        }
        break;

        case PROP_ICON_SIZE:
        {
            mautilus_view_item_model_set_icon_size (self, g_value_get_int (value));
        }
        break;

        case PROP_ITEM_UI:
        {
            mautilus_view_item_model_set_item_ui (self, g_value_get_object (value));
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        }
    }
}

static void
mautilus_view_item_model_init (mautilusViewItemModel *self)
{
}

static void
mautilus_view_item_model_class_init (mautilusViewItemModelClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = mautilus_view_item_model_finalize;
    object_class->get_property = mautilus_view_item_model_get_property;
    object_class->set_property = mautilus_view_item_model_set_property;

    g_object_class_install_property (object_class,
                                     PROP_ICON_SIZE,
                                     g_param_spec_int ("icon-size",
                                                       "Icon size",
                                                       "The size in pixels of the icon",
                                                       MAUTILUS_CANVAS_ICON_SIZE_SMALL,
                                                       MAUTILUS_CANVAS_ICON_SIZE_LARGEST,
                                                       MAUTILUS_CANVAS_ICON_SIZE_LARGE,
                                                       G_PARAM_READWRITE | G_PARAM_CONSTRUCT));
    g_object_class_install_property (object_class,
                                     PROP_FILE,
                                     g_param_spec_object ("file",
                                                          "File",
                                                          "The file the icon item represents",
                                                          MAUTILUS_TYPE_FILE,
                                                          G_PARAM_READWRITE | G_PARAM_CONSTRUCT));

    g_object_class_install_property (object_class,
                                     PROP_ITEM_UI,
                                     g_param_spec_object ("item-ui",
                                                          "Item ui",
                                                          "The UI that reprensents the item model",
                                                          GTK_TYPE_WIDGET,
                                                          G_PARAM_READWRITE));
}

mautilusViewItemModel *
mautilus_view_item_model_new (mautilusFile *file,
                              guint         icon_size)
{
    return g_object_new (MAUTILUS_TYPE_VIEW_ITEM_MODEL,
                         "file", file,
                         "icon-size", icon_size,
                         NULL);
}

guint
mautilus_view_item_model_get_icon_size (mautilusViewItemModel *self)
{
    g_return_val_if_fail (MAUTILUS_IS_VIEW_ITEM_MODEL (self), -1);

    return self->icon_size;
}

void
mautilus_view_item_model_set_icon_size (mautilusViewItemModel *self,
                                        guint                  icon_size)
{
    g_return_if_fail (MAUTILUS_IS_VIEW_ITEM_MODEL (self));

    self->icon_size = icon_size;

    g_object_notify (G_OBJECT (self), "icon-size");
}

mautilusFile *
mautilus_view_item_model_get_file (mautilusViewItemModel *self)
{
    g_return_val_if_fail (MAUTILUS_IS_VIEW_ITEM_MODEL (self), NULL);

    return self->file;
}

void
mautilus_view_item_model_set_file (mautilusViewItemModel *self,
                                   mautilusFile          *file)
{
    g_return_if_fail (MAUTILUS_IS_VIEW_ITEM_MODEL (self));

    g_clear_object (&self->file);
    self->file = g_object_ref (file);

    g_object_notify (G_OBJECT (self), "file");
}

GtkWidget *
mautilus_view_item_model_get_item_ui (mautilusViewItemModel *self)
{
    g_return_val_if_fail (MAUTILUS_IS_VIEW_ITEM_MODEL (self), NULL);

    return self->item_ui;
}

void
mautilus_view_item_model_set_item_ui (mautilusViewItemModel *self,
                                      GtkWidget             *item_ui)
{
    g_return_if_fail (MAUTILUS_IS_VIEW_ITEM_MODEL (self));

    g_clear_object (&self->item_ui);
    self->item_ui = g_object_ref (item_ui);

    g_object_notify (G_OBJECT (self), "item-ui");
}
