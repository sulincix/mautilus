/* mautilus-other-locations-window-slot.c
 *
 * Copyright (C) 2016 Carlos Soriano <csoriano@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mautilus-other-locations-window-slot.h"
#include "mautilus-places-view.h"

struct _mautilusOtherLocationsWindowSlot
{
    mautilusWindowSlot parent_instance;
};

G_DEFINE_TYPE (mautilusOtherLocationsWindowSlot, mautilus_other_locations_window_slot, MAUTILUS_TYPE_WINDOW_SLOT)

static gboolean
real_handles_location (mautilusWindowSlot *self,
                       GFile              *location)
{
    mautilusFile *file;
    gboolean handles_location;

    file = mautilus_file_get (location);
    handles_location = mautilus_file_is_other_locations (file);
    mautilus_file_unref (file);

    return handles_location;
}

static mautilusView *
real_get_view_for_location (mautilusWindowSlot *self,
                            GFile              *location)
{
    return MAUTILUS_VIEW (mautilus_places_view_new ());
}

mautilusOtherLocationsWindowSlot *
mautilus_other_locations_window_slot_new (mautilusWindow *window)
{
    return g_object_new (MAUTILUS_TYPE_OTHER_LOCATIONS_WINDOW_SLOT,
                         "window", window,
                         NULL);
}

static void
mautilus_other_locations_window_slot_class_init (mautilusOtherLocationsWindowSlotClass *klass)
{
    mautilusWindowSlotClass *parent_class = MAUTILUS_WINDOW_SLOT_CLASS (klass);

    parent_class->get_view_for_location = real_get_view_for_location;
    parent_class->handles_location = real_handles_location;
}

static void
mautilus_other_locations_window_slot_init (mautilusOtherLocationsWindowSlot *self)
{
    GAction *action;
    GActionGroup *action_group;

    /* Disable the ability to change between types of views */
    action_group = gtk_widget_get_action_group (GTK_WIDGET (self), "slot");

    action = g_action_map_lookup_action (G_ACTION_MAP (action_group), "files-view-mode");
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), FALSE);
    action = g_action_map_lookup_action (G_ACTION_MAP (action_group), "files-view-mode-toggle");
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action), FALSE);
}
