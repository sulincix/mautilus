/* mautilus-view-icon-ui.h
 *
 * Copyright (C) 2016 Carlos Soriano <csoriano@gnome.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MAUTILUS_VIEW_ICON_UI_H
#define MAUTILUS_VIEW_ICON_UI_H

#include <glib.h>
#include <gtk/gtk.h>

#include "mautilus-view-icon-controller.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_VIEW_ICON_UI (mautilus_view_icon_ui_get_type())

G_DECLARE_FINAL_TYPE (mautilusViewIconUi, mautilus_view_icon_ui, MAUTILUS, VIEW_ICON_UI, GtkFlowBox)

mautilusViewIconUi * mautilus_view_icon_ui_new (mautilusViewIconController *controller);
/* TODO: this should become the "mautilus_view_set_selection" once we have a proper
 * MVC also in the mautilus-view level. */
void mautilus_view_icon_ui_set_selection (mautilusViewIconUi *self,
                                          GQueue             *selection);

G_END_DECLS

#endif /* MAUTILUS_VIEW_ICON_UI_H */

