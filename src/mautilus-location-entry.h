
/*
 * mautilus
 *
 * Copyright (C) 2000 Eazel, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Author: Maciej Stachowiak <mjs@eazel.com>
 *         Ettore Perazzoli <ettore@gnu.org>
 */

#ifndef MAUTILUS_LOCATION_ENTRY_H
#define MAUTILUS_LOCATION_ENTRY_H

#include <gtk/gtk.h>

#define MAUTILUS_TYPE_LOCATION_ENTRY mautilus_location_entry_get_type()
G_DECLARE_DERIVABLE_TYPE (mautilusLocationEntry, mautilus_location_entry,
                          MAUTILUS, LOCATION_ENTRY,
                          GtkEntry)

typedef struct _mautilusLocationEntryClass {
	GtkEntryClass parent_class;
	/* for GtkBindingSet */
	void         (* cancel)           (mautilusLocationEntry *entry);
} mautilusLocationEntryClass;

typedef enum {
	MAUTILUS_LOCATION_ENTRY_ACTION_GOTO,
	MAUTILUS_LOCATION_ENTRY_ACTION_CLEAR
} mautilusLocationEntryAction;

GtkWidget* mautilus_location_entry_new          	(void);
void       mautilus_location_entry_set_special_text     (mautilusLocationEntry *entry,
							 const char            *special_text);
void       mautilus_location_entry_set_secondary_action (mautilusLocationEntry *entry,
							 mautilusLocationEntryAction secondary_action);
void       mautilus_location_entry_set_location         (mautilusLocationEntry *entry,
							 GFile                 *location);

#endif /* MAUTILUS_LOCATION_ENTRY_H */
