
/* 
   mautilus-trash-monitor.h: mautilus trash state watcher.
 
   Copyright (C) 2000 Eazel, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Pavel Cisler <pavel@eazel.com>
*/

#ifndef MAUTILUS_TRASH_MONITOR_H
#define MAUTILUS_TRASH_MONITOR_H

#include <gtk/gtk.h>
#include <gio/gio.h>

typedef struct mautilusTrashMonitor mautilusTrashMonitor;
typedef struct mautilusTrashMonitorClass mautilusTrashMonitorClass;
typedef struct mautilusTrashMonitorDetails mautilusTrashMonitorDetails;

#define MAUTILUS_TYPE_TRASH_MONITOR mautilus_trash_monitor_get_type()
#define MAUTILUS_TRASH_MONITOR(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_TRASH_MONITOR, mautilusTrashMonitor))
#define MAUTILUS_TRASH_MONITOR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_TRASH_MONITOR, mautilusTrashMonitorClass))
#define MAUTILUS_IS_TRASH_MONITOR(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_TRASH_MONITOR))
#define MAUTILUS_IS_TRASH_MONITOR_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_TRASH_MONITOR))
#define MAUTILUS_TRASH_MONITOR_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_TRASH_MONITOR, mautilusTrashMonitorClass))

struct mautilusTrashMonitor {
	GObject object;
	mautilusTrashMonitorDetails *details;
};

struct mautilusTrashMonitorClass {
	GObjectClass parent_class;

	void (* trash_state_changed)		(mautilusTrashMonitor 	*trash_monitor,
				      		 gboolean 		 new_state);
};

GType			mautilus_trash_monitor_get_type				(void);

mautilusTrashMonitor   *mautilus_trash_monitor_get 				(void);
gboolean		mautilus_trash_monitor_is_empty 			(void);
GIcon                  *mautilus_trash_monitor_get_icon                         (void);

#endif
