/* mautilus-global-preferences.c - mautilus specific preference keys and
 *                                  functions.
 *
 *  Copyright (C) 1999, 2000, 2001 Eazel, Inc.
 *
 *  The Gnome Library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  The Gnome Library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with the Gnome Library; see the file COPYING.LIB.  If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *  Authors: Ramiro Estrugo <ramiro@eazel.com>
 */

#include <config.h>
#include "mautilus-global-preferences.h"

#include "mautilus-file-utilities.h"
#include "mautilus-file.h"
#include "src/mautilus-files-view.h"
#include <eel/eel-glib-extensions.h>
#include <eel/eel-gtk-extensions.h>
#include <eel/eel-stock-dialogs.h>
#include <eel/eel-string.h>
#include <glib/gi18n.h>

GSettings *mautilus_preferences;
GSettings *mautilus_compression_preferences;
GSettings *mautilus_icon_view_preferences;
GSettings *mautilus_list_view_preferences;
GSettings *mautilus_desktop_preferences;
GSettings *mautilus_window_state;
GSettings *gtk_filechooser_preferences;
GSettings *gnome_lockdown_preferences;
GSettings *gnome_background_preferences;
GSettings *gnome_interface_preferences;
GSettings *gnome_privacy_preferences;

void
mautilus_global_preferences_init (void)
{
    static gboolean initialized = FALSE;

    if (initialized)
    {
        return;
    }

    initialized = TRUE;

    mautilus_preferences = g_settings_new ("org.gnome.mautilus.preferences");
    mautilus_compression_preferences = g_settings_new ("org.gnome.mautilus.compression");
    mautilus_window_state = g_settings_new ("org.gnome.mautilus.window-state");
    mautilus_icon_view_preferences = g_settings_new ("org.gnome.mautilus.icon-view");
    mautilus_list_view_preferences = g_settings_new ("org.gnome.mautilus.list-view");
    mautilus_desktop_preferences = g_settings_new ("org.gnome.mautilus.desktop");
    /* Some settings such as show hidden files are shared between mautilus and GTK file chooser */
    gtk_filechooser_preferences = g_settings_new_with_path ("org.gtk.Settings.FileChooser",
                                                            "/org/gtk/settings/file-chooser/");
    gnome_lockdown_preferences = g_settings_new ("org.gnome.desktop.lockdown");
    gnome_background_preferences = g_settings_new ("org.gnome.desktop.background");
    gnome_interface_preferences = g_settings_new ("org.gnome.desktop.interface");
    gnome_privacy_preferences = g_settings_new ("org.gnome.desktop.privacy");
}
