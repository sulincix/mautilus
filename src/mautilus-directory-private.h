/*
   mautilus-directory-private.h: mautilus directory model.
 
   Copyright (C) 1999, 2000, 2001 Eazel, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Darin Adler <darin@bentspoon.com>
*/

#include <gio/gio.h>
#include <eel/eel-vfs-extensions.h>
#include "mautilus-directory.h"
#include "mautilus-file-queue.h"
#include "mautilus-file.h"
#include "mautilus-monitor.h"
#include <libmautilus-extension/mautilus-info-provider.h>
#include <libxml/tree.h>

typedef struct LinkInfoReadState LinkInfoReadState;
typedef struct TopLeftTextReadState TopLeftTextReadState;
typedef struct FileMonitors FileMonitors;
typedef struct DirectoryLoadState DirectoryLoadState;
typedef struct DirectoryCountState DirectoryCountState;
typedef struct DeepCountState DeepCountState;
typedef struct GetInfoState GetInfoState;
typedef struct NewFilesState NewFilesState;
typedef struct MimeListState MimeListState;
typedef struct ThumbnailState ThumbnailState;
typedef struct MountState MountState;
typedef struct FilesystemInfoState FilesystemInfoState;

typedef enum {
	REQUEST_LINK_INFO,
	REQUEST_DEEP_COUNT,
	REQUEST_DIRECTORY_COUNT,
	REQUEST_FILE_INFO,
	REQUEST_FILE_LIST, /* always FALSE if file != NULL */
	REQUEST_MIME_LIST,
	REQUEST_EXTENSION_INFO,
	REQUEST_THUMBNAIL,
	REQUEST_MOUNT,
	REQUEST_FILESYSTEM_INFO,
	REQUEST_TYPE_LAST
} RequestType;

/* A request for information about one or more files. */
typedef guint32 Request;
typedef gint32 RequestCounter[REQUEST_TYPE_LAST];

#define REQUEST_WANTS_TYPE(request, type) ((request) & (1<<(type)))
#define REQUEST_SET_TYPE(request, type) (request) |= (1<<(type))

struct mautilusDirectoryDetails
{
	/* The location. */
	GFile *location;

	/* The file objects. */
	mautilusFile *as_file;
	GList *file_list;
	GHashTable *file_hash;

	/* Queues of files needing some I/O done. */
	mautilusFileQueue *high_priority_queue;
	mautilusFileQueue *low_priority_queue;
	mautilusFileQueue *extension_queue;

	/* These lists are going to be pretty short.  If we think they
	 * are going to get big, we can use hash tables instead.
	 */
	GList *call_when_ready_list;
	RequestCounter call_when_ready_counters;
	GList *monitor_list;
	RequestCounter monitor_counters;
	guint call_ready_idle_id;

	mautilusMonitor *monitor;
	gulong 		 mime_db_monitor;

	gboolean in_async_service_loop;
	gboolean state_changed;

	gboolean file_list_monitored;
	gboolean directory_loaded;
	gboolean directory_loaded_sent_notification;
	DirectoryLoadState *directory_load_in_progress;

	GList *pending_file_info; /* list of GnomeVFSFileInfo's that are pending */
	int confirmed_file_count;
        guint dequeue_pending_idle_id;

	GList *new_files_in_progress; /* list of NewFilesState * */

	DirectoryCountState *count_in_progress;

	mautilusFile *deep_count_file;
	DeepCountState *deep_count_in_progress;

	MimeListState *mime_list_in_progress;

	mautilusFile *get_info_file;
	GetInfoState *get_info_in_progress;

	mautilusFile *extension_info_file;
	mautilusInfoProvider *extension_info_provider;
	mautilusOperationHandle *extension_info_in_progress;
	guint extension_info_idle;

	ThumbnailState *thumbnail_state;

	MountState *mount_state;

	FilesystemInfoState *filesystem_info_state;
	
	LinkInfoReadState *link_info_read_state;

	GList *file_operations_in_progress; /* list of FileOperation * */
};

mautilusDirectory *mautilus_directory_get_existing                    (GFile                     *location);

/* async. interface */
void               mautilus_directory_async_state_changed             (mautilusDirectory         *directory);
void               mautilus_directory_call_when_ready_internal        (mautilusDirectory         *directory,
								       mautilusFile              *file,
								       mautilusFileAttributes     file_attributes,
								       gboolean                   wait_for_file_list,
								       mautilusDirectoryCallback  directory_callback,
								       mautilusFileCallback       file_callback,
								       gpointer                   callback_data);
gboolean           mautilus_directory_check_if_ready_internal         (mautilusDirectory         *directory,
								       mautilusFile              *file,
								       mautilusFileAttributes     file_attributes);
void               mautilus_directory_cancel_callback_internal        (mautilusDirectory         *directory,
								       mautilusFile              *file,
								       mautilusDirectoryCallback  directory_callback,
								       mautilusFileCallback       file_callback,
								       gpointer                   callback_data);
void               mautilus_directory_monitor_add_internal            (mautilusDirectory         *directory,
								       mautilusFile              *file,
								       gconstpointer              client,
								       gboolean                   monitor_hidden_files,
								       mautilusFileAttributes     attributes,
								       mautilusDirectoryCallback  callback,
								       gpointer                   callback_data);
void               mautilus_directory_monitor_remove_internal         (mautilusDirectory         *directory,
								       mautilusFile              *file,
								       gconstpointer              client);
void               mautilus_directory_get_info_for_new_files          (mautilusDirectory         *directory,
								       GList                     *vfs_uris);
mautilusFile *     mautilus_directory_get_existing_corresponding_file (mautilusDirectory         *directory);
void               mautilus_directory_invalidate_count_and_mime_list  (mautilusDirectory         *directory);
gboolean           mautilus_directory_is_file_list_monitored          (mautilusDirectory         *directory);
gboolean           mautilus_directory_is_anyone_monitoring_file_list  (mautilusDirectory         *directory);
gboolean           mautilus_directory_has_active_request_for_file     (mautilusDirectory         *directory,
								       mautilusFile              *file);
void               mautilus_directory_remove_file_monitor_link        (mautilusDirectory         *directory,
								       GList                     *link);
void               mautilus_directory_schedule_dequeue_pending        (mautilusDirectory         *directory);
void               mautilus_directory_stop_monitoring_file_list       (mautilusDirectory         *directory);
void               mautilus_directory_cancel                          (mautilusDirectory         *directory);
void               mautilus_async_destroying_file                     (mautilusFile              *file);
void               mautilus_directory_force_reload_internal           (mautilusDirectory         *directory,
								       mautilusFileAttributes     file_attributes);
void               mautilus_directory_cancel_loading_file_attributes  (mautilusDirectory         *directory,
								       mautilusFile              *file,
								       mautilusFileAttributes     file_attributes);

/* Calls shared between directory, file, and async. code. */
void               mautilus_directory_emit_files_added                (mautilusDirectory         *directory,
								       GList                     *added_files);
void               mautilus_directory_emit_files_changed              (mautilusDirectory         *directory,
								       GList                     *changed_files);
void               mautilus_directory_emit_change_signals             (mautilusDirectory         *directory,
								       GList                     *changed_files);
void               emit_change_signals_for_all_files		      (mautilusDirectory	 *directory);
void               emit_change_signals_for_all_files_in_all_directories (void);
void               mautilus_directory_emit_done_loading               (mautilusDirectory         *directory);
void               mautilus_directory_emit_load_error                 (mautilusDirectory         *directory,
								       GError                    *error);
mautilusDirectory *mautilus_directory_get_internal                    (GFile                     *location,
								       gboolean                   create);
char *             mautilus_directory_get_name_for_self_as_new_file   (mautilusDirectory         *directory);
Request            mautilus_directory_set_up_request                  (mautilusFileAttributes     file_attributes);

/* Interface to the file list. */
mautilusFile *     mautilus_directory_find_file_by_name               (mautilusDirectory         *directory,
								       const char                *filename);

void               mautilus_directory_add_file                        (mautilusDirectory         *directory,
								       mautilusFile              *file);
void               mautilus_directory_remove_file                     (mautilusDirectory         *directory,
								       mautilusFile              *file);
FileMonitors *     mautilus_directory_remove_file_monitors            (mautilusDirectory         *directory,
								       mautilusFile              *file);
void               mautilus_directory_add_file_monitors               (mautilusDirectory         *directory,
								       mautilusFile              *file,
								       FileMonitors              *monitors);
void               mautilus_directory_add_file                        (mautilusDirectory         *directory,
								       mautilusFile              *file);
GList *            mautilus_directory_begin_file_name_change          (mautilusDirectory         *directory,
								       mautilusFile              *file);
void               mautilus_directory_end_file_name_change            (mautilusDirectory         *directory,
								       mautilusFile              *file,
								       GList                     *node);
void               mautilus_directory_moved                           (const char                *from_uri,
								       const char                *to_uri);
/* Interface to the work queue. */

void               mautilus_directory_add_file_to_work_queue          (mautilusDirectory *directory,
								       mautilusFile *file);
void               mautilus_directory_remove_file_from_work_queue     (mautilusDirectory *directory,
								       mautilusFile *file);


/* debugging functions */
int                mautilus_directory_number_outstanding              (void);
