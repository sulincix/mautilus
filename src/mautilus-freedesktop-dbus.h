/*
 * mautilus-freedesktop-dbus: Implementation for the org.freedesktop DBus file-management interfaces
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Akshay Gupta <kitallis@gmail.com>
 *          Federico Mena Quintero <federico@gnome.org>
 */


#ifndef __MAUTILUS_FREEDESKTOP_DBUS_H__
#define __MAUTILUS_FREEDESKTOP_DBUS_H__

#include <glib-object.h>

#define MAUTILUS_FDO_DBUS_IFACE "org.freedesktop.FileManager1"
#define MAUTILUS_FDO_DBUS_NAME  "org.freedesktop.FileManager1"
#define MAUTILUS_FDO_DBUS_PATH  "/org/freedesktop/FileManager1"

#define MAUTILUS_TYPE_FREEDESKTOP_DBUS mautilus_freedesktop_dbus_get_type()
#define MAUTILUS_FREEDESKTOP_DBUS(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_FREEDESKTOP_DBUS, mautilusFreedesktopDBus))
#define MAUTILUS_FREEDESKTOP_DBUS_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_FREEDESKTOP_DBUS, mautilusFreedesktopDBusClass))
#define MAUTILUS_IS_FREEDESKTOP_DBUS(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_FREEDESKTOP_DBUS))
#define MAUTILUS_IS_FREEDESKTOP_DBUS_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_FREEDESKTOP_DBUS))
#define MAUTILUS_FREEDESKTOP_DBUS_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_FREEDESKTOP_DBUS, mautilusFreedesktopDBusClass))

typedef struct _mautilusFreedesktopDBus mautilusFreedesktopDBus;
typedef struct _mautilusFreedesktopDBusClass mautilusFreedesktopDBusClass;

GType mautilus_freedesktop_dbus_get_type (void);
mautilusFreedesktopDBus * mautilus_freedesktop_dbus_new (void);

void mautilus_freedesktop_dbus_set_open_locations (mautilusFreedesktopDBus *fdb, const gchar **locations);

#endif /* __MAUTILUS_FREEDESKTOP_DBUS_H__ */
