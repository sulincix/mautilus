
/* mautilus - Canvas item for floating selection.
 *
 * Copyright (C) 1997, 1998, 1999, 2000 Free Software Foundation
 * Copyright (C) 2011 Red Hat Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Federico Mena <federico@nuclecu.unam.mx>
 *          Cosimo Cecchi <cosimoc@redhat.com>
 */

#ifndef __MAUTILUS_SELECTION_CANVAS_ITEM_H__
#define __MAUTILUS_SELECTION_CANVAS_ITEM_H__

#include <eel/eel-canvas.h>

G_BEGIN_DECLS

#define MAUTILUS_TYPE_SELECTION_CANVAS_ITEM mautilus_selection_canvas_item_get_type()
#define MAUTILUS_SELECTION_CANVAS_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_SELECTION_CANVAS_ITEM, mautilusSelectionCanvasItem))
#define MAUTILUS_SELECTION_CANVAS_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_SELECTION_CANVAS_ITEM, mautilusSelectionCanvasItemClass))
#define MAUTILUS_IS_SELECTION_CANVAS_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_SELECTION_CANVAS_ITEM))
#define MAUTILUS_IS_SELECTION_CANVAS_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_SELECTION_CANVAS_ITEM))
#define MAUTILUS_SELECTION_CANVAS_ITEM_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_SELECTION_CANVAS_ITEM, mautilusSelectionCanvasItemClass))

typedef struct _mautilusSelectionCanvasItem mautilusSelectionCanvasItem;
typedef struct _mautilusSelectionCanvasItemClass mautilusSelectionCanvasItemClass;
typedef struct _mautilusSelectionCanvasItemDetails mautilusSelectionCanvasItemDetails;

struct _mautilusSelectionCanvasItem {
	EelCanvasItem item;
	mautilusSelectionCanvasItemDetails *priv;
	gpointer user_data;
};

struct _mautilusSelectionCanvasItemClass {
	EelCanvasItemClass parent_class;
};

/* GObject */
GType       mautilus_selection_canvas_item_get_type                 (void);

void mautilus_selection_canvas_item_fade_out (mautilusSelectionCanvasItem *self,
					      guint transition_time);

G_END_DECLS

#endif /* __MAUTILUS_SELECTION_CANVAS_ITEM_H__ */
