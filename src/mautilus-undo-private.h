
/* xxx
 *
 * Copyright (C) 2000 Eazel, Inc.
 *
 * Author: Gene Z. Ragan <gzr@eazel.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAUTILUS_UNDO_PRIVATE_H
#define MAUTILUS_UNDO_PRIVATE_H

#include "mautilus-undo.h"
#include "mautilus-undo-manager.h"
#include <glib-object.h>

mautilusUndoManager * mautilus_undo_get_undo_manager    (GObject               *attached_object);
void                  mautilus_undo_attach_undo_manager (GObject               *object,
							 mautilusUndoManager   *manager);

#endif /* MAUTILUS_UNDO_PRIVATE_H */
