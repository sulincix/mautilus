
/* mautilus-column-choose.h - A column chooser widget

   Copyright (C) 2004 Novell, Inc.

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the column COPYING.LIB.  If not,
   see <http://www.gnu.org/licenses/>.

   Authors: Dave Camp <dave@ximian.com>
*/

#ifndef MAUTILUS_COLUMN_CHOOSER_H
#define MAUTILUS_COLUMN_CHOOSER_H

#include <gtk/gtk.h>
#include "mautilus-file.h"

#define MAUTILUS_TYPE_COLUMN_CHOOSER mautilus_column_chooser_get_type()

G_DECLARE_FINAL_TYPE (mautilusColumnChooser, mautilus_column_chooser, MAUTILUS, COLUMN_CHOOSER, GtkBox);

GtkWidget *mautilus_column_chooser_new                 (mautilusFile *file);
void       mautilus_column_chooser_set_settings    (mautilusColumnChooser   *chooser,
						    char                   **visible_columns, 
						    char                   **column_order);
void       mautilus_column_chooser_get_settings    (mautilusColumnChooser *chooser,
						    char                  ***visible_columns, 
						    char                  ***column_order);

#endif /* MAUTILUS_COLUMN_CHOOSER_H */
