
/* fm-icon-container.h - the container widget for file manager icons

   Copyright (C) 2002 Sun Microsystems, Inc.

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   see <http://www.gnu.org/licenses/>.

   Author: Michael Meeks <michael@ximian.com>
*/

#ifndef MAUTILUS_CANVAS_VIEW_CONTAINER_H
#define MAUTILUS_CANVAS_VIEW_CONTAINER_H

#include "mautilus-canvas-view.h"

#include "mautilus-canvas-container.h"

typedef struct mautilusCanvasViewContainer mautilusCanvasViewContainer;
typedef struct mautilusCanvasViewContainerClass mautilusCanvasViewContainerClass;

#define MAUTILUS_TYPE_CANVAS_VIEW_CONTAINER mautilus_canvas_view_container_get_type()
#define MAUTILUS_CANVAS_VIEW_CONTAINER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_CANVAS_VIEW_CONTAINER, mautilusCanvasViewContainer))
#define MAUTILUS_CANVAS_VIEW_CONTAINER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_CANVAS_VIEW_CONTAINER, mautilusCanvasViewContainerClass))
#define MAUTILUS_IS_CANVAS_VIEW_CONTAINER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_CANVAS_VIEW_CONTAINER))
#define MAUTILUS_IS_CANVAS_VIEW_CONTAINER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_CANVAS_VIEW_CONTAINER))
#define MAUTILUS_CANVAS_VIEW_CONTAINER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_CANVAS_VIEW_CONTAINER, mautilusCanvasViewContainerClass))

G_DEFINE_AUTOPTR_CLEANUP_FUNC(mautilusCanvasViewContainer, g_object_unref)

typedef struct mautilusCanvasViewContainerDetails mautilusCanvasViewContainerDetails;

struct mautilusCanvasViewContainer {
	mautilusCanvasContainer parent;

	mautilusCanvasView *view;
};

struct mautilusCanvasViewContainerClass {
	mautilusCanvasContainerClass parent_class;
};

GType                  mautilus_canvas_view_container_get_type         (void);
mautilusCanvasContainer *mautilus_canvas_view_container_construct        (mautilusCanvasViewContainer *canvas_container,
								      mautilusCanvasView      *view);
mautilusCanvasContainer *mautilus_canvas_view_container_new              (mautilusCanvasView      *view);

#endif /* MAUTILUS_CANVAS_VIEW_CONTAINER_H */
