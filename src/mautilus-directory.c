/*
 *  mautilus-directory.c: mautilus directory model.
 *
 *  Copyright (C) 1999, 2000, 2001 Eazel, Inc.
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Darin Adler <darin@bentspoon.com>
 */

#include <config.h>
#include "mautilus-directory-private.h"

#include "mautilus-directory-notify.h"
#include "mautilus-file-attributes.h"
#include "mautilus-file-private.h"
#include "mautilus-file-utilities.h"
#include "mautilus-search-directory.h"
#include "mautilus-favorite-directory.h"
#include "mautilus-search-directory-file.h"
#include "mautilus-vfs-file.h"
#include "mautilus-global-preferences.h"
#include "mautilus-lib-self-check-functions.h"
#include "mautilus-metadata.h"
#include "mautilus-profile.h"
#include "mautilus-vfs-directory.h"
#include <eel/eel-glib-extensions.h>
#include <eel/eel-string.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>

enum
{
    FILES_ADDED,
    FILES_CHANGED,
    DONE_LOADING,
    LOAD_ERROR,
    LAST_SIGNAL
};

enum
{
    PROP_LOCATION = 1,
    NUM_PROPERTIES
};

static guint signals[LAST_SIGNAL];
static GParamSpec *properties[NUM_PROPERTIES] = { NULL, };

static GHashTable *directories;

static mautilusDirectory *mautilus_directory_new (GFile *location);
static void               set_directory_location (mautilusDirectory *directory,
                                                  GFile             *location);

G_DEFINE_TYPE (mautilusDirectory, mautilus_directory, G_TYPE_OBJECT);

static gboolean
real_contains_file (mautilusDirectory *self,
                    mautilusFile      *file)
{
    mautilusDirectory *directory;

    directory = mautilus_file_get_directory (file);

    return directory == self;
}

static gboolean
real_are_all_files_seen (mautilusDirectory *directory)
{
    return directory->details->directory_loaded;
}

static gboolean
real_is_not_empty (mautilusDirectory *directory)
{
    return directory->details->file_list != NULL;
}

static gboolean
is_tentative (mautilusFile *file,
              gpointer      callback_data)
{
    g_assert (callback_data == NULL);

    /* Avoid returning files with !is_added, because these
     * will later be sent with the files_added signal, and a
     * user doing get_file_list + files_added monitoring will
     * then see the file twice */
    return !file->details->got_file_info || !file->details->is_added;
}

static GList *
real_get_file_list (mautilusDirectory *directory)
{
    GList *tentative_files, *non_tentative_files;

    tentative_files = mautilus_file_list_filter (directory->details->file_list,
                                                 &non_tentative_files, is_tentative, NULL);
    mautilus_file_list_free (tentative_files);

    return non_tentative_files;
}

static gboolean
real_is_editable (mautilusDirectory *directory)
{
    return TRUE;
}

static mautilusFile *
real_new_file_from_filename (mautilusDirectory *directory,
                             const char        *filename,
                             gboolean           self_owned)
{
    mautilusFile *file;

    g_assert (MAUTILUS_IS_DIRECTORY (directory));
    g_assert (filename != NULL);
    g_assert (filename[0] != '\0');

    if (MAUTILUS_IS_SEARCH_DIRECTORY (directory))
    {
        if (self_owned)
        {
            file = MAUTILUS_FILE (g_object_new (MAUTILUS_TYPE_SEARCH_DIRECTORY_FILE, NULL));
        }
        else
        {
            /* This doesn't normally happen, unless the user somehow types in a uri
             * that references a file like this. (See #349840) */
            file = MAUTILUS_FILE (g_object_new (MAUTILUS_TYPE_VFS_FILE, NULL));
        }
    }
    else
    {
        file = MAUTILUS_FILE (g_object_new (MAUTILUS_TYPE_VFS_FILE, NULL));
    }
    mautilus_file_set_directory (file, directory);

    return file;
}

static gboolean
real_handles_location (GFile *location)
{
    /* This class is the fallback on handling any location */
    return TRUE;
}

static void
mautilus_directory_finalize (GObject *object)
{
    mautilusDirectory *directory;

    directory = MAUTILUS_DIRECTORY (object);

    g_hash_table_remove (directories, directory->details->location);

    mautilus_directory_cancel (directory);
    g_assert (directory->details->count_in_progress == NULL);

    if (directory->details->monitor_list != NULL)
    {
        g_warning ("destroying a mautilusDirectory while it's being monitored");
        g_list_free_full (directory->details->monitor_list, g_free);
    }

    if (directory->details->monitor != NULL)
    {
        mautilus_monitor_cancel (directory->details->monitor);
    }

    if (directory->details->dequeue_pending_idle_id != 0)
    {
        g_source_remove (directory->details->dequeue_pending_idle_id);
    }

    if (directory->details->call_ready_idle_id != 0)
    {
        g_source_remove (directory->details->call_ready_idle_id);
    }

    if (directory->details->location)
    {
        g_object_unref (directory->details->location);
    }

    g_assert (directory->details->file_list == NULL);
    g_hash_table_destroy (directory->details->file_hash);

    mautilus_file_queue_destroy (directory->details->high_priority_queue);
    mautilus_file_queue_destroy (directory->details->low_priority_queue);
    mautilus_file_queue_destroy (directory->details->extension_queue);
    g_assert (directory->details->directory_load_in_progress == NULL);
    g_assert (directory->details->count_in_progress == NULL);
    g_assert (directory->details->dequeue_pending_idle_id == 0);
    g_list_free_full (directory->details->pending_file_info, g_object_unref);

    G_OBJECT_CLASS (mautilus_directory_parent_class)->finalize (object);
}

static void
mautilus_directory_set_property (GObject      *object,
                                 guint         property_id,
                                 const GValue *value,
                                 GParamSpec   *pspec)
{
    mautilusDirectory *directory = MAUTILUS_DIRECTORY (object);

    switch (property_id)
    {
        case PROP_LOCATION:
        {
            set_directory_location (directory, g_value_get_object (value));
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
mautilus_directory_get_property (GObject    *object,
                                 guint       property_id,
                                 GValue     *value,
                                 GParamSpec *pspec)
{
    mautilusDirectory *directory = MAUTILUS_DIRECTORY (object);

    switch (property_id)
    {
        case PROP_LOCATION:
        {
            g_value_set_object (value, directory->details->location);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
mautilus_directory_class_init (mautilusDirectoryClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    klass->contains_file = real_contains_file;
    klass->are_all_files_seen = real_are_all_files_seen;
    klass->is_not_empty = real_is_not_empty;
    klass->get_file_list = real_get_file_list;
    klass->is_editable = real_is_editable;
    klass->new_file_from_filename = real_new_file_from_filename;
    klass->handles_location = real_handles_location;

    object_class->finalize = mautilus_directory_finalize;
    object_class->set_property = mautilus_directory_set_property;
    object_class->get_property = mautilus_directory_get_property;

    signals[FILES_ADDED] =
        g_signal_new ("files-added",
                      G_TYPE_FROM_CLASS (object_class),
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET (mautilusDirectoryClass, files_added),
                      NULL, NULL,
                      g_cclosure_marshal_VOID__POINTER,
                      G_TYPE_NONE, 1, G_TYPE_POINTER);
    signals[FILES_CHANGED] =
        g_signal_new ("files-changed",
                      G_TYPE_FROM_CLASS (object_class),
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET (mautilusDirectoryClass, files_changed),
                      NULL, NULL,
                      g_cclosure_marshal_VOID__POINTER,
                      G_TYPE_NONE, 1, G_TYPE_POINTER);
    signals[DONE_LOADING] =
        g_signal_new ("done-loading",
                      G_TYPE_FROM_CLASS (object_class),
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET (mautilusDirectoryClass, done_loading),
                      NULL, NULL,
                      g_cclosure_marshal_VOID__VOID,
                      G_TYPE_NONE, 0);
    signals[LOAD_ERROR] =
        g_signal_new ("load-error",
                      G_TYPE_FROM_CLASS (object_class),
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET (mautilusDirectoryClass, load_error),
                      NULL, NULL,
                      g_cclosure_marshal_VOID__POINTER,
                      G_TYPE_NONE, 1, G_TYPE_POINTER);

    properties[PROP_LOCATION] =
        g_param_spec_object ("location",
                             "The location",
                             "The location of this directory",
                             G_TYPE_FILE,
                             G_PARAM_CONSTRUCT_ONLY | G_PARAM_READWRITE);

    g_type_class_add_private (klass, sizeof (mautilusDirectoryDetails));
    g_object_class_install_properties (object_class, NUM_PROPERTIES, properties);
}

static void
mautilus_directory_init (mautilusDirectory *directory)
{
    directory->details = G_TYPE_INSTANCE_GET_PRIVATE ((directory), MAUTILUS_TYPE_DIRECTORY, mautilusDirectoryDetails);
    directory->details->file_hash = g_hash_table_new_full (g_str_hash, g_str_equal,
                                                           g_free, NULL);
    directory->details->high_priority_queue = mautilus_file_queue_new ();
    directory->details->low_priority_queue = mautilus_file_queue_new ();
    directory->details->extension_queue = mautilus_file_queue_new ();
}

mautilusDirectory *
mautilus_directory_ref (mautilusDirectory *directory)
{
    if (directory == NULL)
    {
        return directory;
    }

    g_return_val_if_fail (MAUTILUS_IS_DIRECTORY (directory), NULL);

    g_object_ref (directory);
    return directory;
}

void
mautilus_directory_unref (mautilusDirectory *directory)
{
    if (directory == NULL)
    {
        return;
    }

    g_return_if_fail (MAUTILUS_IS_DIRECTORY (directory));

    g_object_unref (directory);
}

static void
collect_all_directories (gpointer key,
                         gpointer value,
                         gpointer callback_data)
{
    mautilusDirectory *directory;
    GList **dirs;

    directory = MAUTILUS_DIRECTORY (value);
    dirs = callback_data;

    *dirs = g_list_prepend (*dirs, mautilus_directory_ref (directory));
}

static void
filtering_changed_callback (gpointer callback_data)
{
    GList *dirs, *l;
    mautilusDirectory *directory;

    g_assert (callback_data == NULL);

    dirs = NULL;
    g_hash_table_foreach (directories, collect_all_directories, &dirs);

    /* Preference about which items to show has changed, so we
     * can't trust any of our precomputed directory counts.
     */
    for (l = dirs; l != NULL; l = l->next)
    {
        directory = MAUTILUS_DIRECTORY (l->data);
        mautilus_directory_invalidate_count_and_mime_list (directory);
    }

    mautilus_directory_list_unref (dirs);
}

void
emit_change_signals_for_all_files (mautilusDirectory *directory)
{
    GList *files;

    files = g_list_copy (directory->details->file_list);
    if (directory->details->as_file != NULL)
    {
        files = g_list_prepend (files, directory->details->as_file);
    }

    mautilus_file_list_ref (files);
    mautilus_directory_emit_change_signals (directory, files);

    mautilus_file_list_free (files);
}

void
emit_change_signals_for_all_files_in_all_directories (void)
{
    GList *dirs, *l;
    mautilusDirectory *directory;

    dirs = NULL;
    g_hash_table_foreach (directories,
                          collect_all_directories,
                          &dirs);

    for (l = dirs; l != NULL; l = l->next)
    {
        directory = MAUTILUS_DIRECTORY (l->data);
        emit_change_signals_for_all_files (directory);
        mautilus_directory_unref (directory);
    }

    g_list_free (dirs);
}

static void
async_state_changed_one (gpointer key,
                         gpointer value,
                         gpointer user_data)
{
    mautilusDirectory *directory;

    g_assert (key != NULL);
    g_assert (MAUTILUS_IS_DIRECTORY (value));
    g_assert (user_data == NULL);

    directory = MAUTILUS_DIRECTORY (value);

    mautilus_directory_async_state_changed (directory);
    emit_change_signals_for_all_files (directory);
}

static void
async_data_preference_changed_callback (gpointer callback_data)
{
    g_assert (callback_data == NULL);

    /* Preference involving fetched async data has changed, so
     * we have to kick off refetching all async data, and tell
     * each file that it (might have) changed.
     */
    g_hash_table_foreach (directories, async_state_changed_one, NULL);
}

static void
add_preferences_callbacks (void)
{
    mautilus_global_preferences_init ();

    g_signal_connect_swapped (gtk_filechooser_preferences,
                              "changed::" MAUTILUS_PREFERENCES_SHOW_HIDDEN_FILES,
                              G_CALLBACK (filtering_changed_callback),
                              NULL);
    g_signal_connect_swapped (mautilus_preferences,
                              "changed::" MAUTILUS_PREFERENCES_SHOW_DIRECTORY_ITEM_COUNTS,
                              G_CALLBACK (async_data_preference_changed_callback),
                              NULL);
}

/**
 * mautilus_directory_get_by_uri:
 * @uri: URI of directory to get.
 *
 * Get a directory given a uri.
 * Creates the appropriate subclass given the uri mappings.
 * Returns a referenced object, not a floating one. Unref when finished.
 * If two windows are viewing the same uri, the directory object is shared.
 */
mautilusDirectory *
mautilus_directory_get_internal (GFile    *location,
                                 gboolean  create)
{
    mautilusDirectory *directory;

    /* Create the hash table first time through. */
    if (directories == NULL)
    {
        directories = g_hash_table_new (g_file_hash, (GCompareFunc) g_file_equal);
        add_preferences_callbacks ();
    }

    /* If the object is already in the hash table, look it up. */

    directory = g_hash_table_lookup (directories,
                                     location);
    if (directory != NULL)
    {
        mautilus_directory_ref (directory);
    }
    else if (create)
    {
        /* Create a new directory object instead. */
        directory = mautilus_directory_new (location);
        if (directory == NULL)
        {
            return NULL;
        }

        /* Put it in the hash table. */
        g_hash_table_insert (directories,
                             directory->details->location,
                             directory);
    }

    return directory;
}

mautilusDirectory *
mautilus_directory_get (GFile *location)
{
    if (location == NULL)
    {
        return NULL;
    }

    return mautilus_directory_get_internal (location, TRUE);
}

mautilusDirectory *
mautilus_directory_get_existing (GFile *location)
{
    if (location == NULL)
    {
        return NULL;
    }

    return mautilus_directory_get_internal (location, FALSE);
}


mautilusDirectory *
mautilus_directory_get_by_uri (const char *uri)
{
    mautilusDirectory *directory;
    GFile *location;

    if (uri == NULL)
    {
        return NULL;
    }

    location = g_file_new_for_uri (uri);

    directory = mautilus_directory_get_internal (location, TRUE);
    g_object_unref (location);
    return directory;
}

mautilusDirectory *
mautilus_directory_get_for_file (mautilusFile *file)
{
    char *uri;
    mautilusDirectory *directory;

    g_return_val_if_fail (MAUTILUS_IS_FILE (file), NULL);

    uri = mautilus_file_get_uri (file);
    directory = mautilus_directory_get_by_uri (uri);
    g_free (uri);
    return directory;
}

/* Returns a reffed mautilusFile object for this directory.
 */
mautilusFile *
mautilus_directory_get_corresponding_file (mautilusDirectory *directory)
{
    mautilusFile *file;
    char *uri;

    file = mautilus_directory_get_existing_corresponding_file (directory);
    if (file == NULL)
    {
        uri = mautilus_directory_get_uri (directory);
        file = mautilus_file_get_by_uri (uri);
        g_free (uri);
    }

    return file;
}

/* Returns a reffed mautilusFile object for this directory, but only if the
 * mautilusFile object has already been created.
 */
mautilusFile *
mautilus_directory_get_existing_corresponding_file (mautilusDirectory *directory)
{
    mautilusFile *file;
    char *uri;

    file = directory->details->as_file;
    if (file != NULL)
    {
        mautilus_file_ref (file);
        return file;
    }

    uri = mautilus_directory_get_uri (directory);
    file = mautilus_file_get_existing_by_uri (uri);
    g_free (uri);
    return file;
}

/* mautilus_directory_get_name_for_self_as_new_file:
 *
 * Get a name to display for the file representing this
 * directory. This is called only when there's no VFS
 * directory for this mautilusDirectory.
 */
char *
mautilus_directory_get_name_for_self_as_new_file (mautilusDirectory *directory)
{
    GFile *file;
    char *directory_uri;
    char *scheme;
    char *name;
    char *hostname = NULL;

    directory_uri = mautilus_directory_get_uri (directory);
    file = g_file_new_for_uri (directory_uri);
    scheme = g_file_get_uri_scheme (file);
    g_object_unref (file);

    mautilus_uri_parse (directory_uri, &hostname, NULL, NULL);
    if (hostname == NULL || (strlen (hostname) == 0))
    {
        name = g_strdup (directory_uri);
    }
    else if (scheme == NULL)
    {
        name = g_strdup (hostname);
    }
    else
    {
        /* Translators: this is of the format "hostname (uri-scheme)" */
        name = g_strdup_printf (_("%s (%s)"), hostname, scheme);
    }

    g_free (directory_uri);
    g_free (scheme);
    g_free (hostname);

    return name;
}

char *
mautilus_directory_get_uri (mautilusDirectory *directory)
{
    g_return_val_if_fail (MAUTILUS_IS_DIRECTORY (directory), NULL);

    return g_file_get_uri (directory->details->location);
}

GFile *
mautilus_directory_get_location (mautilusDirectory *directory)
{
    g_return_val_if_fail (MAUTILUS_IS_DIRECTORY (directory), NULL);

    return g_object_ref (directory->details->location);
}

mautilusFile *
mautilus_directory_new_file_from_filename (mautilusDirectory *directory,
                                           const char        *filename,
                                           gboolean           self_owned)
{
    return MAUTILUS_DIRECTORY_CLASS (G_OBJECT_GET_CLASS (directory))->new_file_from_filename (directory,
                                                                                              filename,
                                                                                              self_owned);
}

static GList *
mautilus_directory_provider_get_all (void)
{
    GIOExtensionPoint *extension_point;
    GList *extensions;

    extension_point = g_io_extension_point_lookup (MAUTILUS_DIRECTORY_PROVIDER_EXTENSION_POINT_NAME);
    extensions = g_io_extension_point_get_extensions (extension_point);

    return extensions;
}

static mautilusDirectory *
mautilus_directory_new (GFile *location)
{
    GList *extensions;
    GList *l;
    GIOExtension *gio_extension;
    GType handling_provider_type;
    gboolean handled = FALSE;
    mautilusDirectoryClass *current_provider_class;
    mautilusDirectory *handling_instance;

    extensions = mautilus_directory_provider_get_all ();

    for (l = extensions; l != NULL; l = l->next)
    {
        gio_extension = l->data;
        current_provider_class = MAUTILUS_DIRECTORY_CLASS (g_io_extension_ref_class (gio_extension));
        if (current_provider_class->handles_location (location))
        {
            handling_provider_type = g_io_extension_get_type (gio_extension);
            handled = TRUE;
            break;
        }
    }

    if (!handled)
    {
        /* This class is the fallback for any location */
        handling_provider_type = MAUTILUS_TYPE_VFS_DIRECTORY;
    }

    handling_instance = g_object_new (handling_provider_type,
                                      "location", location,
                                      NULL);


    return handling_instance;
}

gboolean
mautilus_directory_is_local (mautilusDirectory *directory)
{
    g_return_val_if_fail (MAUTILUS_IS_DIRECTORY (directory), FALSE);

    if (directory->details->location == NULL)
    {
        return TRUE;
    }

    return mautilus_directory_is_in_trash (directory) ||
           mautilus_directory_is_in_recent (directory) ||
           g_file_is_native (directory->details->location);
}

gboolean
mautilus_directory_is_local_or_fuse (mautilusDirectory *directory)
{
    g_autofree char *path = NULL;

    g_return_val_if_fail (MAUTILUS_IS_DIRECTORY (directory), FALSE);

    if (directory->details->location == NULL)
    {
        return TRUE;
    }

    /* If the glib reports a path, then it can use FUSE to convert the uri
     * to a local path
     */
    path = g_file_get_path (directory->details->location);

    return mautilus_directory_is_in_trash (directory) ||
           mautilus_directory_is_in_recent (directory) ||
           g_file_is_native (directory->details->location) ||
           path != NULL;
}

gboolean
mautilus_directory_is_in_trash (mautilusDirectory *directory)
{
    g_assert (MAUTILUS_IS_DIRECTORY (directory));

    if (directory->details->location == NULL)
    {
        return FALSE;
    }

    return g_file_has_uri_scheme (directory->details->location, "trash");
}

gboolean
mautilus_directory_is_in_recent (mautilusDirectory *directory)
{
    g_assert (MAUTILUS_IS_DIRECTORY (directory));

    if (directory->details->location == NULL)
    {
        return FALSE;
    }

    return g_file_has_uri_scheme (directory->details->location, "recent");
}

gboolean
mautilus_directory_is_in_starred (mautilusDirectory *directory)
{
    g_assert (MAUTILUS_IS_DIRECTORY (directory));

    if (directory->details->location == NULL)
    {
        return FALSE;
    }

    return g_file_has_uri_scheme (directory->details->location, "favorites");
}

gboolean
mautilus_directory_is_in_admin (mautilusDirectory *directory)
{
    g_assert (MAUTILUS_IS_DIRECTORY (directory));

    if (directory->details->location == NULL)
    {
        return FALSE;
    }

    return g_file_has_uri_scheme (directory->details->location, "admin");
}

gboolean
mautilus_directory_are_all_files_seen (mautilusDirectory *directory)
{
    g_return_val_if_fail (MAUTILUS_IS_DIRECTORY (directory), FALSE);

    return MAUTILUS_DIRECTORY_CLASS (G_OBJECT_GET_CLASS (directory))->are_all_files_seen (directory);
}

static void
add_to_hash_table (mautilusDirectory *directory,
                   mautilusFile      *file,
                   GList             *node)
{
    gchar *name;

    name = mautilus_file_get_name (file);

    g_assert (name != NULL);
    g_assert (node != NULL);
    g_assert (g_hash_table_lookup (directory->details->file_hash,
                                   name) == NULL);
    g_hash_table_insert (directory->details->file_hash, name, node);
}

static GList *
extract_from_hash_table (mautilusDirectory *directory,
                         mautilusFile      *file)
{
    g_autofree gchar *name = NULL;
    GList *node;

    name = mautilus_file_get_name (file);
    if (name == NULL)
    {
        return NULL;
    }

    /* Find the list node in the hash table. */
    node = g_hash_table_lookup (directory->details->file_hash, name);
    g_hash_table_remove (directory->details->file_hash, name);

    return node;
}

void
mautilus_directory_add_file (mautilusDirectory *directory,
                             mautilusFile      *file)
{
    GList *node;
    gboolean add_to_work_queue;

    g_assert (MAUTILUS_IS_DIRECTORY (directory));
    g_assert (MAUTILUS_IS_FILE (file));

    /* Add to list. */
    node = g_list_prepend (directory->details->file_list, file);
    directory->details->file_list = node;

    /* Add to hash table. */
    add_to_hash_table (directory, file, node);

    directory->details->confirmed_file_count++;

    add_to_work_queue = FALSE;
    if (mautilus_directory_is_file_list_monitored (directory))
    {
        /* Ref if we are monitoring, since monitoring owns the file list. */
        mautilus_file_ref (file);
        add_to_work_queue = TRUE;
    }
    else if (mautilus_directory_has_active_request_for_file (directory, file))
    {
        /* We're waiting for the file in a call_when_ready. Make sure
         *  we add the file to the work queue so that said waiter won't
         *  wait forever for e.g. all files in the directory to be done */
        add_to_work_queue = TRUE;
    }

    if (add_to_work_queue)
    {
        mautilus_directory_add_file_to_work_queue (directory, file);
    }
}

void
mautilus_directory_remove_file (mautilusDirectory *directory,
                                mautilusFile      *file)
{
    GList *node;

    g_assert (MAUTILUS_IS_DIRECTORY (directory));
    g_assert (MAUTILUS_IS_FILE (file));

    /* Find the list node in the hash table. */
    node = extract_from_hash_table (directory, file);
    g_assert (node != NULL);
    g_assert (node->data == file);

    /* Remove the item from the list. */
    directory->details->file_list = g_list_remove_link
                                        (directory->details->file_list, node);
    g_list_free_1 (node);

    mautilus_directory_remove_file_from_work_queue (directory, file);

    if (!file->details->unconfirmed)
    {
        directory->details->confirmed_file_count--;
    }

    /* Unref if we are monitoring. */
    if (mautilus_directory_is_file_list_monitored (directory))
    {
        mautilus_file_unref (file);
    }
}

GList *
mautilus_directory_begin_file_name_change (mautilusDirectory *directory,
                                           mautilusFile      *file)
{
    /* Find the list node in the hash table. */
    return extract_from_hash_table (directory, file);
}

void
mautilus_directory_end_file_name_change (mautilusDirectory *directory,
                                         mautilusFile      *file,
                                         GList             *node)
{
    /* Add the list node to the hash table. */
    if (node != NULL)
    {
        add_to_hash_table (directory, file, node);
    }
}

mautilusFile *
mautilus_directory_find_file_by_name (mautilusDirectory *directory,
                                      const char        *name)
{
    GList *node;

    g_return_val_if_fail (MAUTILUS_IS_DIRECTORY (directory), NULL);
    g_return_val_if_fail (name != NULL, NULL);

    node = g_hash_table_lookup (directory->details->file_hash,
                                name);
    return node == NULL ? NULL : MAUTILUS_FILE (node->data);
}

void
mautilus_directory_emit_files_added (mautilusDirectory *directory,
                                     GList             *added_files)
{
    mautilus_profile_start (NULL);
    if (added_files != NULL)
    {
        g_signal_emit (directory,
                       signals[FILES_ADDED], 0,
                       added_files);
    }
    mautilus_profile_end (NULL);
}

void
mautilus_directory_emit_files_changed (mautilusDirectory *directory,
                                       GList             *changed_files)
{
    mautilus_profile_start (NULL);
    if (changed_files != NULL)
    {
        g_signal_emit (directory,
                       signals[FILES_CHANGED], 0,
                       changed_files);
    }
    mautilus_profile_end (NULL);
}

void
mautilus_directory_emit_change_signals (mautilusDirectory *directory,
                                        GList             *changed_files)
{
    GList *p;

    mautilus_profile_start (NULL);
    for (p = changed_files; p != NULL; p = p->next)
    {
        mautilus_file_emit_changed (p->data);
    }
    mautilus_directory_emit_files_changed (directory, changed_files);
    mautilus_profile_end (NULL);
}

void
mautilus_directory_emit_done_loading (mautilusDirectory *directory)
{
    g_signal_emit (directory,
                   signals[DONE_LOADING], 0);
}

void
mautilus_directory_emit_load_error (mautilusDirectory *directory,
                                    GError            *error)
{
    g_signal_emit (directory,
                   signals[LOAD_ERROR], 0,
                   error);
}

/* Return a directory object for this one's parent. */
static mautilusDirectory *
get_parent_directory (GFile *location)
{
    mautilusDirectory *directory;
    GFile *parent;

    parent = g_file_get_parent (location);
    if (parent)
    {
        directory = mautilus_directory_get_internal (parent, TRUE);
        g_object_unref (parent);
        return directory;
    }
    return NULL;
}

/* If a directory object exists for this one's parent, then
 * return it, otherwise return NULL.
 */
static mautilusDirectory *
get_parent_directory_if_exists (GFile *location)
{
    mautilusDirectory *directory;
    GFile *parent;

    parent = g_file_get_parent (location);
    if (parent)
    {
        directory = mautilus_directory_get_internal (parent, FALSE);
        g_object_unref (parent);
        return directory;
    }
    return NULL;
}

static void
hash_table_list_prepend (GHashTable    *table,
                         gconstpointer  key,
                         gpointer       data)
{
    GList *list;

    list = g_hash_table_lookup (table, key);
    list = g_list_prepend (list, data);
    g_hash_table_insert (table, (gpointer) key, list);
}

static void
call_files_added_free_list (gpointer key,
                            gpointer value,
                            gpointer user_data)
{
    g_assert (MAUTILUS_IS_DIRECTORY (key));
    g_assert (value != NULL);
    g_assert (user_data == NULL);

    g_signal_emit (key,
                   signals[FILES_ADDED], 0,
                   value);
    g_list_free (value);
}

static void
call_files_changed_common (mautilusDirectory *self,
                           GList             *file_list)
{
    GList *node;
    mautilusFile *file;

    for (node = file_list; node != NULL; node = node->next)
    {
        mautilusDirectory *directory;

        file = node->data;
        directory = mautilus_file_get_directory (file);

        if (directory == self)
        {
            mautilus_directory_add_file_to_work_queue (self, file);
        }
    }
    mautilus_directory_async_state_changed (self);
    mautilus_directory_emit_change_signals (self, file_list);
}

static void
call_files_changed_free_list (gpointer key,
                              gpointer value,
                              gpointer user_data)
{
    g_assert (value != NULL);
    g_assert (user_data == NULL);

    call_files_changed_common (MAUTILUS_DIRECTORY (key), value);
    g_list_free (value);
}

static void
call_files_changed_unref_free_list (gpointer key,
                                    gpointer value,
                                    gpointer user_data)
{
    g_assert (value != NULL);
    g_assert (user_data == NULL);

    call_files_changed_common (MAUTILUS_DIRECTORY (key), value);
    mautilus_file_list_free (value);
}

static void
call_get_file_info_free_list (gpointer key,
                              gpointer value,
                              gpointer user_data)
{
    mautilusDirectory *directory;
    GList *files;

    g_assert (MAUTILUS_IS_DIRECTORY (key));
    g_assert (value != NULL);
    g_assert (user_data == NULL);

    directory = key;
    files = value;

    mautilus_directory_get_info_for_new_files (directory, files);
    g_list_foreach (files, (GFunc) g_object_unref, NULL);
    g_list_free (files);
}

static void
invalidate_count_and_unref (gpointer key,
                            gpointer value,
                            gpointer user_data)
{
    g_assert (MAUTILUS_IS_DIRECTORY (key));
    g_assert (value == key);
    g_assert (user_data == NULL);

    mautilus_directory_invalidate_count_and_mime_list (key);
    mautilus_directory_unref (key);
}

static void
collect_parent_directories (GHashTable        *hash_table,
                            mautilusDirectory *directory)
{
    g_assert (hash_table != NULL);
    g_assert (MAUTILUS_IS_DIRECTORY (directory));

    if (g_hash_table_lookup (hash_table, directory) == NULL)
    {
        mautilus_directory_ref (directory);
        g_hash_table_insert (hash_table, directory, directory);
    }
}

void
mautilus_directory_notify_files_added (GList *files)
{
    GHashTable *added_lists;
    GList *p;
    mautilusDirectory *directory;
    GHashTable *parent_directories;
    mautilusFile *file;
    GFile *location, *parent;

    mautilus_profile_start (NULL);

    /* Make a list of added files in each directory. */
    added_lists = g_hash_table_new (NULL, NULL);

    /* Make a list of parent directories that will need their counts updated. */
    parent_directories = g_hash_table_new (NULL, NULL);

    for (p = files; p != NULL; p = p->next)
    {
        location = p->data;

        /* See if the directory is already known. */
        directory = get_parent_directory_if_exists (location);
        if (directory == NULL)
        {
            /* In case the directory is not being
             * monitored, but the corresponding file is,
             * we must invalidate it's item count.
             */


            file = NULL;
            parent = g_file_get_parent (location);
            if (parent)
            {
                file = mautilus_file_get_existing (parent);
                g_object_unref (parent);
            }

            if (file != NULL)
            {
                mautilus_file_invalidate_count_and_mime_list (file);
                mautilus_file_unref (file);
            }

            continue;
        }

        collect_parent_directories (parent_directories, directory);

        /* If no one is monitoring files in the directory, nothing to do. */
        if (!mautilus_directory_is_file_list_monitored (directory))
        {
            mautilus_directory_unref (directory);
            continue;
        }

        file = mautilus_file_get_existing (location);
        /* We check is_added here, because the file could have been added
         * to the directory by a mautilus_file_get() but not gotten
         * files_added emitted
         */
        if (file && file->details->is_added)
        {
            /* A file already exists, it was probably renamed.
             * If it was renamed this could be ignored, but
             * queue a change just in case */
            mautilus_file_changed (file);
            mautilus_file_unref (file);
        }
        else
        {
            hash_table_list_prepend (added_lists,
                                     directory,
                                     g_object_ref (location));
        }
        mautilus_directory_unref (directory);
    }

    /* Now get file info for the new files. This creates mautilusFile
     * objects for the new files, and sends out a files_added signal.
     */
    g_hash_table_foreach (added_lists, call_get_file_info_free_list, NULL);
    g_hash_table_destroy (added_lists);

    /* Invalidate count for each parent directory. */
    g_hash_table_foreach (parent_directories, invalidate_count_and_unref, NULL);
    g_hash_table_destroy (parent_directories);

    mautilus_profile_end (NULL);
}

void
mautilus_directory_notify_files_changed (GList *files)
{
    GHashTable *changed_lists;
    GList *node;
    GFile *location;
    mautilusFile *file;

    /* Make a list of changed files in each directory. */
    changed_lists = g_hash_table_new (NULL, NULL);

    /* Go through all the notifications. */
    for (node = files; node != NULL; node = node->next)
    {
        location = node->data;

        /* Find the file. */
        file = mautilus_file_get_existing (location);
        if (file != NULL)
        {
            mautilusDirectory *directory;

            directory = mautilus_file_get_directory (file);

            /* Tell it to re-get info now, and later emit
             * a changed signal.
             */
            file->details->file_info_is_up_to_date = FALSE;
            file->details->link_info_is_up_to_date = FALSE;
            mautilus_file_invalidate_extension_info_internal (file);

            hash_table_list_prepend (changed_lists, directory, file);
        }
    }

    /* Now send out the changed signals. */
    g_hash_table_foreach (changed_lists, call_files_changed_unref_free_list, NULL);
    g_hash_table_destroy (changed_lists);
}

void
mautilus_directory_notify_files_removed (GList *files)
{
    GHashTable *changed_lists;
    GList *p;
    mautilusDirectory *directory;
    GHashTable *parent_directories;
    mautilusFile *file;
    GFile *location;

    /* Make a list of changed files in each directory. */
    changed_lists = g_hash_table_new (NULL, NULL);

    /* Make a list of parent directories that will need their counts updated. */
    parent_directories = g_hash_table_new (NULL, NULL);

    /* Go through all the notifications. */
    for (p = files; p != NULL; p = p->next)
    {
        location = p->data;

        /* Update file count for parent directory if anyone might care. */
        directory = get_parent_directory_if_exists (location);
        if (directory != NULL)
        {
            collect_parent_directories (parent_directories, directory);
            mautilus_directory_unref (directory);
        }

        /* Find the file. */
        file = mautilus_file_get_existing (location);
        if (file != NULL && !mautilus_file_rename_in_progress (file))
        {
            mautilusDirectory *directory;

            directory = mautilus_file_get_directory (file);

            /* Mark it gone and prepare to send the changed signal. */
            mautilus_file_mark_gone (file);
            hash_table_list_prepend (changed_lists,
                                     directory, mautilus_file_ref (file));
        }
        mautilus_file_unref (file);
    }

    /* Now send out the changed signals. */
    g_hash_table_foreach (changed_lists, call_files_changed_unref_free_list, NULL);
    g_hash_table_destroy (changed_lists);

    /* Invalidate count for each parent directory. */
    g_hash_table_foreach (parent_directories, invalidate_count_and_unref, NULL);
    g_hash_table_destroy (parent_directories);
}

static void
set_directory_location (mautilusDirectory *directory,
                        GFile             *location)
{
    if (directory->details->location)
    {
        g_object_unref (directory->details->location);
    }
    directory->details->location = g_object_ref (location);

    g_object_notify_by_pspec (G_OBJECT (directory), properties[PROP_LOCATION]);
}

static void
change_directory_location (mautilusDirectory *directory,
                           GFile             *new_location)
{
    /* I believe it's impossible for a self-owned file/directory
     * to be moved. But if that did somehow happen, this function
     * wouldn't do enough to handle it.
     */
    g_assert (directory->details->as_file == NULL);

    g_hash_table_remove (directories,
                         directory->details->location);

    set_directory_location (directory, new_location);

    g_hash_table_insert (directories,
                         directory->details->location,
                         directory);
}

typedef struct
{
    GFile *container;
    GList *directories;
} CollectData;

static void
collect_directories_by_container (gpointer key,
                                  gpointer value,
                                  gpointer callback_data)
{
    mautilusDirectory *directory;
    CollectData *collect_data;
    GFile *location;

    location = (GFile *) key;
    directory = MAUTILUS_DIRECTORY (value);
    collect_data = (CollectData *) callback_data;

    if (g_file_has_prefix (location, collect_data->container) ||
        g_file_equal (collect_data->container, location))
    {
        mautilus_directory_ref (directory);
        collect_data->directories =
            g_list_prepend (collect_data->directories,
                            directory);
    }
}

static GList *
mautilus_directory_moved_internal (GFile *old_location,
                                   GFile *new_location)
{
    CollectData collection;
    mautilusDirectory *directory;
    GList *node, *affected_files;
    GFile *new_directory_location;
    char *relative_path;

    collection.container = old_location;
    collection.directories = NULL;

    g_hash_table_foreach (directories,
                          collect_directories_by_container,
                          &collection);

    affected_files = NULL;

    for (node = collection.directories; node != NULL; node = node->next)
    {
        directory = MAUTILUS_DIRECTORY (node->data);
        new_directory_location = NULL;

        if (g_file_equal (directory->details->location, old_location))
        {
            new_directory_location = g_object_ref (new_location);
        }
        else
        {
            relative_path = g_file_get_relative_path (old_location,
                                                      directory->details->location);
            if (relative_path != NULL)
            {
                new_directory_location = g_file_resolve_relative_path (new_location, relative_path);
                g_free (relative_path);
            }
        }

        if (new_directory_location)
        {
            change_directory_location (directory, new_directory_location);
            g_object_unref (new_directory_location);

            /* Collect affected files. */
            if (directory->details->as_file != NULL)
            {
                affected_files = g_list_prepend
                                     (affected_files,
                                     mautilus_file_ref (directory->details->as_file));
            }
            affected_files = g_list_concat
                                 (affected_files,
                                 mautilus_file_list_copy (directory->details->file_list));
        }

        mautilus_directory_unref (directory);
    }

    g_list_free (collection.directories);

    return affected_files;
}

void
mautilus_directory_moved (const char *old_uri,
                          const char *new_uri)
{
    GList *list, *node;
    GHashTable *hash;
    mautilusFile *file;
    GFile *old_location;
    GFile *new_location;

    hash = g_hash_table_new (NULL, NULL);

    old_location = g_file_new_for_uri (old_uri);
    new_location = g_file_new_for_uri (new_uri);

    list = mautilus_directory_moved_internal (old_location, new_location);
    for (node = list; node != NULL; node = node->next)
    {
        mautilusDirectory *directory;

        file = MAUTILUS_FILE (node->data);
        directory = mautilus_file_get_directory (file);

        hash_table_list_prepend (hash, directory, mautilus_file_ref (file));
    }
    mautilus_file_list_free (list);

    g_object_unref (old_location);
    g_object_unref (new_location);

    g_hash_table_foreach (hash, call_files_changed_unref_free_list, NULL);
    g_hash_table_destroy (hash);
}

void
mautilus_directory_notify_files_moved (GList *file_pairs)
{
    GList *p, *affected_files, *node;
    GFilePair *pair;
    mautilusFile *file;
    mautilusDirectory *old_directory, *new_directory;
    GHashTable *parent_directories;
    GList *new_files_list, *unref_list;
    GHashTable *added_lists, *changed_lists;
    char *name;
    mautilusFileAttributes cancel_attributes;
    GFile *to_location, *from_location;

    /* Make a list of added and changed files in each directory. */
    new_files_list = NULL;
    added_lists = g_hash_table_new (NULL, NULL);
    changed_lists = g_hash_table_new (NULL, NULL);
    unref_list = NULL;

    /* Make a list of parent directories that will need their counts updated. */
    parent_directories = g_hash_table_new (NULL, NULL);

    cancel_attributes = mautilus_file_get_all_attributes ();

    for (p = file_pairs; p != NULL; p = p->next)
    {
        pair = p->data;
        from_location = pair->from;
        to_location = pair->to;

        /* Handle overwriting a file. */
        file = mautilus_file_get_existing (to_location);
        if (file != NULL)
        {
            mautilusDirectory *directory;

            directory = mautilus_file_get_directory (file);

            /* Mark it gone and prepare to send the changed signal. */
            mautilus_file_mark_gone (file);
            hash_table_list_prepend (changed_lists, directory, file);
            collect_parent_directories (parent_directories, directory);
        }

        /* Update any directory objects that are affected. */
        affected_files = mautilus_directory_moved_internal (from_location,
                                                            to_location);
        for (node = affected_files; node != NULL; node = node->next)
        {
            mautilusDirectory *directory;

            file = MAUTILUS_FILE (node->data);
            directory = mautilus_file_get_directory (file);
            hash_table_list_prepend (changed_lists, directory, file);
        }
        unref_list = g_list_concat (unref_list, affected_files);

        /* Move an existing file. */
        file = mautilus_file_get_existing (from_location);
        if (file == NULL)
        {
            /* Handle this as if it was a new file. */
            new_files_list = g_list_prepend (new_files_list,
                                             to_location);
        }
        else
        {
            mautilusDirectory *directory;

            directory = mautilus_file_get_directory (file);

            /* Handle notification in the old directory. */
            old_directory = directory;
            collect_parent_directories (parent_directories, old_directory);

            /* Cancel loading of attributes in the old directory */
            mautilus_directory_cancel_loading_file_attributes
                (old_directory, file, cancel_attributes);

            /* Locate the new directory. */
            new_directory = get_parent_directory (to_location);
            collect_parent_directories (parent_directories, new_directory);
            /* We can unref now -- new_directory is in the
             * parent directories list so it will be
             * around until the end of this function
             * anyway.
             */
            mautilus_directory_unref (new_directory);

            /* Update the file's name and directory. */
            name = g_file_get_basename (to_location);
            mautilus_file_update_name_and_directory
                (file, name, new_directory);
            g_free (name);

            /* Update file attributes */
            mautilus_file_invalidate_attributes (file, MAUTILUS_FILE_ATTRIBUTE_INFO);

            hash_table_list_prepend (changed_lists,
                                     old_directory,
                                     file);
            if (old_directory != new_directory)
            {
                hash_table_list_prepend (added_lists,
                                         new_directory,
                                         file);
            }

            /* Unref each file once to balance out mautilus_file_get_by_uri. */
            unref_list = g_list_prepend (unref_list, file);
        }
    }

    /* Now send out the changed and added signals for existing file objects. */
    g_hash_table_foreach (changed_lists, call_files_changed_free_list, NULL);
    g_hash_table_destroy (changed_lists);
    g_hash_table_foreach (added_lists, call_files_added_free_list, NULL);
    g_hash_table_destroy (added_lists);

    /* Let the file objects go. */
    mautilus_file_list_free (unref_list);

    /* Invalidate count for each parent directory. */
    g_hash_table_foreach (parent_directories, invalidate_count_and_unref, NULL);
    g_hash_table_destroy (parent_directories);

    /* Separate handling for brand new file objects. */
    mautilus_directory_notify_files_added (new_files_list);
    g_list_free (new_files_list);
}

void
mautilus_directory_schedule_position_set (GList *position_setting_list)
{
    GList *p;
    const mautilusFileChangesQueuePosition *item;
    mautilusFile *file;
    char str[64];
    time_t now;

    time (&now);

    for (p = position_setting_list; p != NULL; p = p->next)
    {
        item = (mautilusFileChangesQueuePosition *) p->data;

        file = mautilus_file_get (item->location);

        if (item->set)
        {
            g_snprintf (str, sizeof (str), "%d,%d", item->point.x, item->point.y);
        }
        else
        {
            str[0] = 0;
        }
        mautilus_file_set_metadata
            (file,
            MAUTILUS_METADATA_KEY_ICON_POSITION,
            NULL,
            str);

        if (item->set)
        {
            mautilus_file_set_time_metadata
                (file,
                MAUTILUS_METADATA_KEY_ICON_POSITION_TIMESTAMP,
                now);
        }
        else
        {
            mautilus_file_set_time_metadata
                (file,
                MAUTILUS_METADATA_KEY_ICON_POSITION_TIMESTAMP,
                UNDEFINED_TIME);
        }

        if (item->set)
        {
            g_snprintf (str, sizeof (str), "%d", item->screen);
        }
        else
        {
            str[0] = 0;
        }
        mautilus_file_set_metadata
            (file,
            MAUTILUS_METADATA_KEY_SCREEN,
            NULL,
            str);

        mautilus_file_unref (file);
    }
}

gboolean
mautilus_directory_contains_file (mautilusDirectory *directory,
                                  mautilusFile      *file)
{
    g_return_val_if_fail (MAUTILUS_IS_DIRECTORY (directory), FALSE);
    g_return_val_if_fail (MAUTILUS_IS_FILE (file), FALSE);

    if (mautilus_file_is_gone (file))
    {
        return FALSE;
    }

    return MAUTILUS_DIRECTORY_CLASS (G_OBJECT_GET_CLASS (directory))->contains_file (directory, file);
}

mautilusFile *
mautilus_directory_get_file_by_name (mautilusDirectory *directory,
                                     const gchar       *name)
{
    GList *files;
    GList *l;
    mautilusFile *result = NULL;

    files = mautilus_directory_get_file_list (directory);

    for (l = files; l != NULL; l = l->next)
    {
        if (mautilus_file_compare_display_name (l->data, name) == 0)
        {
            result = mautilus_file_ref (l->data);
            break;
        }
    }

    mautilus_file_list_free (files);

    return result;
}

void
mautilus_directory_call_when_ready (mautilusDirectory         *directory,
                                    mautilusFileAttributes     file_attributes,
                                    gboolean                   wait_for_all_files,
                                    mautilusDirectoryCallback  callback,
                                    gpointer                   callback_data)
{
    g_return_if_fail (MAUTILUS_IS_DIRECTORY (directory));
    g_return_if_fail (callback != NULL);

    MAUTILUS_DIRECTORY_CLASS (G_OBJECT_GET_CLASS (directory))->call_when_ready
        (directory, file_attributes, wait_for_all_files,
        callback, callback_data);
}

void
mautilus_directory_cancel_callback (mautilusDirectory         *directory,
                                    mautilusDirectoryCallback  callback,
                                    gpointer                   callback_data)
{
    g_return_if_fail (MAUTILUS_IS_DIRECTORY (directory));
    g_return_if_fail (callback != NULL);

    MAUTILUS_DIRECTORY_CLASS (G_OBJECT_GET_CLASS (directory))->cancel_callback
        (directory, callback, callback_data);
}

void
mautilus_directory_file_monitor_add (mautilusDirectory         *directory,
                                     gconstpointer              client,
                                     gboolean                   monitor_hidden_files,
                                     mautilusFileAttributes     file_attributes,
                                     mautilusDirectoryCallback  callback,
                                     gpointer                   callback_data)
{
    g_return_if_fail (MAUTILUS_IS_DIRECTORY (directory));
    g_return_if_fail (client != NULL);

    MAUTILUS_DIRECTORY_CLASS (G_OBJECT_GET_CLASS (directory))->file_monitor_add
        (directory, client,
        monitor_hidden_files,
        file_attributes,
        callback, callback_data);
}

void
mautilus_directory_file_monitor_remove (mautilusDirectory *directory,
                                        gconstpointer      client)
{
    g_return_if_fail (MAUTILUS_IS_DIRECTORY (directory));
    g_return_if_fail (client != NULL);

    MAUTILUS_DIRECTORY_CLASS (G_OBJECT_GET_CLASS (directory))->file_monitor_remove
        (directory, client);
}

void
mautilus_directory_force_reload (mautilusDirectory *directory)
{
    g_return_if_fail (MAUTILUS_IS_DIRECTORY (directory));

    MAUTILUS_DIRECTORY_CLASS (G_OBJECT_GET_CLASS (directory))->force_reload (directory);
}

gboolean
mautilus_directory_is_not_empty (mautilusDirectory *directory)
{
    g_return_val_if_fail (MAUTILUS_IS_DIRECTORY (directory), FALSE);

    return MAUTILUS_DIRECTORY_CLASS (G_OBJECT_GET_CLASS (directory))->is_not_empty (directory);
}

GList *
mautilus_directory_get_file_list (mautilusDirectory *directory)
{
    return MAUTILUS_DIRECTORY_CLASS (G_OBJECT_GET_CLASS (directory))->get_file_list (directory);
}

gboolean
mautilus_directory_is_editable (mautilusDirectory *directory)
{
    return MAUTILUS_DIRECTORY_CLASS (G_OBJECT_GET_CLASS (directory))->is_editable (directory);
}

GList *
mautilus_directory_match_pattern (mautilusDirectory *directory,
                                  const char        *pattern)
{
    GList *files, *l, *ret;
    GPatternSpec *spec;


    ret = NULL;
    spec = g_pattern_spec_new (pattern);

    files = mautilus_directory_get_file_list (directory);
    for (l = files; l; l = l->next)
    {
        mautilusFile *file;
        char *name;

        file = MAUTILUS_FILE (l->data);
        name = mautilus_file_get_display_name (file);

        if (g_pattern_match_string (spec, name))
        {
            ret = g_list_prepend (ret, mautilus_file_ref (file));
        }

        g_free (name);
    }

    g_pattern_spec_free (spec);
    mautilus_file_list_free (files);

    return ret;
}

/**
 * mautilus_directory_list_ref
 *
 * Ref all the directories in a list.
 * @list: GList of directories.
 **/
GList *
mautilus_directory_list_ref (GList *list)
{
    g_list_foreach (list, (GFunc) mautilus_directory_ref, NULL);
    return list;
}

/**
 * mautilus_directory_list_unref
 *
 * Unref all the directories in a list.
 * @list: GList of directories.
 **/
void
mautilus_directory_list_unref (GList *list)
{
    g_list_foreach (list, (GFunc) mautilus_directory_unref, NULL);
}

/**
 * mautilus_directory_list_free
 *
 * Free a list of directories after unrefing them.
 * @list: GList of directories.
 **/
void
mautilus_directory_list_free (GList *list)
{
    mautilus_directory_list_unref (list);
    g_list_free (list);
}

/**
 * mautilus_directory_list_copy
 *
 * Copy the list of directories, making a new ref of each,
 * @list: GList of directories.
 **/
GList *
mautilus_directory_list_copy (GList *list)
{
    return g_list_copy (mautilus_directory_list_ref (list));
}

static int
compare_by_uri (mautilusDirectory *a,
                mautilusDirectory *b)
{
    char *uri_a, *uri_b;
    int res;

    uri_a = g_file_get_uri (a->details->location);
    uri_b = g_file_get_uri (b->details->location);

    res = strcmp (uri_a, uri_b);

    g_free (uri_a);
    g_free (uri_b);

    return res;
}

static int
compare_by_uri_cover (gconstpointer a,
                      gconstpointer b)
{
    return compare_by_uri (MAUTILUS_DIRECTORY (a), MAUTILUS_DIRECTORY (b));
}

/**
 * mautilus_directory_list_sort_by_uri
 *
 * Sort the list of directories by directory uri.
 * @list: GList of directories.
 **/
GList *
mautilus_directory_list_sort_by_uri (GList *list)
{
    return g_list_sort (list, compare_by_uri_cover);
}

#if !defined (MAUTILUS_OMIT_SELF_CHECK)

#include <eel/eel-debug.h>
#include "mautilus-file-attributes.h"

static int data_dummy;
static gboolean got_files_flag;

static void
got_files_callback (mautilusDirectory *directory,
                    GList             *files,
                    gpointer           callback_data)
{
    g_assert (MAUTILUS_IS_DIRECTORY (directory));
    g_assert (g_list_length (files) > 10);
    g_assert (callback_data == &data_dummy);

    got_files_flag = TRUE;
}

/* Return the number of extant mautilusDirectories */
int
mautilus_directory_number_outstanding (void)
{
    return directories ? g_hash_table_size (directories) : 0;
}

void
mautilus_directory_dump (mautilusDirectory *directory)
{
    g_autofree gchar *uri = NULL;

    uri = g_file_get_uri (directory->details->location);
    g_print ("uri: %s\n", uri);
    g_print ("ref count: %d\n", G_OBJECT (directory)->ref_count);
}

void
mautilus_self_check_directory (void)
{
    mautilusDirectory *directory;
    mautilusFile *file;

    directory = mautilus_directory_get_by_uri ("file:///etc");
    file = mautilus_file_get_by_uri ("file:///etc/passwd");

    EEL_CHECK_INTEGER_RESULT (g_hash_table_size (directories), 1);

    mautilus_directory_file_monitor_add
        (directory, &data_dummy,
        TRUE, 0, NULL, NULL);

    /* FIXME: these need to be updated to the new metadata infrastructure
     *  as make check doesn't pass.
     *  mautilus_file_set_metadata (file, "test", "default", "value");
     *  EEL_CHECK_STRING_RESULT (mautilus_file_get_metadata (file, "test", "default"), "value");
     *
     *  mautilus_file_set_boolean_metadata (file, "test_boolean", TRUE, TRUE);
     *  EEL_CHECK_BOOLEAN_RESULT (mautilus_file_get_boolean_metadata (file, "test_boolean", TRUE), TRUE);
     *  mautilus_file_set_boolean_metadata (file, "test_boolean", TRUE, FALSE);
     *  EEL_CHECK_BOOLEAN_RESULT (mautilus_file_get_boolean_metadata (file, "test_boolean", TRUE), FALSE);
     *  EEL_CHECK_BOOLEAN_RESULT (mautilus_file_get_boolean_metadata (NULL, "test_boolean", TRUE), TRUE);
     *
     *  mautilus_file_set_integer_metadata (file, "test_integer", 0, 17);
     *  EEL_CHECK_INTEGER_RESULT (mautilus_file_get_integer_metadata (file, "test_integer", 0), 17);
     *  mautilus_file_set_integer_metadata (file, "test_integer", 0, -1);
     *  EEL_CHECK_INTEGER_RESULT (mautilus_file_get_integer_metadata (file, "test_integer", 0), -1);
     *  mautilus_file_set_integer_metadata (file, "test_integer", 42, 42);
     *  EEL_CHECK_INTEGER_RESULT (mautilus_file_get_integer_metadata (file, "test_integer", 42), 42);
     *  EEL_CHECK_INTEGER_RESULT (mautilus_file_get_integer_metadata (NULL, "test_integer", 42), 42);
     *  EEL_CHECK_INTEGER_RESULT (mautilus_file_get_integer_metadata (file, "nonexistent_key", 42), 42);
     */

    EEL_CHECK_BOOLEAN_RESULT (mautilus_directory_get_by_uri ("file:///etc") == directory, TRUE);
    mautilus_directory_unref (directory);

    EEL_CHECK_BOOLEAN_RESULT (mautilus_directory_get_by_uri ("file:///etc/") == directory, TRUE);
    mautilus_directory_unref (directory);

    EEL_CHECK_BOOLEAN_RESULT (mautilus_directory_get_by_uri ("file:///etc////") == directory, TRUE);
    mautilus_directory_unref (directory);

    mautilus_file_unref (file);

    mautilus_directory_file_monitor_remove (directory, &data_dummy);

    mautilus_directory_unref (directory);

    while (g_hash_table_size (directories) != 0)
    {
        gtk_main_iteration ();
    }

    EEL_CHECK_INTEGER_RESULT (g_hash_table_size (directories), 0);

    directory = mautilus_directory_get_by_uri ("file:///etc");

    got_files_flag = FALSE;

    mautilus_directory_call_when_ready (directory,
                                        MAUTILUS_FILE_ATTRIBUTE_INFO |
                                        MAUTILUS_FILE_ATTRIBUTE_DEEP_COUNTS,
                                        TRUE,
                                        got_files_callback, &data_dummy);

    while (!got_files_flag)
    {
        gtk_main_iteration ();
    }

    EEL_CHECK_BOOLEAN_RESULT (directory->details->file_list == NULL, TRUE);

    EEL_CHECK_INTEGER_RESULT (g_hash_table_size (directories), 1);

    file = mautilus_file_get_by_uri ("file:///etc/passwd");

    /* EEL_CHECK_STRING_RESULT (mautilus_file_get_metadata (file, "test", "default"), "value"); */

    mautilus_file_unref (file);

    mautilus_directory_unref (directory);

    EEL_CHECK_INTEGER_RESULT (g_hash_table_size (directories), 0);
}

#endif /* !MAUTILUS_OMIT_SELF_CHECK */
