
/* mautilus-file-undo-manager.h - Manages the undo/redo stack
 *
 * Copyright (C) 2007-2011 Amos Brocco
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Amos Brocco <amos.brocco@gmail.com>
 */

#ifndef __MAUTILUS_FILE_UNDO_MANAGER_H__
#define __MAUTILUS_FILE_UNDO_MANAGER_H__

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <gio/gio.h>

#include "mautilus-file-undo-operations.h"

#define MAUTILUS_TYPE_FILE_UNDO_MANAGER\
	(mautilus_file_undo_manager_get_type())

G_DECLARE_FINAL_TYPE (mautilusFileUndoManager, mautilus_file_undo_manager, MAUTILUS, FILE_UNDO_MANAGER, GObject)

typedef enum {
	MAUTILUS_FILE_UNDO_MANAGER_STATE_NONE,
	MAUTILUS_FILE_UNDO_MANAGER_STATE_UNDO,
	MAUTILUS_FILE_UNDO_MANAGER_STATE_REDO
} mautilusFileUndoManagerState;

mautilusFileUndoManager *mautilus_file_undo_manager_new (void);
mautilusFileUndoManager * mautilus_file_undo_manager_get (void);

void mautilus_file_undo_manager_set_action (mautilusFileUndoInfo *info);
mautilusFileUndoInfo *mautilus_file_undo_manager_get_action (void);

mautilusFileUndoManagerState mautilus_file_undo_manager_get_state (void);

void mautilus_file_undo_manager_undo (GtkWindow *parent_window);
void mautilus_file_undo_manager_redo (GtkWindow *parent_window);

gboolean mautilus_file_undo_manager_is_operating (void);

#endif /* __MAUTILUS_FILE_UNDO_MANAGER_H__ */
