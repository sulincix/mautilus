/*
 * mautilus-debug: debug loggers for mautilus
 *
 * Copyright (C) 2007 Collabora Ltd.
 * Copyright (C) 2007 Nokia Corporation
 * Copyright (C) 2010 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * Based on Empathy's empathy-debug.
 */

#ifndef __MAUTILUS_DEBUG_H__
#define __MAUTILUS_DEBUG_H__

#include <config.h>
#include <glib.h>

G_BEGIN_DECLS

typedef enum {
  MAUTILUS_DEBUG_APPLICATION = 1 << 1,
  MAUTILUS_DEBUG_BOOKMARKS = 1 << 2,
  MAUTILUS_DEBUG_DBUS = 1 << 3,
  MAUTILUS_DEBUG_DIRECTORY_VIEW = 1 << 4,
  MAUTILUS_DEBUG_FILE = 1 << 5,
  MAUTILUS_DEBUG_CANVAS_CONTAINER = 1 << 6,
  MAUTILUS_DEBUG_CANVAS_VIEW = 1 << 7,
  MAUTILUS_DEBUG_LIST_VIEW = 1 << 8,
  MAUTILUS_DEBUG_MIME = 1 << 9,
  MAUTILUS_DEBUG_PLACES = 1 << 10,
  MAUTILUS_DEBUG_PREVIEWER = 1 << 11,
  MAUTILUS_DEBUG_SMCLIENT = 1 << 12,
  MAUTILUS_DEBUG_WINDOW = 1 << 13,
  MAUTILUS_DEBUG_UNDO = 1 << 14,
  MAUTILUS_DEBUG_SEARCH = 1 << 15,
  MAUTILUS_DEBUG_SEARCH_HIT = 1 << 16,
} DebugFlags;

void mautilus_debug_set_flags (DebugFlags flags);
gboolean mautilus_debug_flag_is_set (DebugFlags flag);

void mautilus_debug_valist (DebugFlags flag,
                            const gchar *format, va_list args);

void mautilus_debug (DebugFlags flag, const gchar *format, ...)
  G_GNUC_PRINTF (2, 3);

void mautilus_debug_files (DebugFlags flag, GList *files,
                           const gchar *format, ...) G_GNUC_PRINTF (3, 4);

#ifdef DEBUG_FLAG

#define DEBUG(format, ...) \
  mautilus_debug (DEBUG_FLAG, "%s: %s: " format, G_STRFUNC, G_STRLOC, \
                  ##__VA_ARGS__)

#define DEBUG_FILES(files, format, ...) \
  mautilus_debug_files (DEBUG_FLAG, files, "%s:" format, G_STRFUNC, \
                        ##__VA_ARGS__)

#define DEBUGGING mautilus_debug_flag_is_set(DEBUG_FLAG)

#endif /* DEBUG_FLAG */

G_END_DECLS

#endif /* __MAUTILUS_DEBUG_H__ */
