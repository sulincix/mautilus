/* fm-icon-container.h - the container widget for file manager icons
 *
 *  Copyright (C) 2002 Sun Microsystems, Inc.
 *
 *  The Gnome Library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  The Gnome Library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with the Gnome Library; see the file COPYING.LIB.  If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *  Author: Michael Meeks <michael@ximian.com>
 */
#include <config.h>

#include "mautilus-canvas-view-container.h"

#include <string.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include <eel/eel-glib-extensions.h>
#include "mautilus-global-preferences.h"
#include "mautilus-file-attributes.h"
#include "mautilus-thumbnails.h"

G_DEFINE_TYPE (mautilusCanvasViewContainer, mautilus_canvas_view_container, MAUTILUS_TYPE_CANVAS_CONTAINER);

static GQuark attribute_none_q;

static mautilusCanvasView *
get_canvas_view (mautilusCanvasContainer *container)
{
    /* Type unsafe comparison for performance */
    return ((mautilusCanvasViewContainer *) container)->view;
}

static mautilusIconInfo *
mautilus_canvas_view_container_get_icon_images (mautilusCanvasContainer *container,
                                                mautilusCanvasIconData  *data,
                                                int                      size,
                                                gboolean                 for_drag_accept)
{
    mautilusCanvasView *canvas_view;
    mautilusFile *file;
    mautilusFileIconFlags flags;
    mautilusIconInfo *icon_info;
    gint scale;

    file = (mautilusFile *) data;

    g_assert (MAUTILUS_IS_FILE (file));
    canvas_view = get_canvas_view (container);
    g_return_val_if_fail (canvas_view != NULL, NULL);

    flags = MAUTILUS_FILE_ICON_FLAGS_USE_EMBLEMS |
            MAUTILUS_FILE_ICON_FLAGS_USE_THUMBNAILS;

    if (for_drag_accept)
    {
        flags |= MAUTILUS_FILE_ICON_FLAGS_FOR_DRAG_ACCEPT;
    }

    scale = gtk_widget_get_scale_factor (GTK_WIDGET (canvas_view));
    icon_info = mautilus_file_get_icon (file, size, scale, flags);

    return icon_info;
}

static char *
mautilus_canvas_view_container_get_icon_description (mautilusCanvasContainer *container,
                                                     mautilusCanvasIconData  *data)
{
    mautilusFile *file;
    char *mime_type;
    const char *description;

    file = MAUTILUS_FILE (data);
    g_assert (MAUTILUS_IS_FILE (file));

    mime_type = mautilus_file_get_mime_type (file);
    description = g_content_type_get_description (mime_type);
    g_free (mime_type);
    return g_strdup (description);
}

static void
mautilus_canvas_view_container_prioritize_thumbnailing (mautilusCanvasContainer *container,
                                                        mautilusCanvasIconData  *data)
{
    mautilusFile *file;
    char *uri;

    file = (mautilusFile *) data;

    g_assert (MAUTILUS_IS_FILE (file));

    if (mautilus_file_is_thumbnailing (file))
    {
        uri = mautilus_file_get_uri (file);
        mautilus_thumbnail_prioritize (uri);
        g_free (uri);
    }
}

static GQuark *
get_quark_from_strv (gchar **value)
{
    GQuark *quark;
    int i;

    quark = g_new0 (GQuark, g_strv_length (value) + 1);
    for (i = 0; value[i] != NULL; ++i)
    {
        quark[i] = g_quark_from_string (value[i]);
    }

    return quark;
}

/*
 * Get the preference for which caption text should appear
 * beneath icons.
 */
static GQuark *
mautilus_canvas_view_container_get_icon_text_attributes_from_preferences (void)
{
    GQuark *attributes;
    gchar **value;

    value = g_settings_get_strv (mautilus_icon_view_preferences,
                                 MAUTILUS_PREFERENCES_ICON_VIEW_CAPTIONS);
    attributes = get_quark_from_strv (value);
    g_strfreev (value);

    /* We don't need to sanity check the attributes list even though it came
     * from preferences.
     *
     * There are 2 ways that the values in the list could be bad.
     *
     * 1) The user picks "bad" values.  "bad" values are those that result in
     *    there being duplicate attributes in the list.
     *
     * 2) Value stored in GConf are tampered with.  Its possible physically do
     *    this by pulling the rug underneath GConf and manually editing its
     *    config files.  Its also possible to use a third party GConf key
     *    editor and store garbage for the keys in question.
     *
     * Thankfully, the mautilus preferences machinery deals with both of
     * these cases.
     *
     * In the first case, the preferences dialog widgetry prevents
     * duplicate attributes by making "bad" choices insensitive.
     *
     * In the second case, the preferences getter (and also the auto storage) for
     * string_array values are always valid members of the enumeration associated
     * with the preference.
     *
     * So, no more error checking on attributes is needed here and we can return
     * a the auto stored value.
     */
    return attributes;
}

static int
quarkv_length (GQuark *attributes)
{
    int i;
    i = 0;
    while (attributes[i] != 0)
    {
        i++;
    }
    return i;
}

/**
 * mautilus_canvas_view_get_icon_text_attribute_names:
 *
 * Get a list representing which text attributes should be displayed
 * beneath an icon. The result is dependent on zoom level and possibly
 * user configuration. Don't free the result.
 * @view: mautilusCanvasView to query.
 *
 **/
static GQuark *
mautilus_canvas_view_container_get_icon_text_attribute_names (mautilusCanvasContainer *container,
                                                              int                     *len)
{
    GQuark *attributes;
    int piece_count;

    const int pieces_by_level[] =
    {
        1,              /* MAUTILUS_ZOOM_LEVEL_SMALL */
        2,              /* MAUTILUS_ZOOM_LEVEL_STANDARD */
        3,              /* MAUTILUS_ZOOM_LEVEL_LARGE */
        3,              /* MAUTILUS_ZOOM_LEVEL_LARGER */
    };

    piece_count = pieces_by_level[mautilus_canvas_container_get_zoom_level (container)];

    attributes = mautilus_canvas_view_container_get_icon_text_attributes_from_preferences ();

    *len = MIN (piece_count, quarkv_length (attributes));

    return attributes;
}

/* This callback returns the text, both the editable part, and the
 * part below that is not editable.
 */
static void
mautilus_canvas_view_container_get_icon_text (mautilusCanvasContainer  *container,
                                              mautilusCanvasIconData   *data,
                                              char                    **editable_text,
                                              char                    **additional_text,
                                              gboolean                  include_invisible)
{
    GQuark *attributes;
    char *text_array[4];
    int i, j, num_attributes;
    mautilusCanvasView *canvas_view;
    mautilusFile *file;
    gboolean use_additional;

    file = MAUTILUS_FILE (data);

    g_assert (MAUTILUS_IS_FILE (file));
    g_assert (editable_text != NULL);
    canvas_view = get_canvas_view (container);
    g_return_if_fail (canvas_view != NULL);

    use_additional = (additional_text != NULL);

    /* Strip the suffix for mautilus object xml files. */
    *editable_text = mautilus_file_get_display_name (file);

    if (!use_additional)
    {
        return;
    }

    if (mautilus_file_is_mautilus_link (file))
    {
        /* Don't show the normal extra information for desktop files,
         * it doesn't make sense. */
        *additional_text = NULL;
        return;
    }

    /* Find out what attributes go below each icon. */
    attributes = mautilus_canvas_view_container_get_icon_text_attribute_names (container,
                                                                               &num_attributes);

    /* Get the attributes. */
    j = 0;
    for (i = 0; i < num_attributes; ++i)
    {
        char *text;
        if (attributes[i] == attribute_none_q)
        {
            continue;
        }
        text = mautilus_file_get_string_attribute_q (file, attributes[i]);
        if (text == NULL)
        {
            continue;
        }
        text_array[j++] = text;
    }
    text_array[j] = NULL;

    /* Return them. */
    if (j == 0)
    {
        *additional_text = NULL;
    }
    else if (j == 1)
    {
        /* Only one item, avoid the strdup + free */
        *additional_text = text_array[0];
    }
    else
    {
        *additional_text = g_strjoinv ("\n", text_array);

        for (i = 0; i < j; i++)
        {
            g_free (text_array[i]);
        }
    }

    g_free (attributes);
}

static int
mautilus_canvas_view_container_compare_icons (mautilusCanvasContainer *container,
                                              mautilusCanvasIconData  *icon_a,
                                              mautilusCanvasIconData  *icon_b)
{
    mautilusCanvasView *canvas_view;

    canvas_view = get_canvas_view (container);
    g_return_val_if_fail (canvas_view != NULL, 0);

    /* Type unsafe comparisons for performance */
    return mautilus_canvas_view_compare_files (canvas_view,
                                               (mautilusFile *) icon_a,
                                               (mautilusFile *) icon_b);
}

static int
mautilus_canvas_view_container_compare_icons_by_name (mautilusCanvasContainer *container,
                                                      mautilusCanvasIconData  *icon_a,
                                                      mautilusCanvasIconData  *icon_b)
{
    return mautilus_file_compare_for_sort
               (MAUTILUS_FILE (icon_a),
               MAUTILUS_FILE (icon_b),
               MAUTILUS_FILE_SORT_BY_DISPLAY_NAME,
               FALSE, FALSE);
}

static void
mautilus_canvas_view_container_class_init (mautilusCanvasViewContainerClass *klass)
{
    mautilusCanvasContainerClass *ic_class;

    ic_class = &klass->parent_class;

    attribute_none_q = g_quark_from_static_string ("none");

    ic_class->get_icon_text = mautilus_canvas_view_container_get_icon_text;
    ic_class->get_icon_images = mautilus_canvas_view_container_get_icon_images;
    ic_class->get_icon_description = mautilus_canvas_view_container_get_icon_description;
    ic_class->prioritize_thumbnailing = mautilus_canvas_view_container_prioritize_thumbnailing;

    ic_class->compare_icons = mautilus_canvas_view_container_compare_icons;
    ic_class->compare_icons_by_name = mautilus_canvas_view_container_compare_icons_by_name;
}

static void
mautilus_canvas_view_container_init (mautilusCanvasViewContainer *canvas_container)
{
    gtk_style_context_add_class (gtk_widget_get_style_context (GTK_WIDGET (canvas_container)),
                                 GTK_STYLE_CLASS_VIEW);
}

mautilusCanvasContainer *
mautilus_canvas_view_container_construct (mautilusCanvasViewContainer *canvas_container,
                                          mautilusCanvasView          *view)
{
    AtkObject *atk_obj;

    g_return_val_if_fail (MAUTILUS_IS_CANVAS_VIEW (view), NULL);

    canvas_container->view = view;
    atk_obj = gtk_widget_get_accessible (GTK_WIDGET (canvas_container));
    atk_object_set_name (atk_obj, _("Icon View"));

    return MAUTILUS_CANVAS_CONTAINER (canvas_container);
}

mautilusCanvasContainer *
mautilus_canvas_view_container_new (mautilusCanvasView *view)
{
    return mautilus_canvas_view_container_construct
               (g_object_new (MAUTILUS_TYPE_CANVAS_VIEW_CONTAINER, NULL),
               view);
}
