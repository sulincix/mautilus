/*
   mautilus-vfs-file.h: Subclass of mautilusFile to implement the
   the case of a VFS file.
 
   Copyright (C) 1999, 2000 Eazel, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Darin Adler <darin@bentspoon.com>
*/

#ifndef MAUTILUS_VFS_FILE_H
#define MAUTILUS_VFS_FILE_H

#include "mautilus-file.h"

#define MAUTILUS_TYPE_VFS_FILE mautilus_vfs_file_get_type()
#define MAUTILUS_VFS_FILE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_VFS_FILE, mautilusVFSFile))
#define MAUTILUS_VFS_FILE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_VFS_FILE, mautilusVFSFileClass))
#define MAUTILUS_IS_VFS_FILE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_VFS_FILE))
#define MAUTILUS_IS_VFS_FILE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_VFS_FILE))
#define MAUTILUS_VFS_FILE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_VFS_FILE, mautilusVFSFileClass))

typedef struct mautilusVFSFileDetails mautilusVFSFileDetails;

typedef struct {
	mautilusFile parent_slot;
} mautilusVFSFile;

typedef struct {
	mautilusFileClass parent_slot;
} mautilusVFSFileClass;

GType   mautilus_vfs_file_get_type (void);

#endif /* MAUTILUS_VFS_FILE_H */
