/*
   mautilus-file-private.h:
 
   Copyright (C) 1999, 2000, 2001 Eazel, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Darin Adler <darin@bentspoon.com>
*/

#ifndef MAUTILUS_FILE_PRIVATE_H
#define MAUTILUS_FILE_PRIVATE_H

#include "mautilus-directory.h"
#include "mautilus-file.h"
#include "mautilus-monitor.h"
#include "mautilus-file-undo-operations.h"
#include <eel/eel-glib-extensions.h>
#include <eel/eel-string.h>

#define MAUTILUS_FILE_DEFAULT_ATTRIBUTES				\
	"standard::*,access::*,mountable::*,time::*,unix::*,owner::*,selinux::*,thumbnail::*,id::filesystem,trash::orig-path,trash::deletion-date,metadata::*,recent::*"

/* These are in the typical sort order. Known things come first, then
 * things where we can't know, finally things where we don't yet know.
 */
typedef enum {
	KNOWN,
	UNKNOWABLE,
	UNKNOWN
} Knowledge;

struct mautilusFileDetails
{
	mautilusDirectory *directory;
	
	eel_ref_str name;

	/* File info: */
	GFileType type;

	eel_ref_str display_name;
	char *display_name_collation_key;
	char *directory_name_collation_key;
	eel_ref_str edit_name;

	goffset size; /* -1 is unknown */
	
	int sort_order;
	
	guint32 permissions;
	int uid; /* -1 is none */
	int gid; /* -1 is none */

	eel_ref_str owner;
	eel_ref_str owner_real;
	eel_ref_str group;
	
	time_t atime; /* 0 is unknown */
	time_t mtime; /* 0 is unknown */
	
	char *symlink_name;
	
	eel_ref_str mime_type;
	
	char *selinux_context;
	char *description;
	
	GError *get_info_error;
	
	guint directory_count;

	guint deep_directory_count;
	guint deep_file_count;
	guint deep_unreadable_count;
	goffset deep_size;

	GIcon *icon;
	
	char *thumbnail_path;
	GdkPixbuf *thumbnail;
	time_t thumbnail_mtime;

	GdkPixbuf *scaled_thumbnail;
	double thumbnail_scale;

	GList *mime_list; /* If this is a directory, the list of MIME types in it. */

	/* Info you might get from a link (.desktop, .directory or mautilus link) */
	GIcon *custom_icon;
	char *activation_uri;

	/* used during DND, for checking whether source and destination are on
	 * the same file system.
	 */
	eel_ref_str filesystem_id;

	char *trash_orig_path;

	/* The following is for file operations in progress. Since
	 * there are normally only a few of these, we can move them to
	 * a separate hash table or something if required to keep the
	 * file objects small.
	 */
	GList *operations_in_progress;

	/* mautilusInfoProviders that need to be run for this file */
	GList *pending_info_providers;

	/* Emblems provided by extensions */
	GList *extension_emblems;
	GList *pending_extension_emblems;

	/* Attributes provided by extensions */
	GHashTable *extension_attributes;
	GHashTable *pending_extension_attributes;

	GHashTable *metadata;

	/* Mount for mountpoint or the references GMount for a "mountable" */
	GMount *mount;
	
	/* boolean fields: bitfield to save space, since there can be
           many mautilusFile objects. */

	eel_boolean_bit unconfirmed                   : 1;
	eel_boolean_bit is_gone                       : 1;
	/* Set when emitting files_added on the directory to make sure we
	   add a file, and only once */
	eel_boolean_bit is_added                      : 1;
	/* Set by the mautilusDirectory while it's loading the file
	 * list so the file knows not to do redundant I/O.
	 */
	eel_boolean_bit loading_directory             : 1;
	eel_boolean_bit got_file_info                 : 1;
	eel_boolean_bit get_info_failed               : 1;
	eel_boolean_bit file_info_is_up_to_date       : 1;
	
	eel_boolean_bit got_directory_count           : 1;
	eel_boolean_bit directory_count_failed        : 1;
	eel_boolean_bit directory_count_is_up_to_date : 1;

	eel_boolean_bit deep_counts_status      : 2; /* mautilusRequestStatus */
	/* no deep_counts_are_up_to_date field; since we expose
           intermediate values for this attribute, we do actually
           forget it rather than invalidating. */

	eel_boolean_bit got_mime_list                 : 1;
	eel_boolean_bit mime_list_failed              : 1;
	eel_boolean_bit mime_list_is_up_to_date       : 1;

	eel_boolean_bit mount_is_up_to_date           : 1;
	
	eel_boolean_bit got_link_info                 : 1;
	eel_boolean_bit link_info_is_up_to_date       : 1;
	eel_boolean_bit got_custom_display_name       : 1;
	eel_boolean_bit got_custom_activation_uri     : 1;

	eel_boolean_bit thumbnail_is_up_to_date       : 1;
	eel_boolean_bit thumbnail_wants_original      : 1;
	eel_boolean_bit thumbnail_tried_original      : 1;
	eel_boolean_bit thumbnailing_failed           : 1;
	
	eel_boolean_bit is_thumbnailing               : 1;

	eel_boolean_bit is_launcher                   : 1;
	eel_boolean_bit is_trusted_link               : 1;
	eel_boolean_bit is_foreign_link               : 1;
	eel_boolean_bit is_symlink                    : 1;
	eel_boolean_bit is_mountpoint                 : 1;
	eel_boolean_bit is_hidden                     : 1;

	eel_boolean_bit has_permissions               : 1;
	
	eel_boolean_bit can_read                      : 1;
	eel_boolean_bit can_write                     : 1;
	eel_boolean_bit can_execute                   : 1;
	eel_boolean_bit can_delete                    : 1;
	eel_boolean_bit can_trash                     : 1;
	eel_boolean_bit can_rename                    : 1;
	eel_boolean_bit can_mount                     : 1;
	eel_boolean_bit can_unmount                   : 1;
	eel_boolean_bit can_eject                     : 1;
	eel_boolean_bit can_start                     : 1;
	eel_boolean_bit can_start_degraded            : 1;
	eel_boolean_bit can_stop                      : 1;
	eel_boolean_bit start_stop_type               : 3; /* GDriveStartStopType */
	eel_boolean_bit can_poll_for_media            : 1;
	eel_boolean_bit is_media_check_automatic      : 1;

	eel_boolean_bit filesystem_readonly           : 1;
	eel_boolean_bit filesystem_use_preview        : 2; /* GFilesystemPreviewType */
	eel_boolean_bit filesystem_info_is_up_to_date : 1;
        eel_ref_str     filesystem_type;

	time_t trash_time; /* 0 is unknown */
	time_t recency; /* 0 is unknown */

	gdouble search_relevance;
	gchar *fts_snippet;

	guint64 free_space; /* (guint)-1 for unknown */
	time_t free_space_read; /* The time free_space was updated, or 0 for never */
};

typedef struct {
	mautilusFile *file;
	GList *files;
	gint renamed_files;
	gint skipped_files;
	GCancellable *cancellable;
	mautilusFileOperationCallback callback;
	gpointer callback_data;
	gboolean is_rename;
	
	gpointer data;
	GDestroyNotify free_data;
	mautilusFileUndoInfo *undo_info;
} mautilusFileOperation;

mautilusFile *mautilus_file_new_from_info                  (mautilusDirectory      *directory,
							    GFileInfo              *info);
void          mautilus_file_emit_changed                   (mautilusFile           *file);
void          mautilus_file_mark_gone                      (mautilusFile           *file);

gboolean      mautilus_file_get_date                       (mautilusFile           *file,
							    mautilusDateType        date_type,
							    time_t                 *date);
void          mautilus_file_updated_deep_count_in_progress (mautilusFile           *file);


void          mautilus_file_clear_info                     (mautilusFile           *file);
/* Compare file's state with a fresh file info struct, return FALSE if
 * no change, update file and return TRUE if the file info contains
 * new state.  */
gboolean      mautilus_file_update_info                    (mautilusFile           *file,
							    GFileInfo              *info);
gboolean      mautilus_file_update_name                    (mautilusFile           *file,
							    const char             *name);
gboolean      mautilus_file_update_metadata_from_info      (mautilusFile           *file,
							    GFileInfo              *info);

gboolean      mautilus_file_update_name_and_directory      (mautilusFile           *file,
							    const char             *name,
							    mautilusDirectory      *directory);

gboolean      mautilus_file_set_display_name               (mautilusFile           *file,
							    const char             *display_name,
							    const char             *edit_name,
							    gboolean                custom);
mautilusDirectory *
              mautilus_file_get_directory                  (mautilusFile           *file);
void          mautilus_file_set_directory                  (mautilusFile           *file,
							    mautilusDirectory      *directory);
void          mautilus_file_set_mount                      (mautilusFile           *file,
							    GMount                 *mount);

/* Mark specified attributes for this file out of date without canceling current
 * I/O or kicking off new I/O.
 */
void                   mautilus_file_invalidate_attributes_internal     (mautilusFile           *file,
									 mautilusFileAttributes  file_attributes);
mautilusFileAttributes mautilus_file_get_all_attributes                 (void);
gboolean               mautilus_file_is_self_owned                      (mautilusFile           *file);
void                   mautilus_file_invalidate_count_and_mime_list     (mautilusFile           *file);
gboolean               mautilus_file_rename_in_progress                 (mautilusFile           *file);
void                   mautilus_file_invalidate_extension_info_internal (mautilusFile           *file);
void                   mautilus_file_info_providers_done                (mautilusFile           *file);


/* Thumbnailing: */
void          mautilus_file_set_is_thumbnailing            (mautilusFile           *file,
							    gboolean                is_thumbnailing);

mautilusFileOperation *mautilus_file_operation_new      (mautilusFile                  *file,
							 mautilusFileOperationCallback  callback,
							 gpointer                       callback_data);
void                   mautilus_file_operation_free     (mautilusFileOperation         *op);
void                   mautilus_file_operation_complete (mautilusFileOperation         *op,
							 GFile                         *result_location,
							 GError                        *error);
void                   mautilus_file_operation_cancel   (mautilusFileOperation         *op);

#endif
