#ifndef MAUTILUS_CONTAINER_MAX_WIDTH_H
#define MAUTILUS_CONTAINER_MAX_WIDTH_H

#include <glib.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define MAUTILUS_TYPE_CONTAINER_MAX_WIDTH (mautilus_container_max_width_get_type())

G_DECLARE_FINAL_TYPE (mautilusContainerMaxWidth, mautilus_container_max_width, MAUTILUS, CONTAINER_MAX_WIDTH, GtkBin)

mautilusContainerMaxWidth *mautilus_container_max_width_new (void);

void mautilus_container_max_width_set_max_width (mautilusContainerMaxWidth *self,
                                                 guint                      max_width);
guint mautilus_container_max_width_get_max_width (mautilusContainerMaxWidth *self);

G_END_DECLS

#endif /* MAUTILUS_CONTAINER_MAX_WIDTH_H */

