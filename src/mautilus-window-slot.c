/*
 *  mautilus-window-slot.c: mautilus window slot
 *
 *  Copyright (C) 2008 Free Software Foundation, Inc.
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public
 *  License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Christian Neumair <cneumair@gnome.org>
 */

#include "config.h"

#include "mautilus-window-slot.h"

#include "mautilus-application.h"
#include "mautilus-canvas-view.h"
#include "mautilus-list-view.h"
#include "mautilus-mime-actions.h"
#include "mautilus-special-location-bar.h"
#include "mautilus-toolbar.h"
#include "mautilus-trash-bar.h"
#include "mautilus-trash-monitor.h"
#include "mautilus-view.h"
#include "mautilus-window.h"
#include "mautilus-x-content-bar.h"

#include <glib/gi18n.h>

#include "mautilus-file.h"
#include "mautilus-file-utilities.h"
#include "mautilus-global-preferences.h"
#include "mautilus-module.h"
#include "mautilus-monitor.h"
#include "mautilus-profile.h"
#include <libmautilus-extension/mautilus-location-widget-provider.h>
#include "mautilus-ui-utilities.h"
#include <eel/eel-vfs-extensions.h>

enum
{
    ACTIVE,
    INACTIVE,
    LAST_SIGNAL
};

enum
{
    PROP_ACTIVE = 1,
    PROP_WINDOW,
    PROP_ICON,
    PROP_TOOLBAR_MENU_SECTIONS,
    PROP_LOADING,
    PROP_LOCATION,
    NUM_PROPERTIES
};

typedef struct
{
    mautilusWindow *window;

    gboolean active : 1;
    guint loading : 1;

    /* slot contains
     *  1) an vbox containing extra_location_widgets
     *  2) the view
     */
    GtkWidget *extra_location_widgets;

    /* Slot actions */
    GActionGroup *slot_action_group;

    /* Current location. */
    GFile *location;
    gchar *title;

    /* Viewed file */
    mautilusView *content_view;
    mautilusView *new_content_view;
    mautilusFile *viewed_file;
    gboolean viewed_file_seen;
    gboolean viewed_file_in_trash;

    /* Information about bookmarks and history list */
    mautilusBookmark *current_location_bookmark;
    mautilusBookmark *last_location_bookmark;
    GList *back_list;
    GList *forward_list;

    /* Query editor */
    mautilusQueryEditor *query_editor;
    gulong qe_changed_id;
    gulong qe_cancel_id;
    gulong qe_activated_id;

    /* Load state */
    GCancellable *find_mount_cancellable;
    /* It could be either the view is loading the files or the search didn't
     * finish. Used for showing a spinner to provide feedback to the user. */
    gboolean allow_stop;
    gboolean needs_reload;
    gchar *pending_search_text;

    /* New location. */
    GFile *pending_location;
    mautilusLocationChangeType location_change_type;
    guint location_change_distance;
    char *pending_scroll_to;
    GList *pending_selection;
    mautilusFile *pending_file_to_activate;
    mautilusFile *determine_view_file;
    GCancellable *mount_cancellable;
    GError *mount_error;
    gboolean tried_mount;
    gint view_mode_before_search;
} mautilusWindowSlotPrivate;

G_DEFINE_TYPE_WITH_PRIVATE (mautilusWindowSlot, mautilus_window_slot, GTK_TYPE_BOX);

static guint signals[LAST_SIGNAL] = { 0 };
static GParamSpec *properties[NUM_PROPERTIES] = { NULL, };

static void mautilus_window_slot_force_reload (mautilusWindowSlot *self);
static void change_view (mautilusWindowSlot *self);
static void hide_query_editor (mautilusWindowSlot *self);
static void mautilus_window_slot_sync_actions (mautilusWindowSlot *self);
static void mautilus_window_slot_connect_new_content_view (mautilusWindowSlot *self);
static void mautilus_window_slot_disconnect_content_view (mautilusWindowSlot *self);
static gboolean mautilus_window_slot_content_view_matches (mautilusWindowSlot *self,
                                                           guint               id);
static mautilusView *mautilus_window_slot_get_view_for_location (mautilusWindowSlot *self,
                                                                 GFile              *location);
static void mautilus_window_slot_set_content_view (mautilusWindowSlot *self,
                                                   guint               id);
static void mautilus_window_slot_set_loading (mautilusWindowSlot *self,
                                              gboolean            loading);
char *mautilus_window_slot_get_location_uri (mautilusWindowSlot *self);
static void mautilus_window_slot_set_search_visible (mautilusWindowSlot *self,
                                                     gboolean            visible);
static gboolean mautilus_window_slot_get_search_visible (mautilusWindowSlot *self);
static void mautilus_window_slot_set_location (mautilusWindowSlot *self,
                                               GFile              *location);
static void trash_state_changed_cb (mautilusTrashMonitor *monitor,
                                    gboolean              is_empty,
                                    gpointer              user_data);

void
mautilus_window_slot_restore_from_data (mautilusWindowSlot *self,
                                             RestoreTabData     *data)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);

    priv->back_list = g_list_copy_deep (data->back_list, (GCopyFunc) g_object_ref, NULL);

    priv->forward_list = g_list_copy_deep (data->forward_list, (GCopyFunc) g_object_ref, NULL);

    priv->view_mode_before_search = data->view_before_search;

    priv->location_change_type = MAUTILUS_LOCATION_CHANGE_RELOAD;
}

RestoreTabData*
mautilus_window_slot_get_restore_tab_data (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;
    RestoreTabData *data;
    GList *back_list;
    GList *forward_list;

    priv = mautilus_window_slot_get_instance_private (self);

    if (priv->location == NULL)
    {
        return NULL;
    }

    back_list = g_list_copy_deep (priv->back_list,
                                  (GCopyFunc) g_object_ref,
                                  NULL);
    forward_list = g_list_copy_deep (priv->forward_list,
                                     (GCopyFunc) g_object_ref,
                                     NULL);

    /* This data is used to restore a tab after it was closed.
     * In order to do that we need to keep the history, what was
     * the view mode before search and a reference to the file.
     * A GFile isn't enough, as the mautilusFile also keeps a
     * reference to the search directory */
    data = g_new0 (RestoreTabData, 1);
    data->back_list = back_list;
    data->forward_list = forward_list;
    data->file = mautilus_file_get (priv->location);
    data->view_before_search = priv->view_mode_before_search;

    return data;
}

gboolean
mautilus_window_slot_handles_location (mautilusWindowSlot *self,
                                       GFile              *location)
{
    return MAUTILUS_WINDOW_SLOT_CLASS (G_OBJECT_GET_CLASS (self))->handles_location (self, location);
}

static gboolean
real_handles_location (mautilusWindowSlot *self,
                       GFile              *location)
{
    mautilusFile *file;
    gboolean handles_location;
    g_autofree char *uri = NULL;

    uri = g_file_get_uri(location);

    file = mautilus_file_get (location);
    handles_location = !mautilus_file_is_other_locations (file) &&
                       !eel_uri_is_desktop (uri);
    mautilus_file_unref (file);

    return handles_location;
}

static mautilusView *
mautilus_window_slot_get_view_for_location (mautilusWindowSlot *self,
                                            GFile              *location)
{
    return MAUTILUS_WINDOW_SLOT_CLASS (G_OBJECT_GET_CLASS (self))->get_view_for_location (self, location);
}

static mautilusView *
real_get_view_for_location (mautilusWindowSlot *self,
                            GFile              *location)
{
    mautilusWindowSlotPrivate *priv;

    mautilusView *view;
    mautilusFile *file;

    priv = mautilus_window_slot_get_instance_private (self);
    file = mautilus_file_get (location);
    view = NULL;
    guint view_id;

    view_id = MAUTILUS_VIEW_INVALID_ID;

    /* If we are in search, try to use by default list view. */
    if (mautilus_file_is_in_search (file))
    {
        /* If it's already set, is because we already made the change to search mode,
         * so the view mode of the current view will be the one search is using,
         * which is not the one we are interested in */
        if (priv->view_mode_before_search == MAUTILUS_VIEW_INVALID_ID && priv->content_view)
        {
            priv->view_mode_before_search = mautilus_files_view_get_view_id (priv->content_view);
        }
        view_id = g_settings_get_enum (mautilus_preferences, MAUTILUS_PREFERENCES_SEARCH_VIEW);
    }
    else if (priv->content_view != NULL)
    {
        /* If there is already a view, just use the view mode that it's currently using, or
         * if we were on search before, use what we were using before entering
         * search mode */
        if (priv->view_mode_before_search != MAUTILUS_VIEW_INVALID_ID)
        {
            view_id = priv->view_mode_before_search;
            priv->view_mode_before_search = MAUTILUS_VIEW_INVALID_ID;
        }
        else
        {
            view_id = mautilus_files_view_get_view_id (priv->content_view);
        }
    }

    /* If there is not previous view in this slot, use the default view mode
     * from preferences */
    if (view_id == MAUTILUS_VIEW_INVALID_ID)
    {
        view_id = g_settings_get_enum (mautilus_preferences, MAUTILUS_PREFERENCES_DEFAULT_FOLDER_VIEWER);
    }

    /* Try to reuse the current view */
    if (mautilus_window_slot_content_view_matches (self, view_id))
    {
        view = priv->content_view;
    }
    else
    {
        view = MAUTILUS_VIEW (mautilus_files_view_new (view_id, self));
    }

    mautilus_file_unref (file);

    return view;
}

static gboolean
mautilus_window_slot_content_view_matches (mautilusWindowSlot *self,
                                           guint               id)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->content_view == NULL)
    {
        return FALSE;
    }

    if (id != MAUTILUS_VIEW_INVALID_ID && MAUTILUS_IS_FILES_VIEW (priv->content_view))
    {
        return mautilus_files_view_get_view_id (priv->content_view) == id;
    }
    else
    {
        return FALSE;
    }
}

static void
update_search_visible (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;
    mautilusQuery *query;
    mautilusView *view;

    priv = mautilus_window_slot_get_instance_private (self);

    view = mautilus_window_slot_get_current_view (self);
    /* If we changed location just to another search location, for example,
     * when changing the query, just keep the search visible.
     * Make sure the search is visible though, since we could be returning
     * from a previous search location when using the history */
    if (mautilus_view_is_searching (view))
    {
        mautilus_window_slot_set_search_visible (self, TRUE);
        return;
    }

    query = mautilus_query_editor_get_query (priv->query_editor);
    if (query)
    {
        /* If the view is not searching, but search is visible, and the
         * query is empty, we don't hide it. Some users enable the search
         * and then change locations, then they search. */
        if (!mautilus_query_is_empty (query))
        {
            mautilus_window_slot_set_search_visible (self, FALSE);
        }
        g_object_unref (query);
    }

    if (priv->pending_search_text)
    {
        mautilus_window_slot_search (self, g_strdup (priv->pending_search_text));
    }
}

static void
mautilus_window_slot_sync_actions (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    GAction *action;
    GVariant *variant;

    priv = mautilus_window_slot_get_instance_private (self);
    if (!mautilus_window_slot_get_active (self))
    {
        return;
    }

    if (priv->content_view == NULL || priv->new_content_view != NULL)
    {
        return;
    }

    /* Check if we need to close the search or show search after changing the location.
     * Needs to be done after the change has been done, if not, a loop happens,
     * because setting the search enabled or not actually opens a location */
    update_search_visible (self);

    /* Files view mode */
    action = g_action_map_lookup_action (G_ACTION_MAP (priv->slot_action_group), "files-view-mode");
    if (g_action_get_enabled (action))
    {
        variant = g_variant_new_uint32 (mautilus_files_view_get_view_id (mautilus_window_slot_get_current_view (self)));
        g_action_change_state (action, variant);
    }
}

static void
query_editor_cancel_callback (mautilusQueryEditor *editor,
                              mautilusWindowSlot  *self)
{
    mautilus_window_slot_set_search_visible (self, FALSE);
}

static void
query_editor_activated_callback (mautilusQueryEditor *editor,
                                 mautilusWindowSlot  *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->content_view != NULL)
    {
        if (MAUTILUS_IS_FILES_VIEW (priv->content_view))
        {
            mautilus_files_view_activate_selection (MAUTILUS_FILES_VIEW (priv->content_view));
        }
    }
}

static void
query_editor_changed_callback (mautilusQueryEditor *editor,
                               mautilusQuery       *query,
                               gboolean             reload,
                               mautilusWindowSlot  *self)
{
    mautilusView *view;

    view = mautilus_window_slot_get_current_view (self);

    mautilus_view_set_search_query (view, query);
    mautilus_window_slot_open_location_full (self, mautilus_view_get_location (view), 0, NULL);
}

static void
hide_query_editor (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;
    mautilusView *view;

    priv = mautilus_window_slot_get_instance_private (self);
    view = mautilus_window_slot_get_current_view (self);

    gtk_search_bar_set_search_mode (GTK_SEARCH_BAR (priv->query_editor), FALSE);

    if (priv->qe_changed_id > 0)
    {
        g_signal_handler_disconnect (priv->query_editor, priv->qe_changed_id);
        priv->qe_changed_id = 0;
    }
    if (priv->qe_cancel_id > 0)
    {
        g_signal_handler_disconnect (priv->query_editor, priv->qe_cancel_id);
        priv->qe_cancel_id = 0;
    }
    if (priv->qe_activated_id > 0)
    {
        g_signal_handler_disconnect (priv->query_editor, priv->qe_activated_id);
        priv->qe_activated_id = 0;
    }

    mautilus_query_editor_set_query (priv->query_editor, NULL);

    if (mautilus_view_is_searching (view))
    {
        GList *selection;

        selection = mautilus_view_get_selection (view);

        mautilus_view_set_search_query (view, NULL);
        mautilus_window_slot_open_location_full (self,
                                                 mautilus_view_get_location (view),
                                                 0,
                                                 selection);

        mautilus_file_list_free (selection);
    }

    if (mautilus_window_slot_get_active (self))
    {
        gtk_widget_grab_focus (GTK_WIDGET (priv->window));
    }
}

static GFile *
mautilus_window_slot_get_current_location (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->pending_location != NULL)
    {
        return priv->pending_location;
    }

    if (priv->location != NULL)
    {
        return priv->location;
    }

    return NULL;
}

static void
show_query_editor (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;
    mautilusView *view;

    priv = mautilus_window_slot_get_instance_private (self);
    view = mautilus_window_slot_get_current_view (self);

    if (mautilus_view_is_searching (view))
    {
        mautilusQuery *query;

        query = mautilus_view_get_search_query (view);

        if (query != NULL)
        {
            mautilus_query_editor_set_query (priv->query_editor, query);
        }
    }

    gtk_search_bar_set_search_mode (GTK_SEARCH_BAR (priv->query_editor), TRUE);
    gtk_widget_grab_focus (GTK_WIDGET (priv->query_editor));

    if (priv->qe_changed_id == 0)
    {
        priv->qe_changed_id =
            g_signal_connect (priv->query_editor, "changed",
                              G_CALLBACK (query_editor_changed_callback), self);
    }
    if (priv->qe_cancel_id == 0)
    {
        priv->qe_cancel_id =
            g_signal_connect (priv->query_editor, "cancel",
                              G_CALLBACK (query_editor_cancel_callback), self);
    }
    if (priv->qe_activated_id == 0)
    {
        priv->qe_activated_id =
            g_signal_connect (priv->query_editor, "activated",
                              G_CALLBACK (query_editor_activated_callback), self);
    }
}

static void
mautilus_window_slot_set_search_visible (mautilusWindowSlot *self,
                                         gboolean            visible)
{
    mautilusWindowSlotPrivate *priv;
    GAction *action;

    priv = mautilus_window_slot_get_instance_private (self);

    action = g_action_map_lookup_action (G_ACTION_MAP (priv->slot_action_group),
                                         "search-visible");
    g_action_change_state (action, g_variant_new_boolean (visible));
}

static gboolean
mautilus_window_slot_get_search_visible (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;
    GAction *action;
    GVariant *state;
    gboolean searching;

    priv = mautilus_window_slot_get_instance_private (self);
    action = g_action_map_lookup_action (G_ACTION_MAP (priv->slot_action_group),
                                         "search-visible");
    state = g_action_get_state (action);
    searching = g_variant_get_boolean (state);

    g_variant_unref (state);

    return searching;
}

void
mautilus_window_slot_search (mautilusWindowSlot *self,
                             const gchar        *text)
{
    mautilusWindowSlotPrivate *priv;
    mautilusView *view;

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->pending_search_text)
    {
        g_free (priv->pending_search_text);
        priv->pending_search_text = NULL;
    }

    view = mautilus_window_slot_get_current_view (self);
    /* We could call this when the location is still being checked in the
     * window slot. For that, save the search we want to do for once we have
     * a view set up */
    if (view)
    {
        mautilus_window_slot_set_search_visible (self, TRUE);
        mautilus_query_editor_set_text (priv->query_editor, text);
    }
    else
    {
        priv->pending_search_text = g_strdup (text);
    }
}

gboolean
mautilus_window_slot_handle_event (mautilusWindowSlot *self,
                                   GdkEventKey        *event)
{
    mautilusWindowSlotPrivate *priv;
    gboolean retval;
    GAction *action;

    priv = mautilus_window_slot_get_instance_private (self);
    retval = FALSE;
    action = g_action_map_lookup_action (G_ACTION_MAP (priv->slot_action_group),
                                         "search-visible");

    if (event->keyval == GDK_KEY_Escape)
    {
        g_autoptr (GVariant) state = NULL;

        state = g_action_get_state (action);

        if (g_variant_get_boolean (state))
        {
            mautilus_window_slot_set_search_visible (self, FALSE);
        }
    }

    /* If the action is not enabled, don't try to handle search */
    if (g_action_get_enabled (action))
    {
        retval = gtk_search_bar_handle_event (GTK_SEARCH_BAR (priv->query_editor),
                                              (GdkEvent *) event);
    }

    if (retval)
    {
        mautilus_window_slot_set_search_visible (self, TRUE);
    }

    return retval;
}

static void
real_active (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;
    mautilusWindow *window;
    int page_num;

    priv = mautilus_window_slot_get_instance_private (self);
    window = priv->window;
    page_num = gtk_notebook_page_num (GTK_NOTEBOOK (mautilus_window_get_notebook (window)),
                                      GTK_WIDGET (self));
    g_assert (page_num >= 0);

    gtk_notebook_set_current_page (GTK_NOTEBOOK (mautilus_window_get_notebook (window)), page_num);

    /* sync window to new slot */
    mautilus_window_sync_allow_stop (window, self);
    mautilus_window_sync_title (window, self);
    mautilus_window_sync_location_widgets (window);
    mautilus_window_slot_sync_actions (self);

    gtk_widget_insert_action_group (GTK_WIDGET (window), "slot", priv->slot_action_group);
}

static void
real_inactive (mautilusWindowSlot *self)
{
    mautilusWindow *window;

    window = mautilus_window_slot_get_window (self);
    g_assert (self == mautilus_window_get_active_slot (window));

    gtk_widget_insert_action_group (GTK_WIDGET (window), "slot", NULL);
}

static void
remove_all_extra_location_widgets (GtkWidget *widget,
                                   gpointer   data)
{
    mautilusWindowSlotPrivate *priv;
    mautilusWindowSlot *self = data;
    mautilusDirectory *directory;

    priv = mautilus_window_slot_get_instance_private (self);
    directory = mautilus_directory_get (priv->location);
    if (widget != GTK_WIDGET (priv->query_editor))
    {
        gtk_container_remove (GTK_CONTAINER (priv->extra_location_widgets), widget);
    }

    mautilus_directory_unref (directory);
}

static void
mautilus_window_slot_remove_extra_location_widgets (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    gtk_container_foreach (GTK_CONTAINER (priv->extra_location_widgets),
                           remove_all_extra_location_widgets,
                           self);
}

static void
mautilus_window_slot_add_extra_location_widget (mautilusWindowSlot *self,
                                                GtkWidget          *widget)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    gtk_box_pack_start (GTK_BOX (priv->extra_location_widgets),
                        widget, FALSE, TRUE, 0);
    gtk_widget_show (priv->extra_location_widgets);
}

static void
mautilus_window_slot_set_property (GObject      *object,
                                   guint         property_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
    mautilusWindowSlot *self = MAUTILUS_WINDOW_SLOT (object);

    switch (property_id)
    {
        case PROP_ACTIVE:
        {
            mautilus_window_slot_set_active (self, g_value_get_boolean (value));
        }
        break;

        case PROP_WINDOW:
        {
            mautilus_window_slot_set_window (self, g_value_get_object (value));
        }
        break;

        case PROP_LOCATION:
        {
            mautilus_window_slot_set_location (self, g_value_get_object (value));
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
mautilus_window_slot_get_property (GObject    *object,
                                   guint       property_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
    mautilusWindowSlot *self = MAUTILUS_WINDOW_SLOT (object);
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);

    switch (property_id)
    {
        case PROP_ACTIVE:
        {
            g_value_set_boolean (value, mautilus_window_slot_get_active (self));
        }
        break;

        case PROP_WINDOW:
        {
            g_value_set_object (value, priv->window);
        }
        break;

        case PROP_ICON:
        {
            g_value_set_object (value, mautilus_window_slot_get_icon (self));
        }
        break;

        case PROP_TOOLBAR_MENU_SECTIONS:
        {
            g_value_set_pointer (value, mautilus_window_slot_get_toolbar_menu_sections (self));
        }
        break;

        case PROP_LOADING:
        {
            g_value_set_boolean (value, mautilus_window_slot_get_loading (self));
        }
        break;

        case PROP_LOCATION:
        {
            g_value_set_object (value, mautilus_window_slot_get_current_location (self));
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
        }
        break;
    }
}

static void
mautilus_window_slot_constructed (GObject *object)
{
    mautilusWindowSlotPrivate *priv;
    mautilusWindowSlot *self = MAUTILUS_WINDOW_SLOT (object);
    GtkWidget *extras_vbox;
    GtkStyleContext *style_context;

    priv = mautilus_window_slot_get_instance_private (self);
    G_OBJECT_CLASS (mautilus_window_slot_parent_class)->constructed (object);

    gtk_orientable_set_orientation (GTK_ORIENTABLE (self),
                                    GTK_ORIENTATION_VERTICAL);
    gtk_widget_show (GTK_WIDGET (self));

    extras_vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
    style_context = gtk_widget_get_style_context (extras_vbox);
    gtk_style_context_add_class (style_context, "searchbar-container");
    priv->extra_location_widgets = extras_vbox;
    gtk_box_pack_start (GTK_BOX (self), extras_vbox, FALSE, FALSE, 0);
    gtk_widget_show (extras_vbox);

    priv->query_editor = MAUTILUS_QUERY_EDITOR (mautilus_query_editor_new ());
    gtk_widget_show (GTK_WIDGET (priv->query_editor));
    mautilus_window_slot_add_extra_location_widget (self, GTK_WIDGET (priv->query_editor));

    g_object_bind_property (self, "location",
                            priv->query_editor, "location",
                            G_BINDING_DEFAULT);

    priv->title = g_strdup (_("Loading…"));
}

static void
action_search_visible (GSimpleAction *action,
                       GVariant      *state,
                       gpointer       user_data)
{
    mautilusWindowSlot *self;
    GVariant *current_state;

    self = MAUTILUS_WINDOW_SLOT (user_data);
    current_state = g_action_get_state (G_ACTION (action));
    if (g_variant_get_boolean (current_state) != g_variant_get_boolean (state))
    {
        g_simple_action_set_state (action, state);

        if (g_variant_get_boolean (state))
        {
            show_query_editor (self);
        }
        else
        {
            hide_query_editor (self);
        }
    }

    g_variant_unref (current_state);
}

static void
change_files_view_mode (mautilusWindowSlot *self,
                        guint               view_id)
{
    const gchar *preferences_key;

    mautilus_window_slot_set_content_view (self, view_id);
    preferences_key = mautilus_view_is_searching (mautilus_window_slot_get_current_view (self)) ?
                      MAUTILUS_PREFERENCES_SEARCH_VIEW :
                      MAUTILUS_PREFERENCES_DEFAULT_FOLDER_VIEWER;

    g_settings_set_enum (mautilus_preferences, preferences_key, view_id);
}

static void
action_files_view_mode_toggle (GSimpleAction *action,
                               GVariant      *value,
                               gpointer       user_data)
{
    mautilusWindowSlot *self;
    mautilusWindowSlotPrivate *priv;
    guint current_view_id;

    self = MAUTILUS_WINDOW_SLOT (user_data);
    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->content_view == NULL)
    {
        return;
    }

    current_view_id = mautilus_files_view_get_view_id (priv->content_view);
    if (current_view_id == MAUTILUS_VIEW_LIST_ID)
    {
        change_files_view_mode (self, MAUTILUS_VIEW_GRID_ID);
    }
    else
    {
        change_files_view_mode (self, MAUTILUS_VIEW_LIST_ID);
    }
}

static void
action_files_view_mode (GSimpleAction *action,
                        GVariant      *value,
                        gpointer       user_data)
{
    mautilusWindowSlot *self;
    guint view_id;

    view_id = g_variant_get_uint32 (value);
    self = MAUTILUS_WINDOW_SLOT (user_data);

    if (!MAUTILUS_IS_FILES_VIEW (mautilus_window_slot_get_current_view (self)))
    {
        return;
    }

    change_files_view_mode (self, view_id);

    g_simple_action_set_state (action, value);
}

const GActionEntry slot_entries[] =
{
    /* 4 is MAUTILUS_VIEW_INVALID_ID */
    { "files-view-mode", NULL, "u", "uint32 4", action_files_view_mode },
    { "files-view-mode-toggle", action_files_view_mode_toggle },
    { "search-visible", NULL, NULL, "false", action_search_visible },
};

static void
mautilus_window_slot_init (mautilusWindowSlot *self)
{
    GApplication *app;
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    app = g_application_get_default ();

    g_signal_connect (mautilus_trash_monitor_get (),
                      "trash-state-changed",
                      G_CALLBACK (trash_state_changed_cb), self);

    priv->slot_action_group = G_ACTION_GROUP (g_simple_action_group_new ());
    g_action_map_add_action_entries (G_ACTION_MAP (priv->slot_action_group),
                                     slot_entries,
                                     G_N_ELEMENTS (slot_entries),
                                     self);
    gtk_widget_insert_action_group (GTK_WIDGET (self),
                                    "slot",
                                    G_ACTION_GROUP (priv->slot_action_group));
    mautilus_application_set_accelerator (app, "slot.files-view-mode(uint32 1)", "<control>1");
    mautilus_application_set_accelerator (app, "slot.files-view-mode(uint32 0)", "<control>2");
    mautilus_application_set_accelerator (app, "slot.search-visible", "<control>f");

    priv->view_mode_before_search = MAUTILUS_VIEW_INVALID_ID;
}

#define DEBUG_FLAG MAUTILUS_DEBUG_WINDOW
#include "mautilus-debug.h"

static void begin_location_change (mautilusWindowSlot        *slot,
                                   GFile                     *location,
                                   GFile                     *previous_location,
                                   GList                     *new_selection,
                                   mautilusLocationChangeType type,
                                   guint                      distance,
                                   const char                *scroll_pos);
static void free_location_change (mautilusWindowSlot *self);
static void end_location_change (mautilusWindowSlot *self);
static void got_file_info_for_view_selection_callback (mautilusFile *file,
                                                       gpointer      callback_data);
static gboolean setup_view (mautilusWindowSlot *self,
                            mautilusView       *view);
static void load_new_location (mautilusWindowSlot *slot,
                               GFile              *location,
                               GList              *selection,
                               mautilusFile       *file_to_activate,
                               gboolean            tell_current_content_view,
                               gboolean            tell_new_content_view);

void
mautilus_window_slot_open_location_full (mautilusWindowSlot      *self,
                                         GFile                   *location,
                                         mautilusWindowOpenFlags  flags,
                                         GList                   *new_selection)
{
    mautilusWindowSlotPrivate *priv;
    GFile *old_location;
    GList *old_selection;

    priv = mautilus_window_slot_get_instance_private (self);
    old_selection = NULL;
    old_location = mautilus_window_slot_get_location (self);

    if (priv->content_view)
    {
        old_selection = mautilus_view_get_selection (priv->content_view);
    }
    if (old_location && g_file_equal (old_location, location) &&
        mautilus_file_selection_equal (old_selection, new_selection))
    {
        goto done;
    }

    begin_location_change (self, location, old_location, new_selection,
                           MAUTILUS_LOCATION_CHANGE_STANDARD, 0, NULL);

done:
    mautilus_file_list_free (old_selection);
    mautilus_profile_end (NULL);
}

static GList *
check_select_old_location_containing_folder (GList *new_selection,
                                             GFile *location,
                                             GFile *previous_location)
{
    GFile *from_folder, *parent;

    /* If there is no new selection and the new location is
     * a (grand)parent of the old location then we automatically
     * select the folder the previous location was in */
    if (new_selection == NULL && previous_location != NULL &&
        g_file_has_prefix (previous_location, location))
    {
        from_folder = g_object_ref (previous_location);
        parent = g_file_get_parent (from_folder);
        while (parent != NULL && !g_file_equal (parent, location))
        {
            g_object_unref (from_folder);
            from_folder = parent;
            parent = g_file_get_parent (from_folder);
        }

        if (parent != NULL)
        {
            new_selection = g_list_prepend (NULL, mautilus_file_get (from_folder));
            g_object_unref (parent);
        }

        g_object_unref (from_folder);
    }

    return new_selection;
}

static void
check_force_reload (GFile                      *location,
                    mautilusLocationChangeType  type)
{
    mautilusDirectory *directory;
    mautilusFile *file;
    gboolean force_reload;

    /* The code to force a reload is here because if we do it
     * after determining an initial view (in the components), then
     * we end up fetching things twice.
     */
    directory = mautilus_directory_get (location);
    file = mautilus_file_get (location);

    if (type == MAUTILUS_LOCATION_CHANGE_RELOAD)
    {
        force_reload = TRUE;
    }
    else
    {
        force_reload = !mautilus_directory_is_local (directory);
    }

    /* We need to invalidate file attributes as well due to how mounting works
     * in the window slot and to avoid other caching issues.
     * Read handle_mount_if_needed for one example */
    if (force_reload)
    {
        mautilus_file_invalidate_all_attributes (file);
        mautilus_directory_force_reload (directory);
    }

    mautilus_directory_unref (directory);
    mautilus_file_unref (file);
}

static void
save_scroll_position_for_history (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    char *current_pos;
    /* Set current_bookmark scroll pos */
    if (priv->current_location_bookmark != NULL &&
        priv->content_view != NULL &&
        MAUTILUS_IS_FILES_VIEW (priv->content_view))
    {
        current_pos = mautilus_files_view_get_first_visible_file (MAUTILUS_FILES_VIEW (priv->content_view));
        mautilus_bookmark_set_scroll_pos (priv->current_location_bookmark, current_pos);
        g_free (current_pos);
    }
}

/*
 * begin_location_change
 *
 * Change a window slot's location.
 * @window: The mautilusWindow whose location should be changed.
 * @location: A url specifying the location to load
 * @previous_location: The url that was previously shown in the window that initialized the change, if any
 * @new_selection: The initial selection to present after loading the location
 * @type: Which type of location change is this? Standard, back, forward, or reload?
 * @distance: If type is back or forward, the index into the back or forward chain. If
 * type is standard or reload, this is ignored, and must be 0.
 * @scroll_pos: The file to scroll to when the location is loaded.
 *
 * This is the core function for changing the location of a window. Every change to the
 * location begins here.
 */
static void
begin_location_change (mautilusWindowSlot         *self,
                       GFile                      *location,
                       GFile                      *previous_location,
                       GList                      *new_selection,
                       mautilusLocationChangeType  type,
                       guint                       distance,
                       const char                 *scroll_pos)
{
    mautilusWindowSlotPrivate *priv;

    g_assert (self != NULL);
    g_assert (location != NULL);
    g_assert (type == MAUTILUS_LOCATION_CHANGE_BACK
              || type == MAUTILUS_LOCATION_CHANGE_FORWARD
              || distance == 0);

    mautilus_profile_start (NULL);

    priv = mautilus_window_slot_get_instance_private (self);
    /* Avoid to update status from the current view in our async calls */
    mautilus_window_slot_disconnect_content_view (self);
    /* We are going to change the location, so make sure we stop any loading
     * or searching of the previous view, so we avoid to be slow */
    mautilus_window_slot_stop_loading (self);

    mautilus_window_slot_set_allow_stop (self, TRUE);

    new_selection = check_select_old_location_containing_folder (new_selection, location, previous_location);

    g_assert (priv->pending_location == NULL);

    priv->pending_location = g_object_ref (location);
    priv->location_change_type = type;
    priv->location_change_distance = distance;
    priv->tried_mount = FALSE;
    priv->pending_selection = mautilus_file_list_copy (new_selection);

    priv->pending_scroll_to = g_strdup (scroll_pos);

    check_force_reload (location, type);

    save_scroll_position_for_history (self);

    /* Get the info needed to make decisions about how to open the new location */
    priv->determine_view_file = mautilus_file_get (location);
    g_assert (priv->determine_view_file != NULL);

    mautilus_file_call_when_ready (priv->determine_view_file,
                                   MAUTILUS_FILE_ATTRIBUTE_INFO |
                                   MAUTILUS_FILE_ATTRIBUTE_MOUNT,
                                   got_file_info_for_view_selection_callback,
                                   self);

    mautilus_profile_end (NULL);
}

static void
mautilus_window_slot_set_location (mautilusWindowSlot *self,
                                   GFile              *location)
{
    mautilusWindowSlotPrivate *priv;
    GFile *old_location;

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->location &&
        g_file_equal (location, priv->location))
    {
        /* The location name could be updated even if the location
         * wasn't changed. This is the case for a search.
         */
        mautilus_window_slot_update_title (self);
        return;
    }

    old_location = priv->location;
    priv->location = g_object_ref (location);

    if (mautilus_window_slot_get_active (self))
    {
        mautilus_window_sync_location_widgets (priv->window);
    }

    mautilus_window_slot_update_title (self);

    if (old_location)
    {
        g_object_unref (old_location);
    }

    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_LOCATION]);
}

static void
viewed_file_changed_callback (mautilusFile       *file,
                              mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;
    GFile *new_location;
    gboolean is_in_trash, was_in_trash;

    g_assert (MAUTILUS_IS_FILE (file));
    g_assert (MAUTILUS_IS_WINDOW_SLOT (self));

    priv = mautilus_window_slot_get_instance_private (self);
    g_assert (file == priv->viewed_file);

    if (!mautilus_file_is_not_yet_confirmed (file))
    {
        priv->viewed_file_seen = TRUE;
    }

    was_in_trash = priv->viewed_file_in_trash;

    priv->viewed_file_in_trash = is_in_trash = mautilus_file_is_in_trash (file);

    if (mautilus_file_is_gone (file) || (is_in_trash && !was_in_trash))
    {
        if (priv->viewed_file_seen)
        {
            GFile *go_to_file;
            GFile *parent;
            GFile *location;
            GMount *mount;

            parent = NULL;
            location = mautilus_file_get_location (file);

            if (g_file_is_native (location))
            {
                mount = mautilus_get_mounted_mount_for_root (location);

                if (mount == NULL)
                {
                    parent = g_file_get_parent (location);
                }

                g_clear_object (&mount);
            }

            if (parent != NULL)
            {
                /* auto-show existing parent */
                go_to_file = mautilus_find_existing_uri_in_hierarchy (parent);
            }
            else
            {
                go_to_file = g_file_new_for_path (g_get_home_dir ());
            }

            mautilus_window_slot_open_location_full (self, go_to_file, 0, NULL);

            g_clear_object (&parent);
            g_object_unref (go_to_file);
            g_object_unref (location);
        }
    }
    else
    {
        new_location = mautilus_file_get_location (file);
        mautilus_window_slot_set_location (self, new_location);
        g_object_unref (new_location);
    }
}

static void
mautilus_window_slot_go_home (mautilusWindowSlot      *self,
                              mautilusWindowOpenFlags  flags)
{
    GFile *home;

    g_return_if_fail (MAUTILUS_IS_WINDOW_SLOT (self));

    home = g_file_new_for_path (g_get_home_dir ());
    mautilus_window_slot_open_location_full (self, home, flags, NULL);
    g_object_unref (home);
}

static void
mautilus_window_slot_set_viewed_file (mautilusWindowSlot *self,
                                      mautilusFile       *file)
{
    mautilusWindowSlotPrivate *priv;
    mautilusFileAttributes attributes;

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->viewed_file == file)
    {
        return;
    }

    mautilus_file_ref (file);

    if (priv->viewed_file != NULL)
    {
        g_signal_handlers_disconnect_by_func (priv->viewed_file,
                                              G_CALLBACK (viewed_file_changed_callback),
                                              self);
        mautilus_file_monitor_remove (priv->viewed_file,
                                      self);
    }

    if (file != NULL)
    {
        attributes =
            MAUTILUS_FILE_ATTRIBUTE_INFO |
            MAUTILUS_FILE_ATTRIBUTE_LINK_INFO;
        mautilus_file_monitor_add (file, self, attributes);

        g_signal_connect_object (file, "changed",
                                 G_CALLBACK (viewed_file_changed_callback), self, 0);
    }

    mautilus_file_unref (priv->viewed_file);
    priv->viewed_file = file;
}

typedef struct
{
    GCancellable *cancellable;
    mautilusWindowSlot *slot;
} MountNotMountedData;

static void
mount_not_mounted_callback (GObject      *source_object,
                            GAsyncResult *res,
                            gpointer      user_data)
{
    mautilusWindowSlotPrivate *priv;
    MountNotMountedData *data;
    mautilusWindowSlot *self;
    GError *error;
    GCancellable *cancellable;

    data = user_data;
    self = data->slot;
    priv = mautilus_window_slot_get_instance_private (self);
    cancellable = data->cancellable;
    g_free (data);

    if (g_cancellable_is_cancelled (cancellable))
    {
        /* Cancelled, don't call back */
        g_object_unref (cancellable);
        return;
    }

    priv->mount_cancellable = NULL;

    priv->determine_view_file = mautilus_file_get (priv->pending_location);

    error = NULL;
    if (!g_file_mount_enclosing_volume_finish (G_FILE (source_object), res, &error))
    {
        priv->mount_error = error;
        got_file_info_for_view_selection_callback (priv->determine_view_file, self);
        priv->mount_error = NULL;
        g_error_free (error);
    }
    else
    {
        mautilus_file_invalidate_all_attributes (priv->determine_view_file);
        mautilus_file_call_when_ready (priv->determine_view_file,
                                       MAUTILUS_FILE_ATTRIBUTE_INFO |
                                       MAUTILUS_FILE_ATTRIBUTE_MOUNT,
                                       got_file_info_for_view_selection_callback,
                                       self);
    }

    g_object_unref (cancellable);
}

static void
mautilus_window_slot_display_view_selection_failure (mautilusWindow *window,
                                                     mautilusFile   *file,
                                                     GFile          *location,
                                                     GError         *error)
{
    char *error_message;
    char *detail_message;
    char *scheme_string;

    /* Some sort of failure occurred. How 'bout we tell the user? */

    error_message = g_strdup (_("Oops! Something went wrong."));
    detail_message = NULL;
    if (error == NULL)
    {
        if (mautilus_file_is_directory (file))
        {
            detail_message = g_strdup (_("Unable to display the contents of this folder."));
        }
        else
        {
            detail_message = g_strdup (_("This location doesn’t appear to be a folder."));
        }
    }
    else if (error->domain == G_IO_ERROR)
    {
        switch (error->code)
        {
            case G_IO_ERROR_NOT_FOUND:
            {
                detail_message = g_strdup (_("Unable to find the requested file. Please check the spelling and try again."));
            }
            break;

            case G_IO_ERROR_NOT_SUPPORTED:
            {
                scheme_string = g_file_get_uri_scheme (location);
                if (scheme_string != NULL)
                {
                    detail_message = g_strdup_printf (_("“%s” locations are not supported."),
                                                      scheme_string);
                }
                else
                {
                    detail_message = g_strdup (_("Unable to handle this kind of location."));
                }
                g_free (scheme_string);
            }
            break;

            case G_IO_ERROR_NOT_MOUNTED:
            {
                detail_message = g_strdup (_("Unable to access the requested location."));
            }
            break;

            case G_IO_ERROR_PERMISSION_DENIED:
            {
                detail_message = g_strdup (_("Don’t have permission to access the requested location."));
            }
            break;

            case G_IO_ERROR_HOST_NOT_FOUND:
            {
                /* This case can be hit for user-typed strings like "foo" due to
                 * the code that guesses web addresses when there's no initial "/".
                 * But this case is also hit for legitimate web addresses when
                 * the proxy is set up wrong.
                 */
                detail_message = g_strdup (_("Unable to find the requested location. Please check the spelling or the network settings."));
            }
            break;

            case G_IO_ERROR_CANCELLED:
            case G_IO_ERROR_FAILED_HANDLED:
            {
                goto done;
            }

            default:
            {
            }
            break;
        }
    }

    if (detail_message == NULL)
    {
        detail_message = g_strdup_printf (_("Unhandled error message: %s"), error->message);
    }

    show_error_dialog (error_message, detail_message, GTK_WINDOW (window));

done:
    g_free (error_message);
    g_free (detail_message);
}

/* FIXME: This works in the folowwing way. begin_location_change tries to get the
 * information of the file directly.
 * If the mautilus file finds that there is an error trying to get its
 * information and the error match that the file is not mounted, it sets an
 * internal attribute with the error then we try to mount it here.
 *
 * However, files are cached, and if the file doesn't get finalized in a location
 * change, because needs to be in the navigation history or is a bookmark, and the
 * file is not the root of the mount point, which is tracked by a volume monitor,
 * and it gets unmounted aftwerwards, the file doesn't realize it's unmounted, and
 * therefore this trick to open an unmounted file will fail the next time the user
 * tries to open.
 * For that, we need to always invalidate the file attributes when a location is
 * changed, which is done in check_force_reload.
 * A better way would be to make sure any children of the mounted root gets
 * akwnoledge by it either by adding a reference to its parent volume monitor
 * or with another solution. */
static gboolean
handle_mount_if_needed (mautilusWindowSlot *self,
                        mautilusFile       *file)
{
    mautilusWindowSlotPrivate *priv;
    mautilusWindow *window;
    GMountOperation *mount_op;
    MountNotMountedData *data;
    GFile *location;
    GError *error = NULL;
    gboolean needs_mount_handling = FALSE;

    priv = mautilus_window_slot_get_instance_private (self);
    window = mautilus_window_slot_get_window (self);
    if (priv->mount_error)
    {
        error = g_error_copy (priv->mount_error);
    }
    else if (mautilus_file_get_file_info_error (file) != NULL)
    {
        error = g_error_copy (mautilus_file_get_file_info_error (file));
    }

    if (error && error->domain == G_IO_ERROR && error->code == G_IO_ERROR_NOT_MOUNTED &&
        !priv->tried_mount)
    {
        priv->tried_mount = TRUE;

        mount_op = gtk_mount_operation_new (GTK_WINDOW (window));
        g_mount_operation_set_password_save (mount_op, G_PASSWORD_SAVE_FOR_SESSION);
        location = mautilus_file_get_location (file);
        data = g_new0 (MountNotMountedData, 1);
        data->cancellable = g_cancellable_new ();
        data->slot = self;
        priv->mount_cancellable = data->cancellable;
        g_file_mount_enclosing_volume (location, 0, mount_op, priv->mount_cancellable,
                                       mount_not_mounted_callback, data);
        g_object_unref (location);
        g_object_unref (mount_op);

        needs_mount_handling = TRUE;
    }

    g_clear_error (&error);

    return needs_mount_handling;
}

static gboolean
handle_regular_file_if_needed (mautilusWindowSlot *self,
                               mautilusFile       *file)
{
    mautilusFile *parent_file;
    gboolean needs_regular_file_handling = FALSE;
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    parent_file = mautilus_file_get_parent (file);
    if ((parent_file != NULL) &&
        mautilus_file_get_file_type (file) == G_FILE_TYPE_REGULAR)
    {
        if (priv->pending_selection != NULL)
        {
            mautilus_file_list_free (priv->pending_selection);
        }

        g_clear_object (&priv->pending_location);
        g_clear_object (&priv->pending_file_to_activate);
        g_free (priv->pending_scroll_to);

        priv->pending_location = mautilus_file_get_parent_location (file);
        if (mautilus_file_is_archive (file))
        {
            priv->pending_file_to_activate = mautilus_file_ref (file);
        }
        else
        {
            priv->pending_selection = g_list_prepend (NULL, mautilus_file_ref (file));
        }
        priv->determine_view_file = mautilus_file_ref (parent_file);
        priv->pending_scroll_to = mautilus_file_get_uri (file);

        mautilus_file_invalidate_all_attributes (priv->determine_view_file);
        mautilus_file_call_when_ready (priv->determine_view_file,
                                       MAUTILUS_FILE_ATTRIBUTE_INFO |
                                       MAUTILUS_FILE_ATTRIBUTE_MOUNT,
                                       got_file_info_for_view_selection_callback,
                                       self);

        needs_regular_file_handling = TRUE;
    }

    mautilus_file_unref (parent_file);

    return needs_regular_file_handling;
}

static void
got_file_info_for_view_selection_callback (mautilusFile *file,
                                           gpointer      callback_data)
{
    mautilusWindowSlotPrivate *priv;
    GError *error = NULL;
    mautilusWindow *window;
    mautilusWindowSlot *self;
    mautilusFile *viewed_file;
    mautilusView *view;
    GFile *location;

    mautilusApplication *app;

    self = callback_data;
    priv = mautilus_window_slot_get_instance_private (self);
    window = mautilus_window_slot_get_window (self);

    g_assert (priv->determine_view_file == file);
    priv->determine_view_file = NULL;

    mautilus_profile_start (NULL);

    if (handle_mount_if_needed (self, file))
    {
        goto done;
    }

    if (handle_regular_file_if_needed (self, file))
    {
        goto done;
    }

    if (priv->mount_error)
    {
        error = g_error_copy (priv->mount_error);
    }
    else if (mautilus_file_get_file_info_error (file) != NULL)
    {
        error = g_error_copy (mautilus_file_get_file_info_error (file));
    }

    location = priv->pending_location;

    /* desktop and other-locations GFile operations report G_IO_ERROR_NOT_SUPPORTED,
     * but it's not an actual error for mautilus */
    if (!error || g_error_matches (error, G_IO_ERROR, G_IO_ERROR_NOT_SUPPORTED))
    {
        view = mautilus_window_slot_get_view_for_location (self, location);
        setup_view (self, view);
    }
    else
    {
        if (error == NULL)
        {
            error = g_error_new (G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
                                 _("Unable to load location"));
        }
        mautilus_window_slot_display_view_selection_failure (window,
                                                             file,
                                                             location,
                                                             error);

        if (!gtk_widget_get_visible (GTK_WIDGET (window)))
        {
            /* Destroy never-had-a-chance-to-be-seen window. This case
             * happens when a new window cannot display its initial URI.
             */
            /* if this is the only window, we don't want to quit, so we redirect it to home */

            app = MAUTILUS_APPLICATION (g_application_get_default ());

            if (g_list_length (mautilus_application_get_windows (app)) == 1)
            {
                /* the user could have typed in a home directory that doesn't exist,
                 *  in which case going home would cause an infinite loop, so we
                 *  better test for that */

                if (!mautilus_is_root_directory (location))
                {
                    if (!mautilus_is_home_directory (location))
                    {
                        mautilus_window_slot_go_home (self, FALSE);
                    }
                    else
                    {
                        GFile *root;

                        root = g_file_new_for_path ("/");
                        /* the last fallback is to go to a known place that can't be deleted! */
                        mautilus_window_slot_open_location_full (self, location, 0, NULL);
                        g_object_unref (root);
                    }
                }
                else
                {
                    gtk_widget_destroy (GTK_WIDGET (window));
                }
            }
            else
            {
                /* Since this is a window, destroying it will also unref it. */
                gtk_widget_destroy (GTK_WIDGET (window));
            }
        }
        else
        {
            GFile *slot_location;

            /* Clean up state of already-showing window */
            end_location_change (self);
            slot_location = mautilus_window_slot_get_location (self);

            /* We're missing a previous location (if opened location
             * in a new tab) so close it and return */
            if (slot_location == NULL)
            {
                mautilus_window_slot_close (window, self);
            }
            else
            {
                /* We disconnected this, so we need to re-connect it */
                viewed_file = mautilus_file_get (slot_location);
                mautilus_window_slot_set_viewed_file (self, viewed_file);
                mautilus_file_unref (viewed_file);

                /* Leave the location bar showing the bad location that the user
                 * typed (or maybe achieved by dragging or something). Many times
                 * the mistake will just be an easily-correctable typo. The user
                 * can choose "Refresh" to get the original URI back in the location bar.
                 */
            }
        }
    }

done:
    g_clear_error (&error);

    mautilus_file_unref (file);
    mautilus_profile_end (NULL);
}

/* Load a view into the window, either reusing the old one or creating
 * a new one. This happens when you want to load a new location, or just
 * switch to a different view.
 * If pending_location is set we're loading a new location and
 * pending_location/selection will be used. If not, we're just switching
 * view, and the current location will be used.
 */
static gboolean
setup_view (mautilusWindowSlot *self,
            mautilusView       *view)
{
    gboolean ret = TRUE;
    GFile *old_location;
    mautilusWindowSlotPrivate *priv;

    mautilus_profile_start (NULL);

    priv = mautilus_window_slot_get_instance_private (self);
    mautilus_window_slot_disconnect_content_view (self);

    priv->new_content_view = view;

    mautilus_window_slot_connect_new_content_view (self);

    /* Forward search selection and state before loading the new model */
    old_location = priv->content_view ? mautilus_view_get_location (priv->content_view) : NULL;

    /* Actually load the pending location and selection: */
    if (priv->pending_location != NULL)
    {
        load_new_location (self,
                           priv->pending_location,
                           priv->pending_selection,
                           priv->pending_file_to_activate,
                           FALSE,
                           TRUE);

        mautilus_file_list_free (priv->pending_selection);
        priv->pending_selection = NULL;
    }
    else if (old_location != NULL)
    {
        GList *selection;

        selection = mautilus_view_get_selection (priv->content_view);

        load_new_location (self,
                           old_location,
                           selection,
                           NULL,
                           FALSE,
                           TRUE);
        mautilus_file_list_free (selection);
    }
    else
    {
        ret = FALSE;
        goto out;
    }

    change_view (self);
    gtk_widget_show (GTK_WIDGET (priv->window));

out:
    mautilus_profile_end (NULL);

    return ret;
}

static void
load_new_location (mautilusWindowSlot *self,
                   GFile              *location,
                   GList              *selection,
                   mautilusFile       *file_to_activate,
                   gboolean            tell_current_content_view,
                   gboolean            tell_new_content_view)
{
    mautilusView *view;
    mautilusWindowSlotPrivate *priv;

    g_assert (self != NULL);
    g_assert (location != NULL);

    view = NULL;
    priv = mautilus_window_slot_get_instance_private (self);

    mautilus_profile_start (NULL);
    /* Note, these may recurse into report_load_underway */
    if (priv->content_view != NULL && tell_current_content_view)
    {
        view = priv->content_view;
        mautilus_view_set_location (priv->content_view, location);
    }

    if (priv->new_content_view != NULL && tell_new_content_view &&
        (!tell_current_content_view ||
         priv->new_content_view != priv->content_view))
    {
        view = priv->new_content_view;
        mautilus_view_set_location (priv->new_content_view, location);
    }
    if (view)
    {
        mautilus_view_set_selection (view, selection);
        if (file_to_activate != NULL)
        {
            g_autoptr (GAppInfo) app_info = NULL;
            const gchar *app_id;

            g_return_if_fail (MAUTILUS_IS_FILES_VIEW (view));
            app_info = mautilus_mime_get_default_application_for_file (file_to_activate);
            app_id = g_app_info_get_id (app_info);
            if (g_strcmp0 (app_id, MAUTILUS_DESKTOP_ID) == 0)
            {
                mautilus_files_view_activate_file (MAUTILUS_FILES_VIEW (view),
                                                   file_to_activate, 0);
            }
        }
    }

    mautilus_profile_end (NULL);
}

static void
end_location_change (mautilusWindowSlot *self)
{
    char *uri;
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    uri = mautilus_window_slot_get_location_uri (self);
    if (uri)
    {
        DEBUG ("Finished loading window for uri %s", uri);
        g_free (uri);
    }

    mautilus_window_slot_set_allow_stop (self, FALSE);

    /* Now we can free details->pending_scroll_to, since the load_complete
     * callback already has been emitted.
     */
    g_free (priv->pending_scroll_to);
    priv->pending_scroll_to = NULL;

    free_location_change (self);
}

static void
free_location_change (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    g_clear_object (&priv->pending_location);
    g_clear_object (&priv->pending_file_to_activate);
    mautilus_file_list_free (priv->pending_selection);
    priv->pending_selection = NULL;

    /* Don't free details->pending_scroll_to, since thats needed until
     * the load_complete callback.
     */

    if (priv->mount_cancellable != NULL)
    {
        g_cancellable_cancel (priv->mount_cancellable);
        priv->mount_cancellable = NULL;
    }

    if (priv->determine_view_file != NULL)
    {
        mautilus_file_cancel_call_when_ready
            (priv->determine_view_file,
            got_file_info_for_view_selection_callback, self);
        priv->determine_view_file = NULL;
    }
}

static void
mautilus_window_slot_set_content_view (mautilusWindowSlot *self,
                                       guint               id)
{
    mautilusFilesView *view;
    GList *selection;
    char *uri;
    mautilusWindowSlotPrivate *priv;

    g_assert (self != NULL);

    priv = mautilus_window_slot_get_instance_private (self);
    uri = mautilus_window_slot_get_location_uri (self);
    DEBUG ("Change view of window %s to %d", uri, id);
    g_free (uri);

    if (mautilus_window_slot_content_view_matches (self, id))
    {
        return;
    }

    selection = mautilus_view_get_selection (priv->content_view);
    view = mautilus_files_view_new (id, self);

    mautilus_window_slot_stop_loading (self);

    mautilus_window_slot_set_allow_stop (self, TRUE);

    if (g_list_length (selection) == 0 && MAUTILUS_IS_FILES_VIEW (priv->content_view))
    {
        /* If there is no selection, queue a scroll to the same icon that
         * is currently visible */
        priv->pending_scroll_to = mautilus_files_view_get_first_visible_file (MAUTILUS_FILES_VIEW (priv->content_view));
    }

    priv->location_change_type = MAUTILUS_LOCATION_CHANGE_RELOAD;

    if (!setup_view (self, MAUTILUS_VIEW (view)))
    {
        /* Just load the homedir. */
        mautilus_window_slot_go_home (self, FALSE);
    }
}

void
mautilus_window_back_or_forward (mautilusWindow          *window,
                                 gboolean                 back,
                                 guint                    distance,
                                 mautilusWindowOpenFlags  flags)
{
    mautilusWindowSlot *self;
    GList *list;
    GFile *location;
    guint len;
    mautilusBookmark *bookmark;
    GFile *old_location;
    mautilusWindowSlotPrivate *priv;

    self = mautilus_window_get_active_slot (window);
    priv = mautilus_window_slot_get_instance_private (self);
    list = back ? priv->back_list : priv->forward_list;

    len = (guint) g_list_length (list);

    /* If we can't move in the direction at all, just return. */
    if (len == 0)
    {
        return;
    }

    /* If the distance to move is off the end of the list, go to the end
     *  of the list. */
    if (distance >= len)
    {
        distance = len - 1;
    }

    bookmark = g_list_nth_data (list, distance);
    location = mautilus_bookmark_get_location (bookmark);

    if (flags != 0)
    {
        mautilus_window_slot_open_location_full (self, location, flags, NULL);
    }
    else
    {
        char *scroll_pos;

        old_location = mautilus_window_slot_get_location (self);
        scroll_pos = mautilus_bookmark_get_scroll_pos (bookmark);
        begin_location_change
            (self,
            location, old_location, NULL,
            back ? MAUTILUS_LOCATION_CHANGE_BACK : MAUTILUS_LOCATION_CHANGE_FORWARD,
            distance,
            scroll_pos);

        g_free (scroll_pos);
    }

    g_object_unref (location);
}

/* reload the contents of the window */
static void
mautilus_window_slot_force_reload (mautilusWindowSlot *self)
{
    GFile *location;
    char *current_pos;
    GList *selection;
    mautilusWindowSlotPrivate *priv;

    g_assert (MAUTILUS_IS_WINDOW_SLOT (self));

    priv = mautilus_window_slot_get_instance_private (self);
    location = mautilus_window_slot_get_location (self);
    if (location == NULL)
    {
        return;
    }

    /* peek_slot_field (window, location) can be free'd during the processing
     * of begin_location_change, so make a copy
     */
    g_object_ref (location);
    current_pos = NULL;
    selection = NULL;
    if (priv->new_content_view)
    {
        selection = mautilus_view_get_selection (priv->content_view);

        if (MAUTILUS_IS_FILES_VIEW (priv->new_content_view))
        {
            current_pos = mautilus_files_view_get_first_visible_file (MAUTILUS_FILES_VIEW (priv->content_view));
        }
    }
    begin_location_change
        (self, location, location, selection,
        MAUTILUS_LOCATION_CHANGE_RELOAD, 0, current_pos);
    g_free (current_pos);
    g_object_unref (location);
    mautilus_file_list_free (selection);
}

void
mautilus_window_slot_queue_reload (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    g_assert (MAUTILUS_IS_WINDOW_SLOT (self));

    priv = mautilus_window_slot_get_instance_private (self);
    if (mautilus_window_slot_get_location (self) == NULL)
    {
        return;
    }

    if (priv->pending_location != NULL
        || priv->content_view == NULL
        || mautilus_view_is_loading (priv->content_view))
    {
        /* there is a reload in flight */
        priv->needs_reload = TRUE;
        return;
    }

    mautilus_window_slot_force_reload (self);
}

static void
mautilus_window_slot_clear_forward_list (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    g_assert (MAUTILUS_IS_WINDOW_SLOT (self));

    priv = mautilus_window_slot_get_instance_private (self);
    g_list_free_full (priv->forward_list, g_object_unref);
    priv->forward_list = NULL;
}

static void
mautilus_window_slot_clear_back_list (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    g_assert (MAUTILUS_IS_WINDOW_SLOT (self));

    priv = mautilus_window_slot_get_instance_private (self);
    g_list_free_full (priv->back_list, g_object_unref);
    priv->back_list = NULL;
}

static void
mautilus_window_slot_update_bookmark (mautilusWindowSlot *self,
                                      mautilusFile       *file)
{
    gboolean recreate;
    GFile *new_location;
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    new_location = mautilus_file_get_location (file);

    if (priv->current_location_bookmark == NULL)
    {
        recreate = TRUE;
    }
    else
    {
        GFile *bookmark_location;
        bookmark_location = mautilus_bookmark_get_location (priv->current_location_bookmark);
        recreate = !g_file_equal (bookmark_location, new_location);
        g_object_unref (bookmark_location);
    }

    if (recreate)
    {
        char *display_name = NULL;

        /* We've changed locations, must recreate bookmark for current location. */
        g_clear_object (&priv->last_location_bookmark);
        priv->last_location_bookmark = priv->current_location_bookmark;

        display_name = mautilus_file_get_display_name (file);
        priv->current_location_bookmark = mautilus_bookmark_new (new_location, display_name);
        g_free (display_name);
    }

    g_object_unref (new_location);
}

static void
check_bookmark_location_matches (mautilusBookmark *bookmark,
                                 GFile            *location)
{
    GFile *bookmark_location;
    char *bookmark_uri, *uri;

    bookmark_location = mautilus_bookmark_get_location (bookmark);
    if (!g_file_equal (location, bookmark_location))
    {
        bookmark_uri = g_file_get_uri (bookmark_location);
        uri = g_file_get_uri (location);
        g_warning ("bookmark uri is %s, but expected %s", bookmark_uri, uri);
        g_free (uri);
        g_free (bookmark_uri);
    }
    g_object_unref (bookmark_location);
}

/* Debugging function used to verify that the last_location_bookmark
 * is in the state we expect when we're about to use it to update the
 * Back or Forward list.
 */
static void
check_last_bookmark_location_matches_slot (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    check_bookmark_location_matches (priv->last_location_bookmark,
                                     mautilus_window_slot_get_location (self));
}

static void
handle_go_direction (mautilusWindowSlot *self,
                     GFile              *location,
                     gboolean            forward)
{
    GList **list_ptr, **other_list_ptr;
    GList *list, *other_list, *link;
    mautilusBookmark *bookmark;
    gint i;
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    list_ptr = (forward) ? (&priv->forward_list) : (&priv->back_list);
    other_list_ptr = (forward) ? (&priv->back_list) : (&priv->forward_list);
    list = *list_ptr;
    other_list = *other_list_ptr;

    /* Move items from the list to the other list. */
    g_assert (g_list_length (list) > priv->location_change_distance);
    check_bookmark_location_matches (g_list_nth_data (list, priv->location_change_distance),
                                     location);
    g_assert (mautilus_window_slot_get_location (self) != NULL);

    /* Move current location to list */
    check_last_bookmark_location_matches_slot (self);

    /* Use the first bookmark in the history list rather than creating a new one. */
    other_list = g_list_prepend (other_list, priv->last_location_bookmark);
    g_object_ref (other_list->data);

    /* Move extra links from the list to the other list */
    for (i = 0; i < priv->location_change_distance; ++i)
    {
        bookmark = MAUTILUS_BOOKMARK (list->data);
        list = g_list_remove (list, bookmark);
        other_list = g_list_prepend (other_list, bookmark);
    }

    /* One bookmark falls out of back/forward lists and becomes viewed location */
    link = list;
    list = g_list_remove_link (list, link);
    g_object_unref (link->data);
    g_list_free_1 (link);

    *list_ptr = list;
    *other_list_ptr = other_list;
}

static void
handle_go_elsewhere (mautilusWindowSlot *self,
                     GFile              *location)
{
    GFile *slot_location;
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);

    /* Clobber the entire forward list, and move displayed location to back list */
    mautilus_window_slot_clear_forward_list (self);

    slot_location = mautilus_window_slot_get_location (self);

    if (slot_location != NULL)
    {
        /* If we're returning to the same uri somehow, don't put this uri on back list.
         * This also avoids a problem where set_displayed_location
         * didn't update last_location_bookmark since the uri didn't change.
         */
        if (!g_file_equal (slot_location, location))
        {
            /* Store bookmark for current location in back list, unless there is no current location */
            check_last_bookmark_location_matches_slot (self);
            /* Use the first bookmark in the history list rather than creating a new one. */
            priv->back_list = g_list_prepend (priv->back_list,
                                              priv->last_location_bookmark);
            g_object_ref (priv->back_list->data);
        }
    }
}

static void
update_history (mautilusWindowSlot         *self,
                mautilusLocationChangeType  type,
                GFile                      *new_location)
{
    switch (type)
    {
        case MAUTILUS_LOCATION_CHANGE_STANDARD:
        {
            handle_go_elsewhere (self, new_location);
            return;
        }

        case MAUTILUS_LOCATION_CHANGE_RELOAD:
        {
            /* for reload there is no work to do */
            return;
        }

        case MAUTILUS_LOCATION_CHANGE_BACK:
        {
            handle_go_direction (self, new_location, FALSE);
            return;
        }

        case MAUTILUS_LOCATION_CHANGE_FORWARD:
            handle_go_direction (self, new_location, TRUE);
            return;
    }
    g_return_if_fail (FALSE);
}

typedef struct
{
    mautilusWindowSlot *slot;
    GCancellable *cancellable;
    GMount *mount;
} FindMountData;

static void
mautilus_window_slot_show_x_content_bar (mautilusWindowSlot *self,
                                         GMount             *mount,
                                         const char * const *x_content_types)
{
    GtkWidget *bar;

    g_assert (MAUTILUS_IS_WINDOW_SLOT (self));

    if (!should_handle_content_types (x_content_types))
    {
        return;
    }

    bar = mautilus_x_content_bar_new (mount, x_content_types);
    gtk_widget_show (bar);
    mautilus_window_slot_add_extra_location_widget (self, bar);
}

static void
found_content_type_cb (const char **x_content_types,
                       gpointer     user_data)
{
    mautilusWindowSlot *self;
    FindMountData *data = user_data;
    mautilusWindowSlotPrivate *priv;

    self = data->slot;
    priv = mautilus_window_slot_get_instance_private (self);

    if (g_cancellable_is_cancelled (data->cancellable))
    {
        goto out;
    }


    if (x_content_types != NULL && x_content_types[0] != NULL)
    {
        mautilus_window_slot_show_x_content_bar (self, data->mount, (const char * const *) x_content_types);
    }

    priv->find_mount_cancellable = NULL;

out:
    g_object_unref (data->mount);
    g_object_unref (data->cancellable);
    g_free (data);
}

static void
found_mount_cb (GObject      *source_object,
                GAsyncResult *res,
                gpointer      user_data)
{
    FindMountData *data = user_data;
    mautilusWindowSlot *self;
    GMount *mount;
    mautilusWindowSlotPrivate *priv;

    self = MAUTILUS_WINDOW_SLOT (data->slot);
    priv = mautilus_window_slot_get_instance_private (self);
    if (g_cancellable_is_cancelled (data->cancellable))
    {
        goto out;
    }

    mount = g_file_find_enclosing_mount_finish (G_FILE (source_object),
                                                res,
                                                NULL);
    if (mount != NULL)
    {
        data->mount = mount;
        mautilus_get_x_content_types_for_mount_async (mount,
                                                      found_content_type_cb,
                                                      data->cancellable,
                                                      data);
        return;
    }

    priv->find_mount_cancellable = NULL;

out:
    g_object_unref (data->cancellable);
    g_free (data);
}

static void
trash_state_changed_cb (mautilusTrashMonitor *monitor,
                        gboolean              is_empty,
                        gpointer              user_data)
{
    GFile *location;
    mautilusDirectory *directory;

    location = mautilus_window_slot_get_current_location (user_data);

    if (location == NULL)
    {
        return;
    }

    directory = mautilus_directory_get (location);

    if (mautilus_directory_is_in_trash (directory) &&
        mautilus_trash_monitor_is_empty ())
    {
        mautilus_window_slot_remove_extra_location_widgets (user_data);
    }
}

static void
mautilus_window_slot_show_trash_bar (mautilusWindowSlot *self)
{
    GtkWidget *bar;
    mautilusView *view;

    view = mautilus_window_slot_get_current_view (self);
    bar = mautilus_trash_bar_new (MAUTILUS_FILES_VIEW (view));
    gtk_widget_show (bar);

    mautilus_window_slot_add_extra_location_widget (self, bar);
}

static void
mautilus_window_slot_show_special_location_bar (mautilusWindowSlot      *self,
                                                mautilusSpecialLocation  special_location)
{
    GtkWidget *bar;

    bar = mautilus_special_location_bar_new (special_location);
    gtk_widget_show (bar);

    mautilus_window_slot_add_extra_location_widget (self, bar);
}

static void
slot_add_extension_extra_widgets (mautilusWindowSlot *self)
{
    GList *providers, *l;
    GtkWidget *widget;
    char *uri;
    mautilusWindow *window;

    providers = mautilus_module_get_extensions_for_type (MAUTILUS_TYPE_LOCATION_WIDGET_PROVIDER);
    window = mautilus_window_slot_get_window (self);

    uri = mautilus_window_slot_get_location_uri (self);
    for (l = providers; l != NULL; l = l->next)
    {
        mautilusLocationWidgetProvider *provider;

        provider = MAUTILUS_LOCATION_WIDGET_PROVIDER (l->data);
        widget = mautilus_location_widget_provider_get_widget (provider, uri, GTK_WIDGET (window));
        if (widget != NULL)
        {
            mautilus_window_slot_add_extra_location_widget (self, widget);
        }
    }
    g_free (uri);

    mautilus_module_extension_list_free (providers);
}

static void
mautilus_window_slot_update_for_new_location (mautilusWindowSlot *self)
{
    GFile *new_location;
    mautilusFile *file;
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    new_location = priv->pending_location;
    priv->pending_location = NULL;

    file = mautilus_file_get (new_location);
    mautilus_window_slot_update_bookmark (self, file);

    update_history (self, priv->location_change_type, new_location);

    /* Create a mautilusFile for this location, so we can catch it
     * if it goes away.
     */
    mautilus_window_slot_set_viewed_file (self, file);
    priv->viewed_file_seen = !mautilus_file_is_not_yet_confirmed (file);
    priv->viewed_file_in_trash = mautilus_file_is_in_trash (file);
    mautilus_file_unref (file);

    mautilus_window_slot_set_location (self, new_location);

    /* Sync the actions for this new location. */
    mautilus_window_slot_sync_actions (self);
}

static void
view_started_loading (mautilusWindowSlot *self,
                      mautilusView       *view)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    if (view == priv->content_view)
    {
        mautilus_window_slot_set_allow_stop (self, TRUE);
    }

    /* Only grab focus if the menu isn't showing. Otherwise the menu disappears
     * e.g. when the user toggles Show Hidden Files
     */
    if (!mautilus_toolbar_is_menu_visible (MAUTILUS_TOOLBAR (mautilus_window_get_toolbar (priv->window))))
    {
        gtk_widget_grab_focus (GTK_WIDGET (priv->window));
    }

    gtk_widget_show (GTK_WIDGET (priv->window));

    mautilus_window_slot_set_loading (self, TRUE);
}

static void
view_ended_loading (mautilusWindowSlot *self,
                    mautilusView       *view)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    if (view == priv->content_view)
    {
        if (MAUTILUS_IS_FILES_VIEW (view) && priv->pending_scroll_to != NULL)
        {
            mautilus_files_view_scroll_to_file (MAUTILUS_FILES_VIEW (priv->content_view), priv->pending_scroll_to);
        }

        end_location_change (self);
    }

    if (priv->needs_reload)
    {
        mautilus_window_slot_queue_reload (self);
        priv->needs_reload = FALSE;
    }

    mautilus_window_slot_set_allow_stop (self, FALSE);

    mautilus_window_slot_set_loading (self, FALSE);
}

static void
view_is_loading_changed_cb (GObject            *object,
                            GParamSpec         *pspec,
                            mautilusWindowSlot *self)
{
    mautilusView *view;

    view = MAUTILUS_VIEW (object);

    mautilus_profile_start (NULL);

    if (mautilus_view_is_loading (view))
    {
        view_started_loading (self, view);
    }
    else
    {
        view_ended_loading (self, view);
    }

    mautilus_profile_end (NULL);
}

static void
mautilus_window_slot_setup_extra_location_widgets (mautilusWindowSlot *self)
{
    GFile *location;
    FindMountData *data;
    mautilusDirectory *directory;
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    location = mautilus_window_slot_get_current_location (self);

    if (location == NULL)
    {
        return;
    }

    directory = mautilus_directory_get (location);

    if (mautilus_directory_is_in_trash (directory))
    {
        if (!mautilus_trash_monitor_is_empty ())
        {
            mautilus_window_slot_show_trash_bar (self);
        }
    }
    else
    {
        mautilusFile *file;
        GFile *scripts_file;
        char *scripts_path = mautilus_get_scripts_directory_path ();

        scripts_file = g_file_new_for_path (scripts_path);
        g_free (scripts_path);

        file = mautilus_file_get (location);

        if (mautilus_should_use_templates_directory () &&
            mautilus_file_is_user_special_directory (file, G_USER_DIRECTORY_TEMPLATES))
        {
            mautilus_window_slot_show_special_location_bar (self, MAUTILUS_SPECIAL_LOCATION_TEMPLATES);
        }
        else if (g_file_equal (location, scripts_file))
        {
            mautilus_window_slot_show_special_location_bar (self, MAUTILUS_SPECIAL_LOCATION_SCRIPTS);
        }

        g_object_unref (scripts_file);
        mautilus_file_unref (file);
    }

    /* need the mount to determine if we should put up the x-content cluebar */
    if (priv->find_mount_cancellable != NULL)
    {
        g_cancellable_cancel (priv->find_mount_cancellable);
        priv->find_mount_cancellable = NULL;
    }

    data = g_new (FindMountData, 1);
    data->slot = self;
    data->cancellable = g_cancellable_new ();
    data->mount = NULL;

    priv->find_mount_cancellable = data->cancellable;
    g_file_find_enclosing_mount_async (location,
                                       G_PRIORITY_DEFAULT,
                                       data->cancellable,
                                       found_mount_cb,
                                       data);

    mautilus_directory_unref (directory);

    slot_add_extension_extra_widgets (self);
}

static void
mautilus_window_slot_connect_new_content_view (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->new_content_view)
    {
        g_signal_connect (priv->new_content_view,
                          "notify::is-loading",
                          G_CALLBACK (view_is_loading_changed_cb),
                          self);
    }
}

static void
mautilus_window_slot_disconnect_content_view (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->content_view)
    {
        /* disconnect old view */
        g_signal_handlers_disconnect_by_func (priv->content_view,
                                              G_CALLBACK (view_is_loading_changed_cb),
                                              self);
    }
}

static void
mautilus_window_slot_switch_new_content_view (mautilusWindowSlot *self)
{
    GtkWidget *widget;
    gboolean reusing_view;
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    reusing_view = priv->new_content_view &&
                   gtk_widget_get_parent (GTK_WIDGET (priv->new_content_view)) != NULL;
    /* We are either reusing the view, so new_content_view and content_view
     * are the same, or the new_content_view is invalid */
    if (priv->new_content_view == NULL || reusing_view)
    {
        goto done;
    }

    if (priv->content_view != NULL)
    {
        widget = GTK_WIDGET (priv->content_view);
        gtk_widget_destroy (widget);
        g_object_unref (priv->content_view);
        priv->content_view = NULL;
    }

    if (priv->new_content_view != NULL)
    {
        priv->content_view = priv->new_content_view;
        priv->new_content_view = NULL;

        widget = GTK_WIDGET (priv->content_view);
        gtk_container_add (GTK_CONTAINER (self), widget);
        gtk_widget_set_vexpand (widget, TRUE);
        gtk_widget_show (widget);

        g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ICON]);
        g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_TOOLBAR_MENU_SECTIONS]);
    }

done:
    /* Clean up, so we don't confuse having a new_content_view available or
     * just that we didn't care about it here */
    priv->new_content_view = NULL;
}

/* This is called when we have decided we can actually change to the new view/location situation. */
static void
change_view (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    /* Switch to the new content view.
     * Destroy the extra location widgets first, since they might hold
     * a pointer to the old view, which will possibly be destroyed inside
     * mautilus_window_slot_switch_new_content_view().
     */
    mautilus_window_slot_remove_extra_location_widgets (self);
    mautilus_window_slot_switch_new_content_view (self);

    if (priv->pending_location != NULL)
    {
        /* Tell the window we are finished. */
        mautilus_window_slot_update_for_new_location (self);
    }

    /* Now that we finished switching to the new location,
     * add back the extra location widgets.
     */
    mautilus_window_slot_setup_extra_location_widgets (self);
}

static void
mautilus_window_slot_dispose (GObject *object)
{
    mautilusWindowSlot *self;
    GtkWidget *widget;
    mautilusWindowSlotPrivate *priv;

    self = MAUTILUS_WINDOW_SLOT (object);
    priv = mautilus_window_slot_get_instance_private (self);

    mautilus_window_slot_clear_forward_list (self);
    mautilus_window_slot_clear_back_list (self);

    mautilus_window_slot_remove_extra_location_widgets (self);

    g_signal_handlers_disconnect_by_data (mautilus_trash_monitor_get (), self);

    if (priv->content_view)
    {
        widget = GTK_WIDGET (priv->content_view);
        gtk_widget_destroy (widget);
        g_object_unref (priv->content_view);
        priv->content_view = NULL;
    }

    if (priv->new_content_view)
    {
        widget = GTK_WIDGET (priv->new_content_view);
        gtk_widget_destroy (widget);
        g_object_unref (priv->new_content_view);
        priv->new_content_view = NULL;
    }

    mautilus_window_slot_set_viewed_file (self, NULL);

    g_clear_object (&priv->location);
    g_clear_object (&priv->pending_file_to_activate);

    mautilus_file_list_free (priv->pending_selection);
    priv->pending_selection = NULL;

    g_clear_object (&priv->current_location_bookmark);
    g_clear_object (&priv->last_location_bookmark);

    if (priv->find_mount_cancellable != NULL)
    {
        g_cancellable_cancel (priv->find_mount_cancellable);
        priv->find_mount_cancellable = NULL;
    }

    priv->window = NULL;

    g_free (priv->title);
    priv->title = NULL;

    free_location_change (self);

    G_OBJECT_CLASS (mautilus_window_slot_parent_class)->dispose (object);
}

static void
mautilus_window_slot_grab_focus (GtkWidget *widget)
{
    mautilusWindowSlot *self;
    mautilusWindowSlotPrivate *priv;

    self = MAUTILUS_WINDOW_SLOT (widget);
    priv = mautilus_window_slot_get_instance_private (self);

    GTK_WIDGET_CLASS (mautilus_window_slot_parent_class)->grab_focus (widget);

    if (mautilus_window_slot_get_search_visible (self))
    {
        gtk_widget_grab_focus (GTK_WIDGET (priv->query_editor));
    }
    else if (priv->content_view)
    {
        gtk_widget_grab_focus (GTK_WIDGET (priv->content_view));
    }
    else if (priv->new_content_view)
    {
        gtk_widget_grab_focus (GTK_WIDGET (priv->new_content_view));
    }
}

static void
mautilus_window_slot_class_init (mautilusWindowSlotClass *klass)
{
    GObjectClass *oclass = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

    klass->active = real_active;
    klass->inactive = real_inactive;
    klass->get_view_for_location = real_get_view_for_location;
    klass->handles_location = real_handles_location;

    oclass->dispose = mautilus_window_slot_dispose;
    oclass->constructed = mautilus_window_slot_constructed;
    oclass->set_property = mautilus_window_slot_set_property;
    oclass->get_property = mautilus_window_slot_get_property;

    widget_class->grab_focus = mautilus_window_slot_grab_focus;

    signals[ACTIVE] =
        g_signal_new ("active",
                      G_TYPE_FROM_CLASS (klass),
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET (mautilusWindowSlotClass, active),
                      NULL, NULL,
                      g_cclosure_marshal_VOID__VOID,
                      G_TYPE_NONE, 0);

    signals[INACTIVE] =
        g_signal_new ("inactive",
                      G_TYPE_FROM_CLASS (klass),
                      G_SIGNAL_RUN_LAST,
                      G_STRUCT_OFFSET (mautilusWindowSlotClass, inactive),
                      NULL, NULL,
                      g_cclosure_marshal_VOID__VOID,
                      G_TYPE_NONE, 0);

    properties[PROP_ACTIVE] =
        g_param_spec_boolean ("active",
                              "Whether the slot is active",
                              "Whether the slot is the active slot of the window",
                              FALSE,
                              G_PARAM_READWRITE);
    properties[PROP_LOADING] =
        g_param_spec_boolean ("loading",
                              "Whether the slot loading",
                              "Whether the slot is loading a new location",
                              FALSE,
                              G_PARAM_READABLE);

    properties[PROP_WINDOW] =
        g_param_spec_object ("window",
                             "The mautilusWindow",
                             "The mautilusWindow this slot is part of",
                             MAUTILUS_TYPE_WINDOW,
                             G_PARAM_READWRITE | G_PARAM_CONSTRUCT);

    properties[PROP_ICON] =
        g_param_spec_object ("icon",
                             "Icon that represents the slot",
                             "The icon that represents the slot",
                             G_TYPE_ICON,
                             G_PARAM_READABLE);

    properties[PROP_TOOLBAR_MENU_SECTIONS] =
        g_param_spec_pointer ("toolbar-menu-sections",
                              "Menu sections for the toolbar menu",
                              "The menu sections to add to the toolbar menu for this slot",
                              G_PARAM_READABLE);

    properties[PROP_LOCATION] =
        g_param_spec_object ("location",
                             "Current location visible on the slot",
                             "Either the location that is used currently, or the pending location. Clients will see the same value they set, and therefore it will be cosistent from clients point of view.",
                             G_TYPE_FILE,
                             G_PARAM_READWRITE);

    g_object_class_install_properties (oclass, NUM_PROPERTIES, properties);
}

GFile *
mautilus_window_slot_get_location (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    g_assert (self != NULL);

    priv = mautilus_window_slot_get_instance_private (self);

    return priv->location;
}

const gchar *
mautilus_window_slot_get_title (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);

    return priv->title;
}

char *
mautilus_window_slot_get_location_uri (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    g_assert (MAUTILUS_IS_WINDOW_SLOT (self));

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->location)
    {
        return g_file_get_uri (priv->location);
    }
    return NULL;
}

mautilusWindow *
mautilus_window_slot_get_window (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    g_assert (MAUTILUS_IS_WINDOW_SLOT (self));

    priv = mautilus_window_slot_get_instance_private (self);

    return priv->window;
}

void
mautilus_window_slot_set_window (mautilusWindowSlot *self,
                                 mautilusWindow     *window)
{
    mautilusWindowSlotPrivate *priv;

    g_assert (MAUTILUS_IS_WINDOW_SLOT (self));
    g_assert (MAUTILUS_IS_WINDOW (window));

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->window != window)
    {
        priv->window = window;
        g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_WINDOW]);
    }
}

/* mautilus_window_slot_update_title:
 *
 * Re-calculate the slot title.
 * Called when the location or view has changed.
 * @slot: The mautilusWindowSlot in question.
 *
 */
void
mautilus_window_slot_update_title (mautilusWindowSlot *self)
{
    mautilusWindow *window;
    char *title;
    gboolean do_sync = FALSE;
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    title = mautilus_compute_title_for_location (priv->location);
    window = mautilus_window_slot_get_window (self);

    if (g_strcmp0 (title, priv->title) != 0)
    {
        do_sync = TRUE;

        g_free (priv->title);
        priv->title = title;
        title = NULL;
    }

    if (strlen (priv->title) > 0)
    {
        do_sync = TRUE;
    }

    if (do_sync)
    {
        mautilus_window_sync_title (window, self);
    }

    if (title != NULL)
    {
        g_free (title);
    }
}

gboolean
mautilus_window_slot_get_allow_stop (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);

    return priv->allow_stop;
}

void
mautilus_window_slot_set_allow_stop (mautilusWindowSlot *self,
                                     gboolean            allow)
{
    mautilusWindow *window;
    mautilusWindowSlotPrivate *priv;

    g_assert (MAUTILUS_IS_WINDOW_SLOT (self));

    priv = mautilus_window_slot_get_instance_private (self);

    priv->allow_stop = allow;

    window = mautilus_window_slot_get_window (self);
    mautilus_window_sync_allow_stop (window, self);
}

void
mautilus_window_slot_stop_loading (mautilusWindowSlot *self)
{
    GList *selection;
    GFile *location;
    mautilusDirectory *directory;
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    location = mautilus_window_slot_get_location (self);
    directory = mautilus_directory_get (priv->location);

    if (MAUTILUS_IS_FILES_VIEW (priv->content_view))
    {
        mautilus_files_view_stop_loading (MAUTILUS_FILES_VIEW (priv->content_view));
    }

    mautilus_directory_unref (directory);

    if (priv->pending_location != NULL &&
        location != NULL &&
        priv->content_view != NULL &&
        MAUTILUS_IS_FILES_VIEW (priv->content_view))
    {
        /* No need to tell the new view - either it is the
         * same as the old view, in which case it will already
         * be told, or it is the very pending change we wish
         * to cancel.
         */
        selection = mautilus_view_get_selection (priv->content_view);
        load_new_location (self,
                           location,
                           selection,
                           NULL,
                           TRUE,
                           FALSE);
        mautilus_file_list_free (selection);
    }

    end_location_change (self);

    if (priv->new_content_view)
    {
        g_object_unref (priv->new_content_view);
        priv->new_content_view = NULL;
    }
}

mautilusView *
mautilus_window_slot_get_current_view (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->content_view != NULL)
    {
        return priv->content_view;
    }
    else if (priv->new_content_view)
    {
        return priv->new_content_view;
    }

    return NULL;
}

mautilusBookmark *
mautilus_window_slot_get_bookmark (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);

    return priv->current_location_bookmark;
}

GList *
mautilus_window_slot_get_back_history (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);

    return priv->back_list;
}

GList *
mautilus_window_slot_get_forward_history (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    priv = mautilus_window_slot_get_instance_private (self);

    return priv->forward_list;
}

mautilusWindowSlot *
mautilus_window_slot_new (mautilusWindow *window)
{
    return g_object_new (MAUTILUS_TYPE_WINDOW_SLOT,
                         "window", window,
                         NULL);
}

GIcon *
mautilus_window_slot_get_icon (mautilusWindowSlot *self)
{
    guint current_view_id;
    mautilusWindowSlotPrivate *priv;

    g_return_val_if_fail (MAUTILUS_IS_WINDOW_SLOT (self), NULL);

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->content_view == NULL)
    {
        return NULL;
    }

    current_view_id = mautilus_view_get_view_id (MAUTILUS_VIEW (priv->content_view));
    switch (current_view_id)
    {
        case MAUTILUS_VIEW_LIST_ID:
        {
            return mautilus_view_get_icon (MAUTILUS_VIEW_GRID_ID);
        }
        break;

        case MAUTILUS_VIEW_GRID_ID:
        {
            return mautilus_view_get_icon (MAUTILUS_VIEW_LIST_ID);
        }
        break;

        case MAUTILUS_VIEW_OTHER_LOCATIONS_ID:
        {
            return mautilus_view_get_icon (MAUTILUS_VIEW_OTHER_LOCATIONS_ID);
        }
        break;

        default:
        {
            return NULL;
        }
    }
}

mautilusToolbarMenuSections *
mautilus_window_slot_get_toolbar_menu_sections (mautilusWindowSlot *self)
{
    mautilusView *view;

    g_return_val_if_fail (MAUTILUS_IS_WINDOW_SLOT (self), NULL);

    view = mautilus_window_slot_get_current_view (self);

    return view ? mautilus_view_get_toolbar_menu_sections (view) : NULL;
}

gboolean
mautilus_window_slot_get_active (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    g_return_val_if_fail (MAUTILUS_IS_WINDOW_SLOT (self), FALSE);

    priv = mautilus_window_slot_get_instance_private (self);

    return priv->active;
}

void
mautilus_window_slot_set_active (mautilusWindowSlot *self,
                                 gboolean            active)
{
    mautilusWindowSlotPrivate *priv;

    g_return_if_fail (MAUTILUS_IS_WINDOW_SLOT (self));

    priv = mautilus_window_slot_get_instance_private (self);
    if (priv->active != active)
    {
        priv->active = active;

        if (active)
        {
            g_signal_emit (self, signals[ACTIVE], 0);
        }
        else
        {
            g_signal_emit (self, signals[INACTIVE], 0);
        }

        g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ACTIVE]);
    }
}

static void
mautilus_window_slot_set_loading (mautilusWindowSlot *self,
                                  gboolean            loading)
{
    mautilusWindowSlotPrivate *priv;

    g_return_if_fail (MAUTILUS_IS_WINDOW_SLOT (self));

    priv = mautilus_window_slot_get_instance_private (self);
    priv->loading = loading;

    g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_LOADING]);
}

gboolean
mautilus_window_slot_get_loading (mautilusWindowSlot *self)
{
    mautilusWindowSlotPrivate *priv;

    g_return_val_if_fail (MAUTILUS_IS_WINDOW_SLOT (self), FALSE);

    priv = mautilus_window_slot_get_instance_private (self);

    return priv->loading;
}
