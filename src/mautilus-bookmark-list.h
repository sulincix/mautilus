
/*
 * mautilus
 *
 * Copyright (C) 1999, 2000 Eazel, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: John Sullivan <sullivan@eazel.com>
 */

/* mautilus-bookmark-list.h - interface for centralized list of bookmarks.
 */

#ifndef MAUTILUS_BOOKMARK_LIST_H
#define MAUTILUS_BOOKMARK_LIST_H

#include "mautilus-bookmark.h"
#include <gio/gio.h>

G_BEGIN_DECLS

#define MAUTILUS_TYPE_BOOKMARK_LIST (mautilus_bookmark_list_get_type())

G_DECLARE_FINAL_TYPE (mautilusBookmarkList, mautilus_bookmark_list, MAUTILUS, BOOKMARK_LIST, GObject)

mautilusBookmarkList *  mautilus_bookmark_list_new                 (void);
void                    mautilus_bookmark_list_append              (mautilusBookmarkList   *bookmarks,
								    mautilusBookmark *bookmark);
mautilusBookmark *      mautilus_bookmark_list_item_with_location  (mautilusBookmarkList *bookmarks,
								    GFile                *location,
								    guint                *index);
gboolean                mautilus_bookmark_list_can_bookmark_location (mautilusBookmarkList *list,
								      GFile                *location);
GList *                 mautilus_bookmark_list_get_all             (mautilusBookmarkList   *bookmarks);

G_END_DECLS

#endif /* MAUTILUS_BOOKMARK_LIST_H */
