/* mautilus-view.h
 *
 * Copyright (C) 1999, 2000  Free Software Foundaton
 * Copyright (C) 2000, 2001  Eazel, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Ettore Perazzoli
 *             Darin Adler <darin@bentspoon.com>
 *             John Sullivan <sullivan@eazel.com>
 *          Pavel Cisler <pavel@eazel.com>
 */

#ifndef MAUTILUS_FILES_VIEW_H
#define MAUTILUS_FILES_VIEW_H

#include <gtk/gtk.h>
#include <gio/gio.h>

#include "mautilus-directory.h"
#include "mautilus-file.h"
#include "mautilus-link.h"

#include "mautilus-window.h"
#include "mautilus-view.h"
#include "mautilus-window-slot.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_FILES_VIEW mautilus_files_view_get_type()

G_DECLARE_DERIVABLE_TYPE (mautilusFilesView, mautilus_files_view, MAUTILUS, FILES_VIEW, GtkGrid)

struct _mautilusFilesViewClass {
        GtkGridClass parent_class;

        /* The 'clear' signal is emitted to empty the view of its contents.
         * It must be replaced by each subclass.
         */
        void         (* clear)                  (mautilusFilesView *view);

        /* The 'begin_file_changes' signal is emitted before a set of files
         * are added to the view. It can be replaced by a subclass to do any
         * necessary preparation for a set of new files. The default
         * implementation does nothing.
         */
        void         (* begin_file_changes)     (mautilusFilesView *view);

        /* The 'add_files' signal is emitted to add a set of files to the view.
         * It must be replaced by each subclass.
         */
        void    (* add_files)                    (mautilusFilesView *view,
                                                  GList             *files);
        void    (* remove_file)                 (mautilusFilesView *view,
                                                 mautilusFile      *file,
                                                 mautilusDirectory *directory);

        /* The 'file_changed' signal is emitted to signal a change in a file,
         * including the file being removed.
         * It must be replaced by each subclass.
         */
        void         (* file_changed)         (mautilusFilesView *view,
                                               mautilusFile      *file,
                                               mautilusDirectory *directory);

        /* The 'end_file_changes' signal is emitted after a set of files
         * are added to the view. It can be replaced by a subclass to do any
         * necessary cleanup (typically, cleanup for code in begin_file_changes).
         * The default implementation does nothing.
         */
        void         (* end_file_changes)    (mautilusFilesView *view);

        /* The 'begin_loading' signal is emitted before any of the contents
         * of a directory are added to the view. It can be replaced by a
         * subclass to do any necessary preparation to start dealing with a
         * new directory. The default implementation does nothing.
         */
        void         (* begin_loading)       (mautilusFilesView *view);

        /* The 'end_loading' signal is emitted after all of the contents
         * of a directory are added to the view. It can be replaced by a
         * subclass to do any necessary clean-up. The default implementation
         * does nothing.
         *
         * If all_files_seen is true, the handler may assume that
         * no load error ocurred, and all files of the underlying
         * directory were loaded.
         *
         * Otherwise, end_loading was emitted due to cancellation,
         * which usually means that not all files are available.
         */
        void         (* end_loading)          (mautilusFilesView *view,
                                               gboolean           all_files_seen);

        /* Function pointers that don't have corresponding signals */

        /* get_backing uri is a function pointer for subclasses to
         * override. Subclasses may replace it with a function that
         * returns the URI for the location where to create new folders,
         * files, links and paste the clipboard to.
         */

        char *        (* get_backing_uri)    (mautilusFilesView *view);

        /* get_selection is not a signal; it is just a function pointer for
         * subclasses to replace (override). Subclasses must replace it
         * with a function that returns a newly-allocated GList of
         * mautilusFile pointers.
         */
        GList *        (* get_selection)     (mautilusFilesView *view);

        /* get_selection_for_file_transfer  is a function pointer for
         * subclasses to replace (override). Subclasses must replace it
         * with a function that returns a newly-allocated GList of
         * mautilusFile pointers. The difference from get_selection is
         * that any files in the selection that also has a parent folder
         * in the selection is not included.
         */
        GList *        (* get_selection_for_file_transfer)(mautilusFilesView *view);

        /* select_all is a function pointer that subclasses must override to
         * select all of the items in the view */
        void     (* select_all)              (mautilusFilesView *view);

        /* select_first is a function pointer that subclasses must override to
         * select the first item in the view */
        void     (* select_first)            (mautilusFilesView *view);

        /* set_selection is a function pointer that subclasses must
         * override to select the specified items (and unselect all
         * others). The argument is a list of mautilusFiles. */

        void     (* set_selection)           (mautilusFilesView *view,
                                              GList             *selection);

        /* invert_selection is a function pointer that subclasses must
         * override to invert selection. */

        void     (* invert_selection)        (mautilusFilesView *view);

        /* bump_zoom_level is a function pointer that subclasses must override
         * to change the zoom level of an object. */
        void    (* bump_zoom_level)          (mautilusFilesView *view,
                                              int                zoom_increment);

        /*
         * restore_default_zoom_level: restores the zoom level to 100% (or to
         * whatever is considered the 'standard' zoom level for the view). */
        void    (* restore_standard_zoom_level) (mautilusFilesView *view);

        /* can_zoom_in is a function pointer that subclasses must override to
         * return whether the view is at maximum size (furthest-in zoom level) */
        gboolean (* can_zoom_in)             (mautilusFilesView *view);

        /* can_zoom_out is a function pointer that subclasses must override to
         * return whether the view is at minimum size (furthest-out zoom level) */
        gboolean (* can_zoom_out)            (mautilusFilesView *view);

        /* The current zoom level as a percentage of the default. */
        gfloat   (* get_zoom_level_percentage) (mautilusFilesView *view);

        gboolean (*is_zoom_level_default)      (mautilusFilesView *view);

        /* reveal_selection is a function pointer that subclasses may
         * override to make sure the selected items are sufficiently
         * apparent to the user (e.g., scrolled into view). By default,
         * this does nothing.
         */
        void     (* reveal_selection)        (mautilusFilesView *view);

        /* update_menus is a function pointer that subclasses can override to
         * update the sensitivity or wording of menu items in the menu bar.
         * It is called (at least) whenever the selection changes. If overridden,
         * subclasses must call parent class's function.
         */
        void    (* update_context_menus)     (mautilusFilesView *view);

        void    (* update_actions_state)     (mautilusFilesView *view);

        /* sort_files is a function pointer that subclasses can override
         * to provide a sorting order to determine which files should be
         * presented when only a partial list is provided.
         */
        int     (* compare_files)            (mautilusFilesView *view,
                                              mautilusFile      *a,
                                              mautilusFile      *b);

        /* using_manual_layout is a function pointer that subclasses may
         * override to control whether or not items can be freely positioned
         * on the user-visible area.
         * Note that this value is not guaranteed to be constant within the
         * view's lifecycle. */
        gboolean (* using_manual_layout)     (mautilusFilesView *view);

        /* is_empty is a function pointer that subclasses must
         * override to report whether the view contains any items.
         */
        gboolean (* is_empty)                (mautilusFilesView *view);

        /* convert *point from widget's coordinate system to a coordinate
         * system used for specifying file operation positions, which is view-specific.
         *
         * This is used by the the icon view, which converts the screen position to a zoom
         * level-independent coordinate system.
         */
        void (* widget_to_file_operation_position) (mautilusFilesView *view,
                                                    GdkPoint     *position);

        /* Preference change callbacks, overridden by icon and list views.
         * Icon and list views respond by synchronizing to the new preference
         * values and forcing an update if appropriate.
         */
        void        (* click_policy_changed) (mautilusFilesView *view);
        void        (* sort_directories_first_changed) (mautilusFilesView *view);

        /* Get the id for this view. Its a guint*/
        guint        (* get_view_id)       (mautilusFilesView *view);

        /* Return the uri of the first visible file */
        char *         (* get_first_visible_file) (mautilusFilesView          *view);
        /* Scroll the view so that the file specified by the uri is at the top
           of the view */
        void           (* scroll_to_file)    (mautilusFilesView *view,
                                              const char        *uri);

        mautilusWindow * (*get_window)       (mautilusFilesView *view);

        GdkRectangle * (* compute_rename_popover_pointing_to) (mautilusFilesView *view);

        GIcon *        (* get_icon)          (mautilusFilesView *view);

        /* Use this to show an optional visual feedback when the directory is empty.
         * By default it shows a widget overlay on top of the view */
        void           (* check_empty_states)          (mautilusFilesView *view);
};

mautilusFilesView *      mautilus_files_view_new                         (guint               id,
                                                                          mautilusWindowSlot *slot);

/* Functions callable from the user interface and elsewhere. */
mautilusWindowSlot *mautilus_files_view_get_mautilus_window_slot         (mautilusFilesView *view);
char *              mautilus_files_view_get_uri                          (mautilusFilesView *view);

void                mautilus_files_view_display_selection_info           (mautilusFilesView *view);

/* Wrappers for signal emitters. These are normally called
 * only by mautilusFilesView itself. They have corresponding signals
 * that observers might want to connect with.
 */
gboolean            mautilus_files_view_get_loading                      (mautilusFilesView *view);

/* Hooks for subclasses to call. These are normally called only by
 * mautilusFilesView and its subclasses
 */
void                mautilus_files_view_activate_files                   (mautilusFilesView       *view,
                                                                          GList                   *files,
                                                                          mautilusWindowOpenFlags  flags,
                                                                          gboolean                 confirm_multiple);
void                mautilus_files_view_activate_file                    (mautilusFilesView       *view,
                                                                          mautilusFile            *file,
                                                                          mautilusWindowOpenFlags  flags);
void                mautilus_files_view_preview_files                    (mautilusFilesView *view,
                                                                          GList             *files,
                                                                          GArray            *locations);
void                mautilus_files_view_start_batching_selection_changes (mautilusFilesView *view);
void                mautilus_files_view_stop_batching_selection_changes  (mautilusFilesView *view);
void                mautilus_files_view_notify_selection_changed         (mautilusFilesView *view);
mautilusDirectory  *mautilus_files_view_get_model                        (mautilusFilesView *view);
mautilusFile       *mautilus_files_view_get_directory_as_file            (mautilusFilesView *view);
void                mautilus_files_view_pop_up_background_context_menu   (mautilusFilesView *view,
                                                                          GdkEventButton    *event);
void                mautilus_files_view_pop_up_selection_context_menu    (mautilusFilesView *view,
                                                                          GdkEventButton    *event);
gboolean            mautilus_files_view_should_show_file                 (mautilusFilesView *view,
                                                                          mautilusFile      *file);
gboolean            mautilus_files_view_should_sort_directories_first    (mautilusFilesView *view);
void                mautilus_files_view_ignore_hidden_file_preferences   (mautilusFilesView *view);
void                mautilus_files_view_set_show_foreign                 (mautilusFilesView *view,
                                                                          gboolean           show_foreign);
gboolean            mautilus_files_view_handle_scroll_event              (mautilusFilesView *view,
                                                                          GdkEventScroll    *event);

void                mautilus_files_view_add_subdirectory                (mautilusFilesView *view,
                                                                         mautilusDirectory *directory);
void                mautilus_files_view_remove_subdirectory             (mautilusFilesView *view,
                                                                         mautilusDirectory *directory);

gboolean            mautilus_files_view_is_editable              (mautilusFilesView      *view);
mautilusWindow *    mautilus_files_view_get_window               (mautilusFilesView      *view);

guint               mautilus_files_view_get_view_id                (mautilusView      *view);

/* file operations */
char *            mautilus_files_view_get_backing_uri            (mautilusFilesView      *view);
void              mautilus_files_view_move_copy_items            (mautilusFilesView      *view,
                                                                  const GList            *item_uris,
                                                                  GArray                 *relative_item_points,
                                                                  const char             *target_uri,
                                                                  int                     copy_action,
                                                                  int                     x,
                                                                  int                     y);
void              mautilus_files_view_new_file_with_initial_contents (mautilusFilesView  *view,
                                                                      const char         *parent_uri,
                                                                      const char         *filename,
                                                                      const char         *initial_contents,
                                                                      int                 length,
                                                                      GdkPoint           *pos);

/* selection handling */
void              mautilus_files_view_activate_selection         (mautilusFilesView      *view);
void              mautilus_files_view_stop_loading               (mautilusFilesView      *view);

char *            mautilus_files_view_get_first_visible_file     (mautilusFilesView      *view);
void              mautilus_files_view_scroll_to_file             (mautilusFilesView      *view,
                                                                  const char             *uri);
char *            mautilus_files_view_get_title                  (mautilusFilesView      *view);
gboolean          mautilus_files_view_supports_zooming           (mautilusFilesView      *view);
void              mautilus_files_view_bump_zoom_level            (mautilusFilesView      *view,
                                                                  int                     zoom_increment);
gboolean          mautilus_files_view_can_zoom_in                (mautilusFilesView      *view);
gboolean          mautilus_files_view_can_zoom_out               (mautilusFilesView      *view);

void              mautilus_files_view_update_context_menus       (mautilusFilesView      *view);
void              mautilus_files_view_update_toolbar_menus       (mautilusFilesView      *view);
void              mautilus_files_view_update_actions_state       (mautilusFilesView      *view);

void              mautilus_files_view_action_show_hidden_files   (mautilusFilesView      *view,
                                                                  gboolean                show_hidden);

GActionGroup *    mautilus_files_view_get_action_group           (mautilusFilesView      *view);
GtkWidget*        mautilus_files_view_get_content_widget         (mautilusFilesView      *view);

G_END_DECLS

#endif /* MAUTILUS_FILES_VIEW_H */
