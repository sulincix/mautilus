
/* mautilus-bookmark.h - implementation of individual bookmarks.
 *
 * Copyright (C) 1999, 2000 Eazel, Inc.
 * Copyright (C) 2011, Red Hat, Inc.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Authors: John Sullivan <sullivan@eazel.com>
 *          Cosimo Cecchi <cosimoc@redhat.com>
 */

#ifndef MAUTILUS_BOOKMARK_H
#define MAUTILUS_BOOKMARK_H

#include <gtk/gtk.h>
#include <gio/gio.h>

G_BEGIN_DECLS

#define MAUTILUS_TYPE_BOOKMARK mautilus_bookmark_get_type()

G_DECLARE_FINAL_TYPE (mautilusBookmark, mautilus_bookmark, MAUTILUS, BOOKMARK, GObject)

mautilusBookmark *    mautilus_bookmark_new                    (GFile *location,
                                                                const char *custom_name);
const char *          mautilus_bookmark_get_name               (mautilusBookmark      *bookmark);
GFile *               mautilus_bookmark_get_location           (mautilusBookmark      *bookmark);
char *                mautilus_bookmark_get_uri                (mautilusBookmark      *bookmark);
GIcon *               mautilus_bookmark_get_icon               (mautilusBookmark      *bookmark);
GIcon *               mautilus_bookmark_get_symbolic_icon      (mautilusBookmark      *bookmark);
gboolean              mautilus_bookmark_get_xdg_type           (mautilusBookmark      *bookmark,
								GUserDirectory        *directory);
gboolean              mautilus_bookmark_get_is_builtin         (mautilusBookmark      *bookmark);
gboolean              mautilus_bookmark_get_has_custom_name    (mautilusBookmark      *bookmark);
int                   mautilus_bookmark_compare_with           (gconstpointer          a,
								gconstpointer          b);

void                  mautilus_bookmark_set_scroll_pos         (mautilusBookmark      *bookmark,
								const char            *uri);
char *                mautilus_bookmark_get_scroll_pos         (mautilusBookmark      *bookmark);

/* Helper functions for displaying bookmarks */
GtkWidget *           mautilus_bookmark_menu_item_new          (mautilusBookmark      *bookmark);

G_END_DECLS

#endif /* MAUTILUS_BOOKMARK_H */
