#include "mautilus-view-model.h"
#include "mautilus-view-item-model.h"
#include "mautilus-global-preferences.h"

struct _mautilusViewModel
{
    GObject parent_instance;

    GHashTable *map_files_to_model;
    GListStore *internal_model;
    mautilusViewModelSortData *sort_data;
};

G_DEFINE_TYPE (mautilusViewModel, mautilus_view_model, G_TYPE_OBJECT)

enum
{
    PROP_0,
    PROP_SORT_TYPE,
    PROP_G_MODEL,
    N_PROPS
};

static void
finalize (GObject *object)
{
    mautilusViewModel *self = MAUTILUS_VIEW_MODEL (object);

    G_OBJECT_CLASS (mautilus_view_model_parent_class)->finalize (object);

    g_hash_table_destroy (self->map_files_to_model);
    if (self->sort_data)
    {
        g_free (self->sort_data);
    }
    g_object_unref (self->internal_model);
}

static void
get_property (GObject    *object,
              guint       prop_id,
              GValue     *value,
              GParamSpec *pspec)
{
    mautilusViewModel *self = MAUTILUS_VIEW_MODEL (object);

    switch (prop_id)
    {
        case PROP_SORT_TYPE:
        {
            g_value_set_object (value, self->sort_data);
        }
        break;

        case PROP_G_MODEL:
        {
            g_value_set_object (value, self->internal_model);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        }
    }
}

static void
set_property (GObject      *object,
              guint         prop_id,
              const GValue *value,
              GParamSpec   *pspec)
{
    mautilusViewModel *self = MAUTILUS_VIEW_MODEL (object);

    switch (prop_id)
    {
        case PROP_SORT_TYPE:
        {
            mautilus_view_model_set_sort_type (self, g_value_get_object (value));
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        }
    }
}

static void
constructed (GObject *object)
{
    mautilusViewModel *self = MAUTILUS_VIEW_MODEL (object);

    G_OBJECT_CLASS (mautilus_view_model_parent_class)->constructed (object);

    self->internal_model = g_list_store_new (MAUTILUS_TYPE_VIEW_ITEM_MODEL);
    self->map_files_to_model = g_hash_table_new (NULL, NULL);
}

static void
mautilus_view_model_class_init (mautilusViewModelClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = finalize;
    object_class->get_property = get_property;
    object_class->set_property = set_property;
    object_class->constructed = constructed;
}

static void
mautilus_view_model_init (mautilusViewModel *self)
{
}

static gint
compare_data_func (gconstpointer a,
                   gconstpointer b,
                   gpointer      user_data)
{
    mautilusViewModel *self = MAUTILUS_VIEW_MODEL (user_data);
    mautilusFile *file_a;
    mautilusFile *file_b;

    file_a = mautilus_view_item_model_get_file (MAUTILUS_VIEW_ITEM_MODEL ((gpointer) a));
    file_b = mautilus_view_item_model_get_file (MAUTILUS_VIEW_ITEM_MODEL ((gpointer) b));

    return mautilus_file_compare_for_sort (file_a, file_b,
                                           self->sort_data->sort_type,
                                           self->sort_data->directories_first,
                                           self->sort_data->reversed);
}

mautilusViewModel *
mautilus_view_model_new ()
{
    return g_object_new (MAUTILUS_TYPE_VIEW_MODEL, NULL);
}

void
mautilus_view_model_set_sort_type (mautilusViewModel         *self,
                                   mautilusViewModelSortData *sort_data)
{
    if (self->sort_data)
    {
        g_free (self->sort_data);
    }

    self->sort_data = g_new (mautilusViewModelSortData, 1);
    self->sort_data->sort_type = sort_data->sort_type;
    self->sort_data->reversed = sort_data->reversed;
    self->sort_data->directories_first = sort_data->directories_first;

    g_list_store_sort (self->internal_model, compare_data_func, self);
}

mautilusViewModelSortData *
mautilus_view_model_get_sort_type (mautilusViewModel *self)
{
    return self->sort_data;
}

GListStore *
mautilus_view_model_get_g_model (mautilusViewModel *self)
{
    return self->internal_model;
}

GQueue *
mautilus_view_model_get_items_from_files (mautilusViewModel *self,
                                          GQueue            *files)
{
    GList *l;
    mautilusViewItemModel *item_model;
    GQueue *item_models;

    item_models = g_queue_new ();
    for (l = g_queue_peek_head_link (files); l != NULL; l = l->next)
    {
        mautilusFile *file1;
        gint i = 0;

        file1 = MAUTILUS_FILE (l->data);
        while ((item_model = g_list_model_get_item (G_LIST_MODEL (self->internal_model), i)))
        {
            mautilusFile *file2;
            g_autofree gchar *file1_uri;
            g_autofree gchar *file2_uri;

            file2 = mautilus_view_item_model_get_file (item_model);
            file1_uri = mautilus_file_get_uri (file1);
            file2_uri = mautilus_file_get_uri (file2);
            if (g_strcmp0 (file1_uri, file2_uri) == 0)
            {
                g_queue_push_tail (item_models, item_model);
                break;
            }

            i++;
        }
    }

    return item_models;
}

mautilusViewItemModel *
mautilus_view_model_get_item_from_file (mautilusViewModel *self,
                                        mautilusFile      *file)
{
    return g_hash_table_lookup (self->map_files_to_model, file);
}

void
mautilus_view_model_remove_item (mautilusViewModel     *self,
                                 mautilusViewItemModel *item)
{
    mautilusViewItemModel *item_model;
    gint i;

    i = 0;
    item_model = NULL;
    while ((item_model = g_list_model_get_item (G_LIST_MODEL (self->internal_model), i)))
    {
        if (item_model == item)
        {
            break;
        }

        i++;
    }

    if (item_model != NULL)
    {
        mautilusFile *file;

        file = mautilus_view_item_model_get_file (item_model);
        g_list_store_remove (self->internal_model, i);
        g_hash_table_remove (self->map_files_to_model, file);
    }
}

void
mautilus_view_model_remove_all_items (mautilusViewModel *self)
{
    g_list_store_remove_all (self->internal_model);
    g_hash_table_remove_all (self->map_files_to_model);
}

void
mautilus_view_model_add_item (mautilusViewModel     *self,
                              mautilusViewItemModel *item)
{
    g_hash_table_insert (self->map_files_to_model,
                         mautilus_view_item_model_get_file (item),
                         item);
    g_list_store_insert_sorted (self->internal_model, item, compare_data_func, self);
}

void
mautilus_view_model_add_items (mautilusViewModel *self,
                               GQueue            *items)
{
    g_autofree gpointer *array = NULL;
    GList *l;
    int i = 0;

    array = g_malloc_n (g_queue_get_length (items),
                        sizeof (mautilusViewItemModel *));

    for (l = g_queue_peek_head_link (items); l != NULL; l = l->next)
    {
        array[i] = l->data;
        g_hash_table_insert (self->map_files_to_model,
                             mautilus_view_item_model_get_file (l->data),
                             l->data);
        i++;
    }

    g_list_store_splice (self->internal_model,
                         g_list_model_get_n_items (G_LIST_MODEL (self->internal_model)),
                         0, array, g_queue_get_length (items));

    g_list_store_sort (self->internal_model, compare_data_func, self);
}

void
mautilus_view_model_set_items (mautilusViewModel *self,
                               GQueue            *items)
{
    g_autofree gpointer *array = NULL;
    GList *l;
    int i = 0;

    array = g_malloc_n (g_queue_get_length (items),
                        sizeof (mautilusViewItemModel *));

    g_hash_table_remove_all (self->map_files_to_model);
    for (l = g_queue_peek_head_link (items); l != NULL; l = l->next)
    {
        array[i] = l->data;
        g_hash_table_insert (self->map_files_to_model,
                             mautilus_view_item_model_get_file (l->data),
                             l->data);
        i++;
    }

    g_list_store_splice (self->internal_model,
                         0, g_list_model_get_n_items (G_LIST_MODEL (self->internal_model)),
                         array, g_queue_get_length (items));

    g_list_store_sort (self->internal_model, compare_data_func, self);
}
