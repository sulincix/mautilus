/*
 * mautilus-freedesktop-dbus: Implementation for the org.freedesktop DBus file-management interfaces
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Akshay Gupta <kitallis@gmail.com>
 *          Federico Mena Quintero <federico@gnome.org>
 */

#include <config.h>

#include "mautilus-application.h"
#include "mautilus-freedesktop-dbus.h"
#include "mautilus-freedesktop-generated.h"

/* We share the same debug domain as mautilus-dbus-manager */
#define DEBUG_FLAG MAUTILUS_DEBUG_DBUS
#include "mautilus-debug.h"

#include "mautilus-properties-window.h"

#include <gio/gio.h>

struct _mautilusFreedesktopDBus
{
    GObject parent;

    /* Id from g_dbus_own_name() */
    guint owner_id;

    /* Our DBus implementation skeleton */
    mautilusFreedesktopFileManager1 *skeleton;
};

struct _mautilusFreedesktopDBusClass
{
    GObjectClass parent_class;
};

G_DEFINE_TYPE (mautilusFreedesktopDBus, mautilus_freedesktop_dbus, G_TYPE_OBJECT);

static gboolean
skeleton_handle_show_items_cb (mautilusFreedesktopFileManager1 *object,
                               GDBusMethodInvocation           *invocation,
                               const gchar * const             *uris,
                               const gchar                     *startup_id,
                               gpointer                         data)
{
    mautilusApplication *application;
    int i;

    application = MAUTILUS_APPLICATION (g_application_get_default ());

    for (i = 0; uris[i] != NULL; i++)
    {
        GFile *file;
        GFile *parent;

        file = g_file_new_for_uri (uris[i]);
        parent = g_file_get_parent (file);

        if (parent != NULL)
        {
            mautilus_application_open_location (application, parent, file, startup_id);
            g_object_unref (parent);
        }
        else
        {
            mautilus_application_open_location (application, file, NULL, startup_id);
        }

        g_object_unref (file);
    }

    mautilus_freedesktop_file_manager1_complete_show_items (object, invocation);
    return TRUE;
}

static gboolean
skeleton_handle_show_folders_cb (mautilusFreedesktopFileManager1 *object,
                                 GDBusMethodInvocation           *invocation,
                                 const gchar * const             *uris,
                                 const gchar                     *startup_id,
                                 gpointer                         data)
{
    mautilusApplication *application;
    int i;

    application = MAUTILUS_APPLICATION (g_application_get_default ());

    for (i = 0; uris[i] != NULL; i++)
    {
        GFile *file;

        file = g_file_new_for_uri (uris[i]);

        mautilus_application_open_location (application, file, NULL, startup_id);

        g_object_unref (file);
    }

    mautilus_freedesktop_file_manager1_complete_show_folders (object, invocation);
    return TRUE;
}

static gboolean
skeleton_handle_show_item_properties_cb (mautilusFreedesktopFileManager1 *object,
                                         GDBusMethodInvocation           *invocation,
                                         const gchar * const             *uris,
                                         const gchar                     *startup_id,
                                         gpointer                         data)
{
    GList *files;
    int i;

    files = NULL;

    for (i = 0; uris[i] != NULL; i++)
    {
        files = g_list_prepend (files, mautilus_file_get_by_uri (uris[i]));
    }

    files = g_list_reverse (files);

    mautilus_properties_window_present (files, NULL, startup_id);

    mautilus_file_list_free (files);

    mautilus_freedesktop_file_manager1_complete_show_item_properties (object, invocation);
    return TRUE;
}

static void
bus_acquired_cb (GDBusConnection *conn,
                 const gchar     *name,
                 gpointer         user_data)
{
    mautilusFreedesktopDBus *fdb = user_data;

    DEBUG ("Bus acquired at %s", name);

    fdb->skeleton = mautilus_freedesktop_file_manager1_skeleton_new ();

    g_signal_connect (fdb->skeleton, "handle-show-items",
                      G_CALLBACK (skeleton_handle_show_items_cb), fdb);
    g_signal_connect (fdb->skeleton, "handle-show-folders",
                      G_CALLBACK (skeleton_handle_show_folders_cb), fdb);
    g_signal_connect (fdb->skeleton, "handle-show-item-properties",
                      G_CALLBACK (skeleton_handle_show_item_properties_cb), fdb);

    g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (fdb->skeleton), conn, MAUTILUS_FDO_DBUS_PATH, NULL);
}

static void
name_acquired_cb (GDBusConnection *connection,
                  const gchar     *name,
                  gpointer         user_data)
{
    DEBUG ("Acquired the name %s on the session message bus\n", name);
}

static void
name_lost_cb (GDBusConnection *connection,
              const gchar     *name,
              gpointer         user_data)
{
    DEBUG ("Lost (or failed to acquire) the name %s on the session message bus\n", name);
}

static void
mautilus_freedesktop_dbus_dispose (GObject *object)
{
    mautilusFreedesktopDBus *fdb = (mautilusFreedesktopDBus *) object;

    if (fdb->owner_id != 0)
    {
        g_bus_unown_name (fdb->owner_id);
        fdb->owner_id = 0;
    }

    if (fdb->skeleton != NULL)
    {
        g_dbus_interface_skeleton_unexport (G_DBUS_INTERFACE_SKELETON (fdb->skeleton));
        g_object_unref (fdb->skeleton);
        fdb->skeleton = NULL;
    }

    G_OBJECT_CLASS (mautilus_freedesktop_dbus_parent_class)->dispose (object);
}

static void
mautilus_freedesktop_dbus_class_init (mautilusFreedesktopDBusClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->dispose = mautilus_freedesktop_dbus_dispose;
}

static void
mautilus_freedesktop_dbus_init (mautilusFreedesktopDBus *fdb)
{
    fdb->owner_id = g_bus_own_name (G_BUS_TYPE_SESSION,
                                    MAUTILUS_FDO_DBUS_NAME,
                                    G_BUS_NAME_OWNER_FLAGS_NONE,
                                    bus_acquired_cb,
                                    name_acquired_cb,
                                    name_lost_cb,
                                    fdb,
                                    NULL);
}

void
mautilus_freedesktop_dbus_set_open_locations (mautilusFreedesktopDBus  *fdb,
                                              const gchar             **locations)
{
    g_return_if_fail (MAUTILUS_IS_FREEDESKTOP_DBUS (fdb));

    mautilus_freedesktop_file_manager1_set_open_locations (fdb->skeleton, locations);
}

/* Tries to own the org.freedesktop.FileManager1 service name */
mautilusFreedesktopDBus *
mautilus_freedesktop_dbus_new (void)
{
    return g_object_new (mautilus_freedesktop_dbus_get_type (),
                         NULL);
}
