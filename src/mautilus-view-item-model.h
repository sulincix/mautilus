#ifndef MAUTILUS_VIEW_ITEM_MODEL_H
#define MAUTILUS_VIEW_ITEM_MODEL_H

#include <glib.h>
#include <gtk/gtk.h>

#include "mautilus-file.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_VIEW_ITEM_MODEL (mautilus_view_item_model_get_type())

G_DECLARE_FINAL_TYPE (mautilusViewItemModel, mautilus_view_item_model, MAUTILUS, VIEW_ITEM_MODEL, GObject)

mautilusViewItemModel * mautilus_view_item_model_new (mautilusFile *file,
                                                      guint         icon_size);

void mautilus_view_item_model_set_icon_size (mautilusViewItemModel *self,
                                             guint                 icon_size);

guint mautilus_view_item_model_get_icon_size (mautilusViewItemModel *self);

void mautilus_view_item_model_set_file (mautilusViewItemModel *self,
                                        mautilusFile         *file);

mautilusFile * mautilus_view_item_model_get_file (mautilusViewItemModel *self);

void mautilus_view_item_model_set_item_ui (mautilusViewItemModel *self,
                                           GtkWidget             *item_ui);

GtkWidget * mautilus_view_item_model_get_item_ui (mautilusViewItemModel *self);

G_END_DECLS

#endif /* MAUTILUS_VIEW_ITEM_MODEL_H */

