/* mautilus-rename-file-popover-controller.h
 *
 * Copyright (C) 2016 the mautilus developers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MAUTILUS_RENAME_FILE_POPOVER_CONTROLLER_H
#define MAUTILUS_RENAME_FILE_POPOVER_CONTROLLER_H

#include <glib.h>
#include <gtk/gtk.h>

#include "mautilus-file-name-widget-controller.h"
#include "mautilus-file.h"

#define MAUTILUS_TYPE_RENAME_FILE_POPOVER_CONTROLLER mautilus_rename_file_popover_controller_get_type ()
G_DECLARE_FINAL_TYPE (mautilusRenameFilePopoverController, mautilus_rename_file_popover_controller, MAUTILUS, RENAME_FILE_POPOVER_CONTROLLER, mautilusFileNameWidgetController)

mautilusRenameFilePopoverController * mautilus_rename_file_popover_controller_new (mautilusFile *target_file,
                                                                                   GdkRectangle *pointing_to,
                                                                                   GtkWidget    *relative_to);

mautilusFile * mautilus_rename_file_popover_controller_get_target_file (mautilusRenameFilePopoverController *controller);

#endif /* MAUTILUS_RENAME_FILE_POPOVER_CONTROLLER_H */
