/* mautilus-places-view.c
 *
 * Copyright (C) 2015 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mautilus-mime-actions.h"
#include "mautilus-places-view.h"
#include "mautilus-window-slot.h"
#include "mautilus-application.h"
#include "mautilus-toolbar-menu-sections.h"
#include "gtk/mautilusgtkplacesviewprivate.h"

typedef struct
{
    GFile *location;
    mautilusQuery *search_query;
    mautilusToolbarMenuSections *toolbar_menu_sections;

    GtkWidget *places_view;
} mautilusPlacesViewPrivate;

struct _mautilusPlacesView
{
    GtkFrameClass parent;
};

static void          mautilus_places_view_iface_init (mautilusViewInterface *iface);

G_DEFINE_TYPE_WITH_CODE (mautilusPlacesView, mautilus_places_view, GTK_TYPE_BOX,
                         G_ADD_PRIVATE (mautilusPlacesView)
                         G_IMPLEMENT_INTERFACE (MAUTILUS_TYPE_VIEW, mautilus_places_view_iface_init));

enum
{
    PROP_0,
    PROP_LOCATION,
    PROP_SEARCH_QUERY,
    PROP_IS_LOADING,
    PROP_IS_SEARCHING,
    LAST_PROP
};

static void
open_location_cb (mautilusPlacesView *view,
                  GFile              *location,
                  GtkPlacesOpenFlags  open_flags)
{
    mautilusWindowOpenFlags flags;
    GtkWidget *slot;

    slot = gtk_widget_get_ancestor (GTK_WIDGET (view), MAUTILUS_TYPE_WINDOW_SLOT);

    switch (open_flags)
    {
        case GTK_PLACES_OPEN_NEW_TAB:
        {
            flags = MAUTILUS_WINDOW_OPEN_FLAG_NEW_TAB |
                    MAUTILUS_WINDOW_OPEN_FLAG_DONT_MAKE_ACTIVE;
        }
        break;

        case GTK_PLACES_OPEN_NEW_WINDOW:
        {
            flags = MAUTILUS_WINDOW_OPEN_FLAG_NEW_WINDOW;
        }
        break;

        case GTK_PLACES_OPEN_NORMAL: /* fall-through */
        default:
        {
            flags = 0;
        }
        break;
    }

    if (slot)
    {
        mautilusFile *file;
        GtkWidget *window;
        char *path;

        path = "other-locations:///";
        file = mautilus_file_get (location);
        window = gtk_widget_get_toplevel (GTK_WIDGET (view));

        mautilus_mime_activate_file (GTK_WINDOW (window),
                                     MAUTILUS_WINDOW_SLOT (slot),
                                     file,
                                     path,
                                     flags);
        mautilus_file_unref (file);
    }
}

static void
loading_cb (mautilusView *view)
{
    g_object_notify (G_OBJECT (view), "is-loading");
}

static void
show_error_message_cb (mautilusGtkPlacesView *view,
                       const gchar           *primary,
                       const gchar           *secondary)
{
    GtkWidget *dialog;
    GtkWidget *window;

    window = gtk_widget_get_toplevel (GTK_WIDGET (view));

    dialog = gtk_message_dialog_new (GTK_WINDOW (window),
                                     GTK_DIALOG_DESTROY_WITH_PARENT | GTK_DIALOG_MODAL,
                                     GTK_MESSAGE_ERROR,
                                     GTK_BUTTONS_CLOSE,
                                     "%s", primary);
    gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog),
                                              "%s", secondary);

    gtk_dialog_run (GTK_DIALOG (dialog));

    gtk_widget_destroy (dialog);
}

static void
mautilus_places_view_finalize (GObject *object)
{
    mautilusPlacesView *self = (mautilusPlacesView *) object;
    mautilusPlacesViewPrivate *priv = mautilus_places_view_get_instance_private (self);

    g_clear_object (&priv->location);
    g_clear_object (&priv->search_query);

    g_free (priv->toolbar_menu_sections);

    G_OBJECT_CLASS (mautilus_places_view_parent_class)->finalize (object);
}

static void
mautilus_places_view_get_property (GObject    *object,
                                   guint       prop_id,
                                   GValue     *value,
                                   GParamSpec *pspec)
{
    mautilusView *view = MAUTILUS_VIEW (object);

    switch (prop_id)
    {
        case PROP_LOCATION:
        {
            g_value_set_object (value, mautilus_view_get_location (view));
        }
        break;

        case PROP_SEARCH_QUERY:
        {
            g_value_set_object (value, mautilus_view_get_search_query (view));
        }
        break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mautilus_places_view_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
    mautilusView *view = MAUTILUS_VIEW (object);

    switch (prop_id)
    {
        case PROP_LOCATION:
        {
            mautilus_view_set_location (view, g_value_get_object (value));
        }
        break;

        case PROP_SEARCH_QUERY:
        {
            mautilus_view_set_search_query (view, g_value_get_object (value));
        }
        break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static GFile *
mautilus_places_view_get_location (mautilusView *view)
{
    mautilusPlacesViewPrivate *priv;

    priv = mautilus_places_view_get_instance_private (MAUTILUS_PLACES_VIEW (view));

    return priv->location;
}

static void
mautilus_places_view_set_location (mautilusView *view,
                                   GFile        *location)
{
    if (location)
    {
        mautilusPlacesViewPrivate *priv;
        gchar *uri;

        priv = mautilus_places_view_get_instance_private (MAUTILUS_PLACES_VIEW (view));
        uri = g_file_get_uri (location);

        /*
         * If it's not trying to open the places view itself, simply
         * delegates the location to application, which takes care of
         * selecting the appropriate view.
         */
        if (g_strcmp0 (uri, "other-locations:///") != 0)
        {
            mautilus_application_open_location_full (MAUTILUS_APPLICATION (g_application_get_default ()),
                                                     location, 0, NULL, NULL, NULL);
        }
        else
        {
            g_set_object (&priv->location, location);
        }

        g_free (uri);
    }
}

static GList *
mautilus_places_view_get_selection (mautilusView *view)
{
    /* STUB */
    return NULL;
}

static void
mautilus_places_view_set_selection (mautilusView *view,
                                    GList        *selection)
{
    /* STUB */
}

static mautilusQuery *
mautilus_places_view_get_search_query (mautilusView *view)
{
    mautilusPlacesViewPrivate *priv;

    priv = mautilus_places_view_get_instance_private (MAUTILUS_PLACES_VIEW (view));

    return priv->search_query;
}

static void
mautilus_places_view_set_search_query (mautilusView  *view,
                                       mautilusQuery *query)
{
    mautilusPlacesViewPrivate *priv;
    gchar *text;

    priv = mautilus_places_view_get_instance_private (MAUTILUS_PLACES_VIEW (view));

    g_set_object (&priv->search_query, query);

    text = query ? mautilus_query_get_text (query) : NULL;

    mautilus_gtk_places_view_set_search_query (MAUTILUS_GTK_PLACES_VIEW (priv->places_view), text);

    g_free (text);
}

static mautilusToolbarMenuSections *
mautilus_places_view_get_toolbar_menu_sections (mautilusView *view)
{
    mautilusPlacesViewPrivate *priv;

    priv = mautilus_places_view_get_instance_private (MAUTILUS_PLACES_VIEW (view));

    return priv->toolbar_menu_sections;
}

static gboolean
mautilus_places_view_is_loading (mautilusView *view)
{
    mautilusPlacesViewPrivate *priv;

    priv = mautilus_places_view_get_instance_private (MAUTILUS_PLACES_VIEW (view));

    return mautilus_gtk_places_view_get_loading (MAUTILUS_GTK_PLACES_VIEW (priv->places_view));
}

static gboolean
mautilus_places_view_is_searching (mautilusView *view)
{
    mautilusPlacesViewPrivate *priv;

    priv = mautilus_places_view_get_instance_private (MAUTILUS_PLACES_VIEW (view));

    return priv->search_query != NULL;
}

static guint
mautilus_places_view_get_view_id (mautilusView *view)
{
    return MAUTILUS_VIEW_OTHER_LOCATIONS_ID;
}

static void
mautilus_places_view_iface_init (mautilusViewInterface *iface)
{
    iface->get_location = mautilus_places_view_get_location;
    iface->set_location = mautilus_places_view_set_location;
    iface->get_selection = mautilus_places_view_get_selection;
    iface->set_selection = mautilus_places_view_set_selection;
    iface->get_search_query = mautilus_places_view_get_search_query;
    iface->set_search_query = mautilus_places_view_set_search_query;
    iface->get_toolbar_menu_sections = mautilus_places_view_get_toolbar_menu_sections;
    iface->is_loading = mautilus_places_view_is_loading;
    iface->is_searching = mautilus_places_view_is_searching;
    iface->get_view_id = mautilus_places_view_get_view_id;
}

static void
mautilus_places_view_class_init (mautilusPlacesViewClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = mautilus_places_view_finalize;
    object_class->get_property = mautilus_places_view_get_property;
    object_class->set_property = mautilus_places_view_set_property;

    g_object_class_override_property (object_class, PROP_IS_LOADING, "is-loading");
    g_object_class_override_property (object_class, PROP_IS_SEARCHING, "is-searching");
    g_object_class_override_property (object_class, PROP_LOCATION, "location");
    g_object_class_override_property (object_class, PROP_SEARCH_QUERY, "search-query");
}

static void
mautilus_places_view_init (mautilusPlacesView *self)
{
    mautilusPlacesViewPrivate *priv;

    priv = mautilus_places_view_get_instance_private (self);

    /* Location */
    priv->location = g_file_new_for_uri ("other-locations:///");

    /* Places view */
    priv->places_view = mautilus_gtk_places_view_new ();
    mautilus_gtk_places_view_set_open_flags (MAUTILUS_GTK_PLACES_VIEW (priv->places_view),
                                             GTK_PLACES_OPEN_NEW_TAB | GTK_PLACES_OPEN_NEW_WINDOW | GTK_PLACES_OPEN_NORMAL);
    gtk_widget_set_hexpand (priv->places_view, TRUE);
    gtk_widget_set_vexpand (priv->places_view, TRUE);
    gtk_widget_show (priv->places_view);
    gtk_container_add (GTK_CONTAINER (self), priv->places_view);

    g_signal_connect_swapped (priv->places_view,
                              "notify::loading",
                              G_CALLBACK (loading_cb),
                              self);

    g_signal_connect_swapped (priv->places_view,
                              "open-location",
                              G_CALLBACK (open_location_cb),
                              self);

    g_signal_connect_swapped (priv->places_view,
                              "show-error-message",
                              G_CALLBACK (show_error_message_cb),
                              self);

    /* Toolbar menu */
    priv->toolbar_menu_sections = g_new0 (mautilusToolbarMenuSections, 1);
    priv->toolbar_menu_sections->supports_undo_redo = FALSE;
}

mautilusPlacesView *
mautilus_places_view_new (void)
{
    mautilusPlacesView *view;

    view = g_object_new (MAUTILUS_TYPE_PLACES_VIEW, NULL);
    if (g_object_is_floating (view))
    {
        g_object_ref_sink (view);
    }

    return view;
}
