/*
   mautilus-search-directory.h: Subclass of mautilusDirectory to implement
   a virtual directory consisting of the search directory and the search
   icons
 
   Copyright (C) 2005 Novell, Inc
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MAUTILUS_SEARCH_DIRECTORY_H
#define MAUTILUS_SEARCH_DIRECTORY_H

#include "mautilus-directory.h"
#include "mautilus-query.h"

#define MAUTILUS_SEARCH_DIRECTORY_PROVIDER_NAME "search-directory-provider"
#define MAUTILUS_TYPE_SEARCH_DIRECTORY (mautilus_search_directory_get_type ())

G_DECLARE_FINAL_TYPE (mautilusSearchDirectory, mautilus_search_directory,
                      MAUTILUS, SEARCH_DIRECTORY, mautilusDirectory)

char   *mautilus_search_directory_generate_new_uri     (void);

mautilusQuery *mautilus_search_directory_get_query       (mautilusSearchDirectory *self);
void           mautilus_search_directory_set_query       (mautilusSearchDirectory *self,
							  mautilusQuery           *query);

mautilusDirectory *
               mautilus_search_directory_get_base_model (mautilusSearchDirectory  *self);
void           mautilus_search_directory_set_base_model (mautilusSearchDirectory  *self,
							 mautilusDirectory        *base_model);

#endif /* MAUTILUS_SEARCH_DIRECTORY_H */
