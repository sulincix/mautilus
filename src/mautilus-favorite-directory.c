/* mautilus-favorite-directory.c
 *
 * Copyright (C) 2017 Alexandru Pandelea <alexandru.pandelea@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mautilus-favorite-directory.h"
#include "mautilus-tag-manager.h"
#include "mautilus-file-utilities.h"
#include "mautilus-directory-private.h"
#include <glib/gi18n.h>

struct _mautilusFavoriteDirectory {
    mautilusDirectory parent_slot;

    mautilusTagManager *tag_manager;
    GList *files;

    GList *monitor_list;
    GList *callback_list;
    GList *pending_callback_list;
};

typedef struct
{
    gboolean monitor_hidden_files;
    mautilusFileAttributes monitor_attributes;

    gconstpointer client;
} FavoriteMonitor;

typedef struct
{
    mautilusFavoriteDirectory *favorite_directory;

    mautilusDirectoryCallback callback;
    gpointer callback_data;

    mautilusFileAttributes wait_for_attributes;
    gboolean wait_for_file_list;
    GList *file_list;
} FavoriteCallback;

G_DEFINE_TYPE_WITH_CODE (mautilusFavoriteDirectory, mautilus_favorite_directory, MAUTILUS_TYPE_DIRECTORY,
                         mautilus_ensure_extension_points ();
                         g_io_extension_point_implement (MAUTILUS_DIRECTORY_PROVIDER_EXTENSION_POINT_NAME,
                                                         g_define_type_id,
                                                         MAUTILUS_FAVORITE_DIRECTORY_PROVIDER_NAME,
                                                         0));

static void
file_changed (mautilusFile              *file,
              mautilusFavoriteDirectory *favorite)
{
    GList list;

    list.data = file;
    list.next = NULL;

    mautilus_directory_emit_files_changed (MAUTILUS_DIRECTORY (favorite), &list);
}

static void
mautilus_favorite_directory_update_files (mautilusFavoriteDirectory *self)
{
    GList *l;
    GList *tmp_l;
    GList *new_favorite_files;
    GList *monitor_list;
    FavoriteMonitor *monitor;
    mautilusFile *file;
    GHashTable *uri_table;
    GList *files_added;
    GList *files_removed;
    gchar *uri;

    files_added = NULL;
    files_removed = NULL;

    uri_table = g_hash_table_new_full (g_str_hash,
                                       g_str_equal,
                                       (GDestroyNotify) g_free,
                                       NULL);

    for (l = self->files; l != NULL; l = l->next)
    {
        g_hash_table_add (uri_table, mautilus_file_get_uri (MAUTILUS_FILE (l->data)));
    }

    new_favorite_files = mautilus_tag_manager_get_favorite_files (self->tag_manager);

    for (l = new_favorite_files; l != NULL; l = l->next)
    {
        if (!g_hash_table_contains (uri_table, l->data))
        {
            file = mautilus_file_get_by_uri ((gchar*) l->data);

            for (monitor_list = self->monitor_list; monitor_list; monitor_list = monitor_list->next)
            {
                monitor = monitor_list->data;

                /* Add monitors */
                mautilus_file_monitor_add (file, monitor, monitor->monitor_attributes);
            }

            g_signal_connect (file, "changed", G_CALLBACK (file_changed), self);

            files_added = g_list_prepend (files_added, file);
        }
    }

    l = self->files;
    while (l != NULL)
    {
        uri = mautilus_file_get_uri (MAUTILUS_FILE (l->data));

        if (!mautilus_tag_manager_file_is_favorite (self->tag_manager, uri))
        {
            files_removed = g_list_prepend (files_removed,
                                            mautilus_file_ref (MAUTILUS_FILE (l->data)));

            g_signal_handlers_disconnect_by_func (MAUTILUS_FILE (l->data),
                                                  file_changed,
                                                  self);

            /* Remove monitors */
            for (monitor_list = self->monitor_list; monitor_list;
                 monitor_list = monitor_list->next)
            {
                monitor = monitor_list->data;
                mautilus_file_monitor_remove (MAUTILUS_FILE (l->data), monitor);
            }

            if (l == self->files)
            {
                self->files = g_list_delete_link (self->files, l);
                l = self->files;
            }
            else
            {
                tmp_l = l->prev;
                self->files = g_list_delete_link (self->files, l);
                l = tmp_l->next;
            }
        }
        else
        {
            l = l->next;
        }

        g_free (uri);
    }

    if (files_added)
    {
        mautilus_directory_emit_files_added (MAUTILUS_DIRECTORY (self), files_added);

        for (l = files_added; l != NULL; l = l->next)
        {
            self->files = g_list_prepend (self->files, mautilus_file_ref (MAUTILUS_FILE (l->data)));
        }
    }

    if (files_removed)
    {
        mautilus_directory_emit_files_changed (MAUTILUS_DIRECTORY (self), files_removed);
    }

    mautilus_file_list_free (files_added);
    mautilus_file_list_free (files_removed);
    g_hash_table_destroy (uri_table);
}

static void
on_favorites_files_changed (mautilusTagManager        *tag_manager,
                            GList                     *changed_files,
                            gpointer                   user_data)
{
    mautilusFavoriteDirectory *self;

    self = MAUTILUS_FAVORITE_DIRECTORY (user_data);

    mautilus_favorite_directory_update_files (self);
}

static gboolean
real_contains_file (mautilusDirectory *directory,
                    mautilusFile      *file)
{
    mautilusFavoriteDirectory *self;
    g_autofree gchar *uri = NULL;

    self = MAUTILUS_FAVORITE_DIRECTORY (directory);

    uri = mautilus_file_get_uri (file);

    return mautilus_tag_manager_file_is_favorite (self->tag_manager, uri);
}

static gboolean
real_is_editable (mautilusDirectory *directory)
{
    return FALSE;
}

static void
real_force_reload (mautilusDirectory *directory)
{
    mautilus_favorite_directory_update_files (MAUTILUS_FAVORITE_DIRECTORY (directory));
}

static void
real_call_when_ready (mautilusDirectory         *directory,
                      mautilusFileAttributes     file_attributes,
                      gboolean                   wait_for_file_list,
                      mautilusDirectoryCallback  callback,
                      gpointer                   callback_data)
{
    GList *file_list;
    mautilusFavoriteDirectory *favorite;

    favorite = MAUTILUS_FAVORITE_DIRECTORY (directory);

    file_list = mautilus_file_list_copy (favorite->files);

    callback (MAUTILUS_DIRECTORY (directory),
                                  file_list,
                                  callback_data);
}

static gboolean
real_are_all_files_seen (mautilusDirectory *directory)
{
    return TRUE;
}

static void
real_file_monitor_add (mautilusDirectory         *directory,
                       gconstpointer              client,
                       gboolean                   monitor_hidden_files,
                       mautilusFileAttributes     file_attributes,
                       mautilusDirectoryCallback  callback,
                       gpointer                   callback_data)
{
    GList *list;
    FavoriteMonitor *monitor;
    mautilusFavoriteDirectory *favorite;
    mautilusFile *file;

    favorite = MAUTILUS_FAVORITE_DIRECTORY (directory);

    monitor = g_new0 (FavoriteMonitor, 1);
    monitor->monitor_hidden_files = monitor_hidden_files;
    monitor->monitor_attributes = file_attributes;
    monitor->client = client;

    favorite->monitor_list = g_list_prepend (favorite->monitor_list, monitor);

    if (callback != NULL)
    {
        (*callback) (directory, favorite->files, callback_data);
    }

    for (list = favorite->files; list != NULL; list = list->next)
    {
        file = list->data;

        /* Add monitors */
        mautilus_file_monitor_add (file, monitor, file_attributes);
    }
}

static void
favorite_monitor_destroy (FavoriteMonitor           *monitor,
                          mautilusFavoriteDirectory *favorite)
{
    GList *l;
    mautilusFile *file;

    for (l = favorite->files; l != NULL; l = l->next)
    {
        file = l->data;

        mautilus_file_monitor_remove (file, monitor);
    }

    g_free (monitor);
}

static void
real_monitor_remove (mautilusDirectory *directory,
                     gconstpointer      client)
{
    mautilusFavoriteDirectory *favorite;
    FavoriteMonitor *monitor;
    GList *list;

    favorite = MAUTILUS_FAVORITE_DIRECTORY (directory);

    for (list = favorite->monitor_list; list != NULL; list = list->next)
    {
        monitor = list->data;

        if (monitor->client != client)
            continue;

        favorite->monitor_list = g_list_delete_link (favorite->monitor_list, list);

        favorite_monitor_destroy (monitor, favorite);

        break;
    }
}

static gboolean
real_handles_location (GFile *location)
{
    g_autofree gchar *uri = NULL;

    uri = g_file_get_uri (location);

    if (eel_uri_is_favorites (uri))
    {
        return TRUE;
    }

    return FALSE;
}

static FavoriteCallback*
favorite_callback_find_pending (mautilusFavoriteDirectory *favorite,
                                mautilusDirectoryCallback  callback,
                                gpointer                   callback_data)
{
    FavoriteCallback *favorite_callback;
    GList *list;

    for (list = favorite->pending_callback_list; list != NULL; list = list->next)
    {
        favorite_callback = list->data;

        if (favorite_callback->callback == callback &&
            favorite_callback->callback_data == callback_data)
        {
            return favorite_callback;
        }
    }

    return NULL;
}

static FavoriteCallback*
favorite_callback_find (mautilusFavoriteDirectory *favorite,
                        mautilusDirectoryCallback  callback,
                        gpointer                   callback_data)
{
    FavoriteCallback *favorite_callback;
    GList *list;

    for (list = favorite->callback_list; list != NULL; list = list->next)
    {
        favorite_callback = list->data;

        if (favorite_callback->callback == callback &&
            favorite_callback->callback_data == callback_data)
        {
            return favorite_callback;
        }
    }

    return NULL;
}

static void
favorite_callback_destroy (FavoriteCallback *favorite_callback)
{
    mautilus_file_list_free (favorite_callback->file_list);

    g_free (favorite_callback);
}

static void
real_cancel_callback (mautilusDirectory        *directory,
                      mautilusDirectoryCallback callback,
                      gpointer                  callback_data)
{
    mautilusFavoriteDirectory *favorite;
    FavoriteCallback *favorite_callback;

    favorite = MAUTILUS_FAVORITE_DIRECTORY (directory);
    favorite_callback = favorite_callback_find (favorite, callback, callback_data);

    if (favorite_callback)
    {
        favorite->callback_list = g_list_remove (favorite->callback_list, favorite_callback);

        favorite_callback_destroy (favorite_callback);

        return;
    }

    /* Check for a pending callback */
    favorite_callback = favorite_callback_find_pending (favorite, callback, callback_data);

    if (favorite_callback)
    {
        favorite->pending_callback_list = g_list_remove (favorite->pending_callback_list, favorite_callback);

        favorite_callback_destroy (favorite_callback);
    }
}

static GList*
real_get_file_list (mautilusDirectory *directory)
{
    mautilusFavoriteDirectory *favorite;

    favorite = MAUTILUS_FAVORITE_DIRECTORY (directory);

    return mautilus_file_list_copy (favorite->files);
}

static void
mautilus_favorite_directory_set_files (mautilusFavoriteDirectory *self)
{
    GList *favorite_files;
    mautilusFile *file;
    GList *l;
    GList *file_list;
    FavoriteMonitor *monitor;
    GList *monitor_list;

    file_list = NULL;

    favorite_files = mautilus_tag_manager_get_favorite_files (self->tag_manager);

    for (l = favorite_files; l != NULL; l = l->next)
    {
        file = mautilus_file_get_by_uri ((gchar*) l->data);

        g_signal_connect (file, "changed", G_CALLBACK (file_changed), self);

        for (monitor_list = self->monitor_list; monitor_list; monitor_list = monitor_list->next)
        {
            monitor = monitor_list->data;

            /* Add monitors */
            mautilus_file_monitor_add (file, monitor, monitor->monitor_attributes);
        }

        file_list = g_list_prepend (file_list, file);
    }

    mautilus_directory_emit_files_added (MAUTILUS_DIRECTORY (self), file_list);

    self->files = file_list;
}

static void
mautilus_favorite_directory_finalize (GObject *object)
{
    mautilusFavoriteDirectory *self;

    self = MAUTILUS_FAVORITE_DIRECTORY (object);

    g_signal_handlers_disconnect_by_func (self->tag_manager,
                                          on_favorites_files_changed,
                                          self);

    g_object_unref (self->tag_manager);
    mautilus_file_list_free (self->files);

    G_OBJECT_CLASS (mautilus_favorite_directory_parent_class)->finalize (object);
}

static void
mautilus_favorite_directory_dispose (GObject *object)
{
    mautilusFavoriteDirectory *favorite;
    GList *l;
    GList *monitor_list;
    FavoriteMonitor *monitor;
    mautilusFile *file;

    favorite = MAUTILUS_FAVORITE_DIRECTORY (object);

    /* Remove file connections */
    for (l = favorite->files; l != NULL; l = l->next)
    {
        file = l->data;

        /* Disconnect change handler */
        g_signal_handlers_disconnect_by_func (file, file_changed, favorite);

        /* Remove monitors */
        for (monitor_list = favorite->monitor_list; monitor_list;
             monitor_list = monitor_list->next)
        {
            monitor = monitor_list->data;
            mautilus_file_monitor_remove (file, monitor);
        }
    }

    /* Remove search monitors */
    if (favorite->monitor_list)
    {
        for (l = favorite->monitor_list; l != NULL; l = l->next)
        {
            favorite_monitor_destroy ((FavoriteMonitor*) l->data, favorite);
        }

        g_list_free (favorite->monitor_list);
        favorite->monitor_list = NULL;
    }

    G_OBJECT_CLASS (mautilus_favorite_directory_parent_class)->dispose (object);
}

static void
mautilus_favorite_directory_class_init (mautilusFavoriteDirectoryClass *klass)
{
    GObjectClass *oclass;
    mautilusDirectoryClass *directory_class;

    oclass = G_OBJECT_CLASS (klass);
    directory_class = MAUTILUS_DIRECTORY_CLASS (klass);

    oclass->finalize = mautilus_favorite_directory_finalize;
    oclass->dispose = mautilus_favorite_directory_dispose;

    directory_class->handles_location = real_handles_location;
    directory_class->contains_file = real_contains_file;
    directory_class->is_editable = real_is_editable;
    directory_class->force_reload = real_force_reload;
    directory_class->call_when_ready = real_call_when_ready;
    directory_class->are_all_files_seen = real_are_all_files_seen;
    directory_class->file_monitor_add = real_file_monitor_add;
    directory_class->file_monitor_remove = real_monitor_remove;
    directory_class->cancel_callback = real_cancel_callback;
    directory_class->get_file_list = real_get_file_list;
}

mautilusFavoriteDirectory*
mautilus_favorite_directory_new ()
{
    mautilusFavoriteDirectory *self;

    self = g_object_new (MAUTILUS_TYPE_FAVORITE_DIRECTORY, NULL);

    return self;
}

static void
mautilus_favorite_directory_init (mautilusFavoriteDirectory *self)
{
    mautilusTagManager *tag_manager;

    tag_manager = mautilus_tag_manager_get ();

    g_signal_connect (tag_manager,
                      "favorites-changed",
                      (GCallback) on_favorites_files_changed,
                      self);

    self->tag_manager = tag_manager;

    mautilus_favorite_directory_set_files (self);

}
