/*
 * Copyright (C) 2005 Red Hat, Inc
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Author: Alexander Larsson <alexl@redhat.com>
 *
 */

#include <config.h>
#include "mautilus-search-hit.h"
#include "mautilus-search-provider.h"
#include "mautilus-search-engine-model.h"
#include "mautilus-directory.h"
#include "mautilus-directory-private.h"
#include "mautilus-file.h"
#include "mautilus-ui-utilities.h"
#include "mautilus-tag-manager.h"
#define DEBUG_FLAG MAUTILUS_DEBUG_SEARCH
#include "mautilus-debug.h"

#include <string.h>
#include <glib.h>
#include <gio/gio.h>

struct _mautilusSearchEngineModel
{
    GObject parent;
    
    mautilusQuery *query;

    GList *hits;
    mautilusDirectory *directory;

    gboolean query_pending;
    guint finished_id;
};

enum
{
    PROP_0,
    PROP_RUNNING,
    LAST_PROP
};

static void mautilus_search_provider_init (mautilusSearchProviderInterface *iface);

G_DEFINE_TYPE_WITH_CODE (mautilusSearchEngineModel,
                         mautilus_search_engine_model,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (MAUTILUS_TYPE_SEARCH_PROVIDER,
                                                mautilus_search_provider_init))

static void
finalize (GObject *object)
{
    mautilusSearchEngineModel *model;

    model = MAUTILUS_SEARCH_ENGINE_MODEL (object);

    if (model->hits != NULL)
    {
        g_list_free_full (model->hits, g_object_unref);
        model->hits = NULL;
    }

    if (model->finished_id != 0)
    {
        g_source_remove (model->finished_id);
        model->finished_id = 0;
    }

    g_clear_object (&model->directory);
    g_clear_object (&model->query);

    G_OBJECT_CLASS (mautilus_search_engine_model_parent_class)->finalize (object);
}

static gboolean
search_finished (mautilusSearchEngineModel *model)
{
    model->finished_id = 0;

    if (model->hits != NULL)
    {
        DEBUG ("Model engine hits added");
        mautilus_search_provider_hits_added (MAUTILUS_SEARCH_PROVIDER (model),
                                             model->hits);
        g_list_free_full (model->hits, g_object_unref);
        model->hits = NULL;
    }

    model->query_pending = FALSE;

    g_object_notify (G_OBJECT (model), "running");

    DEBUG ("Model engine finished");
    mautilus_search_provider_finished (MAUTILUS_SEARCH_PROVIDER (model),
                                       MAUTILUS_SEARCH_PROVIDER_STATUS_NORMAL);
    g_object_unref (model);

    return FALSE;
}

static void
search_finished_idle (mautilusSearchEngineModel *model)
{
    if (model->finished_id != 0)
    {
        return;
    }

    model->finished_id = g_idle_add ((GSourceFunc) search_finished, model);
}

static void
model_directory_ready_cb (mautilusDirectory *directory,
                          GList             *list,
                          gpointer           user_data)
{
    mautilusSearchEngineModel *model = user_data;
    gchar *uri, *display_name;
    GList *files, *hits, *mime_types, *l, *m;
    mautilusFile *file;
    gdouble match;
    gboolean found;
    mautilusSearchHit *hit;
    GDateTime *initial_date;
    GDateTime *end_date;
    GPtrArray *date_range;
    mautilusTagManager *tag_manager;

    files = mautilus_directory_get_file_list (directory);
    mime_types = mautilus_query_get_mime_types (model->query);
    hits = NULL;

    for (l = files; l != NULL; l = l->next)
    {
        file = l->data;

        display_name = mautilus_file_get_display_name (file);
        match = mautilus_query_matches_string (model->query, display_name);
        found = (match > -1);

        if (found && mime_types)
        {
            found = FALSE;

            for (m = mime_types; m != NULL; m = m->next)
            {
                if (mautilus_file_is_mime_type (file, m->data))
                {
                    found = TRUE;
                    break;
                }
            }
        }

        date_range = mautilus_query_get_date_range (model->query);
        if (found && date_range != NULL)
        {
            mautilusQuerySearchType type;
            guint64 current_file_unix_time;

            type = mautilus_query_get_search_type (model->query);
            initial_date = g_ptr_array_index (date_range, 0);
            end_date = g_ptr_array_index (date_range, 1);

            if (type == MAUTILUS_QUERY_SEARCH_TYPE_LAST_ACCESS)
            {
                current_file_unix_time = mautilus_file_get_atime (file);
            }
            else
            {
                current_file_unix_time = mautilus_file_get_mtime (file);
            }

            found = mautilus_file_date_in_between (current_file_unix_time,
                                                   initial_date,
                                                   end_date);
            g_ptr_array_unref (date_range);
        }

        if (mautilus_query_get_search_favorite (model->query))
        {
            tag_manager = mautilus_tag_manager_get ();

            uri = mautilus_file_get_uri (file);

            if (!mautilus_tag_manager_file_is_favorite (tag_manager, uri))
            {
                found = FALSE;
            }

            g_free (uri);
        }

        if (found)
        {
            uri = mautilus_file_get_uri (file);
            hit = mautilus_search_hit_new (uri);
            mautilus_search_hit_set_fts_rank (hit, match);
            hits = g_list_prepend (hits, hit);

            g_free (uri);
        }

        g_free (display_name);
    }

    g_list_free_full (mime_types, g_free);
    mautilus_file_list_free (files);
    model->hits = hits;

    search_finished (model);
}

static void
mautilus_search_engine_model_start (mautilusSearchProvider *provider)
{
    mautilusSearchEngineModel *model;

    model = MAUTILUS_SEARCH_ENGINE_MODEL (provider);

    if (model->query_pending)
    {
        return;
    }

    DEBUG ("Model engine start");

    g_object_ref (model);
    model->query_pending = TRUE;

    g_object_notify (G_OBJECT (provider), "running");

    if (model->directory == NULL)
    {
        search_finished_idle (model);
        return;
    }

    mautilus_directory_call_when_ready (model->directory,
                                        MAUTILUS_FILE_ATTRIBUTE_INFO,
                                        TRUE, model_directory_ready_cb, model);
}

static void
mautilus_search_engine_model_stop (mautilusSearchProvider *provider)
{
    mautilusSearchEngineModel *model;

    model = MAUTILUS_SEARCH_ENGINE_MODEL (provider);

    if (model->query_pending)
    {
        DEBUG ("Model engine stop");

        mautilus_directory_cancel_callback (model->directory,
                                            model_directory_ready_cb, model);
        search_finished_idle (model);
    }

    g_clear_object (&model->directory);
}

static void
mautilus_search_engine_model_set_query (mautilusSearchProvider *provider,
                                        mautilusQuery          *query)
{
    mautilusSearchEngineModel *model;

    model = MAUTILUS_SEARCH_ENGINE_MODEL (provider);

    g_object_ref (query);
    g_clear_object (&model->query);
    model->query = query;
}

static gboolean
mautilus_search_engine_model_is_running (mautilusSearchProvider *provider)
{
    mautilusSearchEngineModel *model;

    model = MAUTILUS_SEARCH_ENGINE_MODEL (provider);

    return model->query_pending;
}

static void
mautilus_search_provider_init (mautilusSearchProviderInterface *iface)
{
    iface->set_query = mautilus_search_engine_model_set_query;
    iface->start = mautilus_search_engine_model_start;
    iface->stop = mautilus_search_engine_model_stop;
    iface->is_running = mautilus_search_engine_model_is_running;
}

static void
mautilus_search_engine_model_get_property (GObject    *object,
                                           guint       prop_id,
                                           GValue     *value,
                                           GParamSpec *pspec)
{
    mautilusSearchProvider *self = MAUTILUS_SEARCH_PROVIDER (object);

    switch (prop_id)
    {
        case PROP_RUNNING:
        {
            g_value_set_boolean (value, mautilus_search_engine_model_is_running (self));
        }
        break;

        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
mautilus_search_engine_model_class_init (mautilusSearchEngineModelClass *class)
{
    GObjectClass *gobject_class;

    gobject_class = G_OBJECT_CLASS (class);
    gobject_class->finalize = finalize;
    gobject_class->get_property = mautilus_search_engine_model_get_property;

    /**
     * mautilusSearchEngine::running:
     *
     * Whether the search engine is running a search.
     */
    g_object_class_override_property (gobject_class, PROP_RUNNING, "running");

}

static void
mautilus_search_engine_model_init (mautilusSearchEngineModel *engine)
{
}

mautilusSearchEngineModel *
mautilus_search_engine_model_new (void)
{
    mautilusSearchEngineModel *engine;

    engine = g_object_new (MAUTILUS_TYPE_SEARCH_ENGINE_MODEL, NULL);

    return engine;
}

void
mautilus_search_engine_model_set_model (mautilusSearchEngineModel *model,
                                        mautilusDirectory         *directory)
{
    g_clear_object (&model->directory);
    model->directory = mautilus_directory_ref (directory);
}

mautilusDirectory *
mautilus_search_engine_model_get_model (mautilusSearchEngineModel *model)
{
    return model->directory;
}
