#ifndef MAUTILUS_ICON_NAMES_H
#define MAUTILUS_ICON_NAMES_H

/* Icons for places */
#define MAUTILUS_ICON_FILESYSTEM	"drive-harddisk-symbolic"
#define MAUTILUS_ICON_FOLDER		"folder-symbolic"
#define MAUTILUS_ICON_FOLDER_REMOTE	"folder-remote-symbolic"
#define MAUTILUS_ICON_HOME		"user-home-symbolic"
#define MAUTILUS_ICON_NETWORK		"network-workgroup-symbolic"
#define MAUTILUS_ICON_NETWORK_SERVER	"network-server-symbolic"
#define MAUTILUS_ICON_SEARCH		"system-search"
#define MAUTILUS_ICON_TRASH		"user-trash-symbolic"
#define MAUTILUS_ICON_TRASH_FULL	"user-trash-full-symbolic"
#define MAUTILUS_ICON_DELETE		"edit-delete-symbolic"

#define MAUTILUS_ICON_FOLDER_DOCUMENTS  "folder-documents-symbolic"
#define MAUTILUS_ICON_FOLDER_DOWNLOAD   "folder-download-symbolic"
#define MAUTILUS_ICON_FOLDER_MUSIC      "folder-music-symbolic"
#define MAUTILUS_ICON_FOLDER_PICTURES   "folder-pictures-symbolic"
#define MAUTILUS_ICON_FOLDER_PUBLIC_SHARE "folder-publicshare-symbolic"
#define MAUTILUS_ICON_FOLDER_TEMPLATES  "folder-templates-symbolic"
#define MAUTILUS_ICON_FOLDER_VIDEOS     "folder-videos-symbolic"

/* Icons for desktop */
#define MAUTILUS_DESKTOP_ICON_DESKTOP    "user-desktop"
#define MAUTILUS_DESKTOP_ICON_TRASH      "user-trash"
#define MAUTILUS_DESKTOP_ICON_TRASH_FULL "user-trash-full"
#define MAUTILUS_DESKTOP_ICON_HOME       "user-home"
#define MAUTILUS_DESKTOP_ICON_NETWORK    "network-workgroup"

/* Fullcolor icons */
#define MAUTILUS_ICON_FULLCOLOR_FOLDER              "folder"
#define MAUTILUS_ICON_FULLCOLOR_FOLDER_REMOTE       "folder-remote"
#define MAUTILUS_ICON_FULLCOLOR_FOLDER_DOCUMENTS    "folder-documents"
#define MAUTILUS_ICON_FULLCOLOR_FOLDER_DOWNLOAD     "folder-download"
#define MAUTILUS_ICON_FULLCOLOR_FOLDER_MUSIC        "folder-music"
#define MAUTILUS_ICON_FULLCOLOR_FOLDER_PICTURES     "folder-pictures"
#define MAUTILUS_ICON_FULLCOLOR_FOLDER_PUBLIC_SHARE "folder-publicshare"
#define MAUTILUS_ICON_FULLCOLOR_FOLDER_TEMPLATES    "folder-templates"
#define MAUTILUS_ICON_FULLCOLOR_FOLDER_VIDEOS       "folder-videos"

/* Other icons */
#define MAUTILUS_ICON_TEMPLATE		"text-x-generic-template"

/* Icons not provided by fd.o naming spec or mautilus itself */
#define MAUTILUS_ICON_BURN		"mautilus-cd-burner"

#endif /* MAUTILUS_ICON_NAMES_H */

