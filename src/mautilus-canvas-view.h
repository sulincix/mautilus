/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */

/* mautilus-canvas-view.h - interface for canvas view of directory.
 *
 * Copyright (C) 2000 Eazel, Inc.
 *
 * The Gnome Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * The Gnome Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with the Gnome Library; see the file COPYING.LIB.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Authors: John Sullivan <sullivan@eazel.com>
 *
 */

#ifndef MAUTILUS_CANVAS_VIEW_H
#define MAUTILUS_CANVAS_VIEW_H

#include "mautilus-files-view.h"
#include "mautilus-canvas-container.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_CANVAS_VIEW mautilus_canvas_view_get_type()

G_DECLARE_DERIVABLE_TYPE (mautilusCanvasView, mautilus_canvas_view, MAUTILUS, CANVAS_VIEW, mautilusFilesView)

struct _mautilusCanvasViewClass {
	mautilusFilesViewClass parent_class;

        mautilusCanvasContainer * (* create_canvas_container) (mautilusCanvasView *canvas_view);
};

int     mautilus_canvas_view_compare_files (mautilusCanvasView   *canvas_view,
					  mautilusFile *a,
					  mautilusFile *b);
void    mautilus_canvas_view_filter_by_screen (mautilusCanvasView *canvas_view,
					     gboolean filter);
void    mautilus_canvas_view_clean_up_by_name (mautilusCanvasView *canvas_view);

mautilusFilesView * mautilus_canvas_view_new (mautilusWindowSlot *slot);

mautilusCanvasContainer * mautilus_canvas_view_get_canvas_container (mautilusCanvasView *view);

G_END_DECLS

#endif /* MAUTILUS_CANVAS_VIEW_H */
