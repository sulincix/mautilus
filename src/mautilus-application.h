/*
 * mautilus-application: main mautilus application class.
 *
 * Copyright (C) 2000 Red Hat, Inc.
 * Copyright (C) 2010 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __MAUTILUS_APPLICATION_H__
#define __MAUTILUS_APPLICATION_H__

#include <gdk/gdk.h>
#include <gio/gio.h>
#include <gtk/gtk.h>

#include "mautilus-bookmark-list.h"
#include "mautilus-window.h"

G_BEGIN_DECLS
#define MAUTILUS_TYPE_APPLICATION (mautilus_application_get_type())
G_DECLARE_DERIVABLE_TYPE (mautilusApplication, mautilus_application, MAUTILUS, APPLICATION, GtkApplication)

struct _mautilusApplicationClass {
	GtkApplicationClass parent_class;

        void  (*open_location_full) (mautilusApplication     *application,
                                     GFile                   *location,
                                     mautilusWindowOpenFlags  flags,
                                     GList                   *selection,
                                     mautilusWindow          *target_window,
                                     mautilusWindowSlot      *target_slot);
};

mautilusApplication * mautilus_application_new (void);

mautilusWindow *     mautilus_application_create_window (mautilusApplication *application,
							 GdkScreen           *screen);

void mautilus_application_set_accelerator (GApplication *app,
					   const gchar  *action_name,
					   const gchar  *accel);

void mautilus_application_set_accelerators (GApplication *app,
					    const gchar  *action_name,
					    const gchar **accels);

GList * mautilus_application_get_windows (mautilusApplication *application);

void mautilus_application_open_location (mautilusApplication *application,
					 GFile *location,
					 GFile *selection,
					 const char *startup_id);

void mautilus_application_open_location_full (mautilusApplication     *application,
                                              GFile                   *location,
                                              mautilusWindowOpenFlags  flags,
                                              GList                   *selection,
                                              mautilusWindow          *target_window,
                                              mautilusWindowSlot      *target_slot);

mautilusApplication *mautilus_application_get_default (void);
void mautilus_application_send_notification (mautilusApplication *self,
                                             const gchar         *notification_id,
                                             GNotification       *notification);
void mautilus_application_withdraw_notification (mautilusApplication *self,
                                                 const gchar         *notification_id);

mautilusBookmarkList *
     mautilus_application_get_bookmarks  (mautilusApplication *application);
void mautilus_application_edit_bookmarks (mautilusApplication *application,
					  mautilusWindow      *window);

GtkWidget * mautilus_application_connect_server (mautilusApplication *application,
						 mautilusWindow      *window);

void mautilus_application_search (mautilusApplication *application,
                                  const gchar         *uri,
                                  const gchar         *text);
void mautilus_application_startup_common (mautilusApplication *application);
G_END_DECLS

#endif /* __MAUTILUS_APPLICATION_H__ */
