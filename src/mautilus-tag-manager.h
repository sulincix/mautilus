/* mautilus-tag-manager.h
 *
 * Copyright (C) 2017 Alexandru Pandelea <alexandru.pandelea@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAUTILUS_TAG_MANAGER_H
#define MAUTILUS_TAG_MANAGER_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define MAUTILUS_TYPE_TAG_MANAGER (mautilus_tag_manager_get_type ())

G_DECLARE_FINAL_TYPE (mautilusTagManager, mautilus_tag_manager, MAUTILUS, TAG_MANAGER, GObject);

mautilusTagManager* mautilus_tag_manager_get                ();

void                mautilus_tag_manager_set_cancellable    (mautilusTagManager *tag_manager,
                                                             GCancellable *cancellable);

GList*              mautilus_tag_manager_get_favorite_files (mautilusTagManager *self);

void                mautilus_tag_manager_star_files         (mautilusTagManager  *self,
                                                             GObject             *object,
                                                             GList               *selection,
                                                             GAsyncReadyCallback  callback,
                                                             GCancellable        *cancellable);

void                mautilus_tag_manager_unstar_files       (mautilusTagManager  *self,
                                                             GObject             *object,
                                                             GList               *selection,
                                                             GAsyncReadyCallback  callback,
                                                             GCancellable        *cancellable);


gboolean            mautilus_tag_manager_file_is_favorite   (mautilusTagManager *self,
                                                             const gchar        *file_name);

G_END_DECLS

#endif