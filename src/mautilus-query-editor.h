/*
 * Copyright (C) 2005 Red Hat, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Author: Alexander Larsson <alexl@redhat.com>
 *
 */

#ifndef MAUTILUS_QUERY_EDITOR_H
#define MAUTILUS_QUERY_EDITOR_H

#include <gtk/gtk.h>

#include "mautilus-query.h"

#define MAUTILUS_TYPE_QUERY_EDITOR mautilus_query_editor_get_type()

G_DECLARE_DERIVABLE_TYPE (mautilusQueryEditor, mautilus_query_editor, MAUTILUS, QUERY_EDITOR, GtkSearchBar)

struct _mautilusQueryEditorClass {
        GtkSearchBarClass parent_class;

	void (* changed)   (mautilusQueryEditor  *editor,
			    mautilusQuery        *query,
			    gboolean              reload);
	void (* cancel)    (mautilusQueryEditor *editor);
	void (* activated) (mautilusQueryEditor *editor);
};

#include "mautilus-window-slot.h"

GType      mautilus_query_editor_get_type     	   (void);
GtkWidget* mautilus_query_editor_new          	   (void);

mautilusQuery *mautilus_query_editor_get_query   (mautilusQueryEditor *editor);
void           mautilus_query_editor_set_query   (mautilusQueryEditor *editor,
						  mautilusQuery       *query);
GFile *        mautilus_query_editor_get_location (mautilusQueryEditor *editor);
void           mautilus_query_editor_set_location (mautilusQueryEditor *editor,
						   GFile               *location);
void           mautilus_query_editor_set_text     (mautilusQueryEditor *editor,
                                                   const gchar         *text);

#endif /* MAUTILUS_QUERY_EDITOR_H */
