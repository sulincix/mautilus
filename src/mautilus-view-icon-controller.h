#ifndef MAUTILUS_VIEW_ICON_CONTROLLER_H
#define MAUTILUS_VIEW_ICON_CONTROLLER_H

#include <glib.h>
#include <gtk/gtk.h>

#include "mautilus-files-view.h"
#include "mautilus-window-slot.h"
#include "mautilus-view-model.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_VIEW_ICON_CONTROLLER (mautilus_view_icon_controller_get_type())

G_DECLARE_FINAL_TYPE (mautilusViewIconController, mautilus_view_icon_controller, MAUTILUS, VIEW_ICON_CONTROLLER, mautilusFilesView)

mautilusViewIconController *mautilus_view_icon_controller_new (mautilusWindowSlot *slot);

mautilusViewModel * mautilus_view_icon_controller_get_model (mautilusViewIconController *self);

G_END_DECLS

#endif /* MAUTILUS_VIEW_ICON_CONTROLLER_H */

