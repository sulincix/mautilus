/*
   mautilus-directory.h: mautilus directory model.
 
   Copyright (C) 1999, 2000, 2001 Eazel, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Darin Adler <darin@bentspoon.com>
*/

#ifndef MAUTILUS_DIRECTORY_H
#define MAUTILUS_DIRECTORY_H

#include <gtk/gtk.h>
#include <gio/gio.h>
#include "mautilus-file-attributes.h"

/* mautilusDirectory is a class that manages the model for a directory,
   real or virtual, for mautilus, mainly the file-manager component. The directory is
   responsible for managing both real data and cached metadata. On top of
   the file system independence provided by gio, the directory
   object also provides:
  
       1) A synchronization framework, which notifies via signals as the
          set of known files changes.
       2) An abstract interface for getting attributes and performing
          operations on files.
*/

#define MAUTILUS_DIRECTORY_PROVIDER_EXTENSION_POINT_NAME "mautilus-directory-provider"

#define MAUTILUS_TYPE_DIRECTORY mautilus_directory_get_type()
#define MAUTILUS_DIRECTORY(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_DIRECTORY, mautilusDirectory))
#define MAUTILUS_DIRECTORY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_DIRECTORY, mautilusDirectoryClass))
#define MAUTILUS_IS_DIRECTORY(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_DIRECTORY))
#define MAUTILUS_IS_DIRECTORY_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_DIRECTORY))
#define MAUTILUS_DIRECTORY_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_DIRECTORY, mautilusDirectoryClass))

/* mautilusFile is defined both here and in mautilus-file.h. */
#ifndef MAUTILUS_FILE_DEFINED
#define MAUTILUS_FILE_DEFINED
typedef struct mautilusFile mautilusFile;
#endif

typedef struct mautilusDirectoryDetails mautilusDirectoryDetails;

typedef struct
{
	GObject object;
	mautilusDirectoryDetails *details;
} mautilusDirectory;

typedef void (*mautilusDirectoryCallback) (mautilusDirectory *directory,
					   GList             *files,
					   gpointer           callback_data);

typedef struct
{
	GObjectClass parent_class;

	/*** Notification signals for clients to connect to. ***/

	/* The files_added signal is emitted as the directory model 
	 * discovers new files.
	 */
	void     (* files_added)         (mautilusDirectory          *directory,
					  GList                      *added_files);

	/* The files_changed signal is emitted as changes occur to
	 * existing files that are noticed by the synchronization framework,
	 * including when an old file has been deleted. When an old file
	 * has been deleted, this is the last chance to forget about these
	 * file objects, which are about to be unref'd. Use a call to
	 * mautilus_file_is_gone () to test for this case.
	 */
	void     (* files_changed)       (mautilusDirectory         *directory,
					  GList                     *changed_files);

	/* The done_loading signal is emitted when a directory load
	 * request completes. This is needed because, at least in the
	 * case where the directory is empty, the caller will receive
	 * no kind of notification at all when a directory load
	 * initiated by `mautilus_directory_file_monitor_add' completes.
	 */
	void     (* done_loading)        (mautilusDirectory         *directory);

	void     (* load_error)          (mautilusDirectory         *directory,
					  GError                    *error);

	/*** Virtual functions for subclasses to override. ***/
	gboolean (* contains_file)       (mautilusDirectory         *directory,
					  mautilusFile              *file);
	void     (* call_when_ready)     (mautilusDirectory         *directory,
					  mautilusFileAttributes     file_attributes,
					  gboolean                   wait_for_file_list,
					  mautilusDirectoryCallback  callback,
					  gpointer                   callback_data);
	void     (* cancel_callback)     (mautilusDirectory         *directory,
					  mautilusDirectoryCallback  callback,
					  gpointer                   callback_data);
	void     (* file_monitor_add)    (mautilusDirectory          *directory,
					  gconstpointer              client,
					  gboolean                   monitor_hidden_files,
					  mautilusFileAttributes     monitor_attributes,
					  mautilusDirectoryCallback  initial_files_callback,
					  gpointer                   callback_data);
	void     (* file_monitor_remove) (mautilusDirectory         *directory,
					  gconstpointer              client);
	void     (* force_reload)        (mautilusDirectory         *directory);
	gboolean (* are_all_files_seen)  (mautilusDirectory         *directory);
	gboolean (* is_not_empty)        (mautilusDirectory         *directory);

	/* get_file_list is a function pointer that subclasses may override to
	 * customize collecting the list of files in a directory.
	 * For example, the mautilusDesktopDirectory overrides this so that it can
	 * merge together the list of files in the $HOME/Desktop directory with
	 * the list of standard icons (Home, Trash) on the desktop.
	 */
	GList *	 (* get_file_list)	 (mautilusDirectory *directory);

	/* Should return FALSE if the directory is read-only and doesn't
	 * allow setting of metadata.
	 * An example of this is the search directory.
	 */
	gboolean (* is_editable)         (mautilusDirectory *directory);

        /* Subclasses can use this to create custom files when asked by the user
         * or the mautilus cache. */
        mautilusFile * (* new_file_from_filename) (mautilusDirectory *directory,
                                                   const char        *filename,
                                                   gboolean           self_owned);
        /* Subclasses can say if they handle the location provided or should the
         * mautilus file class handle it.
         */
        gboolean       (* handles_location)       (GFile             *location);
} mautilusDirectoryClass;

/* Basic GObject requirements. */
GType              mautilus_directory_get_type                 (void);

/* Get a directory given a uri.
 * Creates the appropriate subclass given the uri mappings.
 * Returns a referenced object, not a floating one. Unref when finished.
 * If two windows are viewing the same uri, the directory object is shared.
 */
mautilusDirectory *mautilus_directory_get                      (GFile                     *location);
mautilusDirectory *mautilus_directory_get_by_uri               (const char                *uri);
mautilusDirectory *mautilus_directory_get_for_file             (mautilusFile              *file);

/* Covers for g_object_ref and g_object_unref that provide two conveniences:
 * 1) Using these is type safe.
 * 2) You are allowed to call these with NULL,
 */
mautilusDirectory *mautilus_directory_ref                      (mautilusDirectory         *directory);
void               mautilus_directory_unref                    (mautilusDirectory         *directory);

G_DEFINE_AUTOPTR_CLEANUP_FUNC (mautilusDirectory, mautilus_directory_unref)

/* Access to a URI. */
char *             mautilus_directory_get_uri                  (mautilusDirectory         *directory);
GFile *            mautilus_directory_get_location             (mautilusDirectory         *directory);

/* Is this file still alive and in this directory? */
gboolean           mautilus_directory_contains_file            (mautilusDirectory         *directory,
								mautilusFile              *file);

mautilusFile*           mautilus_directory_get_file_by_name            (mautilusDirectory *directory,
                                                                        const gchar       *name);
/* Get (and ref) a mautilusFile object for this directory. */
mautilusFile *     mautilus_directory_get_corresponding_file   (mautilusDirectory         *directory);

/* Waiting for data that's read asynchronously.
 * The file attribute and metadata keys are for files in the directory.
 */
void               mautilus_directory_call_when_ready          (mautilusDirectory         *directory,
								mautilusFileAttributes     file_attributes,
								gboolean                   wait_for_all_files,
								mautilusDirectoryCallback  callback,
								gpointer                   callback_data);
void               mautilus_directory_cancel_callback          (mautilusDirectory         *directory,
								mautilusDirectoryCallback  callback,
								gpointer                   callback_data);


/* Monitor the files in a directory. */
void               mautilus_directory_file_monitor_add         (mautilusDirectory         *directory,
								gconstpointer              client,
								gboolean                   monitor_hidden_files,
								mautilusFileAttributes     attributes,
								mautilusDirectoryCallback  initial_files_callback,
								gpointer                   callback_data);
void               mautilus_directory_file_monitor_remove      (mautilusDirectory         *directory,
								gconstpointer              client);
void               mautilus_directory_force_reload             (mautilusDirectory         *directory);

/* Get a list of all files currently known in the directory. */
GList *            mautilus_directory_get_file_list            (mautilusDirectory         *directory);

GList *            mautilus_directory_match_pattern            (mautilusDirectory         *directory,
							        const char *glob);


/* Return true if the directory has information about all the files.
 * This will be false until the directory has been read at least once.
 */
gboolean           mautilus_directory_are_all_files_seen       (mautilusDirectory         *directory);

/* Return true if the directory is local. */
gboolean           mautilus_directory_is_local                 (mautilusDirectory         *directory);
gboolean           mautilus_directory_is_local_or_fuse         (mautilusDirectory         *directory);

gboolean           mautilus_directory_is_in_trash              (mautilusDirectory         *directory);
gboolean           mautilus_directory_is_in_recent             (mautilusDirectory         *directory);
gboolean           mautilus_directory_is_in_starred            (mautilusDirectory         *directory);
gboolean           mautilus_directory_is_in_admin              (mautilusDirectory         *directory);

/* Return false if directory contains anything besides a mautilus metafile.
 * Only valid if directory is monitored. Used by the Trash monitor.
 */
gboolean           mautilus_directory_is_not_empty             (mautilusDirectory         *directory);

/* Convenience functions for dealing with a list of mautilusDirectory objects that each have a ref.
 * These are just convenient names for functions that work on lists of GtkObject *.
 */
GList *            mautilus_directory_list_ref                 (GList                     *directory_list);
void               mautilus_directory_list_unref               (GList                     *directory_list);
void               mautilus_directory_list_free                (GList                     *directory_list);
GList *            mautilus_directory_list_copy                (GList                     *directory_list);
GList *            mautilus_directory_list_sort_by_uri         (GList                     *directory_list);

gboolean           mautilus_directory_is_editable              (mautilusDirectory         *directory);

void               mautilus_directory_dump                     (mautilusDirectory         *directory);

mautilusFile *     mautilus_directory_new_file_from_filename   (mautilusDirectory *directory,
                                                                const char        *filename,
                                                                gboolean           self_owned);

#endif /* MAUTILUS_DIRECTORY_H */
