/*
   mautilus-metadata.h: #defines and other metadata-related info
 
   Copyright (C) 2000 Eazel, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: John Sullivan <sullivan@eazel.com>
*/

#ifndef MAUTILUS_METADATA_H
#define MAUTILUS_METADATA_H

/* Keys for getting/setting mautilus metadata. All metadata used in mautilus
 * should define its key here, so we can keep track of the whole set easily.
 * Any updates here needs to be added in mautilus-metadata.c too.
 */

#include <glib.h>

/* Per-file */

#define MAUTILUS_METADATA_KEY_LOCATION_BACKGROUND_COLOR 	"folder-background-color"
#define MAUTILUS_METADATA_KEY_LOCATION_BACKGROUND_IMAGE 	"folder-background-image"

#define MAUTILUS_METADATA_KEY_ICON_VIEW_AUTO_LAYOUT      	"mautilus-icon-view-auto-layout"
#define MAUTILUS_METADATA_KEY_ICON_VIEW_SORT_BY          	"mautilus-icon-view-sort-by"
#define MAUTILUS_METADATA_KEY_ICON_VIEW_SORT_REVERSED    	"mautilus-icon-view-sort-reversed"
#define MAUTILUS_METADATA_KEY_ICON_VIEW_KEEP_ALIGNED            "mautilus-icon-view-keep-aligned"
#define MAUTILUS_METADATA_KEY_ICON_VIEW_LAYOUT_TIMESTAMP	"mautilus-icon-view-layout-timestamp"

#define MAUTILUS_METADATA_KEY_DESKTOP_ICON_SIZE           "mautilus-desktop-icon-size"

#define MAUTILUS_METADATA_KEY_LIST_VIEW_SORT_COLUMN      	"mautilus-list-view-sort-column"
#define MAUTILUS_METADATA_KEY_LIST_VIEW_SORT_REVERSED    	"mautilus-list-view-sort-reversed"
#define MAUTILUS_METADATA_KEY_LIST_VIEW_VISIBLE_COLUMNS    	"mautilus-list-view-visible-columns"
#define MAUTILUS_METADATA_KEY_LIST_VIEW_COLUMN_ORDER    	"mautilus-list-view-column-order"

#define MAUTILUS_METADATA_KEY_WINDOW_GEOMETRY			"mautilus-window-geometry"
#define MAUTILUS_METADATA_KEY_WINDOW_SCROLL_POSITION		"mautilus-window-scroll-position"
#define MAUTILUS_METADATA_KEY_WINDOW_SHOW_HIDDEN_FILES		"mautilus-window-show-hidden-files"
#define MAUTILUS_METADATA_KEY_WINDOW_MAXIMIZED			"mautilus-window-maximized"
#define MAUTILUS_METADATA_KEY_WINDOW_STICKY			"mautilus-window-sticky"
#define MAUTILUS_METADATA_KEY_WINDOW_KEEP_ABOVE			"mautilus-window-keep-above"

#define MAUTILUS_METADATA_KEY_SIDEBAR_BACKGROUND_COLOR   	"mautilus-sidebar-background-color"
#define MAUTILUS_METADATA_KEY_SIDEBAR_BACKGROUND_IMAGE   	"mautilus-sidebar-background-image"
#define MAUTILUS_METADATA_KEY_SIDEBAR_BUTTONS			"mautilus-sidebar-buttons"

#define MAUTILUS_METADATA_KEY_ICON_POSITION              	"mautilus-icon-position"
#define MAUTILUS_METADATA_KEY_ICON_POSITION_TIMESTAMP		"mautilus-icon-position-timestamp"
#define MAUTILUS_METADATA_KEY_ANNOTATION                 	"annotation"
#define MAUTILUS_METADATA_KEY_ICON_SCALE                 	"icon-scale"
#define MAUTILUS_METADATA_KEY_CUSTOM_ICON                	"custom-icon"
#define MAUTILUS_METADATA_KEY_CUSTOM_ICON_NAME                	"custom-icon-name"
#define MAUTILUS_METADATA_KEY_SCREEN				"screen"
#define MAUTILUS_METADATA_KEY_EMBLEMS				"emblems"

#define MAUTILUS_METADATA_KEY_DESKTOP_FILE_TRUSTED				"trusted"

guint mautilus_metadata_get_id (const char *metadata);

#endif /* MAUTILUS_METADATA_H */
