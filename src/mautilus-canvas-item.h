
/* mautilus - Canvas item class for canvas container.
 *
 * Copyright (C) 2000 Eazel, Inc.
 *
 * Author: Andy Hertzfeld <andy@eazel.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAUTILUS_CANVAS_ITEM_H
#define MAUTILUS_CANVAS_ITEM_H

#include <eel/eel-canvas.h>
#include <eel/eel-art-extensions.h>

G_BEGIN_DECLS

#define MAUTILUS_TYPE_CANVAS_ITEM mautilus_canvas_item_get_type()
#define MAUTILUS_CANVAS_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_CANVAS_ITEM, mautilusCanvasItem))
#define MAUTILUS_CANVAS_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_CANVAS_ITEM, mautilusCanvasItemClass))
#define MAUTILUS_IS_CANVAS_ITEM(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_CANVAS_ITEM))
#define MAUTILUS_IS_CANVAS_ITEM_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_CANVAS_ITEM))
#define MAUTILUS_CANVAS_ITEM_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_CANVAS_ITEM, mautilusCanvasItemClass))

typedef struct mautilusCanvasItem mautilusCanvasItem;
typedef struct mautilusCanvasItemClass mautilusCanvasItemClass;
typedef struct mautilusCanvasItemDetails mautilusCanvasItemDetails;

struct mautilusCanvasItem {
	EelCanvasItem item;
	mautilusCanvasItemDetails *details;
	gpointer user_data;
};

struct mautilusCanvasItemClass {
	EelCanvasItemClass parent_class;
};

/* not namespaced due to their length */
typedef enum {
	BOUNDS_USAGE_FOR_LAYOUT,
	BOUNDS_USAGE_FOR_ENTIRE_ITEM,
	BOUNDS_USAGE_FOR_DISPLAY
} mautilusCanvasItemBoundsUsage;

/* GObject */
GType       mautilus_canvas_item_get_type                 (void);

/* attributes */
void        mautilus_canvas_item_set_image                (mautilusCanvasItem       *item,
							   GdkPixbuf                *image);
cairo_surface_t* mautilus_canvas_item_get_drag_surface    (mautilusCanvasItem       *item);
void        mautilus_canvas_item_set_emblems              (mautilusCanvasItem       *item,
							   GList                    *emblem_pixbufs);
void        mautilus_canvas_item_set_show_stretch_handles (mautilusCanvasItem       *item,
							   gboolean                  show_stretch_handles);

/* geometry and hit testing */
gboolean    mautilus_canvas_item_hit_test_rectangle       (mautilusCanvasItem       *item,
							   EelIRect                  canvas_rect);
gboolean    mautilus_canvas_item_hit_test_stretch_handles (mautilusCanvasItem       *item,
							   gdouble                   world_x,
							   gdouble                   world_y,
							   GtkCornerType            *corner);
void        mautilus_canvas_item_invalidate_label         (mautilusCanvasItem       *item);
void        mautilus_canvas_item_invalidate_label_size    (mautilusCanvasItem       *item);
EelDRect    mautilus_canvas_item_get_icon_rectangle     (const mautilusCanvasItem *item);
void        mautilus_canvas_item_get_bounds_for_layout    (mautilusCanvasItem       *item,
							   double *x1, double *y1, double *x2, double *y2);
void        mautilus_canvas_item_get_bounds_for_entire_item (mautilusCanvasItem       *item,
							     double *x1, double *y1, double *x2, double *y2);
void        mautilus_canvas_item_update_bounds            (mautilusCanvasItem       *item,
							   double i2w_dx, double i2w_dy);
void        mautilus_canvas_item_set_is_visible           (mautilusCanvasItem       *item,
							   gboolean                  visible);
/* whether the entire label text must be visible at all times */
void        mautilus_canvas_item_set_entire_text          (mautilusCanvasItem       *canvas_item,
							   gboolean                  entire_text);

G_END_DECLS

#endif /* MAUTILUS_CANVAS_ITEM_H */
