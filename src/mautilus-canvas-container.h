
/* gnome-canvas-container.h - Canvas container widget.

   Copyright (C) 1999, 2000 Free Software Foundation
   Copyright (C) 2000 Eazel, Inc.

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   see <http://www.gnu.org/licenses/>.

   Authors: Ettore Perazzoli <ettore@gnu.org>, Darin Adler <darin@bentspoon.com>
*/

#ifndef MAUTILUS_CANVAS_CONTAINER_H
#define MAUTILUS_CANVAS_CONTAINER_H

#include <eel/eel-canvas.h>
#include "mautilus-icon-info.h"

#define MAUTILUS_TYPE_CANVAS_CONTAINER mautilus_canvas_container_get_type()
#define MAUTILUS_CANVAS_CONTAINER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_CANVAS_CONTAINER, mautilusCanvasContainer))
#define MAUTILUS_CANVAS_CONTAINER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_CANVAS_CONTAINER, mautilusCanvasContainerClass))
#define MAUTILUS_IS_CANVAS_CONTAINER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_CANVAS_CONTAINER))
#define MAUTILUS_IS_CANVAS_CONTAINER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_CANVAS_CONTAINER))
#define MAUTILUS_CANVAS_CONTAINER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_CANVAS_CONTAINER, mautilusCanvasContainerClass))


#define MAUTILUS_CANVAS_ICON_DATA(pointer) \
	((mautilusCanvasIconData *) (pointer))

typedef struct mautilusCanvasIconData mautilusCanvasIconData;

typedef void (* mautilusCanvasCallback) (mautilusCanvasIconData *icon_data,
					 gpointer callback_data);

typedef struct {
	int x;
	int y;
	double scale;
} mautilusCanvasPosition;

#define	MAUTILUS_CANVAS_CONTAINER_TYPESELECT_FLUSH_DELAY 1000000

typedef struct mautilusCanvasContainerDetails mautilusCanvasContainerDetails;

typedef struct {
	EelCanvas canvas;
	mautilusCanvasContainerDetails *details;
} mautilusCanvasContainer;

typedef struct {
	EelCanvasClass parent_slot;

	/* Operations on the container. */
	int          (* button_press) 	          (mautilusCanvasContainer *container,
						   GdkEventButton *event);
	void         (* context_click_background) (mautilusCanvasContainer *container,
						   GdkEventButton *event);
	void         (* middle_click) 		  (mautilusCanvasContainer *container,
						   GdkEventButton *event);

	/* Operations on icons. */
	void         (* activate)	  	  (mautilusCanvasContainer *container,
						   mautilusCanvasIconData *data);
	void         (* activate_alternate)       (mautilusCanvasContainer *container,
						   mautilusCanvasIconData *data);
	void         (* activate_previewer)       (mautilusCanvasContainer *container,
						   GList *files,
						   GArray *locations);
	void         (* context_click_selection)  (mautilusCanvasContainer *container,
						   GdkEventButton *event);
	void	     (* move_copy_items)	  (mautilusCanvasContainer *container,
						   const GList *item_uris,
						   GdkPoint *relative_item_points,
						   const char *target_uri,
						   GdkDragAction action,
						   int x,
						   int y);
	void	     (* handle_netscape_url)	  (mautilusCanvasContainer *container,
						   const char *url,
						   const char *target_uri,
						   GdkDragAction action,
						   int x,
						   int y);
	void	     (* handle_uri_list)    	  (mautilusCanvasContainer *container,
						   const char *uri_list,
						   const char *target_uri,
						   GdkDragAction action,
						   int x,
						   int y);
	void	     (* handle_text)		  (mautilusCanvasContainer *container,
						   const char *text,
						   const char *target_uri,
						   GdkDragAction action,
						   int x,
						   int y);
	void	     (* handle_raw)		  (mautilusCanvasContainer *container,
						   char *raw_data,
						   int length,
						   const char *target_uri,
						   const char *direct_save_uri,
						   GdkDragAction action,
						   int x,
						   int y);
	void	     (* handle_hover)		  (mautilusCanvasContainer *container,
						   const char *target_uri);

	/* Queries on the container for subclass/client.
	 * These must be implemented. The default "do nothing" is not good enough.
	 */
	char *	     (* get_container_uri)	  (mautilusCanvasContainer *container);

	/* Queries on icons for subclass/client.
	 * These must be implemented. The default "do nothing" is not
	 * good enough, these are _not_ signals.
	 */
	mautilusIconInfo *(* get_icon_images)     (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *data,
						     int canvas_size,
						     gboolean for_drag_accept);
	void         (* get_icon_text)            (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *data,
						     char **editable_text,
						     char **additional_text,
						     gboolean include_invisible);
	char *       (* get_icon_description)     (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *data);
	int          (* compare_icons)            (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *canvas_a,
						     mautilusCanvasIconData *canvas_b);
	int          (* compare_icons_by_name)    (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *canvas_a,
						     mautilusCanvasIconData *canvas_b);
	void         (* prioritize_thumbnailing)  (mautilusCanvasContainer *container,
						   mautilusCanvasIconData *data);

	/* Queries on icons for subclass/client.
	 * These must be implemented => These are signals !
	 * The default "do nothing" is not good enough.
	 */
	gboolean     (* can_accept_item)	  (mautilusCanvasContainer *container,
						   mautilusCanvasIconData *target, 
						   const char *item_uri);
	gboolean     (* get_stored_icon_position) (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *data,
						     mautilusCanvasPosition *position);
	char *       (* get_icon_uri)             (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *data);
	char *       (* get_icon_activation_uri)  (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *data);
	char *       (* get_icon_drop_target_uri) (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *data);

	/* If canvas data is NULL, the layout timestamp of the container should be retrieved.
	 * That is the time when the container displayed a fully loaded directory with
	 * all canvas positions assigned.
	 *
	 * If canvas data is not NULL, the position timestamp of the canvas should be retrieved.
	 * That is the time when the file (i.e. canvas data payload) was last displayed in a
	 * fully loaded directory with all canvas positions assigned.
	 */
	gboolean     (* get_stored_layout_timestamp) (mautilusCanvasContainer *container,
						      mautilusCanvasIconData *data,
						      time_t *time);
	/* If canvas data is NULL, the layout timestamp of the container should be stored.
	 * If canvas data is not NULL, the position timestamp of the container should be stored.
	 */
	gboolean     (* store_layout_timestamp) (mautilusCanvasContainer *container,
						 mautilusCanvasIconData *data,
						 const time_t *time);

	/* Notifications for the whole container. */
	void	     (* band_select_started)	  (mautilusCanvasContainer *container);
	void	     (* band_select_ended)	  (mautilusCanvasContainer *container);
	void         (* selection_changed) 	  (mautilusCanvasContainer *container);
	void         (* layout_changed)           (mautilusCanvasContainer *container);

	/* Notifications for icons. */
	void         (* icon_position_changed)    (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *data,
						     const mautilusCanvasPosition *position);
	void	     (* icon_stretch_started)     (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *data);
	void	     (* icon_stretch_ended)       (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *data);
	int	     (* preview)		  (mautilusCanvasContainer *container,
						   mautilusCanvasIconData *data,
						   gboolean start_flag);
        void         (* icon_added)               (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *data);
        void         (* icon_removed)             (mautilusCanvasContainer *container,
						     mautilusCanvasIconData *data);
        void         (* cleared)                  (mautilusCanvasContainer *container);
	gboolean     (* start_interactive_search) (mautilusCanvasContainer *container);
} mautilusCanvasContainerClass;

/* GtkObject */
GType             mautilus_canvas_container_get_type                      (void);
GtkWidget *       mautilus_canvas_container_new                           (void);


/* adding, removing, and managing icons */
void              mautilus_canvas_container_clear                         (mautilusCanvasContainer  *view);
gboolean          mautilus_canvas_container_add                           (mautilusCanvasContainer  *view,
									   mautilusCanvasIconData       *data);
void              mautilus_canvas_container_layout_now                    (mautilusCanvasContainer *container);
gboolean          mautilus_canvas_container_remove                        (mautilusCanvasContainer  *view,
									   mautilusCanvasIconData       *data);
void              mautilus_canvas_container_for_each                      (mautilusCanvasContainer  *view,
									   mautilusCanvasCallback    callback,
									   gpointer                callback_data);
void              mautilus_canvas_container_request_update                (mautilusCanvasContainer  *view,
									   mautilusCanvasIconData       *data);
void              mautilus_canvas_container_request_update_all            (mautilusCanvasContainer  *container);
void              mautilus_canvas_container_reveal                        (mautilusCanvasContainer  *container,
									   mautilusCanvasIconData       *data);
gboolean          mautilus_canvas_container_is_empty                      (mautilusCanvasContainer  *container);
mautilusCanvasIconData *mautilus_canvas_container_get_first_visible_icon        (mautilusCanvasContainer  *container);
void              mautilus_canvas_container_scroll_to_canvas                (mautilusCanvasContainer  *container,
									     mautilusCanvasIconData       *data);

void              mautilus_canvas_container_begin_loading                 (mautilusCanvasContainer  *container);
void              mautilus_canvas_container_end_loading                   (mautilusCanvasContainer  *container,
									   gboolean                all_icons_added);

/* control the layout */
gboolean          mautilus_canvas_container_is_auto_layout                (mautilusCanvasContainer  *container);
void              mautilus_canvas_container_set_auto_layout               (mautilusCanvasContainer  *container,
									   gboolean                auto_layout);

gboolean          mautilus_canvas_container_is_keep_aligned               (mautilusCanvasContainer  *container);
void              mautilus_canvas_container_set_keep_aligned              (mautilusCanvasContainer  *container,
									   gboolean                keep_aligned);
void              mautilus_canvas_container_sort                          (mautilusCanvasContainer  *container);
void              mautilus_canvas_container_freeze_icon_positions         (mautilusCanvasContainer  *container);

int               mautilus_canvas_container_get_max_layout_lines           (mautilusCanvasContainer  *container);
int               mautilus_canvas_container_get_max_layout_lines_for_pango (mautilusCanvasContainer  *container);

void              mautilus_canvas_container_set_highlighted_for_clipboard (mautilusCanvasContainer  *container,
									   GList                  *clipboard_canvas_data);

/* operations on all icons */
void              mautilus_canvas_container_unselect_all                  (mautilusCanvasContainer  *view);
void              mautilus_canvas_container_select_all                    (mautilusCanvasContainer  *view);


void              mautilus_canvas_container_select_first                  (mautilusCanvasContainer  *view);


/* operations on the selection */
GList     *       mautilus_canvas_container_get_selection                 (mautilusCanvasContainer  *view);
void			  mautilus_canvas_container_invert_selection				(mautilusCanvasContainer  *view);
void              mautilus_canvas_container_set_selection                 (mautilusCanvasContainer  *view,
									   GList                  *selection);
GArray    *       mautilus_canvas_container_get_selected_icon_locations   (mautilusCanvasContainer  *view);
GArray    *       mautilus_canvas_container_get_selected_icons_bounding_box (mautilusCanvasContainer *container);
gboolean          mautilus_canvas_container_has_stretch_handles           (mautilusCanvasContainer  *container);
gboolean          mautilus_canvas_container_is_stretched                  (mautilusCanvasContainer  *container);
void              mautilus_canvas_container_show_stretch_handles          (mautilusCanvasContainer  *container);
void              mautilus_canvas_container_unstretch                     (mautilusCanvasContainer  *container);

/* options */
mautilusCanvasZoomLevel mautilus_canvas_container_get_zoom_level                (mautilusCanvasContainer  *view);
void              mautilus_canvas_container_set_zoom_level                (mautilusCanvasContainer  *view,
									   int                     new_zoom_level);
void              mautilus_canvas_container_set_single_click_mode         (mautilusCanvasContainer  *container,
									   gboolean                single_click_mode);
void              mautilus_canvas_container_enable_linger_selection       (mautilusCanvasContainer  *view,
									   gboolean                enable);
gboolean          mautilus_canvas_container_get_is_fixed_size             (mautilusCanvasContainer  *container);
void              mautilus_canvas_container_set_is_fixed_size             (mautilusCanvasContainer  *container,
									   gboolean                is_fixed_size);
gboolean          mautilus_canvas_container_get_is_desktop                (mautilusCanvasContainer  *container);
void              mautilus_canvas_container_set_is_desktop                (mautilusCanvasContainer  *container,
									   gboolean                is_desktop);
void              mautilus_canvas_container_reset_scroll_region           (mautilusCanvasContainer  *container);
void              mautilus_canvas_container_set_font                      (mautilusCanvasContainer  *container,
									   const char             *font); 
void              mautilus_canvas_container_set_margins                   (mautilusCanvasContainer  *container,
									   int                     left_margin,
									   int                     right_margin,
									   int                     top_margin,
									   int                     bottom_margin);
char*             mautilus_canvas_container_get_icon_description          (mautilusCanvasContainer  *container,
									     mautilusCanvasIconData       *data);

gboolean	  mautilus_canvas_container_is_layout_rtl			(mautilusCanvasContainer  *container);
gboolean	  mautilus_canvas_container_is_layout_vertical		(mautilusCanvasContainer  *container);

gboolean          mautilus_canvas_container_get_store_layout_timestamps   (mautilusCanvasContainer  *container);
void              mautilus_canvas_container_set_store_layout_timestamps   (mautilusCanvasContainer  *container,
									   gboolean                store_layout);

void              mautilus_canvas_container_widget_to_file_operation_position (mautilusCanvasContainer *container,
									       GdkPoint              *position);
guint             mautilus_canvas_container_get_icon_size_for_zoom_level (mautilusCanvasZoomLevel zoom_level);

#define CANVAS_WIDTH(container,allocation) ((allocation.width		\
					     - container->details->left_margin \
					     - container->details->right_margin) \
					    /  EEL_CANVAS (container)->pixels_per_unit)

#define CANVAS_HEIGHT(container,allocation) ((allocation.height		\
					      - container->details->top_margin \
					      - container->details->bottom_margin) \
					     / EEL_CANVAS (container)->pixels_per_unit)

#endif /* MAUTILUS_CANVAS_CONTAINER_H */
