/*
   mautilus-file.h: mautilus file model.
 
   Copyright (C) 1999, 2000, 2001 Eazel, Inc.
  
   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
  
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
  
   Author: Darin Adler <darin@bentspoon.com>
*/

#ifndef MAUTILUS_FILE_H
#define MAUTILUS_FILE_H

#include <gtk/gtk.h>
#include <gio/gio.h>
#include "mautilus-file-attributes.h"
#include "mautilus-icon-info.h"

/* mautilusFile is an object used to represent a single element of a
 * mautilusDirectory. It's lightweight and relies on mautilusDirectory
 * to do most of the work.
 */

/* mautilusFile is defined both here and in mautilus-directory.h. */
#ifndef MAUTILUS_FILE_DEFINED
#define MAUTILUS_FILE_DEFINED
typedef struct mautilusFile mautilusFile;
#endif

#define MAUTILUS_TYPE_FILE mautilus_file_get_type()
#define MAUTILUS_FILE(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_FILE, mautilusFile))
#define MAUTILUS_FILE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_FILE, mautilusFileClass))
#define MAUTILUS_IS_FILE(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_FILE))
#define MAUTILUS_IS_FILE_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), MAUTILUS_TYPE_FILE))
#define MAUTILUS_FILE_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), MAUTILUS_TYPE_FILE, mautilusFileClass))

typedef enum {
	MAUTILUS_FILE_SORT_NONE,
	MAUTILUS_FILE_SORT_BY_DISPLAY_NAME,
	MAUTILUS_FILE_SORT_BY_SIZE,
	MAUTILUS_FILE_SORT_BY_TYPE,
	MAUTILUS_FILE_SORT_BY_FAVORITE,
	MAUTILUS_FILE_SORT_BY_MTIME,
        MAUTILUS_FILE_SORT_BY_ATIME,
	MAUTILUS_FILE_SORT_BY_TRASHED_TIME,
	MAUTILUS_FILE_SORT_BY_SEARCH_RELEVANCE,
	MAUTILUS_FILE_SORT_BY_RECENCY
} mautilusFileSortType;	

typedef enum {
	MAUTILUS_REQUEST_NOT_STARTED,
	MAUTILUS_REQUEST_IN_PROGRESS,
	MAUTILUS_REQUEST_DONE
} mautilusRequestStatus;

typedef enum {
	MAUTILUS_FILE_ICON_FLAGS_NONE = 0,
	MAUTILUS_FILE_ICON_FLAGS_USE_THUMBNAILS = (1<<0),
	MAUTILUS_FILE_ICON_FLAGS_IGNORE_VISITING = (1<<1),
	MAUTILUS_FILE_ICON_FLAGS_FOR_DRAG_ACCEPT = (1<<2),
	MAUTILUS_FILE_ICON_FLAGS_FOR_OPEN_FOLDER = (1<<3),
	/* whether the thumbnail size must match the display icon size */
	MAUTILUS_FILE_ICON_FLAGS_FORCE_THUMBNAIL_SIZE = (1<<4),
	/* uses the icon of the mount if present */
	MAUTILUS_FILE_ICON_FLAGS_USE_MOUNT_ICON = (1<<5),
	/* render emblems */
	MAUTILUS_FILE_ICON_FLAGS_USE_EMBLEMS = (1<<6),
	MAUTILUS_FILE_ICON_FLAGS_USE_ONE_EMBLEM = (1<<7)
} mautilusFileIconFlags;	

/* Standard Drag & Drop types. */
typedef enum {
	MAUTILUS_ICON_DND_GNOME_ICON_LIST,
	MAUTILUS_ICON_DND_URI_LIST,
	MAUTILUS_ICON_DND_NETSCAPE_URL,
	MAUTILUS_ICON_DND_TEXT,
	MAUTILUS_ICON_DND_XDNDDIRECTSAVE,
	MAUTILUS_ICON_DND_RAW,
	MAUTILUS_ICON_DND_ROOTWINDOW_DROP
} mautilusIconDndTargetType;

/* Item of the drag selection list */
typedef struct {
	mautilusFile *file;
	char *uri;
	gboolean got_icon_position;
	int icon_x, icon_y;
	int icon_width, icon_height;
} mautilusDragSelectionItem;

/* Emblems sometimes displayed for mautilusFiles. Do not localize. */
#define MAUTILUS_FILE_EMBLEM_NAME_SYMBOLIC_LINK "symbolic-link"
#define MAUTILUS_FILE_EMBLEM_NAME_CANT_READ "unreadable"
#define MAUTILUS_FILE_EMBLEM_NAME_CANT_WRITE "readonly"
#define MAUTILUS_FILE_EMBLEM_NAME_TRASH "trash"
#define MAUTILUS_FILE_EMBLEM_NAME_NOTE "note"

typedef void (*mautilusFileCallback)          (mautilusFile  *file,
				               gpointer       callback_data);
typedef gboolean (*mautilusFileFilterFunc)    (mautilusFile  *file,
                                               gpointer       callback_data);
typedef void (*mautilusFileListCallback)      (GList         *file_list,
				               gpointer       callback_data);
typedef void (*mautilusFileOperationCallback) (mautilusFile  *file,
					       GFile         *result_location,
					       GError        *error,
					       gpointer       callback_data);
typedef int (*mautilusWidthMeasureCallback)   (const char    *string,
					       void	     *context);
typedef char * (*mautilusTruncateCallback)    (const char    *string,
					       int	      width,
					       void	     *context);


#define MAUTILUS_FILE_ATTRIBUTES_FOR_ICON (MAUTILUS_FILE_ATTRIBUTE_INFO | MAUTILUS_FILE_ATTRIBUTE_LINK_INFO | MAUTILUS_FILE_ATTRIBUTE_THUMBNAIL)

typedef void mautilusFileListHandle;

/* GObject requirements. */
GType                   mautilus_file_get_type                          (void);

/* Getting at a single file. */
mautilusFile *          mautilus_file_get                               (GFile                          *location);
mautilusFile *          mautilus_file_get_by_uri                        (const char                     *uri);

/* Get a file only if the mautilus version already exists */
mautilusFile *          mautilus_file_get_existing                      (GFile                          *location);
mautilusFile *          mautilus_file_get_existing_by_uri               (const char                     *uri);

/* Covers for g_object_ref and g_object_unref that provide two conveniences:
 * 1) Using these is type safe.
 * 2) You are allowed to call these with NULL,
 */
mautilusFile *          mautilus_file_ref                               (mautilusFile                   *file);
void                    mautilus_file_unref                             (mautilusFile                   *file);

/* Monitor the file. */
void                    mautilus_file_monitor_add                       (mautilusFile                   *file,
									 gconstpointer                   client,
									 mautilusFileAttributes          attributes);
void                    mautilus_file_monitor_remove                    (mautilusFile                   *file,
									 gconstpointer                   client);

/* Waiting for data that's read asynchronously.
 * This interface currently works only for metadata, but could be expanded
 * to other attributes as well.
 */
void                    mautilus_file_call_when_ready                   (mautilusFile                   *file,
									 mautilusFileAttributes          attributes,
									 mautilusFileCallback            callback,
									 gpointer                        callback_data);
void                    mautilus_file_cancel_call_when_ready            (mautilusFile                   *file,
									 mautilusFileCallback            callback,
									 gpointer                        callback_data);
gboolean                mautilus_file_check_if_ready                    (mautilusFile                   *file,
									 mautilusFileAttributes          attributes);
void                    mautilus_file_invalidate_attributes             (mautilusFile                   *file,
									 mautilusFileAttributes          attributes);
void                    mautilus_file_invalidate_all_attributes         (mautilusFile                   *file);

/* Basic attributes for file objects. */
gboolean                mautilus_file_contains_text                     (mautilusFile                   *file);
char *                  mautilus_file_get_display_name                  (mautilusFile                   *file);
char *                  mautilus_file_get_edit_name                     (mautilusFile                   *file);
char *                  mautilus_file_get_name                          (mautilusFile                   *file);
GFile *                 mautilus_file_get_location                      (mautilusFile                   *file);
char *			 mautilus_file_get_description			 (mautilusFile			 *file);
char *                  mautilus_file_get_uri                           (mautilusFile                   *file);
char *                  mautilus_file_get_uri_scheme                    (mautilusFile                   *file);
mautilusFile *          mautilus_file_get_parent                        (mautilusFile                   *file);
GFile *                 mautilus_file_get_parent_location               (mautilusFile                   *file);
char *                  mautilus_file_get_parent_uri                    (mautilusFile                   *file);
char *                  mautilus_file_get_parent_uri_for_display        (mautilusFile                   *file);
char *                  mautilus_file_get_thumbnail_path                (mautilusFile                   *file);
gboolean                mautilus_file_can_get_size                      (mautilusFile                   *file);
goffset                 mautilus_file_get_size                          (mautilusFile                   *file);
time_t                  mautilus_file_get_mtime                         (mautilusFile                   *file);
time_t                  mautilus_file_get_atime                         (mautilusFile                   *file);
time_t                  mautilus_file_get_recency                       (mautilusFile                   *file);
time_t                  mautilus_file_get_trash_time                    (mautilusFile                   *file);
GFileType               mautilus_file_get_file_type                     (mautilusFile                   *file);
char *                  mautilus_file_get_mime_type                     (mautilusFile                   *file);
char *                  mautilus_file_get_extension                     (mautilusFile                   *file);
gboolean                mautilus_file_is_mime_type                      (mautilusFile                   *file,
									 const char                     *mime_type);
gboolean                mautilus_file_is_launchable                     (mautilusFile                   *file);
gboolean                mautilus_file_is_symbolic_link                  (mautilusFile                   *file);
GMount *                mautilus_file_get_mount                         (mautilusFile                   *file);
char *                  mautilus_file_get_volume_free_space             (mautilusFile                   *file);
char *                  mautilus_file_get_volume_name                   (mautilusFile                   *file);
char *                  mautilus_file_get_symbolic_link_target_path     (mautilusFile                   *file);
char *                  mautilus_file_get_symbolic_link_target_uri      (mautilusFile                   *file);
gboolean                mautilus_file_is_broken_symbolic_link           (mautilusFile                   *file);
gboolean                mautilus_file_is_mautilus_link                  (mautilusFile                   *file);
gboolean                mautilus_file_is_executable                     (mautilusFile                   *file);
gboolean                mautilus_file_is_directory                      (mautilusFile                   *file);
gboolean                mautilus_file_is_regular_file                   (mautilusFile                   *file);
gboolean                mautilus_file_is_user_special_directory         (mautilusFile                   *file,
									 GUserDirectory                 special_directory);
gboolean                mautilus_file_is_special_link                   (mautilusFile                   *file);
gboolean		mautilus_file_is_archive			(mautilusFile			*file);
gboolean                mautilus_file_is_in_search                      (mautilusFile                   *file);
gboolean                mautilus_file_is_in_trash                       (mautilusFile                   *file);
gboolean                mautilus_file_is_in_recent                      (mautilusFile                   *file);
gboolean                mautilus_file_is_in_starred                     (mautilusFile                   *file);
gboolean                mautilus_file_is_in_admin                       (mautilusFile                   *file);
gboolean                mautilus_file_is_remote                         (mautilusFile                   *file);
gboolean                mautilus_file_is_other_locations                (mautilusFile                   *file);
gboolean                mautilus_file_is_favorite_location              (mautilusFile                   *file);
gboolean		mautilus_file_is_home				(mautilusFile                   *file);
gboolean                mautilus_file_is_desktop_directory              (mautilusFile                   *file);
gboolean                mautilus_file_is_child_of_desktop_directory     (mautilusFile                   *file);
GError *                mautilus_file_get_file_info_error               (mautilusFile                   *file);
gboolean                mautilus_file_get_directory_item_count          (mautilusFile                   *file,
									 guint                          *count,
									 gboolean                       *count_unreadable);
void                    mautilus_file_recompute_deep_counts             (mautilusFile                   *file);
mautilusRequestStatus   mautilus_file_get_deep_counts                   (mautilusFile                   *file,
									 guint                          *directory_count,
									 guint                          *file_count,
									 guint                          *unreadable_directory_count,
									 goffset               *total_size,
									 gboolean                        force);
gboolean                mautilus_file_should_show_thumbnail             (mautilusFile                   *file);
gboolean                mautilus_file_should_show_directory_item_count  (mautilusFile                   *file);

void                    mautilus_file_set_search_relevance              (mautilusFile                   *file,
									 gdouble                         relevance);
void                    mautilus_file_set_search_fts_snippet            (mautilusFile                   *file,
                                                                         const gchar                    *fts_snippet);
const gchar*            mautilus_file_get_search_fts_snippet            (mautilusFile                   *file);

void                    mautilus_file_set_attributes                    (mautilusFile                   *file, 
									 GFileInfo                      *attributes,
									 mautilusFileOperationCallback   callback,
									 gpointer                        callback_data);
GFilesystemPreviewType  mautilus_file_get_filesystem_use_preview        (mautilusFile *file);

char *                  mautilus_file_get_filesystem_id                 (mautilusFile                   *file);

char *                  mautilus_file_get_filesystem_type               (mautilusFile                   *file);

mautilusFile *          mautilus_file_get_trash_original_file           (mautilusFile                   *file);

/* Permissions. */
gboolean                mautilus_file_can_get_permissions               (mautilusFile                   *file);
gboolean                mautilus_file_can_set_permissions               (mautilusFile                   *file);
guint                   mautilus_file_get_permissions                   (mautilusFile                   *file);
gboolean                mautilus_file_can_get_owner                     (mautilusFile                   *file);
gboolean                mautilus_file_can_set_owner                     (mautilusFile                   *file);
gboolean                mautilus_file_can_get_group                     (mautilusFile                   *file);
gboolean                mautilus_file_can_set_group                     (mautilusFile                   *file);
char *                  mautilus_file_get_owner_name                    (mautilusFile                   *file);
char *                  mautilus_file_get_group_name                    (mautilusFile                   *file);
GList *                 mautilus_get_user_names                         (void);
GList *                 mautilus_get_all_group_names                    (void);
GList *                 mautilus_file_get_settable_group_names          (mautilusFile                   *file);
gboolean                mautilus_file_can_get_selinux_context           (mautilusFile                   *file);
char *                  mautilus_file_get_selinux_context               (mautilusFile                   *file);

/* "Capabilities". */
gboolean                mautilus_file_can_read                          (mautilusFile                   *file);
gboolean                mautilus_file_can_write                         (mautilusFile                   *file);
gboolean                mautilus_file_can_execute                       (mautilusFile                   *file);
gboolean                mautilus_file_can_rename                        (mautilusFile                   *file);
gboolean                mautilus_file_can_delete                        (mautilusFile                   *file);
gboolean                mautilus_file_can_trash                         (mautilusFile                   *file);

gboolean                mautilus_file_can_mount                         (mautilusFile                   *file);
gboolean                mautilus_file_can_unmount                       (mautilusFile                   *file);
gboolean                mautilus_file_can_eject                         (mautilusFile                   *file);
gboolean                mautilus_file_can_start                         (mautilusFile                   *file);
gboolean                mautilus_file_can_start_degraded                (mautilusFile                   *file);
gboolean                mautilus_file_can_stop                          (mautilusFile                   *file);
GDriveStartStopType     mautilus_file_get_start_stop_type               (mautilusFile                   *file);
gboolean                mautilus_file_can_poll_for_media                (mautilusFile                   *file);
gboolean                mautilus_file_is_media_check_automatic          (mautilusFile                   *file);

void                    mautilus_file_mount                             (mautilusFile                   *file,
									 GMountOperation                *mount_op,
									 GCancellable                   *cancellable,
									 mautilusFileOperationCallback   callback,
									 gpointer                        callback_data);
void                    mautilus_file_unmount                           (mautilusFile                   *file,
									 GMountOperation                *mount_op,
									 GCancellable                   *cancellable,
									 mautilusFileOperationCallback   callback,
									 gpointer                        callback_data);
void                    mautilus_file_eject                             (mautilusFile                   *file,
									 GMountOperation                *mount_op,
									 GCancellable                   *cancellable,
									 mautilusFileOperationCallback   callback,
									 gpointer                        callback_data);

void                    mautilus_file_start                             (mautilusFile                   *file,
									 GMountOperation                *start_op,
									 GCancellable                   *cancellable,
									 mautilusFileOperationCallback   callback,
									 gpointer                        callback_data);
void                    mautilus_file_stop                              (mautilusFile                   *file,
									 GMountOperation                *mount_op,
									 GCancellable                   *cancellable,
									 mautilusFileOperationCallback   callback,
									 gpointer                        callback_data);
void                    mautilus_file_poll_for_media                    (mautilusFile                   *file);

/* Basic operations for file objects. */
void                    mautilus_file_set_owner                         (mautilusFile                   *file,
									 const char                     *user_name_or_id,
									 mautilusFileOperationCallback   callback,
									 gpointer                        callback_data);
void                    mautilus_file_set_group                         (mautilusFile                   *file,
									 const char                     *group_name_or_id,
									 mautilusFileOperationCallback   callback,
									 gpointer                        callback_data);
void                    mautilus_file_set_permissions                   (mautilusFile                   *file,
									 guint32                         permissions,
									 mautilusFileOperationCallback   callback,
									 gpointer                        callback_data);
void                    mautilus_file_rename                            (mautilusFile                   *file,
									 const char                     *new_name,
									 mautilusFileOperationCallback   callback,
									 gpointer                        callback_data);
void                    mautilus_file_batch_rename                      (GList                          *files,
                                                                         GList                          *new_names,
                                                                         mautilusFileOperationCallback   callback,
                                                                         gpointer                        callback_data);
void                    mautilus_file_cancel                            (mautilusFile                   *file,
									 mautilusFileOperationCallback   callback,
									 gpointer                        callback_data);

/* Return true if this file has already been deleted.
 * This object will be unref'd after sending the files_removed signal,
 * but it could hang around longer if someone ref'd it.
 */
gboolean                mautilus_file_is_gone                           (mautilusFile                   *file);

/* Used in subclasses that handles the rename of a file. This handles the case
 * when the file is gone. If this returns TRUE, simply do nothing
 */
gboolean                mautilus_file_rename_handle_file_gone           (mautilusFile                   *file,
                                                                         mautilusFileOperationCallback   callback,
                                                                         gpointer                        callback_data);

/* Return true if this file is not confirmed to have ever really
 * existed. This is true when the mautilusFile object has been created, but no I/O
 * has yet confirmed the existence of a file by that name.
 */
gboolean                mautilus_file_is_not_yet_confirmed              (mautilusFile                   *file);

/* Simple getting and setting top-level metadata. */
char *                  mautilus_file_get_metadata                      (mautilusFile                   *file,
									 const char                     *key,
									 const char                     *default_metadata);
GList *                 mautilus_file_get_metadata_list                 (mautilusFile                   *file,
									 const char                     *key);
void                    mautilus_file_set_metadata                      (mautilusFile                   *file,
									 const char                     *key,
									 const char                     *default_metadata,
									 const char                     *metadata);
void                    mautilus_file_set_metadata_list                 (mautilusFile                   *file,
									 const char                     *key,
									 GList                          *list);

/* Covers for common data types. */
gboolean                mautilus_file_get_boolean_metadata              (mautilusFile                   *file,
									 const char                     *key,
									 gboolean                        default_metadata);
void                    mautilus_file_set_boolean_metadata              (mautilusFile                   *file,
									 const char                     *key,
									 gboolean                        default_metadata,
									 gboolean                        metadata);
int                     mautilus_file_get_integer_metadata              (mautilusFile                   *file,
									 const char                     *key,
									 int                             default_metadata);
void                    mautilus_file_set_integer_metadata              (mautilusFile                   *file,
									 const char                     *key,
									 int                             default_metadata,
									 int                             metadata);

#define UNDEFINED_TIME ((time_t) (-1))

time_t                  mautilus_file_get_time_metadata                 (mautilusFile                  *file,
									 const char                    *key);
void                    mautilus_file_set_time_metadata                 (mautilusFile                  *file,
									 const char                    *key,
									 time_t                         time);


/* Attributes for file objects as user-displayable strings. */
char *                  mautilus_file_get_string_attribute              (mautilusFile                   *file,
									 const char                     *attribute_name);
char *                  mautilus_file_get_string_attribute_q            (mautilusFile                   *file,
									 GQuark                          attribute_q);
char *                  mautilus_file_get_string_attribute_with_default (mautilusFile                   *file,
									 const char                     *attribute_name);
char *                  mautilus_file_get_string_attribute_with_default_q (mautilusFile                  *file,
									 GQuark                          attribute_q);

/* Matching with another URI. */
gboolean                mautilus_file_matches_uri                       (mautilusFile                   *file,
									 const char                     *uri);

/* Is the file local? */
gboolean                mautilus_file_is_local                          (mautilusFile                   *file);
gboolean                mautilus_file_is_local_or_fuse                  (mautilusFile                   *file);

/* Comparing two file objects for sorting */
mautilusFileSortType    mautilus_file_get_default_sort_type             (mautilusFile                   *file,
									 gboolean                       *reversed);
const gchar *           mautilus_file_get_default_sort_attribute        (mautilusFile                   *file,
									 gboolean                       *reversed);

int                     mautilus_file_compare_for_sort                  (mautilusFile                   *file_1,
									 mautilusFile                   *file_2,
									 mautilusFileSortType            sort_type,
									 gboolean			 directories_first,
									 gboolean		  	 reversed);
int                     mautilus_file_compare_for_sort_by_attribute     (mautilusFile                   *file_1,
									 mautilusFile                   *file_2,
									 const char                     *attribute,
									 gboolean                        directories_first,
									 gboolean                        reversed);
int                     mautilus_file_compare_for_sort_by_attribute_q   (mautilusFile                   *file_1,
									 mautilusFile                   *file_2,
									 GQuark                          attribute,
									 gboolean                        directories_first,
									 gboolean                        reversed);
gboolean                mautilus_file_is_date_sort_attribute_q          (GQuark                          attribute);

int                     mautilus_file_compare_location                  (mautilusFile                    *file_1,
                                                                         mautilusFile                    *file_2);

/* Compare display name of file with string for equality */
int                     mautilus_file_compare_display_name              (mautilusFile                   *file,
									 const char                     *string);

/* filtering functions for use by various directory views */
gboolean                mautilus_file_is_hidden_file                    (mautilusFile                   *file);
gboolean                mautilus_file_should_show                       (mautilusFile                   *file,
									 gboolean                        show_hidden,
									 gboolean                        show_foreign);
GList                  *mautilus_file_list_filter_hidden                (GList                          *files,
									 gboolean                        show_hidden);


/* Get the URI that's used when activating the file.
 * Getting this can require reading the contents of the file.
 */
gboolean                mautilus_file_is_launcher                       (mautilusFile                   *file);
gboolean                mautilus_file_is_foreign_link                   (mautilusFile                   *file);
gboolean                mautilus_file_is_trusted_link                   (mautilusFile                   *file);
gboolean                mautilus_file_has_activation_uri                (mautilusFile                   *file);
char *                  mautilus_file_get_activation_uri                (mautilusFile                   *file);
GFile *                 mautilus_file_get_activation_location           (mautilusFile                   *file);

char *                  mautilus_file_get_target_uri                    (mautilusFile                   *file);

GIcon *                 mautilus_file_get_gicon                         (mautilusFile                   *file,
									 mautilusFileIconFlags           flags);
mautilusIconInfo *      mautilus_file_get_icon                          (mautilusFile                   *file,
									 int                             size,
									 int                             scale,
									 mautilusFileIconFlags           flags);
GdkPixbuf *             mautilus_file_get_icon_pixbuf                   (mautilusFile                   *file,
									 int                             size,
									 gboolean                        force_size,
									 int                             scale,
									 mautilusFileIconFlags           flags);

/* Whether the file should open inside a view */
gboolean                mautilus_file_opens_in_view                     (mautilusFile                   *file);
/* Thumbnailing handling */
gboolean                mautilus_file_is_thumbnailing                   (mautilusFile                   *file);

/* Convenience functions for dealing with a list of mautilusFile objects that each have a ref.
 * These are just convenient names for functions that work on lists of GtkObject *.
 */
GList *                 mautilus_file_list_ref                          (GList                          *file_list);
void                    mautilus_file_list_unref                        (GList                          *file_list);
void                    mautilus_file_list_free                         (GList                          *file_list);
GList *                 mautilus_file_list_copy                         (GList                          *file_list);
GList *			mautilus_file_list_sort_by_display_name		(GList				*file_list);
void                    mautilus_file_list_call_when_ready              (GList                          *file_list,
									 mautilusFileAttributes          attributes,
									 mautilusFileListHandle        **handle,
									 mautilusFileListCallback        callback,
									 gpointer                        callback_data);
void                    mautilus_file_list_cancel_call_when_ready       (mautilusFileListHandle         *handle);

GList *                 mautilus_file_list_filter                       (GList                          *files,
                                                                         GList                         **failed,
                                                                         mautilusFileFilterFunc          filter_function,
                                                                         gpointer                        user_data);
gboolean                mautilus_file_list_are_all_folders              (const GList                    *files);
/* DND */
gboolean                mautilus_drag_can_accept_item                   (mautilusFile                   *drop_target_item,
                                                                         const char                     *item_uri);

gboolean                mautilus_drag_can_accept_items                  (mautilusFile                   *drop_target_item,
                                                                         const GList                    *items);

gboolean                mautilus_drag_can_accept_info                   (mautilusFile                   *drop_target_item,
                                                                         mautilusIconDndTargetType       drag_type,
                                                                         const GList                    *items);

/* Debugging */
void                    mautilus_file_dump                              (mautilusFile                   *file);

typedef struct mautilusFileDetails mautilusFileDetails;

struct mautilusFile {
	GObject parent_slot;
	mautilusFileDetails *details;
};

/* This is actually a "protected" type, but it must be here so we can
 * compile the get_date function pointer declaration below.
 */
typedef enum {
	MAUTILUS_DATE_TYPE_MODIFIED,
	MAUTILUS_DATE_TYPE_ACCESSED,
	MAUTILUS_DATE_TYPE_TRASHED,
	MAUTILUS_DATE_TYPE_RECENCY
} mautilusDateType;

gboolean                mautilus_file_get_date                          (mautilusFile                   *file,
                                                                         mautilusDateType                date_type,
                                                                         time_t                         *date);

typedef struct {
	GObjectClass parent_slot;

	/* Subclasses can set this to something other than G_FILE_TYPE_UNKNOWN and
	   it will be used as the default file type. This is useful when creating
	   a "virtual" mautilusFile subclass that you can't actually get real
	   information about. For exaple mautilusDesktopDirectoryFile. */
	GFileType default_file_type; 
	
	/* Called when the file notices any change. */
	void                  (* changed)                (mautilusFile *file);

	/* Called periodically while directory deep count is being computed. */
	void                  (* updated_deep_count_in_progress) (mautilusFile *file);

	/* Virtual functions (mainly used for trash directory). */
	void                  (* monitor_add)            (mautilusFile           *file,
							  gconstpointer           client,
							  mautilusFileAttributes  attributes);
	void                  (* monitor_remove)         (mautilusFile           *file,
							  gconstpointer           client);
	void                  (* call_when_ready)        (mautilusFile           *file,
							  mautilusFileAttributes  attributes,
							  mautilusFileCallback    callback,
							  gpointer                callback_data);
	void                  (* cancel_call_when_ready) (mautilusFile           *file,
							  mautilusFileCallback    callback,
							  gpointer                callback_data);
	gboolean              (* check_if_ready)         (mautilusFile           *file,
							  mautilusFileAttributes  attributes);
	gboolean              (* get_item_count)         (mautilusFile           *file,
							  guint                  *count,
							  gboolean               *count_unreadable);
	mautilusRequestStatus (* get_deep_counts)        (mautilusFile           *file,
							  guint                  *directory_count,
							  guint                  *file_count,
							  guint                  *unreadable_directory_count,
							  goffset       *total_size);
	gboolean              (* get_date)               (mautilusFile           *file,
							  mautilusDateType        type,
							  time_t                 *date);
	char *                (* get_where_string)       (mautilusFile           *file);

	void                  (* set_metadata)           (mautilusFile           *file,
							  const char             *key,
							  const char             *value);
	void                  (* set_metadata_as_list)   (mautilusFile           *file,
							  const char             *key,
							  char                  **value);
	
	void                  (* mount)                  (mautilusFile                   *file,
							  GMountOperation                *mount_op,
							  GCancellable                   *cancellable,
							  mautilusFileOperationCallback   callback,
							  gpointer                        callback_data);
	void                 (* unmount)                 (mautilusFile                   *file,
							  GMountOperation                *mount_op,
							  GCancellable                   *cancellable,
							  mautilusFileOperationCallback   callback,
							  gpointer                        callback_data);
	void                 (* eject)                   (mautilusFile                   *file,
							  GMountOperation                *mount_op,
							  GCancellable                   *cancellable,
							  mautilusFileOperationCallback   callback,
							  gpointer                        callback_data);

	void                  (* start)                  (mautilusFile                   *file,
							  GMountOperation                *start_op,
							  GCancellable                   *cancellable,
							  mautilusFileOperationCallback   callback,
							  gpointer                        callback_data);
	void                 (* stop)                    (mautilusFile                   *file,
							  GMountOperation                *mount_op,
							  GCancellable                   *cancellable,
							  mautilusFileOperationCallback   callback,
							  gpointer                        callback_data);

	void                 (* poll_for_media)          (mautilusFile                   *file);

        gboolean             (* can_rename)              (mautilusFile                   *file);

        void                 (* rename)                  (mautilusFile                   *file,
                                                          const char                     *new_name,
                                                          mautilusFileOperationCallback   callback,
                                                          gpointer                        callback_data);

       char*                 (* get_target_uri)          (mautilusFile                   *file);

       gboolean              (* drag_can_accept_files)   (mautilusFile                   *drop_target_item);

       void                  (* invalidate_attributes_internal) (mautilusFile            *file,
                                                                 mautilusFileAttributes   file_attributes);

       gboolean              (* opens_in_view)           (mautilusFile                   *file);

       /* Use this if the custom file class doesn't support usual operations like
        * copy, delete or move.
        */
       gboolean              (* is_special_link)          (mautilusFile                  *file);
} mautilusFileClass;

#endif /* MAUTILUS_FILE_H */
