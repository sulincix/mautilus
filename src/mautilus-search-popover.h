/* mautilus-search-popover.h
 *
 * Copyright (C) 2015 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAUTILUS_SEARCH_POPOVER_H
#define MAUTILUS_SEARCH_POPOVER_H

#include <glib.h>
#include <gtk/gtk.h>

#include "mautilus-query.h"

G_BEGIN_DECLS

typedef enum {
  MAUTILUS_SEARCH_FILTER_CONTENT, /* Full text or filename */
  MAUTILUS_SEARCH_FILTER_DATE,    /* When */
  MAUTILUS_SEARCH_FILTER_LAST,    /* Last modified or last used */
  MAUTILUS_SEARCH_FILTER_TYPE     /* What */
} mautilusSearchFilter;

#define MAUTILUS_TYPE_SEARCH_POPOVER (mautilus_search_popover_get_type())

G_DECLARE_FINAL_TYPE (mautilusSearchPopover, mautilus_search_popover, MAUTILUS, SEARCH_POPOVER, GtkPopover)

GtkWidget*           mautilus_search_popover_new                 (void);

mautilusQuery*       mautilus_search_popover_get_query           (mautilusSearchPopover *popover);

void                 mautilus_search_popover_set_query           (mautilusSearchPopover *popover,
                                                                  mautilusQuery         *query);
void                 mautilus_search_popover_reset_date_range    (mautilusSearchPopover *popover);
void                 mautilus_search_popover_reset_mime_types    (mautilusSearchPopover *popover);

gboolean             mautilus_search_popover_get_fts_enabled     (mautilusSearchPopover *popover);
void                 mautilus_search_popover_set_fts_sensitive   (mautilusSearchPopover *popover,
                                                                  gboolean               sensitive);

G_END_DECLS

#endif /* MAUTILUS_SEARCH_POPOVER_H */
