#ifndef MAUTILUS_ICON_INFO_H
#define MAUTILUS_ICON_INFO_H

#include <glib-object.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk/gdk.h>
#include <gio/gio.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

/* Names for mautilus's different zoom levels, from tiniest items to largest items */
typedef enum {
	MAUTILUS_CANVAS_ZOOM_LEVEL_SMALL,
	MAUTILUS_CANVAS_ZOOM_LEVEL_STANDARD,
	MAUTILUS_CANVAS_ZOOM_LEVEL_LARGE,
	MAUTILUS_CANVAS_ZOOM_LEVEL_LARGER,
	MAUTILUS_CANVAS_ZOOM_LEVEL_LARGEST,
} mautilusCanvasZoomLevel;

typedef enum {
	MAUTILUS_LIST_ZOOM_LEVEL_SMALL,
	MAUTILUS_LIST_ZOOM_LEVEL_STANDARD,
	MAUTILUS_LIST_ZOOM_LEVEL_LARGE,
	MAUTILUS_LIST_ZOOM_LEVEL_LARGER,
} mautilusListZoomLevel;

#define MAUTILUS_LIST_ZOOM_LEVEL_N_ENTRIES (MAUTILUS_LIST_ZOOM_LEVEL_LARGER + 1)
#define MAUTILUS_CANVAS_ZOOM_LEVEL_N_ENTRIES (MAUTILUS_CANVAS_ZOOM_LEVEL_LARGEST + 1)

/* Nominal icon sizes for each mautilus zoom level.
 * This scheme assumes that icons are designed to
 * fit in a square space, though each image needn't
 * be square. Since individual icons can be stretched,
 * each icon is not constrained to this nominal size.
 */
#define MAUTILUS_LIST_ICON_SIZE_SMALL		16
#define MAUTILUS_LIST_ICON_SIZE_STANDARD	32
#define MAUTILUS_LIST_ICON_SIZE_LARGE		48
#define MAUTILUS_LIST_ICON_SIZE_LARGER		64

#define MAUTILUS_CANVAS_ICON_SIZE_SMALL		48
#define MAUTILUS_CANVAS_ICON_SIZE_STANDARD	64
#define MAUTILUS_CANVAS_ICON_SIZE_LARGE		96
#define MAUTILUS_CANVAS_ICON_SIZE_LARGER	128
#define MAUTILUS_CANVAS_ICON_SIZE_LARGEST	256

/* Maximum size of an icon that the icon factory will ever produce */
#define MAUTILUS_ICON_MAXIMUM_SIZE     320

#define MAUTILUS_TYPE_ICON_INFO (mautilus_icon_info_get_type ())
G_DECLARE_FINAL_TYPE (mautilusIconInfo, mautilus_icon_info, MAUTILUS, ICON_INFO, GObject)

mautilusIconInfo *    mautilus_icon_info_new_for_pixbuf               (GdkPixbuf         *pixbuf,
								       int                scale);
mautilusIconInfo *    mautilus_icon_info_lookup                       (GIcon             *icon,
								       int                size,
								       int                scale);
mautilusIconInfo *    mautilus_icon_info_lookup_from_name             (const char        *name,
								       int                size,
								       int                scale);
mautilusIconInfo *    mautilus_icon_info_lookup_from_path             (const char        *path,
								       int                size,
								       int                scale);
gboolean              mautilus_icon_info_is_fallback                  (mautilusIconInfo  *icon);
GdkPixbuf *           mautilus_icon_info_get_pixbuf                   (mautilusIconInfo  *icon);
GdkPixbuf *           mautilus_icon_info_get_pixbuf_nodefault         (mautilusIconInfo  *icon);
GdkPixbuf *           mautilus_icon_info_get_pixbuf_nodefault_at_size (mautilusIconInfo  *icon,
								       gsize              forced_size);
GdkPixbuf *           mautilus_icon_info_get_pixbuf_at_size           (mautilusIconInfo  *icon,
								       gsize              forced_size);
const char *          mautilus_icon_info_get_used_name                (mautilusIconInfo  *icon);

void                  mautilus_icon_info_clear_caches                 (void);

gint  mautilus_get_icon_size_for_stock_size          (GtkIconSize        size);

G_END_DECLS

#endif /* MAUTILUS_ICON_INFO_H */

