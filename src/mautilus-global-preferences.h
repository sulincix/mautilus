
/* mautilus-global-preferences.h - mautilus specific preference keys and
                                   functions.

   Copyright (C) 1999, 2000, 2001 Eazel, Inc.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with this program; see the file COPYING.LIB.  If not,
   see <http://www.gnu.org/licenses/>.

   Authors: Ramiro Estrugo <ramiro@eazel.com>
*/

#ifndef MAUTILUS_GLOBAL_PREFERENCES_H
#define MAUTILUS_GLOBAL_PREFERENCES_H

#include "mautilus-global-preferences.h"
#include <gio/gio.h>

G_BEGIN_DECLS

/* Trash options */
#define MAUTILUS_PREFERENCES_CONFIRM_TRASH			"confirm-trash"

/* Display  */
#define MAUTILUS_PREFERENCES_SHOW_HIDDEN_FILES			"show-hidden"

/* Mouse */
#define MAUTILUS_PREFERENCES_MOUSE_USE_EXTRA_BUTTONS		"mouse-use-extra-buttons"
#define MAUTILUS_PREFERENCES_MOUSE_FORWARD_BUTTON		"mouse-forward-button"
#define MAUTILUS_PREFERENCES_MOUSE_BACK_BUTTON			"mouse-back-button"

typedef enum
{
	MAUTILUS_NEW_TAB_POSITION_AFTER_CURRENT_TAB,
	MAUTILUS_NEW_TAB_POSITION_END,
} mautilusNewTabPosition;

/* Single/Double click preference  */
#define MAUTILUS_PREFERENCES_CLICK_POLICY			"click-policy"

/* Drag and drop preferences */
#define MAUTILUS_PREFERENCES_OPEN_FOLDER_ON_DND_HOVER   	"open-folder-on-dnd-hover"

/* Activating executable text files */
#define MAUTILUS_PREFERENCES_EXECUTABLE_TEXT_ACTIVATION		"executable-text-activation"

/* Installing new packages when unknown mime type activated */
#define MAUTILUS_PREFERENCES_INSTALL_MIME_ACTIVATION		"install-mime-activation"

/* Spatial or browser mode */
#define MAUTILUS_PREFERENCES_NEW_TAB_POSITION			"tabs-open-position"

#define MAUTILUS_PREFERENCES_ALWAYS_USE_LOCATION_ENTRY		"always-use-location-entry"

/* Which views should be displayed for new windows */
#define MAUTILUS_WINDOW_STATE_START_WITH_SIDEBAR               "start-with-sidebar"
#define MAUTILUS_WINDOW_STATE_GEOMETRY				"geometry"
#define MAUTILUS_WINDOW_STATE_MAXIMIZED				"maximized"
#define MAUTILUS_WINDOW_STATE_SIDEBAR_WIDTH			"sidebar-width"

/* Sorting order */
#define MAUTILUS_PREFERENCES_SORT_DIRECTORIES_FIRST		"sort-directories-first"
#define MAUTILUS_PREFERENCES_DEFAULT_SORT_ORDER			"default-sort-order"
#define MAUTILUS_PREFERENCES_DEFAULT_SORT_IN_REVERSE_ORDER	"default-sort-in-reverse-order"

/* The default folder viewer - one of the two enums below */
#define MAUTILUS_PREFERENCES_DEFAULT_FOLDER_VIEWER		"default-folder-viewer"

/* Compression */
#define MAUTILUS_PREFERENCES_DEFAULT_COMPRESSION_FORMAT         "default-compression-format"

typedef enum
{
        MAUTILUS_COMPRESSION_ZIP = 0,
        MAUTILUS_COMPRESSION_TAR_XZ,
        MAUTILUS_COMPRESSION_7ZIP
} mautilusCompressionFormat;

/* Icon View */
#define MAUTILUS_PREFERENCES_ICON_VIEW_DEFAULT_ZOOM_LEVEL		"default-zoom-level"

/* Experimental views */
#define MAUTILUS_PREFERENCES_USE_EXPERIMENTAL_VIEWS "use-experimental-views"

/* Which text attributes appear beneath icon names */
#define MAUTILUS_PREFERENCES_ICON_VIEW_CAPTIONS				"captions"

/* The default size for thumbnail icons */
#define MAUTILUS_PREFERENCES_ICON_VIEW_THUMBNAIL_SIZE			"thumbnail-size"

/* ellipsization preferences */
#define MAUTILUS_PREFERENCES_ICON_VIEW_TEXT_ELLIPSIS_LIMIT		"text-ellipsis-limit"
#define MAUTILUS_PREFERENCES_DESKTOP_TEXT_ELLIPSIS_LIMIT		"text-ellipsis-limit"

/* List View */
#define MAUTILUS_PREFERENCES_LIST_VIEW_DEFAULT_ZOOM_LEVEL		"default-zoom-level"
#define MAUTILUS_PREFERENCES_LIST_VIEW_DEFAULT_VISIBLE_COLUMNS		"default-visible-columns"
#define MAUTILUS_PREFERENCES_LIST_VIEW_DEFAULT_COLUMN_ORDER		"default-column-order"
#define MAUTILUS_PREFERENCES_LIST_VIEW_USE_TREE                         "use-tree-view"

enum
{
	MAUTILUS_CLICK_POLICY_SINGLE,
	MAUTILUS_CLICK_POLICY_DOUBLE
};

enum
{
	MAUTILUS_EXECUTABLE_TEXT_LAUNCH,
	MAUTILUS_EXECUTABLE_TEXT_DISPLAY,
	MAUTILUS_EXECUTABLE_TEXT_ASK
};

typedef enum
{
	MAUTILUS_SPEED_TRADEOFF_ALWAYS,
	MAUTILUS_SPEED_TRADEOFF_LOCAL_ONLY,
	MAUTILUS_SPEED_TRADEOFF_NEVER
} mautilusSpeedTradeoffValue;

#define MAUTILUS_PREFERENCES_SHOW_DIRECTORY_ITEM_COUNTS "show-directory-item-counts"
#define MAUTILUS_PREFERENCES_SHOW_FILE_THUMBNAILS	"show-image-thumbnails"
#define MAUTILUS_PREFERENCES_FILE_THUMBNAIL_LIMIT	"thumbnail-limit"

typedef enum
{
	MAUTILUS_COMPLEX_SEARCH_BAR,
	MAUTILUS_SIMPLE_SEARCH_BAR
} mautilusSearchBarMode;

#define MAUTILUS_PREFERENCES_DESKTOP_FONT		   "font"
#define MAUTILUS_PREFERENCES_DESKTOP_HOME_VISIBLE          "home-icon-visible"
#define MAUTILUS_PREFERENCES_DESKTOP_HOME_NAME             "home-icon-name"
#define MAUTILUS_PREFERENCES_DESKTOP_TRASH_VISIBLE         "trash-icon-visible"
#define MAUTILUS_PREFERENCES_DESKTOP_TRASH_NAME            "trash-icon-name"
#define MAUTILUS_PREFERENCES_DESKTOP_VOLUMES_VISIBLE	   "volumes-visible"
#define MAUTILUS_PREFERENCES_DESKTOP_NETWORK_VISIBLE       "network-icon-visible"
#define MAUTILUS_PREFERENCES_DESKTOP_NETWORK_NAME          "network-icon-name"
#define MAUTILUS_PREFERENCES_DESKTOP_BACKGROUND_FADE       "background-fade"

/* bulk rename utility */
#define MAUTILUS_PREFERENCES_BULK_RENAME_TOOL              "bulk-rename-tool"

/* Lockdown */
#define MAUTILUS_PREFERENCES_LOCKDOWN_COMMAND_LINE         "disable-command-line"

/* Desktop background */
#define MAUTILUS_PREFERENCES_SHOW_DESKTOP		   "show-desktop-icons"

/* Recent files */
#define MAUTILUS_PREFERENCES_RECENT_FILES_ENABLED          "remember-recent-files"

/* Move to trash shorcut changed dialog */
#define MAUTILUS_PREFERENCES_SHOW_MOVE_TO_TRASH_SHORTCUT_CHANGED_DIALOG "show-move-to-trash-shortcut-changed-dialog"

/* Default view when searching */
#define MAUTILUS_PREFERENCES_SEARCH_VIEW "search-view"

/* Search behaviour */
#define MAUTILUS_PREFERENCES_RECURSIVE_SEARCH "recursive-search"

/* Context menu options */
#define MAUTILUS_PREFERENCES_SHOW_DELETE_PERMANENTLY "show-delete-permanently"
#define MAUTILUS_PREFERENCES_SHOW_CREATE_LINK "show-create-link"

/* Full Text Search enabled */
#define MAUTILUS_PREFERENCES_FTS_ENABLED "fts-enabled"

void mautilus_global_preferences_init                      (void);

extern GSettings *mautilus_preferences;
extern GSettings *mautilus_compression_preferences;
extern GSettings *mautilus_icon_view_preferences;
extern GSettings *mautilus_list_view_preferences;
extern GSettings *mautilus_desktop_preferences;
extern GSettings *mautilus_window_state;
extern GSettings *gtk_filechooser_preferences;
extern GSettings *gnome_lockdown_preferences;
extern GSettings *gnome_background_preferences;
extern GSettings *gnome_interface_preferences;
extern GSettings *gnome_privacy_preferences;

G_END_DECLS

#endif /* MAUTILUS_GLOBAL_PREFERENCES_H */
