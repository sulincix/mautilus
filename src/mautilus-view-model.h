#ifndef MAUTILUS_VIEW_MODEL_H
#define MAUTILUS_VIEW_MODEL_H

#include <glib.h>
#include "mautilus-file.h"
#include "mautilus-view-item-model.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_VIEW_MODEL (mautilus_view_model_get_type())

G_DECLARE_FINAL_TYPE (mautilusViewModel, mautilus_view_model, MAUTILUS, VIEW_MODEL, GObject)

typedef struct
{
    mautilusFileSortType sort_type;
    gboolean reversed;
    gboolean directories_first;
} mautilusViewModelSortData;

mautilusViewModel * mautilus_view_model_new (void);

void mautilus_view_model_set_sort_type (mautilusViewModel         *self,
                                        mautilusViewModelSortData *sort_data);
mautilusViewModelSortData * mautilus_view_model_get_sort_type (mautilusViewModel *self);
GListStore * mautilus_view_model_get_g_model (mautilusViewModel *self);
mautilusViewItemModel * mautilus_view_model_get_item_from_file (mautilusViewModel *self,
                                                                mautilusFile      *file);
GQueue * mautilus_view_model_get_items_from_files (mautilusViewModel *self,
                                                   GQueue            *files);
/* Don't use inside a loop, use mautilus_view_model_remove_all_items instead. */
void mautilus_view_model_remove_item (mautilusViewModel     *self,
                                      mautilusViewItemModel *item);
void mautilus_view_model_remove_all_items (mautilusViewModel *self);
/* Don't use inside a loop, use mautilus_view_model_add_items instead. */
void mautilus_view_model_add_item (mautilusViewModel     *self,
                                   mautilusViewItemModel *item);
void mautilus_view_model_add_items (mautilusViewModel *self,
                                    GQueue            *items);
void mautilus_view_model_set_items (mautilusViewModel *self,
                                    GQueue            *items);
G_END_DECLS

#endif /* MAUTILUS_VIEW_MODEL_H */

