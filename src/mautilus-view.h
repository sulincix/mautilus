/* mautilus-view.h
 *
 * Copyright (C) 2015 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef MAUTILUS_VIEW_H
#define MAUTILUS_VIEW_H

#include <glib.h>
#include <gtk/gtk.h>

#include "mautilus-query.h"
#include "mautilus-toolbar-menu-sections.h"

enum
{
    MAUTILUS_VIEW_GRID_ID,
    MAUTILUS_VIEW_LIST_ID,
    MAUTILUS_VIEW_DESKTOP_ID,
    MAUTILUS_VIEW_EMPTY_ID,
    MAUTILUS_VIEW_OTHER_LOCATIONS_ID,
    MAUTILUS_VIEW_INVALID_ID,
};

G_BEGIN_DECLS

#define MAUTILUS_TYPE_VIEW (mautilus_view_get_type ())

G_DECLARE_INTERFACE (mautilusView, mautilus_view, MAUTILUS, VIEW, GtkWidget)

struct _mautilusViewInterface
{
        GTypeInterface parent;

        guint                           (*get_view_id)               (mautilusView         *view);
        /*
         * Returns the menu sections that should be shown in the toolbar menu
         * when this view is active. Implementations must not return %NULL
         */
        mautilusToolbarMenuSections *   (*get_toolbar_menu_sections) (mautilusView         *view);

        /* Current location of the view */
        GFile*                          (*get_location)              (mautilusView         *view);
        void                            (*set_location)              (mautilusView         *view,
                                                                      GFile                *location);

        /* Selection */
        GList*                          (*get_selection)             (mautilusView         *view);
        void                            (*set_selection)             (mautilusView         *view,
                                                                      GList                *selection);

        /* Search */
        mautilusQuery*                  (*get_search_query)          (mautilusView         *view);
        void                            (*set_search_query)          (mautilusView         *view,
                                                                      mautilusQuery        *query);

        /* Whether the current view is loading the location */
        gboolean                        (*is_loading)                (mautilusView         *view);

        /* Whether the current view is searching or not */
        gboolean                        (*is_searching)              (mautilusView         *view);
};

GIcon *                        mautilus_view_get_icon                  (guint                 view_id);

guint                          mautilus_view_get_view_id               (mautilusView         *view);

mautilusToolbarMenuSections *  mautilus_view_get_toolbar_menu_sections (mautilusView         *view);

GFile *                        mautilus_view_get_location              (mautilusView         *view);

void                           mautilus_view_set_location              (mautilusView         *view,
                                                                        GFile                *location);

GList *                        mautilus_view_get_selection             (mautilusView         *view);

void                           mautilus_view_set_selection             (mautilusView         *view,
                                                                        GList                *selection);

mautilusQuery *                mautilus_view_get_search_query          (mautilusView         *view);

void                           mautilus_view_set_search_query          (mautilusView         *view,
                                                                        mautilusQuery        *query);

gboolean                       mautilus_view_is_loading                (mautilusView         *view);

gboolean                       mautilus_view_is_searching              (mautilusView         *view);

G_END_DECLS

#endif /* MAUTILUS_VIEW_H */
