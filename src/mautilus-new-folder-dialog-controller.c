/* mautilus-new-folder-dialog-controller.c
 *
 * Copyright (C) 2016 the mautilus developers
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <glib/gi18n.h>

#include <eel/eel-vfs-extensions.h>

#include "mautilus-new-folder-dialog-controller.h"


struct _mautilusNewFolderDialogController
{
    mautilusFileNameWidgetController parent_instance;

    GtkWidget *new_folder_dialog;

    gboolean with_selection;

    gint response_handler_id;
};

G_DEFINE_TYPE (mautilusNewFolderDialogController, mautilus_new_folder_dialog_controller, MAUTILUS_TYPE_FILE_NAME_WIDGET_CONTROLLER)

static gboolean
mautilus_new_folder_dialog_controller_name_is_valid (mautilusFileNameWidgetController  *self,
                                                     gchar                             *name,
                                                     gchar                            **error_message)
{
    if (strlen (name) == 0)
    {
        return FALSE;
    }

    if (strstr (name, "/") != NULL)
    {
        *error_message = _("Folder names cannot contain “/”.");
    }
    else if (strcmp (name, ".") == 0)
    {
        *error_message = _("A folder cannot be called “.”.");
    }
    else if (strcmp (name, "..") == 0)
    {
        *error_message = _("A folder cannot be called “..”.");
    }

    return *error_message == NULL;
}

static void
new_folder_dialog_controller_on_response (GtkDialog *dialog,
                                          gint       response_id,
                                          gpointer   user_data)
{
    mautilusNewFolderDialogController *controller;

    controller = MAUTILUS_NEW_FOLDER_DIALOG_CONTROLLER (user_data);

    if (response_id != GTK_RESPONSE_OK)
    {
        g_signal_emit_by_name (controller, "cancelled");
    }
}

mautilusNewFolderDialogController *
mautilus_new_folder_dialog_controller_new (GtkWindow         *parent_window,
                                           mautilusDirectory *destination_directory,
                                           gboolean           with_selection,
                                           gchar             *initial_name)
{
    mautilusNewFolderDialogController *self;
    g_autoptr (GtkBuilder) builder = NULL;
    GtkWidget *new_folder_dialog;
    GtkWidget *error_revealer;
    GtkWidget *error_label;
    GtkWidget *name_entry;
    GtkWidget *activate_button;
    GtkWidget *name_label;

    builder = gtk_builder_new_from_resource ("/org/gnome/mautilus/ui/mautilus-create-folder-dialog.ui");
    new_folder_dialog = GTK_WIDGET (gtk_builder_get_object (builder, "create_folder_dialog"));
    error_revealer = GTK_WIDGET (gtk_builder_get_object (builder, "error_revealer"));
    error_label = GTK_WIDGET (gtk_builder_get_object (builder, "error_label"));
    name_entry = GTK_WIDGET (gtk_builder_get_object (builder, "name_entry"));
    activate_button = GTK_WIDGET (gtk_builder_get_object (builder, "ok_button"));
    name_label = GTK_WIDGET (gtk_builder_get_object (builder, "name_label"));

    gtk_window_set_transient_for (GTK_WINDOW (new_folder_dialog),
                                  parent_window);

    self = g_object_new (MAUTILUS_TYPE_NEW_FOLDER_DIALOG_CONTROLLER,
                         "error-revealer", error_revealer,
                         "error-label", error_label,
                         "name-entry", name_entry,
                         "activate-button", activate_button,
                         "containing-directory", destination_directory, NULL);

    self->with_selection = with_selection;

    self->new_folder_dialog = new_folder_dialog;

    self->response_handler_id = g_signal_connect (new_folder_dialog,
                                                  "response",
                                                  (GCallback) new_folder_dialog_controller_on_response,
                                                  self);

    if (initial_name != NULL)
    {
        gtk_entry_set_text (GTK_ENTRY (name_entry), initial_name);
    }

    gtk_button_set_label (GTK_BUTTON (activate_button), _("Create"));
    gtk_label_set_text (GTK_LABEL (name_label), _("Folder name"));
    gtk_window_set_title (GTK_WINDOW (new_folder_dialog), _("New Folder"));

    gtk_widget_show_all (new_folder_dialog);

    return self;
}

gboolean
mautilus_new_folder_dialog_controller_get_with_selection (mautilusNewFolderDialogController *self)
{
    return self->with_selection;
}

static void
mautilus_new_folder_dialog_controller_init (mautilusNewFolderDialogController *self)
{
}

static void
mautilus_new_folder_dialog_controller_finalize (GObject *object)
{
    mautilusNewFolderDialogController *self;

    self = MAUTILUS_NEW_FOLDER_DIALOG_CONTROLLER (object);

    if (self->new_folder_dialog != NULL)
    {
        if (self->response_handler_id)
        {
            g_signal_handler_disconnect (self->new_folder_dialog,
                                         self->response_handler_id);
            self->response_handler_id = 0;
        }
        gtk_widget_destroy (self->new_folder_dialog);
        self->new_folder_dialog = NULL;
    }

    G_OBJECT_CLASS (mautilus_new_folder_dialog_controller_parent_class)->finalize (object);
}

static void
mautilus_new_folder_dialog_controller_class_init (mautilusNewFolderDialogControllerClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    mautilusFileNameWidgetControllerClass *parent_class = MAUTILUS_FILE_NAME_WIDGET_CONTROLLER_CLASS (klass);

    object_class->finalize = mautilus_new_folder_dialog_controller_finalize;

    parent_class->name_is_valid = mautilus_new_folder_dialog_controller_name_is_valid;
}
