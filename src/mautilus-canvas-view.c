/* fm-canvas-view.c - implementation of canvas view of directory.
 *
 *  Copyright (C) 2000, 2001 Eazel, Inc.
 *
 *  The Gnome Library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public License as
 *  published by the Free Software Foundation; either version 2 of the
 *  License, or (at your option) any later version.
 *
 *  The Gnome Library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with the Gnome Library; see the file COPYING.LIB.  If not,
 *  see <http://www.gnu.org/licenses/>.
 *
 *  Authors: John Sullivan <sullivan@eazel.com>
 */

#include <config.h>

#include "mautilus-canvas-view.h"

#include "mautilus-canvas-view-container.h"
#include "mautilus-error-reporting.h"
#include "mautilus-files-view-dnd.h"
#include "mautilus-toolbar.h"
#include "mautilus-view.h"

#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <gtk/gtk.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include "mautilus-directory.h"
#include "mautilus-dnd.h"
#include "mautilus-file-utilities.h"
#include "mautilus-ui-utilities.h"
#include "mautilus-global-preferences.h"
#include "mautilus-canvas-container.h"
#include "mautilus-canvas-dnd.h"
#include "mautilus-link.h"
#include "mautilus-metadata.h"
#include "mautilus-clipboard.h"

#define DEBUG_FLAG MAUTILUS_DEBUG_CANVAS_VIEW
#include "mautilus-debug.h"

#include <locale.h>
#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

enum
{
    PROP_SUPPORTS_AUTO_LAYOUT = 1,
    PROP_SUPPORTS_SCALING,
    PROP_SUPPORTS_KEEP_ALIGNED,
    PROP_SUPPORTS_MANUAL_LAYOUT,
    NUM_PROPERTIES
};

static GParamSpec *properties[NUM_PROPERTIES] = { NULL, };

typedef gboolean (*SortCriterionMatchFunc) (mautilusFile *file);

typedef struct
{
    const mautilusFileSortType sort_type;
    const char *metadata_text;
    const char *action_target_name;
    const gboolean reverse_order;
    SortCriterionMatchFunc match_func;
} SortCriterion;

typedef enum
{
    MENU_ITEM_TYPE_STANDARD,
    MENU_ITEM_TYPE_CHECK,
    MENU_ITEM_TYPE_RADIO,
    MENU_ITEM_TYPE_TREE
} MenuItemType;

typedef struct
{
    GList *icons_not_positioned;

    guint react_to_canvas_change_idle_id;

    const SortCriterion *sort;

    GtkWidget *canvas_container;

    gboolean supports_auto_layout;
    gboolean supports_manual_layout;
    gboolean supports_scaling;
    gboolean supports_keep_aligned;

    /* FIXME: Needed for async operations. Suposedly we would use cancellable and gtask,
     * sadly gtkclipboard doesn't support that.
     * We follow this pattern for checking validity of the object in the views.
     * Ideally we would connect to a weak reference and do a cancellable.
     */
    gboolean destroyed;
} mautilusCanvasViewPrivate;

/* Note that the first item in this list is the default sort,
 * and that the items show up in the menu in the order they
 * appear in this list.
 */
static const SortCriterion sort_criteria[] =
{
    {
        MAUTILUS_FILE_SORT_BY_DISPLAY_NAME,
        "name",
        "name",
        FALSE
    },
    {
        MAUTILUS_FILE_SORT_BY_DISPLAY_NAME,
        "name",
        "name-desc",
        TRUE
    },
    {
        MAUTILUS_FILE_SORT_BY_SIZE,
        "size",
        "size",
        TRUE
    },
    {
        MAUTILUS_FILE_SORT_BY_TYPE,
        "type",
        "type",
        FALSE
    },
    {
        MAUTILUS_FILE_SORT_BY_MTIME,
        "modification date",
        "modification-date",
        FALSE
    },
    {
        MAUTILUS_FILE_SORT_BY_MTIME,
        "modification date",
        "modification-date-desc",
        TRUE
    },
    {
        MAUTILUS_FILE_SORT_BY_ATIME,
        "access date",
        "access-date",
        FALSE
    },
    {
        MAUTILUS_FILE_SORT_BY_ATIME,
        "access date",
        "access-date-desc",
        TRUE
    },
    {
        MAUTILUS_FILE_SORT_BY_TRASHED_TIME,
        "trashed",
        "trash-time",
        TRUE,
        mautilus_file_is_in_trash
    },
    {
        MAUTILUS_FILE_SORT_BY_SEARCH_RELEVANCE,
        NULL,
        "search-relevance",
        TRUE,
        mautilus_file_is_in_search
    },
    {
        MAUTILUS_FILE_SORT_BY_RECENCY,
        NULL,
        "recency",
        TRUE,
        mautilus_file_is_in_recent
    }
};

G_DEFINE_TYPE_WITH_PRIVATE (mautilusCanvasView, mautilus_canvas_view, MAUTILUS_TYPE_FILES_VIEW);

static void                 mautilus_canvas_view_set_directory_sort_by (mautilusCanvasView  *canvas_view,
                                                                        mautilusFile        *file,
                                                                        const SortCriterion *sort);
static void                 mautilus_canvas_view_update_click_mode (mautilusCanvasView *canvas_view);
static gboolean             mautilus_canvas_view_supports_scaling (mautilusCanvasView *canvas_view);
static void                 mautilus_canvas_view_reveal_selection (mautilusFilesView *view);
static const SortCriterion *get_sort_criterion_by_metadata_text (const char *metadata_text,
                                                                 gboolean    reversed);
static const SortCriterion *get_sort_criterion_by_sort_type (mautilusFileSortType sort_type,
                                                             gboolean             reversed);
static void                 switch_to_manual_layout (mautilusCanvasView *view);
static const SortCriterion *get_default_sort_order (mautilusFile *file);
static void                 mautilus_canvas_view_clear (mautilusFilesView *view);
static void on_clipboard_owner_changed (GtkClipboard *clipboard,
                                        GdkEvent     *event,
                                        gpointer      user_data);

static void
mautilus_canvas_view_destroy (GtkWidget *object)
{
    mautilusCanvasView *canvas_view;
    mautilusCanvasViewPrivate *priv;
    GtkClipboard *clipboard;

    canvas_view = MAUTILUS_CANVAS_VIEW (object);
    priv = mautilus_canvas_view_get_instance_private (canvas_view);

    mautilus_canvas_view_clear (MAUTILUS_FILES_VIEW (object));

    if (priv->react_to_canvas_change_idle_id != 0)
    {
        g_source_remove (priv->react_to_canvas_change_idle_id);
        priv->react_to_canvas_change_idle_id = 0;
    }

    clipboard = gtk_clipboard_get (GDK_SELECTION_CLIPBOARD);
    g_signal_handlers_disconnect_by_func (clipboard,
                                          on_clipboard_owner_changed,
                                          canvas_view);

    if (priv->icons_not_positioned)
    {
        mautilus_file_list_free (priv->icons_not_positioned);
        priv->icons_not_positioned = NULL;
    }

    GTK_WIDGET_CLASS (mautilus_canvas_view_parent_class)->destroy (object);
}

static mautilusCanvasContainer *
get_canvas_container (mautilusCanvasView *canvas_view)
{
    mautilusCanvasViewPrivate *priv;

    priv = mautilus_canvas_view_get_instance_private (canvas_view);

    return MAUTILUS_CANVAS_CONTAINER (priv->canvas_container);
}

mautilusCanvasContainer *
mautilus_canvas_view_get_canvas_container (mautilusCanvasView *canvas_view)
{
    return get_canvas_container (canvas_view);
}

static gboolean
mautilus_canvas_view_supports_manual_layout (mautilusCanvasView *view)
{
    mautilusCanvasViewPrivate *priv;

    g_return_val_if_fail (MAUTILUS_IS_CANVAS_VIEW (view), FALSE);

    priv = mautilus_canvas_view_get_instance_private (view);

    return priv->supports_manual_layout;
}

static gboolean
get_stored_icon_position_callback (mautilusCanvasContainer *container,
                                   mautilusFile            *file,
                                   mautilusCanvasPosition  *position,
                                   mautilusCanvasView      *canvas_view)
{
    char *position_string, *scale_string;
    gboolean position_good;
    char c;

    g_assert (MAUTILUS_IS_CANVAS_CONTAINER (container));
    g_assert (MAUTILUS_IS_FILE (file));
    g_assert (position != NULL);
    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));

    if (!mautilus_canvas_view_supports_manual_layout (canvas_view))
    {
        return FALSE;
    }

    /* Get the current position of this canvas from the metadata. */
    position_string = mautilus_file_get_metadata
                          (file, MAUTILUS_METADATA_KEY_ICON_POSITION, "");
    position_good = sscanf
                        (position_string, " %d , %d %c",
                        &position->x, &position->y, &c) == 2;
    g_free (position_string);

    /* If it is the desktop directory, maybe the gnome-libs metadata has information about it */

    /* Disable scaling if not on the desktop */
    if (mautilus_canvas_view_supports_scaling (canvas_view))
    {
        /* Get the scale of the canvas from the metadata. */
        scale_string = mautilus_file_get_metadata
                           (file, MAUTILUS_METADATA_KEY_ICON_SCALE, "1");
        position->scale = g_ascii_strtod (scale_string, NULL);
        if (errno != 0)
        {
            position->scale = 1.0;
        }

        g_free (scale_string);
    }
    else
    {
        position->scale = 1.0;
    }

    return position_good;
}

static void
update_sort_criterion (mautilusCanvasView  *canvas_view,
                       const SortCriterion *sort,
                       gboolean             set_metadata)
{
    mautilusFile *file;
    mautilusCanvasViewPrivate *priv;
    const SortCriterion *overrided_sort_criterion;

    file = mautilus_files_view_get_directory_as_file (MAUTILUS_FILES_VIEW (canvas_view));
    priv = mautilus_canvas_view_get_instance_private (canvas_view);

    /* Make sure we use the default one and not one that the user used previously
     * of the change to not allow sorting on search and recent, or the
     * case that the user or some app modified directly the metadata */
    if (mautilus_file_is_in_search (file) || mautilus_file_is_in_recent (file))
    {
        overrided_sort_criterion = get_default_sort_order (file);
    }
    else if (sort != NULL && priv->sort != sort)
    {
        overrided_sort_criterion = sort;
        if (set_metadata)
        {
            /* Store the new sort setting. */
            mautilus_canvas_view_set_directory_sort_by (canvas_view,
                                                        file,
                                                        sort);
        }
    }
    else
    {
        return;
    }

    priv->sort = overrided_sort_criterion;
}

void
mautilus_canvas_view_clean_up_by_name (mautilusCanvasView *canvas_view)
{
    mautilusCanvasContainer *canvas_container;

    canvas_container = get_canvas_container (canvas_view);

    update_sort_criterion (canvas_view, &sort_criteria[0], FALSE);

    mautilus_canvas_container_sort (canvas_container);
    mautilus_canvas_container_freeze_icon_positions (canvas_container);
}

static gboolean
mautilus_canvas_view_using_auto_layout (mautilusCanvasView *canvas_view)
{
    return mautilus_canvas_container_is_auto_layout
               (get_canvas_container (canvas_view));
}

static void
list_covers (mautilusCanvasIconData *data,
             gpointer                callback_data)
{
    GSList **file_list;

    file_list = callback_data;

    *file_list = g_slist_prepend (*file_list, data);
}

static void
unref_cover (mautilusCanvasIconData *data,
             gpointer                callback_data)
{
    mautilus_file_unref (MAUTILUS_FILE (data));
}

static void
mautilus_canvas_view_clear (mautilusFilesView *view)
{
    mautilusCanvasContainer *canvas_container;
    GSList *file_list;

    g_return_if_fail (MAUTILUS_IS_CANVAS_VIEW (view));

    canvas_container = get_canvas_container (MAUTILUS_CANVAS_VIEW (view));
    if (!canvas_container)
    {
        return;
    }

    /* Clear away the existing icons. */
    file_list = NULL;
    mautilus_canvas_container_for_each (canvas_container, list_covers, &file_list);
    mautilus_canvas_container_clear (canvas_container);
    g_slist_foreach (file_list, (GFunc) unref_cover, NULL);
    g_slist_free (file_list);
}

static void
mautilus_canvas_view_remove_file (mautilusFilesView *view,
                                  mautilusFile      *file,
                                  mautilusDirectory *directory)
{
    mautilusCanvasView *canvas_view;

    /* This used to assert that 'directory == mautilus_files_view_get_model (view)', but that
     * resulted in a lot of crash reports (bug #352592). I don't see how that trace happens.
     * It seems that somehow we get a files_changed event sent to the view from a directory
     * that isn't the model, but the code disables the monitor and signal callback handlers when
     * changing directories. Maybe we can get some more information when this happens.
     * Further discussion in bug #368178.
     */
    if (directory != mautilus_files_view_get_model (view))
    {
        char *file_uri, *dir_uri, *model_uri;
        file_uri = mautilus_file_get_uri (file);
        dir_uri = mautilus_directory_get_uri (directory);
        model_uri = mautilus_directory_get_uri (mautilus_files_view_get_model (view));
        g_warning ("mautilus_canvas_view_remove_file() - directory not canvas view model, shouldn't happen.\n"
                   "file: %p:%s, dir: %p:%s, model: %p:%s, view loading: %d\n"
                   "If you see this, please add this info to http://bugzilla.gnome.org/show_bug.cgi?id=368178",
                   file, file_uri, directory, dir_uri, mautilus_files_view_get_model (view), model_uri, mautilus_files_view_get_loading (view));
        g_free (file_uri);
        g_free (dir_uri);
        g_free (model_uri);
    }

    canvas_view = MAUTILUS_CANVAS_VIEW (view);

    if (mautilus_canvas_container_remove (get_canvas_container (canvas_view),
                                          MAUTILUS_CANVAS_ICON_DATA (file)))
    {
        mautilus_file_unref (file);
    }
}

static void
mautilus_canvas_view_add_files (mautilusFilesView *view,
                                GList             *files)
{
    mautilusCanvasView *canvas_view;
    mautilusCanvasContainer *canvas_container;
    GList *l;

    canvas_view = MAUTILUS_CANVAS_VIEW (view);
    canvas_container = get_canvas_container (canvas_view);

    /* Reset scroll region for the first canvas added when loading a directory. */
    if (mautilus_files_view_get_loading (view) && mautilus_canvas_container_is_empty (canvas_container))
    {
        mautilus_canvas_container_reset_scroll_region (canvas_container);
    }

    for (l = files; l != NULL; l = l->next)
    {
        if (mautilus_canvas_container_add (canvas_container,
                                           MAUTILUS_CANVAS_ICON_DATA (l->data)))
        {
            mautilus_file_ref (MAUTILUS_FILE (l->data));
        }
    }
}

static void
mautilus_canvas_view_file_changed (mautilusFilesView *view,
                                   mautilusFile      *file,
                                   mautilusDirectory *directory)
{
    mautilusCanvasView *canvas_view;

    g_assert (directory == mautilus_files_view_get_model (view));

    g_return_if_fail (view != NULL);
    canvas_view = MAUTILUS_CANVAS_VIEW (view);

    mautilus_canvas_container_request_update
        (get_canvas_container (canvas_view),
        MAUTILUS_CANVAS_ICON_DATA (file));
}

static gboolean
mautilus_canvas_view_supports_auto_layout (mautilusCanvasView *view)
{
    mautilusCanvasViewPrivate *priv;

    g_return_val_if_fail (MAUTILUS_IS_CANVAS_VIEW (view), FALSE);

    priv = mautilus_canvas_view_get_instance_private (view);

    return priv->supports_auto_layout;
}

static gboolean
mautilus_canvas_view_supports_scaling (mautilusCanvasView *view)
{
    mautilusCanvasViewPrivate *priv;

    g_return_val_if_fail (MAUTILUS_IS_CANVAS_VIEW (view), FALSE);

    priv = mautilus_canvas_view_get_instance_private (view);

    return priv->supports_scaling;
}

static gboolean
mautilus_canvas_view_supports_keep_aligned (mautilusCanvasView *view)
{
    mautilusCanvasViewPrivate *priv;

    g_return_val_if_fail (MAUTILUS_IS_CANVAS_VIEW (view), FALSE);

    priv = mautilus_canvas_view_get_instance_private (view);

    return priv->supports_keep_aligned;
}

static const SortCriterion *
mautilus_canvas_view_get_directory_sort_by (mautilusCanvasView *canvas_view,
                                            mautilusFile       *file)
{
    const SortCriterion *default_sort;
    g_autofree char *sort_by = NULL;
    gboolean reversed;

    if (!mautilus_canvas_view_supports_auto_layout (canvas_view))
    {
        return get_sort_criterion_by_metadata_text ("name", FALSE);
    }

    default_sort = get_default_sort_order (file);
    g_return_val_if_fail (default_sort != NULL, NULL);

    sort_by = mautilus_file_get_metadata (file,
                                          MAUTILUS_METADATA_KEY_ICON_VIEW_SORT_BY,
                                          default_sort->metadata_text);

    reversed = mautilus_file_get_boolean_metadata (file,
                                                   MAUTILUS_METADATA_KEY_ICON_VIEW_SORT_REVERSED,
                                                   default_sort->reverse_order);

    return get_sort_criterion_by_metadata_text (sort_by, reversed);
}

static const SortCriterion *
get_default_sort_order (mautilusFile *file)
{
    mautilusFileSortType sort_type, default_sort_order;
    gboolean reversed;

    default_sort_order = g_settings_get_enum (mautilus_preferences,
                                              MAUTILUS_PREFERENCES_DEFAULT_SORT_ORDER);
    reversed = g_settings_get_boolean (mautilus_preferences,
                                       MAUTILUS_PREFERENCES_DEFAULT_SORT_IN_REVERSE_ORDER);

    /* If this is a special folder (e.g. search or recent), override the sort
     * order and reversed flag with values appropriate for the folder */
    sort_type = mautilus_file_get_default_sort_type (file, &reversed);

    if (sort_type == MAUTILUS_FILE_SORT_NONE)
    {
        sort_type = CLAMP (default_sort_order,
                           MAUTILUS_FILE_SORT_BY_DISPLAY_NAME,
                           MAUTILUS_FILE_SORT_BY_ATIME);
    }

    return get_sort_criterion_by_sort_type (sort_type, reversed);
}

static void
mautilus_canvas_view_set_directory_sort_by (mautilusCanvasView  *canvas_view,
                                            mautilusFile        *file,
                                            const SortCriterion *sort)
{
    const SortCriterion *default_sort_criterion;

    if (!mautilus_canvas_view_supports_auto_layout (canvas_view))
    {
        return;
    }

    default_sort_criterion = get_default_sort_order (file);
    g_return_if_fail (default_sort_criterion != NULL);

    mautilus_file_set_metadata
        (file, MAUTILUS_METADATA_KEY_ICON_VIEW_SORT_BY,
        default_sort_criterion->metadata_text,
        sort->metadata_text);
    mautilus_file_set_boolean_metadata (file,
                                        MAUTILUS_METADATA_KEY_ICON_VIEW_SORT_REVERSED,
                                        default_sort_criterion->reverse_order,
                                        sort->reverse_order);
}

static gboolean
get_default_directory_keep_aligned (void)
{
    return TRUE;
}

static gboolean
mautilus_canvas_view_get_directory_keep_aligned (mautilusCanvasView *canvas_view,
                                                 mautilusFile       *file)
{
    if (!mautilus_canvas_view_supports_keep_aligned (canvas_view))
    {
        return FALSE;
    }

    return mautilus_file_get_boolean_metadata
               (file,
               MAUTILUS_METADATA_KEY_ICON_VIEW_KEEP_ALIGNED,
               get_default_directory_keep_aligned ());
}

static void
mautilus_canvas_view_set_directory_keep_aligned (mautilusCanvasView *canvas_view,
                                                 mautilusFile       *file,
                                                 gboolean            keep_aligned)
{
    if (!mautilus_canvas_view_supports_keep_aligned (canvas_view))
    {
        return;
    }

    mautilus_file_set_boolean_metadata
        (file, MAUTILUS_METADATA_KEY_ICON_VIEW_KEEP_ALIGNED,
        get_default_directory_keep_aligned (),
        keep_aligned);
}

static gboolean
mautilus_canvas_view_get_directory_auto_layout (mautilusCanvasView *canvas_view,
                                                mautilusFile       *file)
{
    if (!mautilus_canvas_view_supports_auto_layout (canvas_view))
    {
        return FALSE;
    }

    if (!mautilus_canvas_view_supports_manual_layout (canvas_view))
    {
        return TRUE;
    }

    return mautilus_file_get_boolean_metadata
               (file, MAUTILUS_METADATA_KEY_ICON_VIEW_AUTO_LAYOUT, TRUE);
}

static void
mautilus_canvas_view_set_directory_auto_layout (mautilusCanvasView *canvas_view,
                                                mautilusFile       *file,
                                                gboolean            auto_layout)
{
    if (!mautilus_canvas_view_supports_auto_layout (canvas_view) ||
        !mautilus_canvas_view_supports_manual_layout (canvas_view))
    {
        return;
    }

    mautilus_file_set_boolean_metadata
        (file, MAUTILUS_METADATA_KEY_ICON_VIEW_AUTO_LAYOUT,
        TRUE,
        auto_layout);
}

static const SortCriterion *
get_sort_criterion_by_metadata_text (const char *metadata_text,
                                     gboolean    reversed)
{
    guint i;

    /* Figure out what the new sort setting should be. */
    for (i = 0; i < G_N_ELEMENTS (sort_criteria); i++)
    {
        if (g_strcmp0 (sort_criteria[i].metadata_text, metadata_text) == 0
            && reversed == sort_criteria[i].reverse_order)
        {
            return &sort_criteria[i];
        }
    }
    return &sort_criteria[0];
}

static const SortCriterion *
get_sort_criterion_by_action_target_name (const char *action_target_name)
{
    guint i;
    /* Figure out what the new sort setting should be. */
    for (i = 0; i < G_N_ELEMENTS (sort_criteria); i++)
    {
        if (g_strcmp0 (sort_criteria[i].action_target_name, action_target_name) == 0)
        {
            return &sort_criteria[i];
        }
    }
    return NULL;
}

static const SortCriterion *
get_sort_criterion_by_sort_type (mautilusFileSortType sort_type,
                                 gboolean             reversed)
{
    guint i;

    /* Figure out what the new sort setting should be. */
    for (i = 0; i < G_N_ELEMENTS (sort_criteria); i++)
    {
        if (sort_type == sort_criteria[i].sort_type
            && reversed == sort_criteria[i].reverse_order)
        {
            return &sort_criteria[i];
        }
    }

    return &sort_criteria[0];
}

static mautilusCanvasZoomLevel
get_default_zoom_level (mautilusCanvasView *canvas_view)
{
    mautilusCanvasZoomLevel default_zoom_level;

    default_zoom_level = g_settings_get_enum (mautilus_icon_view_preferences,
                                              MAUTILUS_PREFERENCES_ICON_VIEW_DEFAULT_ZOOM_LEVEL);

    return CLAMP (default_zoom_level, MAUTILUS_CANVAS_ZOOM_LEVEL_SMALL, MAUTILUS_CANVAS_ZOOM_LEVEL_LARGER);
}

static void
mautilus_canvas_view_begin_loading (mautilusFilesView *view)
{
    mautilusCanvasView *canvas_view;
    GtkWidget *canvas_container;
    mautilusFile *file;
    char *uri;
    const SortCriterion *sort;

    g_return_if_fail (MAUTILUS_IS_CANVAS_VIEW (view));

    canvas_view = MAUTILUS_CANVAS_VIEW (view);
    file = mautilus_files_view_get_directory_as_file (view);
    uri = mautilus_file_get_uri (file);
    canvas_container = GTK_WIDGET (get_canvas_container (canvas_view));

    mautilus_canvas_container_begin_loading (MAUTILUS_CANVAS_CONTAINER (canvas_container));

    g_free (uri);

    /* Set the sort mode.
     * It's OK not to resort the icons because the
     * container doesn't have any icons at this point.
     */
    sort = mautilus_canvas_view_get_directory_sort_by (canvas_view, file);
    update_sort_criterion (canvas_view, sort, FALSE);

    mautilus_canvas_container_set_keep_aligned
        (get_canvas_container (canvas_view),
        mautilus_canvas_view_get_directory_keep_aligned (canvas_view, file));

    /* We must set auto-layout last, because it invokes the layout_changed
     * callback, which works incorrectly if the other layout criteria are
     * not already set up properly (see bug 6500, e.g.)
     */
    mautilus_canvas_container_set_auto_layout
        (get_canvas_container (canvas_view),
        mautilus_canvas_view_get_directory_auto_layout (canvas_view, file));

    /* We could have changed to the trash directory or to searching, and then
     * we need to update the menus */
    mautilus_files_view_update_context_menus (view);
    mautilus_files_view_update_toolbar_menus (view);
}

static void
on_clipboard_contents_received (GtkClipboard     *clipboard,
                                GtkSelectionData *selection_data,
                                gpointer          user_data)
{
    mautilusCanvasViewPrivate *priv;
    mautilusCanvasView *view = MAUTILUS_CANVAS_VIEW (user_data);

    priv = mautilus_canvas_view_get_instance_private (view);

    if (priv->destroyed)
    {
        /* We've been destroyed since call */
        g_object_unref (view);
        return;
    }

    if (mautilus_clipboard_is_cut_from_selection_data (selection_data))
    {
        GList *uris;
        GList *files;

        uris = mautilus_clipboard_get_uri_list_from_selection_data (selection_data);
        files = mautilus_file_list_from_uri_list (uris);
        mautilus_canvas_container_set_highlighted_for_clipboard (get_canvas_container (view),
                                                                 files);

        mautilus_file_list_free (files);
        g_list_free_full (uris, g_free);
    }
    else
    {
        mautilus_canvas_container_set_highlighted_for_clipboard (get_canvas_container (view),
                                                                 NULL);
    }

    g_object_unref (view);
}

static void
update_clipboard_status (mautilusCanvasView *view)
{
    g_object_ref (view);     /* Need to keep the object alive until we get the reply */
    gtk_clipboard_request_contents (mautilus_clipboard_get (GTK_WIDGET (view)),
                                    mautilus_clipboard_get_atom (),
                                    on_clipboard_contents_received,
                                    view);
}

static void
on_clipboard_owner_changed (GtkClipboard *clipboard,
                            GdkEvent     *event,
                            gpointer      user_data)
{
    update_clipboard_status (MAUTILUS_CANVAS_VIEW (user_data));
}

static void
mautilus_canvas_view_end_loading (mautilusFilesView *view,
                                  gboolean           all_files_seen)
{
    mautilusCanvasView *canvas_view;

    canvas_view = MAUTILUS_CANVAS_VIEW (view);
    mautilus_canvas_container_end_loading (mautilus_canvas_view_get_canvas_container (canvas_view),
                                           all_files_seen);
    update_clipboard_status (canvas_view);
}

static mautilusCanvasZoomLevel
mautilus_canvas_view_get_zoom_level (mautilusFilesView *view)
{
    g_return_val_if_fail (MAUTILUS_IS_CANVAS_VIEW (view), MAUTILUS_CANVAS_ZOOM_LEVEL_LARGE);

    return mautilus_canvas_container_get_zoom_level (get_canvas_container (MAUTILUS_CANVAS_VIEW (view)));
}

static void
mautilus_canvas_view_zoom_to_level (mautilusFilesView *view,
                                    gint               new_level)
{
    mautilusCanvasView *canvas_view;
    mautilusCanvasContainer *canvas_container;

    g_return_if_fail (MAUTILUS_IS_CANVAS_VIEW (view));
    g_return_if_fail (new_level >= MAUTILUS_CANVAS_ZOOM_LEVEL_SMALL &&
                      new_level <= MAUTILUS_CANVAS_ZOOM_LEVEL_LARGER);

    canvas_view = MAUTILUS_CANVAS_VIEW (view);
    canvas_container = get_canvas_container (canvas_view);
    if (mautilus_canvas_container_get_zoom_level (canvas_container) == new_level)
    {
        return;
    }

    mautilus_canvas_container_set_zoom_level (canvas_container, new_level);
    g_action_group_change_action_state (mautilus_files_view_get_action_group (view),
                                        "zoom-to-level", g_variant_new_int32 (new_level));

    mautilus_files_view_update_toolbar_menus (view);
}

static void
mautilus_canvas_view_bump_zoom_level (mautilusFilesView *view,
                                      int                zoom_increment)
{
    mautilusCanvasZoomLevel new_level;

    g_return_if_fail (MAUTILUS_IS_CANVAS_VIEW (view));
    if (!mautilus_files_view_supports_zooming (view))
    {
        return;
    }

    new_level = mautilus_canvas_view_get_zoom_level (view) + zoom_increment;

    if (new_level >= MAUTILUS_CANVAS_ZOOM_LEVEL_SMALL &&
        new_level <= MAUTILUS_CANVAS_ZOOM_LEVEL_LARGER)
    {
        mautilus_canvas_view_zoom_to_level (view, new_level);
    }
}

static void
mautilus_canvas_view_restore_standard_zoom_level (mautilusFilesView *view)
{
    mautilus_canvas_view_zoom_to_level (view, MAUTILUS_CANVAS_ZOOM_LEVEL_LARGE);
}

static gboolean
mautilus_canvas_view_can_zoom_in (mautilusFilesView *view)
{
    g_return_val_if_fail (MAUTILUS_IS_CANVAS_VIEW (view), FALSE);

    return mautilus_canvas_view_get_zoom_level (view)
           < MAUTILUS_CANVAS_ZOOM_LEVEL_LARGER;
}

static gboolean
mautilus_canvas_view_can_zoom_out (mautilusFilesView *view)
{
    g_return_val_if_fail (MAUTILUS_IS_CANVAS_VIEW (view), FALSE);

    return mautilus_canvas_view_get_zoom_level (view)
           > MAUTILUS_CANVAS_ZOOM_LEVEL_SMALL;
}

static gfloat
mautilus_canvas_view_get_zoom_level_percentage (mautilusFilesView *view)
{
    guint icon_size;
    mautilusCanvasZoomLevel zoom_level;

    zoom_level = mautilus_canvas_view_get_zoom_level (view);
    icon_size = mautilus_canvas_container_get_icon_size_for_zoom_level (zoom_level);

    return (gfloat) icon_size / MAUTILUS_CANVAS_ICON_SIZE_LARGE;
}

static gboolean
mautilus_canvas_view_is_zoom_level_default (mautilusFilesView *view)
{
    guint icon_size;
    mautilusCanvasZoomLevel zoom_level;

    zoom_level = mautilus_canvas_view_get_zoom_level (view);
    icon_size = mautilus_canvas_container_get_icon_size_for_zoom_level (zoom_level);

    return icon_size == MAUTILUS_CANVAS_ICON_SIZE_LARGE;
}

static gboolean
mautilus_canvas_view_is_empty (mautilusFilesView *view)
{
    g_assert (MAUTILUS_IS_CANVAS_VIEW (view));

    return mautilus_canvas_container_is_empty
               (get_canvas_container (MAUTILUS_CANVAS_VIEW (view)));
}

static GList *
mautilus_canvas_view_get_selection (mautilusFilesView *view)
{
    GList *list;

    g_return_val_if_fail (MAUTILUS_IS_CANVAS_VIEW (view), NULL);

    list = mautilus_canvas_container_get_selection
               (get_canvas_container (MAUTILUS_CANVAS_VIEW (view)));
    mautilus_file_list_ref (list);
    return list;
}

static void
action_keep_aligned (GSimpleAction *action,
                     GVariant      *state,
                     gpointer       user_data)
{
    mautilusFile *file;
    mautilusCanvasView *canvas_view;
    gboolean keep_aligned;

    canvas_view = MAUTILUS_CANVAS_VIEW (user_data);
    file = mautilus_files_view_get_directory_as_file (MAUTILUS_FILES_VIEW (canvas_view));
    keep_aligned = g_variant_get_boolean (state);

    mautilus_canvas_view_set_directory_keep_aligned (canvas_view,
                                                     file,
                                                     keep_aligned);
    mautilus_canvas_container_set_keep_aligned (get_canvas_container (canvas_view),
                                                keep_aligned);

    g_simple_action_set_state (action, state);
}

static void
action_sort_order_changed (GSimpleAction *action,
                           GVariant      *value,
                           gpointer       user_data)
{
    const gchar *target_name;
    const SortCriterion *sort_criterion;

    g_assert (MAUTILUS_IS_CANVAS_VIEW (user_data));

    target_name = g_variant_get_string (value, NULL);
    sort_criterion = get_sort_criterion_by_action_target_name (target_name);

    g_assert (sort_criterion != NULL);
    /* Note that id might be a toggle item.
     * Ignore non-sort ids so that they don't cause sorting.
     */
    if (sort_criterion->sort_type == MAUTILUS_FILE_SORT_NONE)
    {
        switch_to_manual_layout (user_data);
    }
    else
    {
        update_sort_criterion (user_data, sort_criterion, TRUE);

        mautilus_canvas_container_sort (get_canvas_container (user_data));
        mautilus_canvas_view_reveal_selection (MAUTILUS_FILES_VIEW (user_data));
    }

    g_simple_action_set_state (action, value);
}

static void
action_zoom_to_level (GSimpleAction *action,
                      GVariant      *state,
                      gpointer       user_data)
{
    mautilusFilesView *view;
    mautilusCanvasZoomLevel zoom_level;

    g_assert (MAUTILUS_IS_FILES_VIEW (user_data));

    view = MAUTILUS_FILES_VIEW (user_data);
    zoom_level = g_variant_get_int32 (state);
    mautilus_canvas_view_zoom_to_level (view, zoom_level);

    g_simple_action_set_state (G_SIMPLE_ACTION (action), state);
    if (g_settings_get_enum (mautilus_icon_view_preferences,
                             MAUTILUS_PREFERENCES_ICON_VIEW_DEFAULT_ZOOM_LEVEL) != zoom_level)
    {
        g_settings_set_enum (mautilus_icon_view_preferences,
                             MAUTILUS_PREFERENCES_ICON_VIEW_DEFAULT_ZOOM_LEVEL,
                             zoom_level);
    }
}

static void
switch_to_manual_layout (mautilusCanvasView *canvas_view)
{
    mautilusCanvasViewPrivate *priv;

    priv = mautilus_canvas_view_get_instance_private (canvas_view);

    if (!mautilus_canvas_view_using_auto_layout (canvas_view) ||
        !mautilus_files_view_is_editable (MAUTILUS_FILES_VIEW (canvas_view)))
    {
        return;
    }

    priv->sort = &sort_criteria[0];

    mautilus_canvas_container_set_auto_layout
        (get_canvas_container (canvas_view), FALSE);
}

static void
layout_changed_callback (mautilusCanvasContainer *container,
                         mautilusCanvasView      *canvas_view)
{
    mautilusFile *file;

    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));
    g_assert (container == get_canvas_container (canvas_view));

    file = mautilus_files_view_get_directory_as_file (MAUTILUS_FILES_VIEW (canvas_view));

    if (file != NULL)
    {
        mautilus_canvas_view_set_directory_auto_layout
            (canvas_view,
            file,
            mautilus_canvas_view_using_auto_layout (canvas_view));
    }
}

const GActionEntry canvas_view_entries[] =
{
    { "keep-aligned", NULL, NULL, "true", action_keep_aligned },
    { "sort", NULL, "s", "'name'", action_sort_order_changed },
    { "zoom-to-level", NULL, NULL, "1", action_zoom_to_level }
};

static void
update_sort_action_state_hint (mautilusCanvasView *canvas_view)
{
    mautilusFile *file;
    GVariantBuilder builder;
    GActionGroup *action_group;
    GAction *action;
    GVariant *state_hint;
    gint idx;

    file = mautilus_files_view_get_directory_as_file (MAUTILUS_FILES_VIEW (canvas_view));
    g_variant_builder_init (&builder, G_VARIANT_TYPE ("as"));

    for (idx = 0; idx < G_N_ELEMENTS (sort_criteria); idx++)
    {
        if (sort_criteria[idx].match_func == NULL ||
            (file != NULL && sort_criteria[idx].match_func (file)))
        {
            g_variant_builder_add (&builder, "s", sort_criteria[idx].action_target_name);
        }
    }

    state_hint = g_variant_builder_end (&builder);

    action_group = mautilus_files_view_get_action_group (MAUTILUS_FILES_VIEW (canvas_view));
    action = g_action_map_lookup_action (G_ACTION_MAP (action_group), "sort");
    g_simple_action_set_state_hint (G_SIMPLE_ACTION (action), state_hint);

    g_variant_unref (state_hint);
}

static gboolean
showing_recent_directory (mautilusFilesView *view)
{
    mautilusFile *file;

    file = mautilus_files_view_get_directory_as_file (view);
    if (file != NULL)
    {
        return mautilus_file_is_in_recent (file);
    }
    return FALSE;
}

static gboolean
showing_search_directory (mautilusFilesView *view)
{
    mautilusFile *file;

    file = mautilus_files_view_get_directory_as_file (view);
    if (file != NULL)
    {
        return mautilus_file_is_in_search (file);
    }
    return FALSE;
}

static void
mautilus_canvas_view_update_actions_state (mautilusFilesView *view)
{
    GActionGroup *view_action_group;
    GAction *action;
    gboolean keep_aligned;
    mautilusCanvasView *canvas_view;
    mautilusCanvasViewPrivate *priv;

    canvas_view = MAUTILUS_CANVAS_VIEW (view);
    priv = mautilus_canvas_view_get_instance_private (canvas_view);

    MAUTILUS_FILES_VIEW_CLASS (mautilus_canvas_view_parent_class)->update_actions_state (view);

    view_action_group = mautilus_files_view_get_action_group (view);
    if (mautilus_canvas_view_supports_auto_layout (canvas_view))
    {
        GVariant *sort_state;

        /* When we change the sort action state, even using the same value, it triggers
         * the sort action changed handler, which reveals the selection, since we expect
         * the selection to be visible when the user changes the sort order. But we may
         * need to update the actions state for others reason than an actual sort change,
         * so we need to prevent to trigger the sort action changed handler for those cases.
         * To achieve this, check if the action state value actually changed before setting
         * it
         */
        sort_state = g_action_group_get_action_state (view_action_group, "sort");

        if (g_strcmp0 (g_variant_get_string (sort_state, NULL),
                       priv->sort->action_target_name) != 0)
        {
            g_action_group_change_action_state (view_action_group,
                                                "sort",
                                                g_variant_new_string (priv->sort->action_target_name));
        }

        g_variant_unref (sort_state);
    }

    action = g_action_map_lookup_action (G_ACTION_MAP (view_action_group), "sort");
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action),
                                 !showing_recent_directory (view) &&
                                 !showing_search_directory (view));
    action = g_action_map_lookup_action (G_ACTION_MAP (view_action_group), "keep-aligned");
    g_simple_action_set_enabled (G_SIMPLE_ACTION (action),
                                 priv->supports_keep_aligned);
    if (priv->supports_keep_aligned)
    {
        keep_aligned = mautilus_canvas_container_is_keep_aligned (get_canvas_container (canvas_view));
        g_action_change_state (action, g_variant_new_boolean (keep_aligned));
    }

    update_sort_action_state_hint (canvas_view);
}

static void
mautilus_canvas_view_select_all (mautilusFilesView *view)
{
    mautilusCanvasContainer *canvas_container;

    g_return_if_fail (MAUTILUS_IS_CANVAS_VIEW (view));

    canvas_container = get_canvas_container (MAUTILUS_CANVAS_VIEW (view));
    mautilus_canvas_container_select_all (canvas_container);
}

static void
mautilus_canvas_view_select_first (mautilusFilesView *view)
{
    mautilusCanvasContainer *canvas_container;

    g_return_if_fail (MAUTILUS_IS_CANVAS_VIEW (view));

    canvas_container = get_canvas_container (MAUTILUS_CANVAS_VIEW (view));
    mautilus_canvas_container_select_first (canvas_container);
}

static void
mautilus_canvas_view_reveal_selection (mautilusFilesView *view)
{
    GList *selection;

    g_return_if_fail (MAUTILUS_IS_CANVAS_VIEW (view));

    selection = mautilus_view_get_selection (MAUTILUS_VIEW (view));

    /* Make sure at least one of the selected items is scrolled into view */
    if (selection != NULL)
    {
        /* Update the icon ordering to reveal the rigth selection */
        mautilus_canvas_container_layout_now (get_canvas_container (MAUTILUS_CANVAS_VIEW (view)));
        mautilus_canvas_container_reveal
            (get_canvas_container (MAUTILUS_CANVAS_VIEW (view)),
            selection->data);
    }

    mautilus_file_list_free (selection);
}

static GdkRectangle *
mautilus_canvas_view_compute_rename_popover_pointing_to (mautilusFilesView *view)
{
    GArray *bounding_boxes;
    GdkRectangle *bounding_box;
    mautilusCanvasContainer *canvas_container;
    GtkAdjustment *vadjustment, *hadjustment;
    GtkWidget *parent_container;

    canvas_container = get_canvas_container (MAUTILUS_CANVAS_VIEW (view));
    bounding_boxes = mautilus_canvas_container_get_selected_icons_bounding_box (canvas_container);
    /* We only allow renaming one item at once */
    bounding_box = &g_array_index (bounding_boxes, GdkRectangle, 0);
    parent_container = mautilus_files_view_get_content_widget (view);
    vadjustment = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (parent_container));
    hadjustment = gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (parent_container));

    bounding_box->x -= gtk_adjustment_get_value (hadjustment);
    bounding_box->y -= gtk_adjustment_get_value (vadjustment);

    g_array_free (bounding_boxes, FALSE);

    return bounding_box;
}

static void
mautilus_canvas_view_set_selection (mautilusFilesView *view,
                                    GList             *selection)
{
    g_return_if_fail (MAUTILUS_IS_CANVAS_VIEW (view));

    mautilus_canvas_container_set_selection
        (get_canvas_container (MAUTILUS_CANVAS_VIEW (view)), selection);
}

static void
mautilus_canvas_view_invert_selection (mautilusFilesView *view)
{
    g_return_if_fail (MAUTILUS_IS_CANVAS_VIEW (view));

    mautilus_canvas_container_invert_selection
        (get_canvas_container (MAUTILUS_CANVAS_VIEW (view)));
}

static gboolean
mautilus_canvas_view_using_manual_layout (mautilusFilesView *view)
{
    g_return_val_if_fail (MAUTILUS_IS_CANVAS_VIEW (view), FALSE);

    return !mautilus_canvas_view_using_auto_layout (MAUTILUS_CANVAS_VIEW (view));
}

static void
mautilus_canvas_view_widget_to_file_operation_position (mautilusFilesView *view,
                                                        GdkPoint          *position)
{
    g_assert (MAUTILUS_IS_CANVAS_VIEW (view));

    mautilus_canvas_container_widget_to_file_operation_position
        (get_canvas_container (MAUTILUS_CANVAS_VIEW (view)), position);
}

static void
canvas_container_activate_callback (mautilusCanvasContainer *container,
                                    GList                   *file_list,
                                    mautilusCanvasView      *canvas_view)
{
    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));
    g_assert (container == get_canvas_container (canvas_view));

    mautilus_files_view_activate_files (MAUTILUS_FILES_VIEW (canvas_view),
                                        file_list,
                                        0, TRUE);
}

static void
canvas_container_activate_previewer_callback (mautilusCanvasContainer *container,
                                              GList                   *file_list,
                                              GArray                  *locations,
                                              mautilusCanvasView      *canvas_view)
{
    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));
    g_assert (container == get_canvas_container (canvas_view));

    mautilus_files_view_preview_files (MAUTILUS_FILES_VIEW (canvas_view),
                                       file_list, locations);
}

/* this is called in one of these cases:
 * - we activate with enter holding shift
 * - we activate with space holding shift
 * - we double click an canvas holding shift
 * - we middle click an canvas
 *
 * If we don't open in new windows by default, the behavior should be
 * - middle click, shift + activate -> open in new tab
 * - shift + double click -> open in new window
 *
 * If we open in new windows by default, the behaviour should be
 * - middle click, or shift + activate, or shift + double-click -> close parent
 */
static void
canvas_container_activate_alternate_callback (mautilusCanvasContainer *container,
                                              GList                   *file_list,
                                              mautilusCanvasView      *canvas_view)
{
    GdkEvent *event;
    GdkEventButton *button_event;
    GdkEventKey *key_event;
    gboolean open_in_tab, open_in_window;
    mautilusWindowOpenFlags flags;

    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));
    g_assert (container == get_canvas_container (canvas_view));

    flags = 0;
    event = gtk_get_current_event ();
    open_in_tab = FALSE;
    open_in_window = FALSE;

    if (event->type == GDK_BUTTON_PRESS ||
        event->type == GDK_BUTTON_RELEASE ||
        event->type == GDK_2BUTTON_PRESS ||
        event->type == GDK_3BUTTON_PRESS)
    {
        button_event = (GdkEventButton *) event;
        open_in_window = ((button_event->state & GDK_SHIFT_MASK) != 0);
        open_in_tab = !open_in_window;
    }
    else if (event->type == GDK_KEY_PRESS ||
             event->type == GDK_KEY_RELEASE)
    {
        key_event = (GdkEventKey *) event;
        open_in_tab = ((key_event->state & GDK_SHIFT_MASK) != 0);
    }

    if (open_in_tab)
    {
        flags |= MAUTILUS_WINDOW_OPEN_FLAG_NEW_TAB;
        flags |= MAUTILUS_WINDOW_OPEN_FLAG_DONT_MAKE_ACTIVE;
    }

    if (open_in_window)
    {
        flags |= MAUTILUS_WINDOW_OPEN_FLAG_NEW_WINDOW;
    }

    DEBUG ("Activate alternate, open in tab %d, new window %d\n",
           open_in_tab, open_in_window);

    mautilus_files_view_activate_files (MAUTILUS_FILES_VIEW (canvas_view),
                                        file_list,
                                        flags,
                                        TRUE);
}

static void
band_select_started_callback (mautilusCanvasContainer *container,
                              mautilusCanvasView      *canvas_view)
{
    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));
    g_assert (container == get_canvas_container (canvas_view));

    mautilus_files_view_start_batching_selection_changes (MAUTILUS_FILES_VIEW (canvas_view));
}

static void
band_select_ended_callback (mautilusCanvasContainer *container,
                            mautilusCanvasView      *canvas_view)
{
    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));
    g_assert (container == get_canvas_container (canvas_view));

    mautilus_files_view_stop_batching_selection_changes (MAUTILUS_FILES_VIEW (canvas_view));
}

int
mautilus_canvas_view_compare_files (mautilusCanvasView *canvas_view,
                                    mautilusFile       *a,
                                    mautilusFile       *b)
{
    mautilusCanvasViewPrivate *priv;

    priv = mautilus_canvas_view_get_instance_private (canvas_view);

    return mautilus_file_compare_for_sort
               (a, b, priv->sort->sort_type,
               /* Use type-unsafe cast for performance */
               mautilus_files_view_should_sort_directories_first ((mautilusFilesView *) canvas_view),
               priv->sort->reverse_order);
}

static int
compare_files (mautilusFilesView *canvas_view,
               mautilusFile      *a,
               mautilusFile      *b)
{
    return mautilus_canvas_view_compare_files ((mautilusCanvasView *) canvas_view, a, b);
}

static void
selection_changed_callback (mautilusCanvasContainer *container,
                            mautilusCanvasView      *canvas_view)
{
    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));
    g_assert (container == get_canvas_container (canvas_view));

    mautilus_files_view_notify_selection_changed (MAUTILUS_FILES_VIEW (canvas_view));
}

static void
canvas_container_context_click_selection_callback (mautilusCanvasContainer *container,
                                                   GdkEventButton          *event,
                                                   mautilusCanvasView      *canvas_view)
{
    g_assert (MAUTILUS_IS_CANVAS_CONTAINER (container));
    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));

    mautilus_files_view_pop_up_selection_context_menu (MAUTILUS_FILES_VIEW (canvas_view), event);
}

static void
canvas_container_context_click_background_callback (mautilusCanvasContainer *container,
                                                    GdkEventButton          *event,
                                                    mautilusCanvasView      *canvas_view)
{
    g_assert (MAUTILUS_IS_CANVAS_CONTAINER (container));
    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));

    mautilus_files_view_pop_up_background_context_menu (MAUTILUS_FILES_VIEW (canvas_view), event);
}

static void
icon_position_changed_callback (mautilusCanvasContainer      *container,
                                mautilusFile                 *file,
                                const mautilusCanvasPosition *position,
                                mautilusCanvasView           *canvas_view)
{
    char *position_string;
    char scale_string[G_ASCII_DTOSTR_BUF_SIZE];

    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));
    g_assert (container == get_canvas_container (canvas_view));
    g_assert (MAUTILUS_IS_FILE (file));

    /* Store the new position of the canvas in the metadata. */
    if (!mautilus_canvas_view_using_auto_layout (canvas_view))
    {
        position_string = g_strdup_printf
                              ("%d,%d", position->x, position->y);
        mautilus_file_set_metadata
            (file, MAUTILUS_METADATA_KEY_ICON_POSITION,
            NULL, position_string);
        g_free (position_string);
    }


    g_ascii_dtostr (scale_string, sizeof (scale_string), position->scale);
    mautilus_file_set_metadata
        (file, MAUTILUS_METADATA_KEY_ICON_SCALE,
        "1.0", scale_string);
}

static char *
get_icon_uri_callback (mautilusCanvasContainer *container,
                       mautilusFile            *file,
                       mautilusCanvasView      *canvas_view)
{
    g_assert (MAUTILUS_IS_CANVAS_CONTAINER (container));
    g_assert (MAUTILUS_IS_FILE (file));
    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));

    return mautilus_file_get_uri (file);
}

static char *
get_icon_activation_uri_callback (mautilusCanvasContainer *container,
                                  mautilusFile            *file,
                                  mautilusCanvasView      *canvas_view)
{
    g_assert (MAUTILUS_IS_CANVAS_CONTAINER (container));
    g_assert (MAUTILUS_IS_FILE (file));
    g_assert (MAUTILUS_IS_CANVAS_VIEW (canvas_view));

    return mautilus_file_get_activation_uri (file);
}

static char *
get_icon_drop_target_uri_callback (mautilusCanvasContainer *container,
                                   mautilusFile            *file,
                                   mautilusCanvasView      *canvas_view)
{
    g_return_val_if_fail (MAUTILUS_IS_CANVAS_CONTAINER (container), NULL);
    g_return_val_if_fail (MAUTILUS_IS_FILE (file), NULL);
    g_return_val_if_fail (MAUTILUS_IS_CANVAS_VIEW (canvas_view), NULL);

    return mautilus_file_get_target_uri (file);
}

/* Preferences changed callbacks */
static void
mautilus_canvas_view_click_policy_changed (mautilusFilesView *directory_view)
{
    g_assert (MAUTILUS_IS_CANVAS_VIEW (directory_view));

    mautilus_canvas_view_update_click_mode (MAUTILUS_CANVAS_VIEW (directory_view));
}

static void
image_display_policy_changed_callback (gpointer callback_data)
{
    mautilusCanvasView *canvas_view;

    canvas_view = MAUTILUS_CANVAS_VIEW (callback_data);

    mautilus_canvas_container_request_update_all (get_canvas_container (canvas_view));
}

static void
text_attribute_names_changed_callback (gpointer callback_data)
{
    mautilusCanvasView *canvas_view;

    canvas_view = MAUTILUS_CANVAS_VIEW (callback_data);

    mautilus_canvas_container_request_update_all (get_canvas_container (canvas_view));
}

static void
default_sort_order_changed_callback (gpointer callback_data)
{
    mautilusCanvasView *canvas_view;
    mautilusFile *file;
    const SortCriterion *sort;
    mautilusCanvasContainer *canvas_container;

    g_return_if_fail (MAUTILUS_IS_CANVAS_VIEW (callback_data));

    canvas_view = MAUTILUS_CANVAS_VIEW (callback_data);

    file = mautilus_files_view_get_directory_as_file (MAUTILUS_FILES_VIEW (canvas_view));
    sort = mautilus_canvas_view_get_directory_sort_by (canvas_view, file);
    update_sort_criterion (canvas_view, sort, FALSE);

    canvas_container = get_canvas_container (canvas_view);
    g_return_if_fail (MAUTILUS_IS_CANVAS_CONTAINER (canvas_container));

    mautilus_canvas_container_request_update_all (canvas_container);
}

static void
mautilus_canvas_view_sort_directories_first_changed (mautilusFilesView *directory_view)
{
    mautilusCanvasView *canvas_view;

    canvas_view = MAUTILUS_CANVAS_VIEW (directory_view);

    if (mautilus_canvas_view_using_auto_layout (canvas_view))
    {
        mautilus_canvas_container_sort
            (get_canvas_container (canvas_view));
    }
}

static gboolean
canvas_view_can_accept_item (mautilusCanvasContainer *container,
                             mautilusFile            *target_item,
                             const char              *item_uri,
                             mautilusFilesView       *view)
{
    return mautilus_drag_can_accept_item (target_item, item_uri);
}

static char *
canvas_view_get_container_uri (mautilusCanvasContainer *container,
                               mautilusFilesView       *view)
{
    return mautilus_files_view_get_uri (view);
}

static void
canvas_view_move_copy_items (mautilusCanvasContainer *container,
                             const GList             *item_uris,
                             GArray                  *relative_item_points,
                             const char              *target_dir,
                             int                      copy_action,
                             int                      x,
                             int                      y,
                             mautilusFilesView       *view)
{
    mautilus_clipboard_clear_if_colliding_uris (GTK_WIDGET (view),
                                                item_uris);
    mautilus_files_view_move_copy_items (view, item_uris, relative_item_points, target_dir,
                                         copy_action, x, y);
}

static void
mautilus_canvas_view_update_click_mode (mautilusCanvasView *canvas_view)
{
    mautilusCanvasContainer *canvas_container;
    int click_mode;

    canvas_container = get_canvas_container (canvas_view);
    g_assert (canvas_container != NULL);

    click_mode = g_settings_get_enum (mautilus_preferences, MAUTILUS_PREFERENCES_CLICK_POLICY);

    mautilus_canvas_container_set_single_click_mode (canvas_container,
                                                     click_mode == MAUTILUS_CLICK_POLICY_SINGLE);
}

static gboolean
get_stored_layout_timestamp (mautilusCanvasContainer *container,
                             mautilusCanvasIconData  *icon_data,
                             time_t                  *timestamp,
                             mautilusCanvasView      *view)
{
    mautilusFile *file;
    mautilusDirectory *directory;

    if (icon_data == NULL)
    {
        directory = mautilus_files_view_get_model (MAUTILUS_FILES_VIEW (view));
        if (directory == NULL)
        {
            return FALSE;
        }

        file = mautilus_directory_get_corresponding_file (directory);
        *timestamp = mautilus_file_get_time_metadata (file,
                                                      MAUTILUS_METADATA_KEY_ICON_VIEW_LAYOUT_TIMESTAMP);
        mautilus_file_unref (file);
    }
    else
    {
        *timestamp = mautilus_file_get_time_metadata (MAUTILUS_FILE (icon_data),
                                                      MAUTILUS_METADATA_KEY_ICON_POSITION_TIMESTAMP);
    }

    return TRUE;
}

static gboolean
store_layout_timestamp (mautilusCanvasContainer *container,
                        mautilusCanvasIconData  *icon_data,
                        const time_t            *timestamp,
                        mautilusCanvasView      *view)
{
    mautilusFile *file;
    mautilusDirectory *directory;

    if (icon_data == NULL)
    {
        directory = mautilus_files_view_get_model (MAUTILUS_FILES_VIEW (view));
        if (directory == NULL)
        {
            return FALSE;
        }

        file = mautilus_directory_get_corresponding_file (directory);
        mautilus_file_set_time_metadata (file,
                                         MAUTILUS_METADATA_KEY_ICON_VIEW_LAYOUT_TIMESTAMP,
                                         (time_t) *timestamp);
        mautilus_file_unref (file);
    }
    else
    {
        mautilus_file_set_time_metadata (MAUTILUS_FILE (icon_data),
                                         MAUTILUS_METADATA_KEY_ICON_POSITION_TIMESTAMP,
                                         (time_t) *timestamp);
    }

    return TRUE;
}

static mautilusCanvasContainer *
create_canvas_container (mautilusCanvasView *canvas_view)
{
    return MAUTILUS_CANVAS_VIEW_CLASS (G_OBJECT_GET_CLASS (canvas_view))->create_canvas_container (canvas_view);
}

static mautilusCanvasContainer *
real_create_canvas_container (mautilusCanvasView *canvas_view)
{
    return mautilus_canvas_view_container_new (canvas_view);
}

static void
initialize_canvas_container (mautilusCanvasView      *canvas_view,
                             mautilusCanvasContainer *canvas_container)
{
    GtkWidget *content_widget;
    mautilusCanvasViewPrivate *priv;

    priv = mautilus_canvas_view_get_instance_private (canvas_view);

    content_widget = mautilus_files_view_get_content_widget (MAUTILUS_FILES_VIEW (canvas_view));
    priv->canvas_container = GTK_WIDGET (canvas_container);
    g_object_add_weak_pointer (G_OBJECT (canvas_container),
                               (gpointer *) &priv->canvas_container);

    gtk_widget_set_can_focus (GTK_WIDGET (canvas_container), TRUE);

    g_signal_connect_object (canvas_container, "activate",
                             G_CALLBACK (canvas_container_activate_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "activate-alternate",
                             G_CALLBACK (canvas_container_activate_alternate_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "activate-previewer",
                             G_CALLBACK (canvas_container_activate_previewer_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "band-select-started",
                             G_CALLBACK (band_select_started_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "band-select-ended",
                             G_CALLBACK (band_select_ended_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "context-click-selection",
                             G_CALLBACK (canvas_container_context_click_selection_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "context-click-background",
                             G_CALLBACK (canvas_container_context_click_background_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "icon-position-changed",
                             G_CALLBACK (icon_position_changed_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "selection-changed",
                             G_CALLBACK (selection_changed_callback), canvas_view, 0);
    /* FIXME: many of these should move into fm-canvas-container as virtual methods */
    g_signal_connect_object (canvas_container, "get-icon-uri",
                             G_CALLBACK (get_icon_uri_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "get-icon-activation-uri",
                             G_CALLBACK (get_icon_activation_uri_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "get-icon-drop-target-uri",
                             G_CALLBACK (get_icon_drop_target_uri_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "move-copy-items",
                             G_CALLBACK (canvas_view_move_copy_items), canvas_view, 0);
    g_signal_connect_object (canvas_container, "get-container-uri",
                             G_CALLBACK (canvas_view_get_container_uri), canvas_view, 0);
    g_signal_connect_object (canvas_container, "can-accept-item",
                             G_CALLBACK (canvas_view_can_accept_item), canvas_view, 0);
    g_signal_connect_object (canvas_container, "get-stored-icon-position",
                             G_CALLBACK (get_stored_icon_position_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "layout-changed",
                             G_CALLBACK (layout_changed_callback), canvas_view, 0);
    g_signal_connect_object (canvas_container, "icon-stretch-started",
                             G_CALLBACK (mautilus_files_view_update_context_menus), canvas_view,
                             G_CONNECT_SWAPPED);
    g_signal_connect_object (canvas_container, "icon-stretch-ended",
                             G_CALLBACK (mautilus_files_view_update_context_menus), canvas_view,
                             G_CONNECT_SWAPPED);

    g_signal_connect_object (canvas_container, "get-stored-layout-timestamp",
                             G_CALLBACK (get_stored_layout_timestamp), canvas_view, 0);
    g_signal_connect_object (canvas_container, "store-layout-timestamp",
                             G_CALLBACK (store_layout_timestamp), canvas_view, 0);

    gtk_container_add (GTK_CONTAINER (content_widget),
                       GTK_WIDGET (canvas_container));

    mautilus_canvas_view_update_click_mode (canvas_view);
    mautilus_canvas_container_set_zoom_level (canvas_container,
                                              get_default_zoom_level (canvas_view));

    gtk_widget_show (GTK_WIDGET (canvas_container));
}

/* Handles an URL received from Mozilla */
static void
canvas_view_handle_netscape_url (mautilusCanvasContainer *container,
                                 const char              *encoded_url,
                                 const char              *target_uri,
                                 GdkDragAction            action,
                                 int                      x,
                                 int                      y,
                                 mautilusCanvasView      *view)
{
    mautilus_files_view_handle_netscape_url_drop (MAUTILUS_FILES_VIEW (view),
                                                  encoded_url, target_uri, action, x, y);
}

static void
canvas_view_handle_uri_list (mautilusCanvasContainer *container,
                             const char              *item_uris,
                             const char              *target_uri,
                             GdkDragAction            action,
                             int                      x,
                             int                      y,
                             mautilusCanvasView      *view)
{
    mautilus_files_view_handle_uri_list_drop (MAUTILUS_FILES_VIEW (view),
                                              item_uris, target_uri, action, x, y);
}

static void
canvas_view_handle_text (mautilusCanvasContainer *container,
                         const char              *text,
                         const char              *target_uri,
                         GdkDragAction            action,
                         int                      x,
                         int                      y,
                         mautilusCanvasView      *view)
{
    mautilus_files_view_handle_text_drop (MAUTILUS_FILES_VIEW (view),
                                          text, target_uri, action, x, y);
}

static void
canvas_view_handle_raw (mautilusCanvasContainer *container,
                        const char              *raw_data,
                        int                      length,
                        const char              *target_uri,
                        const char              *direct_save_uri,
                        GdkDragAction            action,
                        int                      x,
                        int                      y,
                        mautilusCanvasView      *view)
{
    mautilus_files_view_handle_raw_drop (MAUTILUS_FILES_VIEW (view),
                                         raw_data, length, target_uri, direct_save_uri, action, x, y);
}

static void
canvas_view_handle_hover (mautilusCanvasContainer *container,
                          const char              *target_uri,
                          mautilusCanvasView      *view)
{
    mautilus_files_view_handle_hover (MAUTILUS_FILES_VIEW (view), target_uri);
}

static char *
canvas_view_get_first_visible_file (mautilusFilesView *view)
{
    mautilusFile *file;
    mautilusCanvasView *canvas_view;

    canvas_view = MAUTILUS_CANVAS_VIEW (view);

    file = MAUTILUS_FILE (mautilus_canvas_container_get_first_visible_icon (get_canvas_container (canvas_view)));

    if (file)
    {
        return mautilus_file_get_uri (file);
    }

    return NULL;
}

static void
canvas_view_scroll_to_file (mautilusFilesView *view,
                            const char        *uri)
{
    mautilusFile *file;
    mautilusCanvasView *canvas_view;

    canvas_view = MAUTILUS_CANVAS_VIEW (view);

    if (uri != NULL)
    {
        /* Only if existing, since we don't want to add the file to
         *  the directory if it has been removed since then */
        file = mautilus_file_get_existing_by_uri (uri);
        if (file != NULL)
        {
            mautilus_canvas_container_scroll_to_canvas (get_canvas_container (canvas_view),
                                                        MAUTILUS_CANVAS_ICON_DATA (file));
            mautilus_file_unref (file);
        }
    }
}

static guint
mautilus_canvas_view_get_id (mautilusFilesView *view)
{
    return MAUTILUS_VIEW_GRID_ID;
}

static void
mautilus_canvas_view_set_property (GObject      *object,
                                   guint         prop_id,
                                   const GValue *value,
                                   GParamSpec   *pspec)
{
    mautilusCanvasView *canvas_view;
    mautilusCanvasViewPrivate *priv;

    canvas_view = MAUTILUS_CANVAS_VIEW (object);
    priv = mautilus_canvas_view_get_instance_private (canvas_view);

    switch (prop_id)
    {
        case PROP_SUPPORTS_AUTO_LAYOUT:
        {
            priv->supports_auto_layout = g_value_get_boolean (value);
        }
        break;

        case PROP_SUPPORTS_MANUAL_LAYOUT:
        {
            priv->supports_manual_layout = g_value_get_boolean (value);
        }
        break;

        case PROP_SUPPORTS_SCALING:
        {
            priv->supports_scaling = g_value_get_boolean (value);
        }
        break;

        case PROP_SUPPORTS_KEEP_ALIGNED:
        {
            priv->supports_keep_aligned = g_value_get_boolean (value);
        }
        break;

        default:
        {
            G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
        }
        break;
    }
}

static void
mautilus_canvas_view_dispose (GObject *object)
{
    mautilusCanvasView *canvas_view;
    mautilusCanvasViewPrivate *priv;

    canvas_view = MAUTILUS_CANVAS_VIEW (object);
    priv = mautilus_canvas_view_get_instance_private (canvas_view);
    priv->destroyed = TRUE;

    g_signal_handlers_disconnect_by_func (mautilus_preferences,
                                          default_sort_order_changed_callback,
                                          canvas_view);
    g_signal_handlers_disconnect_by_func (mautilus_preferences,
                                          image_display_policy_changed_callback,
                                          canvas_view);

    g_signal_handlers_disconnect_by_func (mautilus_icon_view_preferences,
                                          text_attribute_names_changed_callback,
                                          canvas_view);


    G_OBJECT_CLASS (mautilus_canvas_view_parent_class)->dispose (object);
}

static void
mautilus_canvas_view_class_init (mautilusCanvasViewClass *klass)
{
    mautilusFilesViewClass *mautilus_files_view_class;
    GObjectClass *oclass;

    mautilus_files_view_class = MAUTILUS_FILES_VIEW_CLASS (klass);
    oclass = G_OBJECT_CLASS (klass);

    oclass->set_property = mautilus_canvas_view_set_property;
    oclass->dispose = mautilus_canvas_view_dispose;

    GTK_WIDGET_CLASS (klass)->destroy = mautilus_canvas_view_destroy;

    klass->create_canvas_container = real_create_canvas_container;

    mautilus_files_view_class->add_files = mautilus_canvas_view_add_files;
    mautilus_files_view_class->begin_loading = mautilus_canvas_view_begin_loading;
    mautilus_files_view_class->bump_zoom_level = mautilus_canvas_view_bump_zoom_level;
    mautilus_files_view_class->can_zoom_in = mautilus_canvas_view_can_zoom_in;
    mautilus_files_view_class->can_zoom_out = mautilus_canvas_view_can_zoom_out;
    mautilus_files_view_class->get_zoom_level_percentage = mautilus_canvas_view_get_zoom_level_percentage;
    mautilus_files_view_class->is_zoom_level_default = mautilus_canvas_view_is_zoom_level_default;
    mautilus_files_view_class->clear = mautilus_canvas_view_clear;
    mautilus_files_view_class->end_loading = mautilus_canvas_view_end_loading;
    mautilus_files_view_class->file_changed = mautilus_canvas_view_file_changed;
    mautilus_files_view_class->compute_rename_popover_pointing_to = mautilus_canvas_view_compute_rename_popover_pointing_to;
    mautilus_files_view_class->get_selection = mautilus_canvas_view_get_selection;
    mautilus_files_view_class->get_selection_for_file_transfer = mautilus_canvas_view_get_selection;
    mautilus_files_view_class->is_empty = mautilus_canvas_view_is_empty;
    mautilus_files_view_class->remove_file = mautilus_canvas_view_remove_file;
    mautilus_files_view_class->restore_standard_zoom_level = mautilus_canvas_view_restore_standard_zoom_level;
    mautilus_files_view_class->reveal_selection = mautilus_canvas_view_reveal_selection;
    mautilus_files_view_class->select_all = mautilus_canvas_view_select_all;
    mautilus_files_view_class->select_first = mautilus_canvas_view_select_first;
    mautilus_files_view_class->set_selection = mautilus_canvas_view_set_selection;
    mautilus_files_view_class->invert_selection = mautilus_canvas_view_invert_selection;
    mautilus_files_view_class->compare_files = compare_files;
    mautilus_files_view_class->click_policy_changed = mautilus_canvas_view_click_policy_changed;
    mautilus_files_view_class->update_actions_state = mautilus_canvas_view_update_actions_state;
    mautilus_files_view_class->sort_directories_first_changed = mautilus_canvas_view_sort_directories_first_changed;
    mautilus_files_view_class->using_manual_layout = mautilus_canvas_view_using_manual_layout;
    mautilus_files_view_class->widget_to_file_operation_position = mautilus_canvas_view_widget_to_file_operation_position;
    mautilus_files_view_class->get_view_id = mautilus_canvas_view_get_id;
    mautilus_files_view_class->get_first_visible_file = canvas_view_get_first_visible_file;
    mautilus_files_view_class->scroll_to_file = canvas_view_scroll_to_file;

    properties[PROP_SUPPORTS_AUTO_LAYOUT] =
        g_param_spec_boolean ("supports-auto-layout",
                              "Supports auto layout",
                              "Whether this view supports auto layout",
                              TRUE,
                              G_PARAM_WRITABLE |
                              G_PARAM_CONSTRUCT_ONLY);
    properties[PROP_SUPPORTS_MANUAL_LAYOUT] =
        g_param_spec_boolean ("supports-manual-layout",
                              "Supports manual layout",
                              "Whether this view supports manual layout",
                              FALSE,
                              G_PARAM_WRITABLE |
                              G_PARAM_CONSTRUCT_ONLY);
    properties[PROP_SUPPORTS_SCALING] =
        g_param_spec_boolean ("supports-scaling",
                              "Supports scaling",
                              "Whether this view supports scaling",
                              FALSE,
                              G_PARAM_WRITABLE |
                              G_PARAM_CONSTRUCT_ONLY);
    properties[PROP_SUPPORTS_KEEP_ALIGNED] =
        g_param_spec_boolean ("supports-keep-aligned",
                              "Supports keep aligned",
                              "Whether this view supports keep aligned",
                              FALSE,
                              G_PARAM_WRITABLE |
                              G_PARAM_CONSTRUCT_ONLY);

    g_object_class_install_properties (oclass, NUM_PROPERTIES, properties);
}

static void
mautilus_canvas_view_init (mautilusCanvasView *canvas_view)
{
    mautilusCanvasViewPrivate *priv;
    mautilusCanvasContainer *canvas_container;
    GActionGroup *view_action_group;
    GtkClipboard *clipboard;

    priv = mautilus_canvas_view_get_instance_private (canvas_view);

    priv->sort = &sort_criteria[0];
    priv->destroyed = FALSE;

    canvas_container = create_canvas_container (canvas_view);
    initialize_canvas_container (canvas_view, canvas_container);

    g_signal_connect_swapped (mautilus_preferences,
                              "changed::" MAUTILUS_PREFERENCES_DEFAULT_SORT_ORDER,
                              G_CALLBACK (default_sort_order_changed_callback),
                              canvas_view);
    g_signal_connect_swapped (mautilus_preferences,
                              "changed::" MAUTILUS_PREFERENCES_DEFAULT_SORT_IN_REVERSE_ORDER,
                              G_CALLBACK (default_sort_order_changed_callback),
                              canvas_view);
    g_signal_connect_swapped (mautilus_preferences,
                              "changed::" MAUTILUS_PREFERENCES_SHOW_FILE_THUMBNAILS,
                              G_CALLBACK (image_display_policy_changed_callback),
                              canvas_view);

    g_signal_connect_swapped (mautilus_icon_view_preferences,
                              "changed::" MAUTILUS_PREFERENCES_ICON_VIEW_CAPTIONS,
                              G_CALLBACK (text_attribute_names_changed_callback),
                              canvas_view);

    g_signal_connect_object (canvas_container, "handle-netscape-url",
                             G_CALLBACK (canvas_view_handle_netscape_url), canvas_view, 0);
    g_signal_connect_object (canvas_container, "handle-uri-list",
                             G_CALLBACK (canvas_view_handle_uri_list), canvas_view, 0);
    g_signal_connect_object (canvas_container, "handle-text",
                             G_CALLBACK (canvas_view_handle_text), canvas_view, 0);
    g_signal_connect_object (canvas_container, "handle-raw",
                             G_CALLBACK (canvas_view_handle_raw), canvas_view, 0);
    g_signal_connect_object (canvas_container, "handle-hover",
                             G_CALLBACK (canvas_view_handle_hover), canvas_view, 0);

    /* React to clipboard changes */
    clipboard = gtk_clipboard_get (GDK_SELECTION_CLIPBOARD);
    g_signal_connect (clipboard, "owner-change",
                      G_CALLBACK (on_clipboard_owner_changed), canvas_view);

    view_action_group = mautilus_files_view_get_action_group (MAUTILUS_FILES_VIEW (canvas_view));
    g_action_map_add_action_entries (G_ACTION_MAP (view_action_group),
                                     canvas_view_entries,
                                     G_N_ELEMENTS (canvas_view_entries),
                                     canvas_view);
    /* Keep the action synced with the actual value, so the toolbar can poll it */
    g_action_group_change_action_state (mautilus_files_view_get_action_group (MAUTILUS_FILES_VIEW (canvas_view)),
                                        "zoom-to-level", g_variant_new_int32 (get_default_zoom_level (canvas_view)));
}

mautilusFilesView *
mautilus_canvas_view_new (mautilusWindowSlot *slot)
{
    return g_object_new (MAUTILUS_TYPE_CANVAS_VIEW,
                         "window-slot", slot,
                         NULL);
}
