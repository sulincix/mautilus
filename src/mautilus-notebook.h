/*
 *  Copyright © 2002 Christophe Fergeau
 *  Copyright © 2003 Marco Pesenti Gritti
 *  Copyright © 2003, 2004 Christian Persch
 *    (ephy-notebook.c)
 *
 *  Copyright © 2008 Free Software Foundation, Inc.
 *    (mautilus-notebook.c)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef MAUTILUS_NOTEBOOK_H
#define MAUTILUS_NOTEBOOK_H

#include <glib.h>
#include <gtk/gtk.h>

#include "mautilus-window-slot.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_NOTEBOOK		(mautilus_notebook_get_type ())
G_DECLARE_FINAL_TYPE (mautilusNotebook, mautilus_notebook, MAUTILUS, NOTEBOOK, GtkNotebook)

int		mautilus_notebook_add_tab	(mautilusNotebook *nb,
						 mautilusWindowSlot *slot,
						 int position,
						 gboolean jump_to);
	
void		mautilus_notebook_sync_tab_label (mautilusNotebook *nb,
						  mautilusWindowSlot *slot);
void		mautilus_notebook_sync_loading   (mautilusNotebook *nb,
						  mautilusWindowSlot *slot);

void		mautilus_notebook_reorder_current_child_relative (mautilusNotebook *notebook,
								  int offset);
gboolean        mautilus_notebook_can_reorder_current_child_relative (mautilusNotebook *notebook,
								      int offset);
void            mautilus_notebook_prev_page (mautilusNotebook *notebook);
void            mautilus_notebook_next_page (mautilusNotebook *notebook);

gboolean        mautilus_notebook_contains_slot (mautilusNotebook   *notebook,
                                                 mautilusWindowSlot *slot);

G_END_DECLS

#endif /* MAUTILUS_NOTEBOOK_H */

