/* mautilus-view.c
 *
 * Copyright (C) 2015 Georges Basile Stavracas Neto <georges.stavracas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.h"

#include "mautilus-view.h"

G_DEFINE_INTERFACE (mautilusView, mautilus_view, GTK_TYPE_WIDGET)

static void
mautilus_view_default_init (mautilusViewInterface *iface)
{
    /**
     * mautilusView::is-loading:
     *
     * %TRUE if the view is loading the location, %FALSE otherwise.
     */
    g_object_interface_install_property (iface,
                                         g_param_spec_boolean ("is-loading",
                                                               "Current view is loading",
                                                               "Whether the current view is loading the location or not",
                                                               FALSE,
                                                               G_PARAM_READABLE));

    /**
     * mautilusView::is-searching:
     *
     * %TRUE if the view is searching, %FALSE otherwise.
     */
    g_object_interface_install_property (iface,
                                         g_param_spec_boolean ("is-searching",
                                                               "Current view is searching",
                                                               "Whether the current view is searching or not",
                                                               FALSE,
                                                               G_PARAM_READABLE));

    /**
     * mautilusView::location:
     *
     * The current location of the view.
     */
    g_object_interface_install_property (iface,
                                         g_param_spec_object ("location",
                                                              "Location displayed by the view",
                                                              "The current location displayed by the view",
                                                              G_TYPE_FILE,
                                                              G_PARAM_READWRITE));

    /**
     * mautilusView::search-query:
     *
     * The search query being performed, or NULL.
     */
    g_object_interface_install_property (iface,
                                         g_param_spec_object ("search-query",
                                                              "Search query being performed",
                                                              "The search query being performed on the view",
                                                              MAUTILUS_TYPE_QUERY,
                                                              G_PARAM_READWRITE));
}

/**
 * mautilus_view_get_icon:
 * @view: a #mautilusView
 *
 * Retrieves the #GIcon that represents @view.
 *
 * Returns: (transfer full): a #GIcon
 */
GIcon *
mautilus_view_get_icon (guint view_id)
{
    if (view_id == MAUTILUS_VIEW_GRID_ID)
    {
        return g_themed_icon_new ("view-grid-symbolic");
    }
    else if (view_id == MAUTILUS_VIEW_LIST_ID)
    {
        return g_themed_icon_new ("view-list-symbolic");
    }
    else if (view_id == MAUTILUS_VIEW_OTHER_LOCATIONS_ID)
    {
        return g_themed_icon_new_with_default_fallbacks ("view-list-symbolic");
    }
    else
    {
        return NULL;
    }
}

/**
 * mautilus_view_get_view_id:
 * @view: a #mautilusView
 *
 * Retrieves the view id that represents the @view type.
 *
 * Returns: a guint representing the view type
 */
guint
mautilus_view_get_view_id (mautilusView *view)
{
    g_return_val_if_fail (MAUTILUS_VIEW_GET_IFACE (view)->get_view_id, MAUTILUS_VIEW_INVALID_ID);

    return MAUTILUS_VIEW_GET_IFACE (view)->get_view_id (view);
}

/**
 * mautilus_view_get_toolbar_menu_sections:
 * @view: a #mautilusView
 *
 * Retrieves the menu sections to show in the main toolbar menu when this view
 * is active
 *
 * Returns: (transfer none): a #mautilusToolbarMenuSections with the sections to
 * be displayed
 */
mautilusToolbarMenuSections *
mautilus_view_get_toolbar_menu_sections (mautilusView *view)
{
    g_return_val_if_fail (MAUTILUS_VIEW_GET_IFACE (view)->get_toolbar_menu_sections, NULL);

    return MAUTILUS_VIEW_GET_IFACE (view)->get_toolbar_menu_sections (view);
}

/**
 * mautilus_view_get_search_query:
 * @view: a #mautilusView
 *
 * Retrieves the current current location of @view.
 *
 * Returns: (transfer none): a #GFile
 */
GFile *
mautilus_view_get_location (mautilusView *view)
{
    g_return_val_if_fail (MAUTILUS_VIEW_GET_IFACE (view)->get_location, NULL);

    return MAUTILUS_VIEW_GET_IFACE (view)->get_location (view);
}

/**
 * mautilus_view_set_location:
 * @view: a #mautilusView
 * @location: the location displayed by @view
 *
 * Sets the location of @view.
 *
 * Returns:
 */
void
mautilus_view_set_location (mautilusView *view,
                            GFile        *location)
{
    g_return_if_fail (MAUTILUS_VIEW_GET_IFACE (view)->set_location);

    MAUTILUS_VIEW_GET_IFACE (view)->set_location (view, location);
}

/**
 * mautilus_view_get_selection:
 * @view: a #mautilusView
 *
 * Get the current selection of the view.
 *
 * Returns: (transfer full) (type GFile): a newly allocated list
 * of the currently selected files.
 */
GList *
mautilus_view_get_selection (mautilusView *view)
{
    g_return_val_if_fail (MAUTILUS_VIEW_GET_IFACE (view)->get_selection, NULL);

    return MAUTILUS_VIEW_GET_IFACE (view)->get_selection (view);
}

/**
 * mautilus_view_set_selection:
 * @view: a #mautilusView
 * @selection: (nullable): a list of files
 *
 * Sets the current selection of the view.
 *
 * Returns:
 */
void
mautilus_view_set_selection (mautilusView *view,
                             GList        *selection)
{
    g_return_if_fail (MAUTILUS_VIEW_GET_IFACE (view)->set_selection);

    MAUTILUS_VIEW_GET_IFACE (view)->set_selection (view, selection);
}

/**
 * mautilus_view_get_search_query:
 * @view: a #mautilusView
 *
 * Retrieves the current search query displayed by @view.
 *
 * Returns: (transfer none): a #
 */
mautilusQuery *
mautilus_view_get_search_query (mautilusView *view)
{
    g_return_val_if_fail (MAUTILUS_VIEW_GET_IFACE (view)->get_search_query, NULL);

    return MAUTILUS_VIEW_GET_IFACE (view)->get_search_query (view);
}

/**
 * mautilus_view_set_search_query:
 * @view: a #mautilusView
 * @query: the search query to be performed, or %NULL
 *
 * Sets the current search query performed by @view.
 *
 * Returns:
 */
void
mautilus_view_set_search_query (mautilusView  *view,
                                mautilusQuery *query)
{
    g_return_if_fail (MAUTILUS_VIEW_GET_IFACE (view)->set_search_query);

    MAUTILUS_VIEW_GET_IFACE (view)->set_search_query (view, query);
}

/**
 * mautilus_view_is_loading:
 * @view: a #mautilusView
 *
 * Whether @view is loading the current location.
 *
 * Returns: %TRUE if @view is loading, %FALSE otherwise.
 */
gboolean
mautilus_view_is_loading (mautilusView *view)
{
    g_return_val_if_fail (MAUTILUS_VIEW_GET_IFACE (view)->is_loading, FALSE);

    return MAUTILUS_VIEW_GET_IFACE (view)->is_loading (view);
}

/**
 * mautilus_view_is_searching:
 * @view: a #mautilusView
 *
 * Whether @view is searching.
 *
 * Returns: %TRUE if @view is searching, %FALSE otherwise.
 */
gboolean
mautilus_view_is_searching (mautilusView *view)
{
    g_return_val_if_fail (MAUTILUS_VIEW_GET_IFACE (view)->is_searching, FALSE);

    return MAUTILUS_VIEW_GET_IFACE (view)->is_searching (view);
}
