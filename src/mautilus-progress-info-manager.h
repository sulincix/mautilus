/*
 * mautilus
 *
 * Copyright (C) 2011 Red Hat, Inc.
 *
 * mautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * mautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; see the file COPYING.  If not,
 * see <http://www.gnu.org/licenses/>.
 *
 * Author: Cosimo Cecchi <cosimoc@redhat.com>
 */

#ifndef __MAUTILUS_PROGRESS_INFO_MANAGER_H__
#define __MAUTILUS_PROGRESS_INFO_MANAGER_H__

#include <glib-object.h>

#include "mautilus-progress-info.h"

#define MAUTILUS_TYPE_PROGRESS_INFO_MANAGER mautilus_progress_info_manager_get_type()
G_DECLARE_FINAL_TYPE (mautilusProgressInfoManager, mautilus_progress_info_manager, MAUTILUS, PROGRESS_INFO_MANAGER, GObject)

mautilusProgressInfoManager* mautilus_progress_info_manager_dup_singleton (void);

void mautilus_progress_info_manager_add_new_info (mautilusProgressInfoManager *self,
                                                  mautilusProgressInfo *info);
GList *mautilus_progress_info_manager_get_all_infos (mautilusProgressInfoManager *self);
void mautilus_progress_info_manager_remove_finished_or_cancelled_infos (mautilusProgressInfoManager *self);
gboolean mautilus_progress_manager_are_all_infos_finished_or_cancelled (mautilusProgressInfoManager *self);

void mautilus_progress_manager_add_viewer (mautilusProgressInfoManager *self, GObject *viewer);
void mautilus_progress_manager_remove_viewer (mautilusProgressInfoManager *self, GObject *viewer);
gboolean mautilus_progress_manager_has_viewers (mautilusProgressInfoManager *self);

G_END_DECLS

#endif /* __MAUTILUS_PROGRESS_INFO_MANAGER_H__ */
