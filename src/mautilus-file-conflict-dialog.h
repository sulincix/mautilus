
/* mautilus-file-conflict-dialog: dialog that handles file conflicts
   during transfer operations.

   Copyright (C) 2008, Cosimo Cecchi

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.
   
   You should have received a copy of the GNU General Public
   License along with this program; if not, see <http://www.gnu.org/licenses/>.
   
   Authors: Cosimo Cecchi <cosimoc@gnome.org>
*/

#ifndef MAUTILUS_FILE_CONFLICT_DIALOG_H
#define MAUTILUS_FILE_CONFLICT_DIALOG_H

#include <glib-object.h>
#include <gio/gio.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define MAUTILUS_TYPE_FILE_CONFLICT_DIALOG (mautilus_file_conflict_dialog_get_type ())

G_DECLARE_FINAL_TYPE (mautilusFileConflictDialog, mautilus_file_conflict_dialog, MAUTILUS, FILE_CONFLICT_DIALOG, GtkDialog)

mautilusFileConflictDialog* mautilus_file_conflict_dialog_new (GtkWindow *parent);

void mautilus_file_conflict_dialog_set_text (mautilusFileConflictDialog *fcd,
                                             gchar *primary_text,
                                             gchar *secondary_text);
void mautilus_file_conflict_dialog_set_images (mautilusFileConflictDialog *fcd,
                                               GdkPixbuf *source_pixbuf,
                                               GdkPixbuf *destination_pixbuf);
void mautilus_file_conflict_dialog_set_file_labels (mautilusFileConflictDialog *fcd,
                                                    gchar *destination_label,
                                                    gchar *source_label);
void mautilus_file_conflict_dialog_set_conflict_name (mautilusFileConflictDialog *fcd,
                                                      gchar *conflict_name);
void mautilus_file_conflict_dialog_set_replace_button_label (mautilusFileConflictDialog *fcd,
                                                             gchar *label);

void mautilus_file_conflict_dialog_disable_skip (mautilusFileConflictDialog *fcd);
void mautilus_file_conflict_dialog_disable_replace (mautilusFileConflictDialog *fcd);
void mautilus_file_conflict_dialog_disable_apply_to_all (mautilusFileConflictDialog *fcd);

char*      mautilus_file_conflict_dialog_get_new_name     (mautilusFileConflictDialog *dialog);
gboolean   mautilus_file_conflict_dialog_get_apply_to_all (mautilusFileConflictDialog *dialog);

G_END_DECLS

#endif /* MAUTILUS_FILE_CONFLICT_DIALOG_H */
