
/* mautilus-file-undo-operations.h - Manages undo/redo of file operations
 *
 * Copyright (C) 2007-2011 Amos Brocco
 * Copyright (C) 2010 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 * Authors: Amos Brocco <amos.brocco@gmail.com>
 *          Cosimo Cecchi <cosimoc@redhat.com>
 *
 */

#ifndef __MAUTILUS_FILE_UNDO_OPERATIONS_H__
#define __MAUTILUS_FILE_UNDO_OPERATIONS_H__

#include <gio/gio.h>
#include <gtk/gtk.h>
#include <gnome-autoar/gnome-autoar.h>

typedef enum {
	MAUTILUS_FILE_UNDO_OP_COPY,
	MAUTILUS_FILE_UNDO_OP_DUPLICATE,
	MAUTILUS_FILE_UNDO_OP_MOVE,
	MAUTILUS_FILE_UNDO_OP_RENAME,
	MAUTILUS_FILE_UNDO_OP_BATCH_RENAME,
	MAUTILUS_FILE_UNDO_OP_FAVORITES,
	MAUTILUS_FILE_UNDO_OP_CREATE_EMPTY_FILE,
	MAUTILUS_FILE_UNDO_OP_CREATE_FILE_FROM_TEMPLATE,
	MAUTILUS_FILE_UNDO_OP_CREATE_FOLDER,
	MAUTILUS_FILE_UNDO_OP_EXTRACT,
	MAUTILUS_FILE_UNDO_OP_COMPRESS,
	MAUTILUS_FILE_UNDO_OP_MOVE_TO_TRASH,
	MAUTILUS_FILE_UNDO_OP_RESTORE_FROM_TRASH,
	MAUTILUS_FILE_UNDO_OP_CREATE_LINK,
	MAUTILUS_FILE_UNDO_OP_RECURSIVE_SET_PERMISSIONS,
	MAUTILUS_FILE_UNDO_OP_SET_PERMISSIONS,
	MAUTILUS_FILE_UNDO_OP_CHANGE_GROUP,
	MAUTILUS_FILE_UNDO_OP_CHANGE_OWNER,
	MAUTILUS_FILE_UNDO_OP_NUM_TYPES,
} mautilusFileUndoOp;

#define MAUTILUS_TYPE_FILE_UNDO_INFO         (mautilus_file_undo_info_get_type ())
#define MAUTILUS_FILE_UNDO_INFO(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_FILE_UNDO_INFO, mautilusFileUndoInfo))
#define MAUTILUS_FILE_UNDO_INFO_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_FILE_UNDO_INFO, mautilusFileUndoInfoClass))
#define MAUTILUS_IS_FILE_UNDO_INFO(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_FILE_UNDO_INFO))
#define MAUTILUS_IS_FILE_UNDO_INFO_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_FILE_UNDO_INFO))
#define MAUTILUS_FILE_UNDO_INFO_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_FILE_UNDO_INFO, mautilusFileUndoInfoClass))

typedef struct _mautilusFileUndoInfo      mautilusFileUndoInfo;
typedef struct _mautilusFileUndoInfoClass mautilusFileUndoInfoClass;
typedef struct _mautilusFileUndoInfoDetails mautilusFileUndoInfoDetails;

struct _mautilusFileUndoInfo {
	GObject parent;
	mautilusFileUndoInfoDetails *priv;
};

struct _mautilusFileUndoInfoClass {
	GObjectClass parent_class;

	void (* undo_func) (mautilusFileUndoInfo *self,
			    GtkWindow            *parent_window);
	void (* redo_func) (mautilusFileUndoInfo *self,
			    GtkWindow            *parent_window);

	void (* strings_func) (mautilusFileUndoInfo *self,
			       gchar **undo_label,
			       gchar **undo_description,
			       gchar **redo_label,
			       gchar **redo_description);
};

GType mautilus_file_undo_info_get_type (void) G_GNUC_CONST;

void mautilus_file_undo_info_apply_async (mautilusFileUndoInfo *self,
					  gboolean undo,
					  GtkWindow *parent_window,
					  GAsyncReadyCallback callback,
					  gpointer user_data);
gboolean mautilus_file_undo_info_apply_finish (mautilusFileUndoInfo *self,
					       GAsyncResult *res,
					       gboolean *user_cancel,
					       GError **error);

void mautilus_file_undo_info_get_strings (mautilusFileUndoInfo *self,
					  gchar **undo_label,
					  gchar **undo_description,
					  gchar **redo_label,
					  gchar **redo_description);

mautilusFileUndoOp mautilus_file_undo_info_get_op_type (mautilusFileUndoInfo *self);

/* copy/move/duplicate/link/restore from trash */
#define MAUTILUS_TYPE_FILE_UNDO_INFO_EXT         (mautilus_file_undo_info_ext_get_type ())
#define MAUTILUS_FILE_UNDO_INFO_EXT(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_EXT, mautilusFileUndoInfoExt))
#define MAUTILUS_FILE_UNDO_INFO_EXT_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_FILE_UNDO_INFO_EXT, mautilusFileUndoInfoExtClass))
#define MAUTILUS_IS_FILE_UNDO_INFO_EXT(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_EXT))
#define MAUTILUS_IS_FILE_UNDO_INFO_EXT_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_FILE_UNDO_INFO_EXT))
#define MAUTILUS_FILE_UNDO_INFO_EXT_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_EXT, mautilusFileUndoInfoExtClass))

typedef struct _mautilusFileUndoInfoExt      mautilusFileUndoInfoExt;
typedef struct _mautilusFileUndoInfoExtClass mautilusFileUndoInfoExtClass;
typedef struct _mautilusFileUndoInfoExtDetails mautilusFileUndoInfoExtDetails;

struct _mautilusFileUndoInfoExt {
	mautilusFileUndoInfo parent;
	mautilusFileUndoInfoExtDetails *priv;
};

struct _mautilusFileUndoInfoExtClass {
	mautilusFileUndoInfoClass parent_class;
};

GType mautilus_file_undo_info_ext_get_type (void) G_GNUC_CONST;
mautilusFileUndoInfo *mautilus_file_undo_info_ext_new (mautilusFileUndoOp op_type,
						       gint item_count,
						       GFile *src_dir,
						       GFile *target_dir);
void mautilus_file_undo_info_ext_add_origin_target_pair (mautilusFileUndoInfoExt *self,
							 GFile                   *origin,
							 GFile                   *target);

/* create new file/folder */
#define MAUTILUS_TYPE_FILE_UNDO_INFO_CREATE         (mautilus_file_undo_info_create_get_type ())
#define MAUTILUS_FILE_UNDO_INFO_CREATE(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_CREATE, mautilusFileUndoInfoCreate))
#define MAUTILUS_FILE_UNDO_INFO_CREATE_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_FILE_UNDO_INFO_CREATE, mautilusFileUndoInfoCreateClass))
#define MAUTILUS_IS_FILE_UNDO_INFO_CREATE(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_CREATE))
#define MAUTILUS_IS_FILE_UNDO_INFO_CREATE_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_FILE_UNDO_INFO_CREATE))
#define MAUTILUS_FILE_UNDO_INFO_CREATE_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_CREATE, mautilusFileUndoInfoCreateClass))

typedef struct _mautilusFileUndoInfoCreate      mautilusFileUndoInfoCreate;
typedef struct _mautilusFileUndoInfoCreateClass mautilusFileUndoInfoCreateClass;
typedef struct _mautilusFileUndoInfoCreateDetails mautilusFileUndoInfoCreateDetails;

struct _mautilusFileUndoInfoCreate {
	mautilusFileUndoInfo parent;
	mautilusFileUndoInfoCreateDetails *priv;
};

struct _mautilusFileUndoInfoCreateClass {
	mautilusFileUndoInfoClass parent_class;
};

GType mautilus_file_undo_info_create_get_type (void) G_GNUC_CONST;
mautilusFileUndoInfo *mautilus_file_undo_info_create_new (mautilusFileUndoOp op_type);
void mautilus_file_undo_info_create_set_data (mautilusFileUndoInfoCreate *self,
					      GFile                      *file,
					      const char                 *template,
					      gint                        length);

/* rename */
#define MAUTILUS_TYPE_FILE_UNDO_INFO_RENAME         (mautilus_file_undo_info_rename_get_type ())
#define MAUTILUS_FILE_UNDO_INFO_RENAME(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_RENAME, mautilusFileUndoInfoRename))
#define MAUTILUS_FILE_UNDO_INFO_RENAME_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_FILE_UNDO_INFO_RENAME, mautilusFileUndoInfoRenameClass))
#define MAUTILUS_IS_FILE_UNDO_INFO_RENAME(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_RENAME))
#define MAUTILUS_IS_FILE_UNDO_INFO_RENAME_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_FILE_UNDO_INFO_RENAME))
#define MAUTILUS_FILE_UNDO_INFO_RENAME_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_RENAME, mautilusFileUndoInfoRenameClass))

typedef struct _mautilusFileUndoInfoRename      mautilusFileUndoInfoRename;
typedef struct _mautilusFileUndoInfoRenameClass mautilusFileUndoInfoRenameClass;
typedef struct _mautilusFileUndoInfoRenameDetails mautilusFileUndoInfoRenameDetails;

struct _mautilusFileUndoInfoRename {
	mautilusFileUndoInfo parent;
	mautilusFileUndoInfoRenameDetails *priv;
};

struct _mautilusFileUndoInfoRenameClass {
	mautilusFileUndoInfoClass parent_class;
};

GType mautilus_file_undo_info_rename_get_type (void) G_GNUC_CONST;
mautilusFileUndoInfo *mautilus_file_undo_info_rename_new (void);
void mautilus_file_undo_info_rename_set_data_pre (mautilusFileUndoInfoRename *self,
						  GFile                      *old_file,
						  gchar                      *old_display_name,
						  gchar                      *new_display_name);
void mautilus_file_undo_info_rename_set_data_post (mautilusFileUndoInfoRename *self,
						   GFile                      *new_file);

/* batch rename */
#define MAUTILUS_TYPE_FILE_UNDO_INFO_BATCH_RENAME         (mautilus_file_undo_info_batch_rename_get_type ())
#define MAUTILUS_FILE_UNDO_INFO_BATCH_RENAME(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_BATCH_RENAME, mautilusFileUndoInfoBatchRename))
#define MAUTILUS_FILE_UNDO_INFO_BATCH_RENAME_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_FILE_UNDO_INFO_BATCH_RENAME, mautilusFileUndoInfoBatchRenameClass))
#define MAUTILUS_IS_FILE_UNDO_INFO_BATCH_RENAME(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_BATCH_RENAME))
#define MAUTILUS_IS_FILE_UNDO_INFO_BATCH_RENAME_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_FILE_UNDO_INFO_BATCH_RENAME))
#define MAUTILUS_FILE_UNDO_INFO_BATCH_RENAME_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_BATCH_RENAME, mautilusFileUndoInfoBatchRenameClass))

typedef struct _mautilusFileUndoInfoBatchRename      mautilusFileUndoInfoBatchRename;
typedef struct _mautilusFileUndoInfoBatchRenameClass mautilusFileUndoInfoBatchRenameClass;
typedef struct _mautilusFileUndoInfoBatchRenameDetails mautilusFileUndoInfoBatchRenameDetails;

struct _mautilusFileUndoInfoBatchRename {
	mautilusFileUndoInfo parent;
	mautilusFileUndoInfoBatchRenameDetails *priv;
};

struct _mautilusFileUndoInfoBatchRenameClass {
	mautilusFileUndoInfoClass parent_class;
};

GType mautilus_file_undo_info_batch_rename_get_type (void) G_GNUC_CONST;
mautilusFileUndoInfo *mautilus_file_undo_info_batch_rename_new (gint item_count);
void mautilus_file_undo_info_batch_rename_set_data_pre (mautilusFileUndoInfoBatchRename *self,
						        GList                           *old_files);
void mautilus_file_undo_info_batch_rename_set_data_post (mautilusFileUndoInfoBatchRename *self,
						         GList                           *new_files);

/* favorite files */
#define MAUTILUS_TYPE_FILE_UNDO_INFO_FAVORITES         (mautilus_file_undo_info_favorites_get_type ())
#define MAUTILUS_FILE_UNDO_INFO_FAVORITES(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_FAVORITES, mautilusFileUndoInfoFavorites))
#define MAUTILUS_FILE_UNDO_INFO_FAVORITES_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_FILE_UNDO_INFO_FAVORITES, mautilusFileUndoInfoFavoritesClass))
#define MAUTILUS_IS_FILE_UNDO_INFO_FAVORITES(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_FAVORITES))
#define MAUTILUS_IS_FILE_UNDO_INFO_FAVORITES_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_FILE_UNDO_INFO_FAVORITES))
#define MAUTILUS_FILE_UNDO_INFO_FAVORITES_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_FAVORITES, mautilusFileUndoInfoFavoritesClass))

typedef struct _mautilusFileUndoInfoFavorites      mautilusFileUndoInfoFavorites;
typedef struct _mautilusFileUndoInfoFavoritesClass mautilusFileUndoInfoFavoritesClass;
typedef struct _mautilusFileUndoInfoFavoritesDetails mautilusFileUndoInfoFavoritesDetails;

struct _mautilusFileUndoInfoFavorites {
    mautilusFileUndoInfo parent;
    mautilusFileUndoInfoFavoritesDetails *priv;
};

struct _mautilusFileUndoInfoFavoritesClass {
    mautilusFileUndoInfoClass parent_class;
};

GType mautilus_file_undo_info_favorites_get_type (void) G_GNUC_CONST;
mautilusFileUndoInfo *mautilus_file_undo_info_favorites_new (GList   *files,
                                                             gboolean starred);

/* trash */
#define MAUTILUS_TYPE_FILE_UNDO_INFO_TRASH         (mautilus_file_undo_info_trash_get_type ())
#define MAUTILUS_FILE_UNDO_INFO_TRASH(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_TRASH, mautilusFileUndoInfoTrash))
#define MAUTILUS_FILE_UNDO_INFO_TRASH_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_FILE_UNDO_INFO_TRASH, mautilusFileUndoInfoTrashClass))
#define MAUTILUS_IS_FILE_UNDO_INFO_TRASH(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_TRASH))
#define MAUTILUS_IS_FILE_UNDO_INFO_TRASH_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_FILE_UNDO_INFO_TRASH))
#define MAUTILUS_FILE_UNDO_INFO_TRASH_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_TRASH, mautilusFileUndoInfoTrashClass))

typedef struct _mautilusFileUndoInfoTrash      mautilusFileUndoInfoTrash;
typedef struct _mautilusFileUndoInfoTrashClass mautilusFileUndoInfoTrashClass;
typedef struct _mautilusFileUndoInfoTrashDetails mautilusFileUndoInfoTrashDetails;

struct _mautilusFileUndoInfoTrash {
	mautilusFileUndoInfo parent;
	mautilusFileUndoInfoTrashDetails *priv;
};

struct _mautilusFileUndoInfoTrashClass {
	mautilusFileUndoInfoClass parent_class;
};

GType mautilus_file_undo_info_trash_get_type (void) G_GNUC_CONST;
mautilusFileUndoInfo *mautilus_file_undo_info_trash_new (gint item_count);
void mautilus_file_undo_info_trash_add_file (mautilusFileUndoInfoTrash *self,
					     GFile                     *file);
GList *mautilus_file_undo_info_trash_get_files (mautilusFileUndoInfoTrash *self);

/* recursive permissions */
#define MAUTILUS_TYPE_FILE_UNDO_INFO_REC_PERMISSIONS         (mautilus_file_undo_info_rec_permissions_get_type ())
#define MAUTILUS_FILE_UNDO_INFO_REC_PERMISSIONS(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_REC_PERMISSIONS, mautilusFileUndoInfoRecPermissions))
#define MAUTILUS_FILE_UNDO_INFO_REC_PERMISSIONS_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_FILE_UNDO_INFO_REC_PERMISSIONS, mautilusFileUndoInfoRecPermissionsClass))
#define MAUTILUS_IS_FILE_UNDO_INFO_REC_PERMISSIONS(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_REC_PERMISSIONS))
#define MAUTILUS_IS_FILE_UNDO_INFO_REC_PERMISSIONS_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_FILE_UNDO_INFO_REC_PERMISSIONS))
#define MAUTILUS_FILE_UNDO_INFO_REC_PERMISSIONS_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_REC_PERMISSIONS, mautilusFileUndoInfoRecPermissionsClass))

typedef struct _mautilusFileUndoInfoRecPermissions      mautilusFileUndoInfoRecPermissions;
typedef struct _mautilusFileUndoInfoRecPermissionsClass mautilusFileUndoInfoRecPermissionsClass;
typedef struct _mautilusFileUndoInfoRecPermissionsDetails mautilusFileUndoInfoRecPermissionsDetails;

struct _mautilusFileUndoInfoRecPermissions {
	mautilusFileUndoInfo parent;
	mautilusFileUndoInfoRecPermissionsDetails *priv;
};

struct _mautilusFileUndoInfoRecPermissionsClass {
	mautilusFileUndoInfoClass parent_class;
};

GType mautilus_file_undo_info_rec_permissions_get_type (void) G_GNUC_CONST;
mautilusFileUndoInfo *mautilus_file_undo_info_rec_permissions_new (GFile   *dest,
								   guint32 file_permissions,
								   guint32 file_mask,
								   guint32 dir_permissions,
								   guint32 dir_mask);
void mautilus_file_undo_info_rec_permissions_add_file (mautilusFileUndoInfoRecPermissions *self,
						       GFile                              *file,
						       guint32                             permission);

/* single file change permissions */
#define MAUTILUS_TYPE_FILE_UNDO_INFO_PERMISSIONS         (mautilus_file_undo_info_permissions_get_type ())
#define MAUTILUS_FILE_UNDO_INFO_PERMISSIONS(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_PERMISSIONS, mautilusFileUndoInfoPermissions))
#define MAUTILUS_FILE_UNDO_INFO_PERMISSIONS_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_FILE_UNDO_INFO_PERMISSIONS, mautilusFileUndoInfoPermissionsClass))
#define MAUTILUS_IS_FILE_UNDO_INFO_PERMISSIONS(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_PERMISSIONS))
#define MAUTILUS_IS_FILE_UNDO_INFO_PERMISSIONS_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_FILE_UNDO_INFO_PERMISSIONS))
#define MAUTILUS_FILE_UNDO_INFO_PERMISSIONS_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_PERMISSIONS, mautilusFileUndoInfoPermissionsClass))

typedef struct _mautilusFileUndoInfoPermissions      mautilusFileUndoInfoPermissions;
typedef struct _mautilusFileUndoInfoPermissionsClass mautilusFileUndoInfoPermissionsClass;
typedef struct _mautilusFileUndoInfoPermissionsDetails mautilusFileUndoInfoPermissionsDetails;

struct _mautilusFileUndoInfoPermissions {
	mautilusFileUndoInfo parent;
	mautilusFileUndoInfoPermissionsDetails *priv;
};

struct _mautilusFileUndoInfoPermissionsClass {
	mautilusFileUndoInfoClass parent_class;
};

GType mautilus_file_undo_info_permissions_get_type (void) G_GNUC_CONST;
mautilusFileUndoInfo *mautilus_file_undo_info_permissions_new (GFile   *file,
							       guint32  current_permissions,
							       guint32  new_permissions);

/* group and owner change */
#define MAUTILUS_TYPE_FILE_UNDO_INFO_OWNERSHIP         (mautilus_file_undo_info_ownership_get_type ())
#define MAUTILUS_FILE_UNDO_INFO_OWNERSHIP(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_OWNERSHIP, mautilusFileUndoInfoOwnership))
#define MAUTILUS_FILE_UNDO_INFO_OWNERSHIP_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_FILE_UNDO_INFO_OWNERSHIP, mautilusFileUndoInfoOwnershipClass))
#define MAUTILUS_IS_FILE_UNDO_INFO_OWNERSHIP(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_OWNERSHIP))
#define MAUTILUS_IS_FILE_UNDO_INFO_OWNERSHIP_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_FILE_UNDO_INFO_OWNERSHIP))
#define MAUTILUS_FILE_UNDO_INFO_OWNERSHIP_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_OWNERSHIP, mautilusFileUndoInfoOwnershipClass))

typedef struct _mautilusFileUndoInfoOwnership      mautilusFileUndoInfoOwnership;
typedef struct _mautilusFileUndoInfoOwnershipClass mautilusFileUndoInfoOwnershipClass;
typedef struct _mautilusFileUndoInfoOwnershipDetails mautilusFileUndoInfoOwnershipDetails;

struct _mautilusFileUndoInfoOwnership {
	mautilusFileUndoInfo parent;
	mautilusFileUndoInfoOwnershipDetails *priv;
};

struct _mautilusFileUndoInfoOwnershipClass {
	mautilusFileUndoInfoClass parent_class;
};

GType mautilus_file_undo_info_ownership_get_type (void) G_GNUC_CONST;
mautilusFileUndoInfo *mautilus_file_undo_info_ownership_new (mautilusFileUndoOp  op_type,
							     GFile              *file,
							     const char         *current_data,
							     const char         *new_data);

/* extract */
#define MAUTILUS_TYPE_FILE_UNDO_INFO_EXTRACT         (mautilus_file_undo_info_extract_get_type ())
#define MAUTILUS_FILE_UNDO_INFO_EXTRACT(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_EXTRACT, mautilusFileUndoInfoExtract))
#define MAUTILUS_FILE_UNDO_INFO_EXTRACT_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_FILE_UNDO_INFO_EXTRACT, mautilusFileUndoInfoExtractClass))
#define MAUTILUS_IS_FILE_UNDO_INFO_EXTRACT(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_EXTRACT))
#define MAUTILUS_IS_FILE_UNDO_INFO_EXTRACT_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_FILE_UNDO_INFO_EXTRACT))
#define MAUTILUS_FILE_UNDO_INFO_EXTRACT_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_EXTRACT, mautilusFileUndoInfoExtractClass))

typedef struct _mautilusFileUndoInfoExtract        mautilusFileUndoInfoExtract;
typedef struct _mautilusFileUndoInfoExtractClass   mautilusFileUndoInfoExtractClass;
typedef struct _mautilusFileUndoInfoExtractDetails mautilusFileUndoInfoExtractDetails;

struct _mautilusFileUndoInfoExtract {
        mautilusFileUndoInfo parent;
        mautilusFileUndoInfoExtractDetails *priv;
};

struct _mautilusFileUndoInfoExtractClass {
        mautilusFileUndoInfoClass parent_class;
};

GType mautilus_file_undo_info_extract_get_type (void) G_GNUC_CONST;
mautilusFileUndoInfo * mautilus_file_undo_info_extract_new (GList *sources,
                                                            GFile *destination_directory);
void mautilus_file_undo_info_extract_set_outputs (mautilusFileUndoInfoExtract *self,
                                                  GList                       *outputs);

/* compress */
#define MAUTILUS_TYPE_FILE_UNDO_INFO_COMPRESS         (mautilus_file_undo_info_compress_get_type ())
#define MAUTILUS_FILE_UNDO_INFO_COMPRESS(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_COMPRESS, mautilusFileUndoInfoCompress))
#define MAUTILUS_FILE_UNDO_INFO_COMPRESS_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_FILE_UNDO_INFO_COMPRESS, mautilusFileUndoInfoCompressClass))
#define MAUTILUS_IS_FILE_UNDO_INFO_COMPRESS(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_COMPRESS))
#define MAUTILUS_IS_FILE_UNDO_INFO_COMPRESS_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_FILE_UNDO_INFO_COMPRESS))
#define MAUTILUS_FILE_UNDO_INFO_COMPRESS_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_FILE_UNDO_INFO_COMPRESS, mautilusFileUndoInfoCompressClass))

typedef struct _mautilusFileUndoInfoCompress        mautilusFileUndoInfoCompress;
typedef struct _mautilusFileUndoInfoCompressClass   mautilusFileUndoInfoCompressClass;
typedef struct _mautilusFileUndoInfoCompressDetails mautilusFileUndoInfoCompressDetails;

struct _mautilusFileUndoInfoCompress {
        mautilusFileUndoInfo parent;
        mautilusFileUndoInfoCompressDetails *priv;
};

struct _mautilusFileUndoInfoCompressClass {
        mautilusFileUndoInfoClass parent_class;
};

GType mautilus_file_undo_info_compress_get_type (void) G_GNUC_CONST;
mautilusFileUndoInfo * mautilus_file_undo_info_compress_new (GList        *sources,
                                                             GFile        *output,
                                                             AutoarFormat  format,
                                                             AutoarFilter  filter);


#endif /* __MAUTILUS_FILE_UNDO_OPERATIONS_H__ */
