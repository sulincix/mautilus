/*
 *  mautilus-info-provider.h - Type definitions for mautilus extensions
 * 
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author: Dave Camp <dave@ximian.com>
 * 
 */

/* This interface is implemented by mautilus extensions that want to 
 * provide information about files.  Extensions are called when mautilus 
 * needs information about a file.  They are passed a mautilusFileInfo 
 * object which should be filled with relevant information */

#ifndef MAUTILUS_EXTENSION_TYPES_H
#define MAUTILUS_EXTENSION_TYPES_H

#include <glib-object.h>

G_BEGIN_DECLS

#define MAUTILUS_TYPE_OPERATION_RESULT (mautilus_operation_result_get_type ())

/**
 * mautilusOperationHandle:
 *
 * Handle for asynchronous interfaces. These are opaque handles that must
 * be unique within an extension object. These are returned by operations
 * that return #MAUTILUS_OPERATION_IN_PROGRESS.
 */
typedef struct _mautilusOperationHandle mautilusOperationHandle;

/**
 * mautilusOperationResult:
 * @MAUTILUS_OPERATION_COMPLETE: the operation succeeded, and the extension
 *  is done with the request.
 * @MAUTILUS_OPERATION_FAILED: the operation failed.
 * @MAUTILUS_OPERATION_IN_PROGRESS: the extension has begin an async operation.
 *  When this value is returned, the extension must set the handle parameter
 *  and call the callback closure when the operation is complete.
 *
 * Return values for asynchronous operations performed by the extension.
 * See mautilus_info_provider_update_file_info().
 */
typedef enum {
	/* Returned if the call succeeded, and the extension is done 
	 * with the request */
	MAUTILUS_OPERATION_COMPLETE,

	/* Returned if the call failed */
	MAUTILUS_OPERATION_FAILED,

	/* Returned if the extension has begun an async operation. 
	 * If this is returned, the extension must set the handle 
	 * parameter and call the callback closure when the 
	 * operation is complete. */
	MAUTILUS_OPERATION_IN_PROGRESS
} mautilusOperationResult;

GType mautilus_operation_result_get_type (void);

/**
 * SECTION:mautilus-extension-types
 * @title: mautilusModule
 * @short_description: Initialize an extension
 * @include: libmautilus-extension/mautilus-extension-types.h
 *
 * Methods that each extension implements.
 */

void mautilus_module_initialize (GTypeModule  *module);
void mautilus_module_shutdown   (void);
void mautilus_module_list_types (const GType **types,
				 int          *num_types);

G_END_DECLS

#endif
