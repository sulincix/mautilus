/*
 *  mautilus-menu.h - Menus exported by mautilusMenuProvider objects.
 *
 *  Copyright (C) 2005 Raffaele Sandrini
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 * 
 *  Author:  Dave Camp <dave@ximian.com>
 *           Raffaele Sandrini <rasa@gmx.ch>
 *
 */

#ifndef MAUTILUS_MENU_H
#define MAUTILUS_MENU_H

#include <glib-object.h>
#include "mautilus-extension-types.h"


G_BEGIN_DECLS

/* mautilusMenu defines */
#define MAUTILUS_TYPE_MENU         (mautilus_menu_get_type ())
#define MAUTILUS_MENU(o)           (G_TYPE_CHECK_INSTANCE_CAST ((o), MAUTILUS_TYPE_MENU, mautilusMenu))
#define MAUTILUS_MENU_CLASS(k)     (G_TYPE_CHECK_CLASS_CAST((k), MAUTILUS_TYPE_MENU, mautilusMenuClass))
#define MAUTILUS_IS_MENU(o)        (G_TYPE_CHECK_INSTANCE_TYPE ((o), MAUTILUS_TYPE_MENU))
#define MAUTILUS_IS_MENU_CLASS(k)  (G_TYPE_CHECK_CLASS_TYPE ((k), MAUTILUS_TYPE_MENU))
#define MAUTILUS_MENU_GET_CLASS(o) (G_TYPE_INSTANCE_GET_CLASS ((o), MAUTILUS_TYPE_MENU, mautilusMenuClass))
/* mautilusMenuItem defines */
#define MAUTILUS_TYPE_MENU_ITEM            (mautilus_menu_item_get_type())
#define MAUTILUS_MENU_ITEM(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_MENU_ITEM, mautilusMenuItem))
#define MAUTILUS_MENU_ITEM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_MENU_ITEM, mautilusMenuItemClass))
#define MAUTILUS_MENU_IS_ITEM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_MENU_ITEM))
#define MAUTILUS_MENU_IS_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), MAUTILUS_TYPE_MENU_ITEM))
#define MAUTILUS_MENU_ITEM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), MAUTILUS_TYPE_MENU_ITEM, mautilusMenuItemClass))


/* mautilusMenu types */
typedef struct _mautilusMenu		mautilusMenu;
typedef struct _mautilusMenuPrivate	mautilusMenuPrivate;
typedef struct _mautilusMenuClass	mautilusMenuClass;
/* mautilusMenuItem types */
typedef struct _mautilusMenuItem        mautilusMenuItem;
typedef struct _mautilusMenuItemDetails mautilusMenuItemDetails;
typedef struct _mautilusMenuItemClass   mautilusMenuItemClass;


/* mautilusMenu structs */
struct _mautilusMenu {
	GObject parent;
	mautilusMenuPrivate *priv;
};

struct _mautilusMenuClass {
	GObjectClass parent_class;
};

/* mautilusMenuItem structs */
struct _mautilusMenuItem {
	GObject parent;

	mautilusMenuItemDetails *details;
};

struct _mautilusMenuItemClass {
	GObjectClass parent;

	void (*activate) (mautilusMenuItem *item);
};


/* mautilusMenu methods */
GType		mautilus_menu_get_type	(void);
mautilusMenu *	mautilus_menu_new	(void);

void	mautilus_menu_append_item	(mautilusMenu      *menu,
					 mautilusMenuItem  *item);
GList*	mautilus_menu_get_items		(mautilusMenu *menu);
void	mautilus_menu_item_list_free	(GList *item_list);

/* mautilusMenuItem methods */
GType             mautilus_menu_item_get_type      (void);
mautilusMenuItem *mautilus_menu_item_new           (const char       *name,
						    const char       *label,
						    const char       *tip,
						    const char       *icon);

void              mautilus_menu_item_activate      (mautilusMenuItem *item);
void              mautilus_menu_item_set_submenu   (mautilusMenuItem *item,
						    mautilusMenu     *menu);
/* mautilusMenuItem has the following properties:
 *   name (string)        - the identifier for the menu item
 *   label (string)       - the user-visible label of the menu item
 *   tip (string)         - the tooltip of the menu item 
 *   icon (string)        - the name of the icon to display in the menu item
 *   sensitive (boolean)  - whether the menu item is sensitive or not
 *   priority (boolean)   - used for toolbar items, whether to show priority
 *                          text.
 *   menu (mautilusMenu)  - The menu belonging to this item. May be null.
 */

G_END_DECLS

#endif /* MAUTILUS_MENU_H */
