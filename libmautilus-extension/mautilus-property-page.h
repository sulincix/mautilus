/*
 *  mautilus-property-page.h - Property pages exported by 
 *                             mautilusPropertyProvider objects.
 *
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 * 
 *  Author:  Dave Camp <dave@ximian.com>
 *
 */

#ifndef MAUTILUS_PROPERTY_PAGE_H
#define MAUTILUS_PROPERTY_PAGE_H

#include <glib-object.h>
#include <gtk/gtk.h>
#include "mautilus-extension-types.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_PROPERTY_PAGE            (mautilus_property_page_get_type())
#define MAUTILUS_PROPERTY_PAGE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_PROPERTY_PAGE, mautilusPropertyPage))
#define MAUTILUS_PROPERTY_PAGE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_PROPERTY_PAGE, mautilusPropertyPageClass))
#define MAUTILUS_IS_PROPERTY_PAGE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_PROPERTY_PAGE))
#define MAUTILUS_IS_PROPERTY_PAGE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), MAUTILUS_TYPE_PROPERTY_PAGE))
#define MAUTILUS_PROPERTY_PAGE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), MAUTILUS_TYPE_PROPERTY_PAGE, mautilusPropertyPageClass))

typedef struct _mautilusPropertyPage        mautilusPropertyPage;
typedef struct _mautilusPropertyPageDetails mautilusPropertyPageDetails;
typedef struct _mautilusPropertyPageClass   mautilusPropertyPageClass;

struct _mautilusPropertyPage
{
	GObject parent;

	mautilusPropertyPageDetails *details;
};

struct _mautilusPropertyPageClass 
{
	GObjectClass parent;
};

GType                 mautilus_property_page_get_type  (void);
mautilusPropertyPage *mautilus_property_page_new       (const char           *name,
							GtkWidget            *label,
							GtkWidget            *page);

/* mautilusPropertyPage has the following properties:
 *   name (string)        - the identifier for the property page
 *   label (widget)       - the user-visible label of the property page
 *   page (widget)        - the property page to display
 */

G_END_DECLS

#endif
