/*
 *  mautilus-info-provider.h - Interface for mautilus extensions that 
 *                             provide info about files.
 *
 *  Copyright (C) 2003 Novell, Inc.
 *  Copyright (C) 2005 Red Hat, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 * 
 *  Author:  Dave Camp <dave@ximian.com>
 *           Alexander Larsson <alexl@redhat.com>
 *
 */

/* This interface is implemented by mautilus extensions that want to 
 * provide extra location widgets for a particular location.
 * Extensions are called when mautilus displays a location.
 */

#ifndef MAUTILUS_LOCATION_WIDGET_PROVIDER_H
#define MAUTILUS_LOCATION_WIDGET_PROVIDER_H

#include <glib-object.h>
#include <gtk/gtk.h>
#include "mautilus-extension-types.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_LOCATION_WIDGET_PROVIDER           (mautilus_location_widget_provider_get_type ())
#define MAUTILUS_LOCATION_WIDGET_PROVIDER(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_LOCATION_WIDGET_PROVIDER, mautilusLocationWidgetProvider))
#define MAUTILUS_IS_LOCATION_WIDGET_PROVIDER(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_LOCATION_WIDGET_PROVIDER))
#define MAUTILUS_LOCATION_WIDGET_PROVIDER_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MAUTILUS_TYPE_LOCATION_WIDGET_PROVIDER, mautilusLocationWidgetProviderIface))

typedef struct _mautilusLocationWidgetProvider       mautilusLocationWidgetProvider;
typedef struct _mautilusLocationWidgetProviderIface  mautilusLocationWidgetProviderIface;

/**
 * mautilusLocationWidgetProviderIface:
 * @g_iface: The parent interface.
 * @get_widget: Returns a #GtkWidget.
 *   See mautilus_location_widget_provider_get_widget() for details.
 *
 * Interface for extensions to provide additional location widgets.
 */
struct _mautilusLocationWidgetProviderIface {
	GTypeInterface g_iface;

	GtkWidget * (*get_widget) (mautilusLocationWidgetProvider *provider,
				   const char                     *uri,
				   GtkWidget                      *window);
};

/* Interface Functions */
GType       mautilus_location_widget_provider_get_type      (void);
GtkWidget * mautilus_location_widget_provider_get_widget    (mautilusLocationWidgetProvider     *provider,
							     const char                         *uri,
							     GtkWidget                          *window);
G_END_DECLS

#endif
