/*
 *  mautilus-info-provider.h - Interface for mautilus extensions that 
 *                             provide info about files.
 *
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 * 
 *  Author:  Dave Camp <dave@ximian.com>
 *
 */

/* This interface is implemented by mautilus extensions that want to 
 * provide information about files.  Extensions are called when mautilus 
 * needs information about a file.  They are passed a mautilusFileInfo 
 * object which should be filled with relevant information */

#ifndef MAUTILUS_INFO_PROVIDER_H
#define MAUTILUS_INFO_PROVIDER_H

#include <glib-object.h>
#include "mautilus-extension-types.h"
#include "mautilus-file-info.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_INFO_PROVIDER           (mautilus_info_provider_get_type ())
#define MAUTILUS_INFO_PROVIDER(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_INFO_PROVIDER, mautilusInfoProvider))
#define MAUTILUS_IS_INFO_PROVIDER(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_INFO_PROVIDER))
#define MAUTILUS_INFO_PROVIDER_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MAUTILUS_TYPE_INFO_PROVIDER, mautilusInfoProviderIface))

typedef struct _mautilusInfoProvider       mautilusInfoProvider;
typedef struct _mautilusInfoProviderIface  mautilusInfoProviderIface;

typedef void (*mautilusInfoProviderUpdateComplete) (mautilusInfoProvider    *provider,
						    mautilusOperationHandle *handle,
						    mautilusOperationResult  result,
						    gpointer                 user_data);
/**
 * mautilusInfoProviderIface:
 * @g_iface: The parent interface.
 * @update_file_info: Returns a #mautilusOperationResult.
 *   See mautilus_info_provider_update_file_info() for details.
 * @cancel_update: Cancels a previous call to mautilus_info_provider_update_file_info().
 *   See mautilus_info_provider_cancel_update() for details.
 *
 * Interface for extensions to provide additional information about files.
 */
struct _mautilusInfoProviderIface {
	GTypeInterface g_iface;

	mautilusOperationResult (*update_file_info) (mautilusInfoProvider     *provider,
						     mautilusFileInfo         *file,
						     GClosure                 *update_complete,
						     mautilusOperationHandle **handle);
	void                    (*cancel_update)    (mautilusInfoProvider     *provider,
						     mautilusOperationHandle  *handle);
};

/* Interface Functions */
GType                   mautilus_info_provider_get_type               (void);
mautilusOperationResult mautilus_info_provider_update_file_info       (mautilusInfoProvider     *provider,
								       mautilusFileInfo         *file,
								       GClosure                 *update_complete,
								       mautilusOperationHandle **handle);
void                    mautilus_info_provider_cancel_update          (mautilusInfoProvider     *provider,
								       mautilusOperationHandle  *handle);



/* Helper functions for implementations */
void                    mautilus_info_provider_update_complete_invoke (GClosure                 *update_complete,
								       mautilusInfoProvider     *provider,
								       mautilusOperationHandle  *handle,
								       mautilusOperationResult   result);

G_END_DECLS

#endif
