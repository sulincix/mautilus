/*
 *  mautilus-column.h - Info columns exported by 
 *                      mautilusColumnProvider objects.
 *
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 * 
 *  Author:  Dave Camp <dave@ximian.com>
 *
 */

#ifndef MAUTILUS_COLUMN_H
#define MAUTILUS_COLUMN_H

#include <glib-object.h>
#include "mautilus-extension-types.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_COLUMN            (mautilus_column_get_type())
#define MAUTILUS_COLUMN(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_COLUMN, mautilusColumn))
#define MAUTILUS_COLUMN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), MAUTILUS_TYPE_COLUMN, mautilusColumnClass))
#define MAUTILUS_INFO_IS_COLUMN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_COLUMN))
#define MAUTILUS_INFO_IS_COLUMN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((obj), MAUTILUS_TYPE_COLUMN))
#define MAUTILUS_COLUMN_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS((obj), MAUTILUS_TYPE_COLUMN, mautilusColumnClass))

typedef struct _mautilusColumn        mautilusColumn;
typedef struct _mautilusColumnDetails mautilusColumnDetails;
typedef struct _mautilusColumnClass   mautilusColumnClass;

struct _mautilusColumn {
	GObject parent;

	mautilusColumnDetails *details;
};

struct _mautilusColumnClass {
	GObjectClass parent;
};

GType             mautilus_column_get_type        (void);
mautilusColumn *  mautilus_column_new             (const char     *name,
						   const char     *attribute,
						   const char     *label,
						   const char     *description);

/* mautilusColumn has the following properties:
 *   name (string)        - the identifier for the column
 *   attribute (string)   - the file attribute to be displayed in the 
 *                          column
 *   label (string)       - the user-visible label for the column
 *   description (string) - a user-visible description of the column
 *   xalign (float)       - x-alignment of the column 
 *   default-sort-order (GtkSortType) - default sort order of the column
 */

G_END_DECLS

#endif
