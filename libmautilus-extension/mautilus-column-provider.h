/*
 *  mautilus-column-provider.h - Interface for mautilus extensions that 
 *                               provide column descriptions.
 *
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 * 
 *  Author:  Dave Camp <dave@ximian.com>
 *
 */

/* This interface is implemented by mautilus extensions that want to
 * add columns to the list view and details to the icon view.
 * Extensions are asked for a list of columns to display.  Each
 * returned column refers to a string attribute which can be filled in
 * by mautilusInfoProvider */

#ifndef MAUTILUS_COLUMN_PROVIDER_H
#define MAUTILUS_COLUMN_PROVIDER_H

#include <glib-object.h>
#include "mautilus-extension-types.h"
#include "mautilus-column.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_COLUMN_PROVIDER           (mautilus_column_provider_get_type ())
#define MAUTILUS_COLUMN_PROVIDER(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_COLUMN_PROVIDER, mautilusColumnProvider))
#define MAUTILUS_IS_COLUMN_PROVIDER(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_COLUMN_PROVIDER))
#define MAUTILUS_COLUMN_PROVIDER_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MAUTILUS_TYPE_COLUMN_PROVIDER, mautilusColumnProviderIface))

typedef struct _mautilusColumnProvider       mautilusColumnProvider;
typedef struct _mautilusColumnProviderIface  mautilusColumnProviderIface;

/**
 * mautilusColumnProviderIface:
 * @g_iface: The parent interface.
 * @get_columns: Returns a #GList of #mautilusColumn.
 *   See mautilus_column_provider_get_columns() for details.
 *
 * Interface for extensions to provide additional list view columns.
 */
struct _mautilusColumnProviderIface {
	GTypeInterface g_iface;

	GList *(*get_columns) (mautilusColumnProvider *provider);
};

/* Interface Functions */
GType                   mautilus_column_provider_get_type       (void);
GList                  *mautilus_column_provider_get_columns    (mautilusColumnProvider *provider);

G_END_DECLS

#endif
