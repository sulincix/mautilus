/*
 *  mautilus-file-info.h - Information about a file 
 *
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 */

/* mautilusFileInfo is an interface to the mautilusFile object.  It 
 * provides access to the asynchronous data in the mautilusFile.
 * Extensions are passed objects of this type for operations. */

#ifndef MAUTILUS_FILE_INFO_H
#define MAUTILUS_FILE_INFO_H

#include <glib-object.h>
#include <gio/gio.h>

G_BEGIN_DECLS

#define MAUTILUS_TYPE_FILE_INFO           (mautilus_file_info_get_type ())
#define MAUTILUS_FILE_INFO(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_FILE_INFO, mautilusFileInfo))
#define MAUTILUS_IS_FILE_INFO(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_FILE_INFO))
#define MAUTILUS_FILE_INFO_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MAUTILUS_TYPE_FILE_INFO, mautilusFileInfoIface))

#ifndef MAUTILUS_FILE_DEFINED
#define MAUTILUS_FILE_DEFINED
/* Using mautilusFile for the vtable to make implementing this in 
 * mautilusFile easier */
typedef struct mautilusFile          mautilusFile;
#endif

typedef mautilusFile                  mautilusFileInfo;
typedef struct _mautilusFileInfoIface mautilusFileInfoIface;

/**
 * mautilusFileInfoIface:
 * @g_iface: The parent interface.
 * @is_gone: Returns whether the file info is gone.
 *   See mautilus_file_info_is_gone() for details.
 * @get_name: Returns the file name as a string.
 *   See mautilus_file_info_get_name() for details.
 * @get_uri: Returns the file URI as a string.
 *   See mautilus_file_info_get_uri() for details.
 * @get_parent_uri: Returns the file parent URI as a string.
 *   See mautilus_file_info_get_parent_uri() for details.
 * @get_uri_scheme: Returns the file URI scheme as a string.
 *   See mautilus_file_info_get_uri_scheme() for details.
 * @get_mime_type: Returns the file mime type as a string.
 *   See mautilus_file_info_get_mime_type() for details.
 * @is_mime_type: Returns whether the file is the given mime type.
 *   See mautilus_file_info_is_mime_type() for details.
 * @is_directory: Returns whether the file is a directory.
 *   See mautilus_file_info_is_directory() for details.
 * @add_emblem: Adds an emblem to this file.
 *   See mautilus_file_info_add_emblem() for details.
 * @get_string_attribute: Returns the specified file attribute as a string.
 *   See mautilus_file_info_get_string_attribute() for details.
 * @add_string_attribute: Sets the specified string file attribute value.
 *   See mautilus_file_info_add_string_attribute() for details.
 * @invalidate_extension_info: Invalidates information of the file provided by extensions.
 *   See mautilus_file_info_invalidate_extension_info() for details.
 * @get_activation_uri: Returns the file activation URI as a string.
 *   See mautilus_file_info_get_activation_uri() for details.
 * @get_file_type: Returns the file type.
 *   See mautilus_file_info_get_file_type() for details.
 * @get_location: Returns the file location as a #GFile.
 *   See mautilus_file_info_get_location() for details.
 * @get_parent_location: Returns the file parent location as a #GFile.
 *   See mautilus_file_info_get_parent_location() for details.
 * @get_parent_info: Returns the file parent #mautilusFileInfo.
 *   See mautilus_file_info_get_parent_info() for details.
 * @get_mount: Returns the file mount as a #GMount.
 *   See mautilus_file_info_get_mount() for details.
 * @can_write: Returns whether the file is writable.
 *   See mautilus_file_info_can_write() for details.
 *
 * Interface for extensions to provide additional menu items.
 */
struct _mautilusFileInfoIface 
{
	GTypeInterface g_iface;

	gboolean          (*is_gone)              (mautilusFileInfo *file);
	
	char *            (*get_name)             (mautilusFileInfo *file);
	char *            (*get_uri)              (mautilusFileInfo *file);
	char *            (*get_parent_uri)       (mautilusFileInfo *file);
	char *            (*get_uri_scheme)       (mautilusFileInfo *file);
	
	char *            (*get_mime_type)        (mautilusFileInfo *file);
	gboolean          (*is_mime_type)         (mautilusFileInfo *file,
						   const char       *mime_Type);
	gboolean          (*is_directory)         (mautilusFileInfo *file);
	
	void              (*add_emblem)           (mautilusFileInfo *file,
						   const char       *emblem_name);
	char *            (*get_string_attribute) (mautilusFileInfo *file,
						   const char       *attribute_name);
	void              (*add_string_attribute) (mautilusFileInfo *file,
						   const char       *attribute_name,
						   const char       *value);
	void              (*invalidate_extension_info) (mautilusFileInfo *file);
	
	char *            (*get_activation_uri)   (mautilusFileInfo *file);

	GFileType         (*get_file_type)        (mautilusFileInfo *file);
	GFile *           (*get_location)         (mautilusFileInfo *file);
	GFile *           (*get_parent_location)  (mautilusFileInfo *file);
	mautilusFileInfo* (*get_parent_info)      (mautilusFileInfo *file);
	GMount *          (*get_mount)            (mautilusFileInfo *file);
	gboolean          (*can_write)            (mautilusFileInfo *file);
  
};

GList            *mautilus_file_info_list_copy            (GList            *files);
void              mautilus_file_info_list_free            (GList            *files);
GType             mautilus_file_info_get_type             (void);

/* Return true if the file has been deleted */
gboolean          mautilus_file_info_is_gone              (mautilusFileInfo *file);

/* Name and Location */
GFileType         mautilus_file_info_get_file_type        (mautilusFileInfo *file);
GFile *           mautilus_file_info_get_location         (mautilusFileInfo *file);
char *            mautilus_file_info_get_name             (mautilusFileInfo *file);
char *            mautilus_file_info_get_uri              (mautilusFileInfo *file);
char *            mautilus_file_info_get_activation_uri   (mautilusFileInfo *file);
GFile *           mautilus_file_info_get_parent_location  (mautilusFileInfo *file);
char *            mautilus_file_info_get_parent_uri       (mautilusFileInfo *file);
GMount *          mautilus_file_info_get_mount            (mautilusFileInfo *file);
char *            mautilus_file_info_get_uri_scheme       (mautilusFileInfo *file);
/* It's not safe to call this recursively multiple times, as it works
 * only for files already cached by mautilus.
 */
mautilusFileInfo* mautilus_file_info_get_parent_info      (mautilusFileInfo *file);

/* File Type */
char *            mautilus_file_info_get_mime_type        (mautilusFileInfo *file);
gboolean          mautilus_file_info_is_mime_type         (mautilusFileInfo *file,
							   const char       *mime_type);
gboolean          mautilus_file_info_is_directory         (mautilusFileInfo *file);
gboolean          mautilus_file_info_can_write            (mautilusFileInfo *file);


/* Modifying the mautilusFileInfo */
void              mautilus_file_info_add_emblem           (mautilusFileInfo *file,
							   const char       *emblem_name);
char *            mautilus_file_info_get_string_attribute (mautilusFileInfo *file,
							   const char       *attribute_name);
void              mautilus_file_info_add_string_attribute (mautilusFileInfo *file,
							   const char       *attribute_name,
							   const char       *value);

/* Invalidating file info */
void              mautilus_file_info_invalidate_extension_info (mautilusFileInfo *file);

mautilusFileInfo *mautilus_file_info_lookup                (GFile *location);
mautilusFileInfo *mautilus_file_info_create                (GFile *location);
mautilusFileInfo *mautilus_file_info_lookup_for_uri        (const char *uri);
mautilusFileInfo *mautilus_file_info_create_for_uri        (const char *uri);

G_END_DECLS

#endif
