/*
 *  mautilus-property-page-provider.h - Interface for mautilus extensions
 *                                      that provide property pages.
 *
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 * 
 *  Author:  Dave Camp <dave@ximian.com>
 *
 */

/* This interface is implemented by mautilus extensions that want to 
 * add property page to property dialogs.  Extensions are called when 
 * mautilus needs property pages for a selection.  They are passed a 
 * list of mautilusFileInfo objects for which information should
 * be displayed  */

#ifndef MAUTILUS_PROPERTY_PAGE_PROVIDER_H
#define MAUTILUS_PROPERTY_PAGE_PROVIDER_H

#include <glib-object.h>
#include "mautilus-extension-types.h"
#include "mautilus-file-info.h"
#include "mautilus-property-page.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_PROPERTY_PAGE_PROVIDER           (mautilus_property_page_provider_get_type ())
#define MAUTILUS_PROPERTY_PAGE_PROVIDER(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_PROPERTY_PAGE_PROVIDER, mautilusPropertyPageProvider))
#define MAUTILUS_IS_PROPERTY_PAGE_PROVIDER(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_PROPERTY_PAGE_PROVIDER))
#define MAUTILUS_PROPERTY_PAGE_PROVIDER_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MAUTILUS_TYPE_PROPERTY_PAGE_PROVIDER, mautilusPropertyPageProviderIface))

typedef struct _mautilusPropertyPageProvider       mautilusPropertyPageProvider;
typedef struct _mautilusPropertyPageProviderIface  mautilusPropertyPageProviderIface;

/**
 * mautilusPropertyPageProviderIface:
 * @g_iface: The parent interface.
 * @get_pages: Returns a #GList of #mautilusPropertyPage.
 *   See mautilus_property_page_provider_get_pages() for details.
 *
 * Interface for extensions to provide additional property pages.
 */
struct _mautilusPropertyPageProviderIface {
	GTypeInterface g_iface;

	GList *(*get_pages) (mautilusPropertyPageProvider *provider,
			     GList                        *files);
};

/* Interface Functions */
GType                   mautilus_property_page_provider_get_type  (void);
GList                  *mautilus_property_page_provider_get_pages (mautilusPropertyPageProvider *provider,
								   GList                        *files);

G_END_DECLS

#endif
