/*
 *  mautilus-file-info.c - Information about a file
 *
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <config.h>
#include "mautilus-file-info.h"
#include "mautilus-extension-private.h"

mautilusFileInfo *(*mautilus_file_info_getter)(GFile * location, gboolean create);

/**
 * SECTION:mautilus-file-info
 * @title: mautilusFileInfo
 * @short_description: File interface for mautilus extensions
 * @include: libmautilus-extension/mautilus-file-info.h
 *
 * #mautilusFileInfo provides methods to get and modify information
 * about file objects in the file manager.
 */

/**
 * mautilus_file_info_list_copy:
 * @files: (element-type mautilusFileInfo): the files to copy
 *
 * Returns: (element-type mautilusFileInfo) (transfer full): a copy of @files.
 *  Use #mautilus_file_info_list_free to free the list and unref its contents.
 */
GList *
mautilus_file_info_list_copy (GList *files)
{
    GList *ret;
    GList *l;

    ret = g_list_copy (files);
    for (l = ret; l != NULL; l = l->next)
    {
        g_object_ref (G_OBJECT (l->data));
    }

    return ret;
}

/**
 * mautilus_file_info_list_free:
 * @files: (element-type mautilusFileInfo): a list created with
 *   #mautilus_file_info_list_copy
 *
 */
void
mautilus_file_info_list_free (GList *files)
{
    GList *l;

    for (l = files; l != NULL; l = l->next)
    {
        g_object_unref (G_OBJECT (l->data));
    }

    g_list_free (files);
}

static void
mautilus_file_info_base_init (gpointer g_class)
{
}

GType
mautilus_file_info_get_type (void)
{
    static GType type = 0;

    if (!type)
    {
        const GTypeInfo info =
        {
            sizeof (mautilusFileInfoIface),
            mautilus_file_info_base_init,
            NULL,
            NULL,
            NULL,
            NULL,
            0,
            0,
            NULL
        };

        type = g_type_register_static (G_TYPE_INTERFACE,
                                       "mautilusFileInfo",
                                       &info, 0);
        g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
    }

    return type;
}

gboolean
mautilus_file_info_is_gone (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), FALSE);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->is_gone != NULL, FALSE);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->is_gone (file);
}

GFileType
mautilus_file_info_get_file_type (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), G_FILE_TYPE_UNKNOWN);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->get_file_type != NULL, G_FILE_TYPE_UNKNOWN);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->get_file_type (file);
}

char *
mautilus_file_info_get_name (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), NULL);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->get_name != NULL, NULL);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->get_name (file);
}

/**
 * mautilus_file_info_get_location:
 * @file: a #mautilusFileInfo
 *
 * Returns: (transfer full): a #GFile for the location of @file
 */
GFile *
mautilus_file_info_get_location (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), NULL);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->get_location != NULL, NULL);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->get_location (file);
}
char *
mautilus_file_info_get_uri (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), NULL);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->get_uri != NULL, NULL);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->get_uri (file);
}

char *
mautilus_file_info_get_activation_uri (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), NULL);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->get_activation_uri != NULL, NULL);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->get_activation_uri (file);
}

/**
 * mautilus_file_info_get_parent_location:
 * @file: a #mautilusFileInfo
 *
 * Returns: (allow-none) (transfer full): a #GFile for the parent location of @file,
 *   or %NULL if @file has no parent
 */
GFile *
mautilus_file_info_get_parent_location (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), NULL);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->get_parent_location != NULL, NULL);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->get_parent_location (file);
}

char *
mautilus_file_info_get_parent_uri (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), NULL);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->get_parent_uri != NULL, NULL);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->get_parent_uri (file);
}

/**
 * mautilus_file_info_get_parent_info:
 * @file: a #mautilusFileInfo
 *
 * Returns: (allow-none) (transfer full): a #mautilusFileInfo for the parent of @file,
 *   or %NULL if @file has no parent
 */
mautilusFileInfo *
mautilus_file_info_get_parent_info (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), NULL);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->get_parent_info != NULL, NULL);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->get_parent_info (file);
}

/**
 * mautilus_file_info_get_mount:
 * @file: a #mautilusFileInfo
 *
 * Returns: (allow-none) (transfer full): a #GMount for the mount of @file,
 *   or %NULL if @file has no mount
 */
GMount *
mautilus_file_info_get_mount (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), NULL);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->get_mount != NULL, NULL);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->get_mount (file);
}

char *
mautilus_file_info_get_uri_scheme (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), NULL);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->get_uri_scheme != NULL, NULL);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->get_uri_scheme (file);
}

char *
mautilus_file_info_get_mime_type (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), NULL);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->get_mime_type != NULL, NULL);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->get_mime_type (file);
}

gboolean
mautilus_file_info_is_mime_type (mautilusFileInfo *file,
                                 const char       *mime_type)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), FALSE);
    g_return_val_if_fail (mime_type != NULL, FALSE);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->is_mime_type != NULL, FALSE);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->is_mime_type (file,
                                                              mime_type);
}

gboolean
mautilus_file_info_is_directory (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), FALSE);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->is_directory != NULL, FALSE);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->is_directory (file);
}

gboolean
mautilus_file_info_can_write (mautilusFileInfo *file)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), FALSE);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->can_write != NULL, FALSE);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->can_write (file);
}

void
mautilus_file_info_add_emblem (mautilusFileInfo *file,
                               const char       *emblem_name)
{
    g_return_if_fail (MAUTILUS_IS_FILE_INFO (file));
    g_return_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->add_emblem != NULL);

    MAUTILUS_FILE_INFO_GET_IFACE (file)->add_emblem (file, emblem_name);
}

char *
mautilus_file_info_get_string_attribute (mautilusFileInfo *file,
                                         const char       *attribute_name)
{
    g_return_val_if_fail (MAUTILUS_IS_FILE_INFO (file), NULL);
    g_return_val_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->get_string_attribute != NULL, NULL);
    g_return_val_if_fail (attribute_name != NULL, NULL);

    return MAUTILUS_FILE_INFO_GET_IFACE (file)->get_string_attribute
               (file, attribute_name);
}

void
mautilus_file_info_add_string_attribute (mautilusFileInfo *file,
                                         const char       *attribute_name,
                                         const char       *value)
{
    g_return_if_fail (MAUTILUS_IS_FILE_INFO (file));
    g_return_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->add_string_attribute != NULL);
    g_return_if_fail (attribute_name != NULL);
    g_return_if_fail (value != NULL);

    MAUTILUS_FILE_INFO_GET_IFACE (file)->add_string_attribute
        (file, attribute_name, value);
}

void
mautilus_file_info_invalidate_extension_info (mautilusFileInfo *file)
{
    g_return_if_fail (MAUTILUS_IS_FILE_INFO (file));
    g_return_if_fail (MAUTILUS_FILE_INFO_GET_IFACE (file)->invalidate_extension_info != NULL);

    MAUTILUS_FILE_INFO_GET_IFACE (file)->invalidate_extension_info (file);
}

/**
 * mautilus_file_info_lookup:
 * @location: the location to lookup the file info for
 *
 * Returns: (transfer full): a #mautilusFileInfo
 */
mautilusFileInfo *
mautilus_file_info_lookup (GFile *location)
{
    return mautilus_file_info_getter (location, FALSE);
}

/**
 * mautilus_file_info_create:
 * @location: the location to create the file info for
 *
 * Returns: (transfer full): a #mautilusFileInfo
 */
mautilusFileInfo *
mautilus_file_info_create (GFile *location)
{
    return mautilus_file_info_getter (location, TRUE);
}

/**
 * mautilus_file_info_lookup_for_uri:
 * @uri: the URI to lookup the file info for
 *
 * Returns: (transfer full): a #mautilusFileInfo
 */
mautilusFileInfo *
mautilus_file_info_lookup_for_uri (const char *uri)
{
    GFile *location;
    mautilusFile *file;

    location = g_file_new_for_uri (uri);
    file = mautilus_file_info_lookup (location);
    g_object_unref (location);

    return file;
}

/**
 * mautilus_file_info_create_for_uri:
 * @uri: the URI to lookup the file info for
 *
 * Returns: (transfer full): a #mautilusFileInfo
 */
mautilusFileInfo *
mautilus_file_info_create_for_uri (const char *uri)
{
    GFile *location;
    mautilusFile *file;

    location = g_file_new_for_uri (uri);
    file = mautilus_file_info_create (location);
    g_object_unref (location);

    return file;
}
