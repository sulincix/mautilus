/*
 *  mautilus-menu-provider.h - Interface for mautilus extensions that 
 *                             provide context menu items.
 *
 *  Copyright (C) 2003 Novell, Inc.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 * 
 *  Author:  Dave Camp <dave@ximian.com>
 *
 */

/* This interface is implemented by mautilus extensions that want to
 * add context menu entries to files.  Extensions are called when
 * mautilus constructs the context menu for a file.  They are passed a
 * list of mautilusFileInfo objects which holds the current selection */

#ifndef MAUTILUS_MENU_PROVIDER_H
#define MAUTILUS_MENU_PROVIDER_H

#include <glib-object.h>
#include <gtk/gtk.h>
#include "mautilus-extension-types.h"
#include "mautilus-file-info.h"
#include "mautilus-menu.h"

G_BEGIN_DECLS

#define MAUTILUS_TYPE_MENU_PROVIDER           (mautilus_menu_provider_get_type ())
#define MAUTILUS_MENU_PROVIDER(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj), MAUTILUS_TYPE_MENU_PROVIDER, mautilusMenuProvider))
#define MAUTILUS_IS_MENU_PROVIDER(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj), MAUTILUS_TYPE_MENU_PROVIDER))
#define MAUTILUS_MENU_PROVIDER_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), MAUTILUS_TYPE_MENU_PROVIDER, mautilusMenuProviderIface))

typedef struct _mautilusMenuProvider       mautilusMenuProvider;
typedef struct _mautilusMenuProviderIface  mautilusMenuProviderIface;

/**
 * mautilusMenuProviderIface:
 * @g_iface: The parent interface.
 * @get_file_items: Returns a #GList of #mautilusMenuItem.
 *   See mautilus_menu_provider_get_file_items() for details.
 * @get_background_items: Returns a #GList of #mautilusMenuItem.
 *   See mautilus_menu_provider_get_background_items() for details.
 *
 * Interface for extensions to provide additional menu items.
 */
struct _mautilusMenuProviderIface {
	GTypeInterface g_iface;

	GList *(*get_file_items)       (mautilusMenuProvider *provider,
					GtkWidget            *window,
					GList                *files);
	GList *(*get_background_items) (mautilusMenuProvider *provider,
					GtkWidget            *window,
					mautilusFileInfo     *current_folder);
};

/* Interface Functions */
GType                   mautilus_menu_provider_get_type             (void);
GList                  *mautilus_menu_provider_get_file_items       (mautilusMenuProvider *provider,
								     GtkWidget            *window,
								     GList                *files);
GList                  *mautilus_menu_provider_get_background_items (mautilusMenuProvider *provider,
								     GtkWidget            *window,
								     mautilusFileInfo     *current_folder);

/* This function emit a signal to inform mautilus that its item list has changed. */
void                    mautilus_menu_provider_emit_items_updated_signal (mautilusMenuProvider *provider);

G_END_DECLS

#endif
