/*
 *  mautilus-menu.h - Menus exported by mautilusMenuProvider objects.
 *
 *  Copyright (C) 2005 Raffaele Sandrini
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, see <http://www.gnu.org/licenses/>.
 *
 *  Author:  Raffaele Sandrini <rasa@gmx.ch>
 *
 */

#include <config.h>
#include "mautilus-menu.h"

#include <glib.h>
#include <glib/gi18n-lib.h>

/**
 * SECTION:mautilus-menu
 * @title: mautilusMenu
 * @short_description: Menu descriptor object
 * @include: libmautilus-extension/mautilus-menu.h
 *
 * #mautilusMenu is an object that describes a submenu in a file manager
 * menu. Extensions can provide #mautilusMenu objects by attaching them to
 * #mautilusMenuItem objects, using mautilus_menu_item_set_submenu().
 */

#define MAUTILUS_MENU_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), MAUTILUS_TYPE_MENU, mautilusMenuPrivate))
G_DEFINE_TYPE (mautilusMenu, mautilus_menu, G_TYPE_OBJECT);

struct _mautilusMenuPrivate
{
    GList *item_list;
};

void
mautilus_menu_append_item (mautilusMenu     *menu,
                           mautilusMenuItem *item)
{
    g_return_if_fail (menu != NULL);
    g_return_if_fail (item != NULL);

    menu->priv->item_list = g_list_append (menu->priv->item_list, g_object_ref (item));
}

/**
 * mautilus_menu_get_items:
 * @menu: a #mautilusMenu
 *
 * Returns: (element-type mautilusMenuItem) (transfer full): the provided #mautilusMenuItem list
 */
GList *
mautilus_menu_get_items (mautilusMenu *menu)
{
    GList *item_list;

    g_return_val_if_fail (menu != NULL, NULL);

    item_list = g_list_copy (menu->priv->item_list);
    g_list_foreach (item_list, (GFunc) g_object_ref, NULL);

    return item_list;
}

/**
 * mautilus_menu_item_list_free:
 * @item_list: (element-type mautilusMenuItem): a list of #mautilusMenuItem
 *
 */
void
mautilus_menu_item_list_free (GList *item_list)
{
    g_return_if_fail (item_list != NULL);

    g_list_foreach (item_list, (GFunc) g_object_unref, NULL);
    g_list_free (item_list);
}

/* Type initialization */

static void
mautilus_menu_finalize (GObject *object)
{
    mautilusMenu *menu = MAUTILUS_MENU (object);

    if (menu->priv->item_list)
    {
        g_list_free (menu->priv->item_list);
    }

    G_OBJECT_CLASS (mautilus_menu_parent_class)->finalize (object);
}

static void
mautilus_menu_init (mautilusMenu *menu)
{
    menu->priv = MAUTILUS_MENU_GET_PRIVATE (menu);

    menu->priv->item_list = NULL;
}

static void
mautilus_menu_class_init (mautilusMenuClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    g_type_class_add_private (klass, sizeof (mautilusMenuPrivate));

    object_class->finalize = mautilus_menu_finalize;
}

/* public constructors */

mautilusMenu *
mautilus_menu_new (void)
{
    mautilusMenu *obj;

    obj = MAUTILUS_MENU (g_object_new (MAUTILUS_TYPE_MENU, NULL));

    return obj;
}
